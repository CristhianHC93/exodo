package ec.com.exodo;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.dao.ie.ConfiguracionDAO;
import ec.com.exodo.dao.ie.PrecioDAO;
import ec.com.exodo.entity.Configuracion;
import ec.com.exodo.entity.Email;
import ec.com.exodo.entity.InformacionNegocio;
import ec.com.exodo.entity.Precio;
import ec.com.exodo.entity.VentaCabecera;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassConfiguracion {

	private String btGuardarEditar = Sistema.getIdioma().getGuardar();
	// Variables para configuracion correo
	private List<Email> listaEmail;
	private Email email;
	private String correoEnviar = "designtechx@gmail.com";
	private String clave;

	// Variables para configurar datos de negocio
	private List<InformacionNegocio> listaInformacionNegocio;
	private InformacionNegocio informacionNegocio;
	private VentaCabecera ventaCabecera;

	/* Variables para configuracion Pvp Porducto */
	private ConfiguracionDAO configuracionDAO = InstanciaDAO.getInstanciaConfiguracionDAO();
	private PrecioDAO precioDAO = InstanciaDAO.getInstanciaPrecioDAO();
	private Configuracion configuracion;
	private List<Precio> listaPrecio;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		listaInformacionNegocio = Sistema.getListaInformacionNegocio();
		listaEmail = Sistema.getListaEmail();
		informacionNegocio = listaInformacionNegocio.get(0);
		email = listaEmail.get(0);
		ventaCabecera = InstanciaDAO.getInstanciaVentaCabeceraDAO()
				.getUltimaVentaCabecera(Sistema.ESTADO_ENTREGADO_VENTA);
		if (ventaCabecera == null) {
			ventaCabecera = new VentaCabecera();
			ventaCabecera.setCodigo("1");
		}

		/* Codigo Pvp Precios */
		configuracion = configuracionDAO.getConfiguracion(Sistema.ID_CONFIGURACION);
		if (configuracion == null) {
			configuracion = new Configuracion(1, 1);
		}
		listaPrecio = precioDAO.getListaByProductoIsNull();
		if (listaPrecio == null) {
			listaPrecio = new ArrayList<>();
		}
	}

	@Command
	@NotifyChange({ "listaInformacionNegocio", "informacionNegocio" })
	public void guardarInformacion() {
		if (informacionNegocio == null) {
			Messagebox.show("Seleccione el registro para modificarlo");
			return;
		}
		if (ventaCabecera.getCodigo().equals("") || ventaCabecera.getCodigo() == null) {
			Messagebox.show("Es necesario Ingresar Codigo");
			return;
		}
		InstanciaDAO.getInstanciaInformacionNegocioDAO().saveorUpdate(informacionNegocio);
		if (ventaCabecera.getId() != null) {
			InstanciaDAO.getInstanciaVentaCabeceraDAO().saveorUpdate(ventaCabecera);
		}

		Clients.showNotification("Datos modificados correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
				"middle_center", 1500);
		listaInformacionNegocio = InstanciaDAO.getInstanciaInformacionNegocioDAO().getListaInformacionNegocio();
		informacionNegocio = listaInformacionNegocio.get(0);
	}

	@Command
	@NotifyChange({ "listaEmail", "email" })
	public void guardarEmail() {
		if (email == null) {
			Messagebox.show("seleccione el registro para modificarlo");
			return;
		}
		Clients.showNotification("Email modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
				"middle_center", 1500);
		InstanciaDAO.getInstanciaEmailDAO().saveorUpdate(email);
		listaEmail = InstanciaDAO.getInstanciaEmailDAO().getListaEmail();
		email = listaEmail.get(0);
		Sistema.datosEmail(email.getCorreo(), email.getClave(), email.getHost(), email.getPuerto());
	}

	@Command
	@NotifyChange({ "clave", "btGuardarEditar" })
	public void desencriptar(@BindingParam("item") Email email) {
		try {
			clave = Sistema.desencriptar(email.getClave());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btGuardarEditar = Sistema.getIdioma().getEditar();
	}

	@Command
	public void enviarCorreoPrueba() {
		if (!Sistema.verificarInternet()) {
			Messagebox.show("Imposible enviar correo, Por favor verifique su conexion a internet");
			return;
		}
		Sistema.enviarCorreoPrueba(correoEnviar, "Prueba", "Mensaje de prueba");
	}

	@Command
	@NotifyChange({ "listaPrecio", "configuracion" })
	public void aceptar() {
		if (configuracion.getPrecios() < 1) {
			configuracion.setPrecios(1);
			Messagebox.show("Cantidad debe ser mayor a 0");
			return;
		}
		listaPrecio.clear();
		for (int i = 1; i <= configuracion.getPrecios(); i++) {
			listaPrecio.add(new Precio("PVP ".concat(String.valueOf(i)), 0, 0));
		}
	}

	private boolean banderaPvp = false;

	@Command
	@NotifyChange({ "listaPrecio", "configuracion" })
	public void guardarPvp() {
		if (configuracion.getPrecios() < 1) {
			Messagebox.show("Cantidad debe ser mayor a 0");
			return;
		}
		if (listaPrecio.size() <= 0) {
			Messagebox.show("Ingrese lista de precios");
			return;
		}
		banderaPvp = false;
		listaPrecio.forEach(x -> {
			if (x.getPorcentaje() < 1) {
				banderaPvp = true;
			}
		});

		if (banderaPvp) {
			Messagebox.show("Todos los Porcentajes deben tener valores");
			return;
		}

		precioDAO.getListaByProductoIsNull().forEach(precio -> {
			precioDAO.eliminar(precio.getId());
		});

		configuracion = configuracionDAO.saveOrUpdate(configuracion);
		listaPrecio.forEach(precio -> {
			precio = precioDAO.saveOrUpdate(precio);
		});
		Clients.showNotification("Datos Almacenados Satidfactoriamente", Clients.NOTIFICATION_TYPE_INFO, null,
				"middle_center", 1500);
	}

	/**/

	public String getBtGuardarEditar() {
		return btGuardarEditar;
	}

	public void setBtGuardarEditar(String btGuardarEditar) {
		this.btGuardarEditar = btGuardarEditar;
	}

	public String getCorreoEnviar() {
		return correoEnviar;
	}

	public void setCorreoEnviar(String correoEnviar) {
		this.correoEnviar = correoEnviar;
	}

	public List<Email> getListaEmail() {
		return listaEmail;
	}

	public void setListaEmail(List<Email> listaEmail) {
		this.listaEmail = listaEmail;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public List<InformacionNegocio> getListaInformacionNegocio() {
		return listaInformacionNegocio;
	}

	public void setListaInformacionNegocio(List<InformacionNegocio> listaInformacionNegocio) {
		this.listaInformacionNegocio = listaInformacionNegocio;
	}

	public InformacionNegocio getInformacionNegocio() {
		return informacionNegocio;
	}

	public void setInformacionNegocio(InformacionNegocio informacionNegocio) {
		this.informacionNegocio = informacionNegocio;
	}

	public VentaCabecera getVentaCabecera() {
		return ventaCabecera;
	}

	public void setVentaCabecera(VentaCabecera ventaCabecera) {
		this.ventaCabecera = ventaCabecera;
	}

	public Configuracion getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(Configuracion configuracion) {
		this.configuracion = configuracion;
	}

	public List<Precio> getListaPrecio() {
		return listaPrecio;
	}

	public void setListaPrecio(List<Precio> listaPrecio) {
		this.listaPrecio = listaPrecio;
	}

}
