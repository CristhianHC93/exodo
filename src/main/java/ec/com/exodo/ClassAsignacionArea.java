package ec.com.exodo;

import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.dao.ie.AreaDAO;
import ec.com.exodo.dao.ie.AreaSucursalDAO;
import ec.com.exodo.dao.ie.SucursalDAO;
import ec.com.exodo.dao.ie.UsuarioDAO;
import ec.com.exodo.entity.Area;
import ec.com.exodo.entity.AreaSucursal;
import ec.com.exodo.entity.Sucursal;
import ec.com.exodo.entity.Usuario;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassAsignacionArea {

	private List<Usuario> listaUsuario;
	private List<Sucursal> listaSucursal;
	private List<Area> listaArea;

	private AreaDAO areaDAO = InstanciaDAO.getInstanciaAreaDAO();
	private AreaSucursalDAO areaSucursalDAO = InstanciaDAO.getInstanciaAreaSucursalDAO();
	private UsuarioDAO usuarioDAO = InstanciaDAO.getInstanciaUsuarioDAO();
	private SucursalDAO sucursalDAO = InstanciaDAO.getInstanciaSucursalDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		listaUsuario = usuarioDAO.getListaUsuarioEmpleados();
		listaSucursal = sucursalDAO.getListaSucursal();
		listaArea = areaDAO.getListaArea();
		listaUsuario.stream().filter(x -> (x.getAreaSucursal() == null))
				.forEach(x -> x.setAreaSucursal(new AreaSucursal()));
	}

	@Command
	@NotifyChange({ "listaUsuario" })
	public void guardarEditar(@BindingParam("item") Usuario usuario) {
		if (validarGuardarEditar(usuario)) {
			usuario.setAreaSucursal(areaSucursalDAO.getAreaSucursalByAreaAndSucursal(
					usuario.getAreaSucursal().getArea().getId(), usuario.getAreaSucursal().getSucursal().getId()));
			if (usuarioDAO.saveorUpdate(usuario) == null) {
				Clients.showNotification(getIdioma().getMensajeImposibleRealizarAccion(usuario.getUsername()),
						Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center", 1500);
			}
		}
	}

	private boolean validarGuardarEditar(final Usuario usuario) {
		boolean devolver = true;
		if (usuario.getAreaSucursal().getArea() == null) {
			Messagebox.show("Usuario sin Area asignada");
			devolver = false;
		}
		if (usuario.getAreaSucursal().getSucursal() == null) {
			Messagebox.show("Usuario sin Sucursal asignada");
			devolver = false;
		}
		return devolver;
	}

	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public List<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

	public List<Sucursal> getListaSucursal() {
		return listaSucursal;
	}

	public void setListaSucursal(List<Sucursal> listaSucursal) {
		this.listaSucursal = listaSucursal;
	}

	public List<Area> getListaArea() {
		return listaArea;
	}

	public void setListaArea(List<Area> listaArea) {
		this.listaArea = listaArea;
	}

}
