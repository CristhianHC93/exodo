package ec.com.exodo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.CompraCabeceraDAO;
import ec.com.exodo.entity.CompraCabecera;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ListaCompra {
	@Wire("#winListaCompra")
	private Window winListaCompra;

	private String selectCombo = getComboBusqueda().get(0);
	private Date fechaBusqueda;
	private String datoBusqueda;
	private boolean buscarVisible = true;

	private CompraCabecera compraCabecera;
	private List<CompraCabecera> listaCompraCabecera;
	private List<CompraCabecera> listaCompraCabeceraProvicional;
	CompraCabeceraDAO CompraCabeceraDAO = InstanciaDAO.getInstanciaCompraCabeceraDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		listaCompraCabeceraProvicional = CompraCabeceraDAO.getCompraCabeceras(Sistema.ESTADO_INGRESADO);
		listaCompraCabecera = new ArrayList<>(listaCompraCabeceraProvicional.size());
		listaCompraCabecera.addAll(listaCompraCabeceraProvicional);
	}

	@Command
	@NotifyChange("buscarVisible")
	public void cambioFecha() {
		if (selectCombo.trim().equals("Fecha")) {
			buscarVisible = false;
		} else {
			buscarVisible = true;
		}
	}

	@Command
	@NotifyChange("listaCompraCabecera")
	public void filtrar() {
		listaCompraCabecera.clear();
		if (selectCombo.trim().equals("Codigo/Proveedor")) {
			if (datoBusqueda.trim().equals("")) {
				// CompraCabeceraDAO
				listaCompraCabecera.addAll(listaCompraCabeceraProvicional);
			} else {
				for (CompraCabecera item : listaCompraCabeceraProvicional) {
					if (item.getCodigo().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0
							|| item.getProveedor().getNombre().trim().toLowerCase()
									.indexOf(datoBusqueda.trim().toLowerCase()) == 0) {
						listaCompraCabecera.add(item);
					}
				}
			}
		} else if (selectCombo.trim().equals("Fecha")) {
			for (CompraCabecera item : listaCompraCabeceraProvicional) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String dateIni = sdf.format(fechaBusqueda);
				String dateFin = sdf.format(item.getFecha());
				if (dateIni.toString().equals(dateFin.toString())) {
					listaCompraCabecera.add(item);
				}
			}
		}

	}

	@Command
	public void salir() {
		winListaCompra.detach();
	}

	@Command
	@NotifyChange({ "compraCabecera", "listaCompraCabecera" })
	public void aceptar(@BindingParam("item") final CompraCabecera compraCabecera) {
		if (compraCabecera == null) {
			Messagebox.show("Debe seleccionar un elemento de la lista ", "Error", Messagebox.OK,
					Messagebox.EXCLAMATION);
			return;
		}
		// Map args = new HashMap();
		// args.put("objetoNotaCompra", CompraCabecera);
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objetoCompra", compraCabecera);
		BindUtils.postGlobalCommand(null, null, "cargarCompra", parametros);
		salir();
	}

	public String getFooter() {
		return String.format(ParametroZul.getInstancia().getFooterMessage(), listaCompraCabecera.size());
	}

	/*
	 * Llenar comboBox para buscar
	 */
	public final List<String> getComboBusqueda() {
		return Sistema.getComboCompra();
	}

	/*
	 * Metodos GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public String getSelectCombo() {
		return selectCombo;
	}

	public void setSelectCombo(String selectCombo) {
		this.selectCombo = selectCombo;
	}

	public Date getFechaBusqueda() {
		return fechaBusqueda;
	}

	public void setFechaBusqueda(Date fechaBusqueda) {
		this.fechaBusqueda = fechaBusqueda;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public boolean isBuscarVisible() {
		return buscarVisible;
	}

	public void setBuscarVisible(boolean buscarVisible) {
		this.buscarVisible = buscarVisible;
	}

	public CompraCabecera getCompraCabecera() {
		return compraCabecera;
	}

	public void setCompraCabecera(CompraCabecera compraCabecera) {
		this.compraCabecera = compraCabecera;
	}

	public List<CompraCabecera> getListaCompraCabecera() {
		return listaCompraCabecera;
	}

	public void setListaCompraCabecera(List<CompraCabecera> listaCompraCabecera) {
		this.listaCompraCabecera = listaCompraCabecera;
	}

}
