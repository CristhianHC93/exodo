package ec.com.exodo.service;

import java.util.Map;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.util.Initiator;

import ec.com.exodo.service.impl.AutenticacionImpl;

public class InicioAutenticacion implements Initiator{

	@SuppressWarnings("static-access")
	Autenticacion authService = new AutenticacionImpl().getIntance();
	
	@Override
	public void doInit(Page arg0, Map<String, Object> arg1) throws Exception {
		//Usuario usuario = (Usuario) Sessions.getCurrent().getAttribute("Usuario");
		if(authService.getUserCredential()==null){
			Executions.sendRedirect("/login.zul");
			return;
		}
	}

}
