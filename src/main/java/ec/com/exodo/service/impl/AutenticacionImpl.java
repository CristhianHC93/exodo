package ec.com.exodo.service.impl;

import java.sql.Time;
import java.util.Date;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import ec.com.exodo.dao.ie.UsuarioDAO;
import ec.com.exodo.entity.Usuario;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.service.*;

public class AutenticacionImpl implements Autenticacion {
	private static final String KEY_USER_MODEL = Autenticacion.class.getName() + "_MODEL";

	@Override
	public Usuario login(String username, String password) {
		UsuarioDAO usuarioDAO = InstanciaDAO.getInstanciaUsuarioDAO();
		Usuario usuario = usuarioDAO.getUsuario(username, password);
		// a simple plan text password verification
		if (usuario == null) {
			return usuario;
		} else {
			return usuario;
		}
	}

	public boolean loginDate(Usuario usuario) {
		usuario = InstanciaDAO.getInstanciaUsuarioDAO().getUsuario(usuario, new Time(new Date().getTime()));
		if (usuario == null) {
			return false;
		} else {
			Session sess = Sessions.getCurrent();
			sess.setAttribute("Usuario", usuario);
			return true;
		}
	}

	@Override
	public void logout() {
		Session sess = Sessions.getCurrent();
		sess.removeAttribute("Usuario");
	}

	public static Autenticacion getIntance() {
		return getIntance(Sessions.getCurrent());
	}

	@Override
	public Usuario getUserCredential() {
		return (Usuario) Sessions.getCurrent().getAttribute("Usuario");
	}

	private static Autenticacion getIntance(Session zkSession) {

		synchronized (zkSession) {

			Autenticacion userModel = (Autenticacion) zkSession.getAttribute(KEY_USER_MODEL);

			if (userModel == null) {

				zkSession.setAttribute(KEY_USER_MODEL, userModel = new AutenticacionImpl());

			}
			return userModel;
		}
	}
}
