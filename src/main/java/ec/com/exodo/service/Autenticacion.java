package ec.com.exodo.service;

import ec.com.exodo.entity.Usuario;

public interface Autenticacion {
	
	/**login with account and password**/
	public Usuario login(String account, String password);
	/**login for date**/
	public boolean loginDate(Usuario usuario);
	
	/**logout current user**/
	public void logout();
	
	/**get current user credential**/
	public Usuario getUserCredential();

}
