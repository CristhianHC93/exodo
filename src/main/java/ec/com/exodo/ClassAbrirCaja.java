package ec.com.exodo;

import java.sql.Timestamp;
import java.util.Date;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.CajaDAO;
import ec.com.exodo.entity.AreaSucursal;
import ec.com.exodo.entity.Caja;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassAbrirCaja {
	@Wire("#winFormAbrirCaja")
	private Window winFormAbrirCaja;

	private String dimensionPantalla;
	private String estadoPantalla;

	private CajaDAO cajaDAO = InstanciaDAO.getInstanciaCajaDAO();

	private String clase;
	private Caja caja;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(view, this, false);
		ajustarPantalla(clase);
		nuevo();
	}

	/*
	 * Metodo que ajusta la pantalla para modal o embebido dependiendo de la clase
	 * que lo llame
	 */
	private void ajustarPantalla(final String clase) {
		if (clase == null) {
			estadoPantalla = "embedded";
			dimensionPantalla = "100%";

		} else if (clase.equals(ClassVenta.class.getSimpleName())) {
			estadoPantalla = "modal";
			dimensionPantalla = "40%";
		}
		this.clase = clase;
	}

	public void nuevo() {
		AreaSucursal areaSucursal = Sistema.getAreaSucursal();
		caja = cajaDAO.getCaja(areaSucursal.getId(), true);
		if (caja == null) {
			caja = new Caja(50, areaSucursal);
		} else {
			Messagebox.show(getIdioma().getCajaAbierta());
			Executions.createComponents("/main.zul", null, null);
			salir();
		}
	}

	@NotifyChange("caja")
	@Command
	public void abrir() {
		caja.setFechaApertura(new Timestamp(new Date().getTime()));
		caja = cajaDAO.saveorUpdate(caja);
		if (clase != null && clase.equals(ClassVenta.class.getSimpleName())) {
			BindUtils.postGlobalCommand(null, null, "cargarCaja", null);
		}
		salir();
	}

	private void salir() {
		winFormAbrirCaja.detach();
	}

	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public Caja getCaja() {
		return caja;
	}

	public void setCaja(Caja caja) {
		this.caja = caja;
	}
}
