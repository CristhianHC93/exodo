package ec.com.exodo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.DoubleConsumer;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.dao.ie.CajaDAO;
import ec.com.exodo.dao.ie.CajaDetalleDAO;
import ec.com.exodo.entity.Caja;
import ec.com.exodo.entity.CajaDetalle;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassCierreCaja {

	private Caja caja;
	private List<CajaDetalle> listaCajaDetalle = new ArrayList<>();

	private CajaDAO cajaDAO = InstanciaDAO.getInstanciaCajaDAO();
	private CajaDetalleDAO cajaDetalleDAO = InstanciaDAO.getInstanciaCajaDetalleDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		aceptar();
	}

	@NotifyChange({ "listaCajaDetalle", "caja" })
	@Command
	public void aceptar() {
		caja = cajaDAO.getCaja(Sistema.getAreaSucursal().getId(), true);
		if (caja == null) {
			Messagebox.show("No existe caja Abierta en esta Area");
			listaCajaDetalle.clear();
			return;
		}
		caja.setCierreCaja(0.0);
		listaCajaDetalle = cajaDetalleDAO.getListaCajaDetalleByCaja(caja.getId());
		listaCajaDetalle.stream().mapToDouble(x -> x.getVentaCabecera().getTotal()).forEach(totalCierreCaja);
	}

	private DoubleConsumer totalCierreCaja = valor -> caja
			.setCierreCaja(Sistema.redondear((caja.getCierreCaja() + valor), 2));

	@NotifyChange("caja")
	@Command
	public void confirmar() {
		caja.setDiferencia(Sistema.redondear(caja.getCierreCaja() - caja.getValorEntregado(), 2));
	}

	@NotifyChange({ "listaCajaDetalle", "caja" })
	@Command
	public void guardar() {
		if (validarGuardar()) {
			caja.setFechaCierre(new Timestamp(new Date().getTime()));
			caja.setEstado(false);
			caja = cajaDAO.saveorUpdate(caja);
			if (caja != null) {
				Clients.showNotification(getIdioma().getIngresoSatisfactorio(), Clients.NOTIFICATION_TYPE_INFO, null,
						"top_center", 2000);
			} else {
				Clients.showNotification(getIdioma().getMensajeImposibleRealizarAccion(getIdioma().getCaja()),
						Clients.NOTIFICATION_TYPE_ERROR, null, "top_center", 2000);
			}

		}
	}

	private boolean validarGuardar() {
		boolean devolver = true;
		if (caja.getValorEntregado() <= 0) {
			Messagebox.show("Debe Ingresar Valor Entregado");
			devolver = false;
		}
		return devolver;
	}

	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public Caja getCaja() {
		return caja;
	}

	public void setCaja(Caja caja) {
		this.caja = caja;
	}

	public List<CajaDetalle> getListaCajaDetalle() {
		return listaCajaDetalle;
	}

	public void setListaCajaDetalle(List<CajaDetalle> listaCajaDetalle) {
		this.listaCajaDetalle = listaCajaDetalle;
	}

}
