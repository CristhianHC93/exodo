package ec.com.exodo.models;

public class Modelos<T, T1> {

	private T objetoT;
	private T1 objetoT1;
	private boolean estado;

	public Modelos(T objetoT, T1 objetoT1, boolean estado) {
		this.objetoT = objetoT;
		this.objetoT1 = objetoT1;
		this.estado = estado;
	}

	public T1 getObjetoT1() {
		return objetoT1;
	}

	public void setObjetoT1(T1 objetoT1) {
		this.objetoT1 = objetoT1;
	}

	public T getObjetoT() {
		return objetoT;
	}

	public void setObjetoT(T objetoT) {
		this.objetoT = objetoT;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

}
