package ec.com.exodo.controlUsuario;

import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.entity.Usuario;
import ec.com.exodo.service.Autenticacion;
import ec.com.exodo.service.impl.AutenticacionImpl;
import ec.com.exodo.sistema.Sistema;

public class login {

	private String username;
	private String password;
	private String mensaje;

	private Usuario usuario;

	private Autenticacion authService = new AutenticacionImpl();

	private List<String> listaIdioma = Sistema.getListaIdioma();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		usuario = authService.getUserCredential();
		if (usuario != null) {
			Executions.sendRedirect("/index.zul");
			return;
		}

	}

	@Command
	public void ingresar() {
		Autenticacion authService = AutenticacionImpl.getIntance();
		String nm = username;
		String pd = Sistema.Encriptar(password);
		usuario = authService.login(nm, pd);
		if (usuario == null) {
			mensaje = "Cuenta Incorrecta";
			Clients.showNotification("Credenciales Incorrectas", Clients.NOTIFICATION_TYPE_WARNING, null,
					"middle_center", 1500);
			return;
		} else {
			/* Las lineas comentadas son para inicio de sesion por hora */
			// if (authService.loginDate(u)) {

			mensaje = "Bienvenido, " + usuario.getNombre();
			mensaje = "";
			if (Sistema.verificarUsuarioEmpleado(usuario)) {
				if (usuario.getRol().getId() == Sistema.ID_ROL_VENDEDOR) {
					if (usuario.getAreaSucursal() == null) {
						Messagebox.show("Este vendedor no tiene asignada Area");
						return;
					} else {
						Sistema.setAreaSucursal(usuario.getAreaSucursal());
					}
				} else {
					Sistema.setAreaSucursal(usuario.getAreaSucursal());
				}
			}
			Sessions.getCurrent().setAttribute("Usuario", usuario);
			Executions.sendRedirect("index.zul");
			Clients.showNotification("Bienvenido " + usuario.getNombre(), Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			Sistema.datosEmail(Sistema.getListaEmail().get(0).getCorreo(), Sistema.getListaEmail().get(0).getClave(),
					Sistema.getListaEmail().get(0).getHost(), Sistema.getListaEmail().get(0).getPuerto());
		}
	}

	@Command
	@NotifyChange("idioma")
	public void selectIdioma(@BindingParam("item") String item) {
		Sistema.setIdioma(item);
	}

	/*
	 * Get and Set
	 */

	public List<String> getListaIdioma() {
		return listaIdioma;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public void setListaIdioma(List<String> listaIdioma) {
		this.listaIdioma = listaIdioma;
	}

}
