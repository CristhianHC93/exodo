package ec.com.exodo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.AreaSucursalDAO;
import ec.com.exodo.dao.ie.ConceptoMovimientoDAO;
import ec.com.exodo.dao.ie.CuadreInventarioCabeceraDAO;
import ec.com.exodo.dao.ie.CuadreInventarioDAO;
import ec.com.exodo.dao.ie.ProductoDAO;
import ec.com.exodo.entity.AreaSucursal;
import ec.com.exodo.entity.CuadreInventario;
import ec.com.exodo.entity.CuadreInventarioCabecera;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.Sucursal;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassTraspasoProducto {
	@Wire("#winDetalleMovimiento")
	private Window winDetalleMovimiento;
	
	
	private List<Sucursal> listaSucursal;
	private List<AreaSucursal> listaAreaSucursal;
	private Sucursal sucursal;
	private AreaSucursal areaSucursal;

	private List<Sucursal> listaSucursal2;
	// private List<AreaSucursal> listaAreaSucursal2;
	private Sucursal sucursal2;
	AreaSucursal areaSucursal2;

	private Producto producto;

	private Timestamp fechaInicial = new Timestamp(new Date().getTime());
	private Timestamp fechaFinal = new Timestamp(new Date().getTime());

	int estado = 0;

	String clase;
	private String dimensionPantalla;
	private String dimensionWidth;
	private String estadoPantalla;
	private boolean estadoVisualComponente = false;

	// Bandera para ver que productos estan repetidos
	boolean banderaProducto = false;
	boolean banderaGuardar = false;

	private ProductoDAO productoDAO = InstanciaDAO.getInstanciaProductoDAO();
	private AreaSucursalDAO areaSucursalDAO = InstanciaDAO.getInstanciaAreaSucursalDAO();
	private CuadreInventarioCabeceraDAO cuadreInventarioCabeceraDAO = InstanciaDAO
			.getInstanciaCuadreInventarioCabeceraDAO();
	private ConceptoMovimientoDAO conceptoMovimientoDAO = InstanciaDAO.getInstanciaConceptoMovimientoDAO();
	private CuadreInventarioDAO cuadreInventarioDAO = InstanciaDAO.getInstanciaCuadreInventarioDAO();

	private List<CuadreInventario> listaKardex = new ArrayList<>();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("cuadreInventario") CuadreInventario cuadreInventario,
			@ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(view, this, false);
		if (clase == null) {
			clase = "this";
		}
		this.clase = clase;
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			dimensionPantalla = "100%";
			dimensionWidth = "100%";
			estadoVisualComponente = true;
			listaSucursal = Sistema.getListaSucursal();
			sucursal = (listaSucursal.size() > 0) ? listaSucursal.get(0) : null;
			listaSucursal2 = Sistema.getListaSucursal();
			sucursal2 = (listaSucursal2.size() > 0) ? listaSucursal2.get(0) : null;
			selectSucursal();
			selectSucursal2();
			limpiar();
		} else {
			estado = 1;
			estadoPantalla = "modal";
			dimensionPantalla = "90%";
			dimensionWidth = "75%";
			estadoVisualComponente = false;
			fechaInicial = cuadreInventario.getCuadreInventarioCabecera().getFechaApertura();
			fechaFinal = (cuadreInventario.getCuadreInventarioCabecera().getFechaCierre() != null)
					? cuadreInventario.getCuadreInventarioCabecera().getFechaCierre()
					: fechaFinal;
			cargarProducto(cuadreInventario.getProducto());
		}
	}

	@NotifyChange({ "listaArea", "area" })
	@Command
	public void selectSucursal() {
		listaAreaSucursal = areaSucursalDAO.getListaAreaSucursalBySucursalActivo(sucursal.getId());
		areaSucursal = (listaAreaSucursal.size() > 0) ? listaAreaSucursal.get(0) : null;
	}

	@NotifyChange({ "listaArea2", "area2" })
	@Command
	public void selectSucursal2() {
		// listaAreaSucursal2 =
		// areaSucursalDAO.getListaAreaSucursalBySucursalActivo(sucursal2.getId());
		areaSucursal2 = areaSucursalDAO.getAreaSucursalByAreaAndSucursal(areaSucursal.getArea().getId(), sucursal2.getId());
	}

	@NotifyChange("listaKardex")
	@Command
	public void selectArea() {
		limpiar();
	}

	//CuadreInventarioCabecera cc;

	private CuadreInventarioCabecera verificarCuadreInventarioCabecera(AreaSucursal areaSucursal) {
		CuadreInventarioCabecera cc = cuadreInventarioCabeceraDAO.getCuadreInventarioByAreaSucursal(areaSucursal.getId(), true);
		if (cc == null) {
			cc = new CuadreInventarioCabecera();
			cc.setEstado(true);
			cc.setFechaApertura(new Timestamp(new Date().getTime()));
			cc.setAreaSucursal(areaSucursal);
			cc = cuadreInventarioCabeceraDAO.saveorUpdate(cc);
		}
		return cc;
	}

	@NotifyChange({ "listaCuadreInventario", "totalStock", "totalStockReal", "totalStockDiferencia", "totalCompra",
			"totalVenta" })
	@Command
	public void guardar() {
		if (listaKardex.size() <= 0) {
			Messagebox.show("No existen elementos");
			return;
		}
		if (areaSucursal.getId() == areaSucursal2.getId()) {
			Messagebox.show("Esta intentando realizar un traspaso a una misma Area");
			return;
		}

		if (banderaGuardar) {
			Messagebox.show("Uno de los productos a traspasar no existe en el inventario");
			banderaGuardar = false;
			return;
		}
		Timestamp fecha = new Timestamp(new Date().getTime());
		listaKardex.forEach(x -> {
			if (x.getProducto().getId() != null) {
				Producto producto = productoDAO.getProductoByCodigoOrDescripcionAndAreaSucursal(x.getProducto().getCodigo(),
						areaSucursal2.getId());
				CuadreInventario c = cuadreInventarioDAO
						.getCuadreInventarioByProductoCodigoAndAreaSucursal(producto.getCodigo(), areaSucursal2.getId());
				if (c == null) {
					c = new CuadreInventario();
					c.setProducto(producto);
					c.setCuadreInventarioCabecera(verificarCuadreInventarioCabecera(areaSucursal2));
					// cuadreInventarioDAO.saveorUpdate(c);
				}
				CuadreInventario entra = new CuadreInventario();
				entra.setFecha(fecha);
				x.setFecha(fecha);
				entra.setConceptoMovimiento(conceptoMovimientoDAO.getConceptoMovimientoById(Sistema.ID_TRASPASO_ENTRADA));
				x.setConceptoMovimiento(conceptoMovimientoDAO.getConceptoMovimientoById(Sistema.ID_TRASPASO_SALIDA));
				entra.setCantidadAntes(c.getProducto().getCantidad());
				entra.setCantidad(x.getCantidad());
				entra.setCantidadDespues(entra.getCantidadAntes() + entra.getCantidad());
				entra.setObservacion("Traslado Entrante de " + x.getCantidad() + " " + c.getProducto().getDescripcion()
						+ " de Sucursal " + areaSucursal.getSucursal().getInformacionNegocio().getNombre() + " a "
						+ areaSucursal2.getSucursal().getInformacionNegocio().getNombre());
				c.getProducto().setCantidad(entra.getCantidadDespues());
				x.getProducto().setCantidad(x.getCantidadDespues());
//				productoDAO.saveorUpdate(c.getProducto());
//				productoDAO.saveorUpdate(x.getProducto());

				cuadreInventarioDAO.saveorUpdate(x);
				cuadreInventarioDAO.saveorUpdate(entra);
			}
		});
		Clients.showNotification("Ingresado Satisfactorio ", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center",
				1500);
	}

	@Command
	@NotifyChange({ "listaKardex", "producto" })
	public void agregarProducto(@BindingParam("valorEstado") int estado) {
		if (areaSucursal == null) {
			return;
		}
		// recibe parametro estado con valor 1 si es para detalle de producto caso
		// cotrario se devolvera 0
		this.estado = estado;
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("clase", "kardex");
		parameters.put("filtroProducto", "");
		parameters.put("area", areaSucursal);
		Executions.createComponents("/view/formProducto.zul", null, parameters);
	}

	@SuppressWarnings("deprecation")
	@GlobalCommand
	@NotifyChange({ "listaKardex", "producto" })
	public void cargarProducto(@BindingParam("objetoProducto") Producto producto) {
		// Codigo para verificar si es un producto repetido
		listaKardex.forEach(v -> {
			if (v.getProducto() != null && v.getProducto().getId() == producto.getId()) {
				banderaProducto = true;
			}
		});
		if (banderaProducto) {
			Messagebox.show("El producto " + producto.getDescripcion() + " ya ha sido ingresado");
			banderaProducto = false;
			return;
		}
		if (estado == 1) {
			this.producto = producto;
			fechaInicial.setHours(0);
			fechaFinal.setHours(24);

			listaKardex = cuadreInventarioDAO.getListaCuadreInventarioByProductoAndFecha(producto.getId(), fechaInicial,
					fechaFinal);
		} else {
			agregarProducto(producto);
		}
	}

	@Command
	@NotifyChange({ "listaKardex" })
	public void cargarProductoCodigo(@BindingParam("item") String codigo) {
		if (codigo == null || codigo.trim().equals("")) {
			Messagebox.show("Ingresar Codigo");
			return;
		}
		try {
			cargarProducto("codigo", codigo);
		} catch (Exception e) {
			Messagebox.show("Codigo no encontrado");
		}
	}

	@NotifyChange({ "listaKardex" })
	@Command
	public void cargarProductoDescripcion(@BindingParam("item") String descripcion) {
		if (descripcion == null || descripcion.trim().equals("")) {
			Messagebox.show("Ingresar Nombre del producto");
			return;
		}
		try {
			cargarProducto("descripcion", descripcion);
		} catch (Exception e) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("clase", "traspasoProducto");
			parameters.put("filtroProducto", descripcion);
			parameters.put("area", areaSucursal);
			Executions.createComponents("/view/formProducto.zul", null, parameters);
		}
	}

	/*
	 * Metodo que se usa al cargar los productos desde el text de codigo y
	 * desripcion
	 */
	private void cargarProducto(String tipo, String dato) {
		Producto producto = new Producto();
		if (tipo.equals("descripcion")) {
			producto = productoDAO.getProductoByCodigoOrDescripcionAndAreaSucursal(dato.toUpperCase(), areaSucursal.getId());
		}
		if (tipo.equals("codigo")) {
			producto = productoDAO.getProductoByCodigoOrDescripcionAndAreaSucursal(dato.toUpperCase(), areaSucursal.getId());
		}

		if (producto.getCantidad() <= 0) {
			Messagebox.show("Producto sin cantidad");
			return;
		}
		for (CuadreInventario kardex : listaKardex) {
			if (kardex.getProducto().getId() == producto.getId()) {
				kardex.setCantidad(kardex.getCantidad() + 1);
				confirmar(kardex);
				listaKardex.get(0).getProducto().setCodigo("");
				listaKardex.get(0).getProducto().setDescripcion("");
				return;
			}
		}
		agregarProducto(producto);
	}

	private void agregarProducto(Producto producto) {
		CuadreInventario k = new CuadreInventario();
		k.setCantidadAntes(producto.getCantidad());
		k.setCantidad(0);
		k.setObservacion("Traslado Saliente de" + producto.getDescripcion() + " de Sucursal "
				+ areaSucursal.getSucursal().getInformacionNegocio().getNombre() + " a "
				+ areaSucursal2.getSucursal().getInformacionNegocio().getNombre());
		List<CuadreInventario> listaKardexProvisional = new ArrayList<>();
		listaKardexProvisional.addAll(listaKardex);
		limpiar();
		listaKardex.add(k);
		listaKardexProvisional.remove(0);
		listaKardex.addAll(listaKardexProvisional);
	}

	@Command
	@NotifyChange({ "listaKardex" })
	public void confirmar(@BindingParam("item") CuadreInventario kardex) {
		if (kardex.getProducto().getId() == null) {
			Messagebox.show("Escoger un Producto");
			return;
		}
		if (kardex.getCantidad() < 0) {
			Messagebox.show("Escoger Ingresar Cantidad");
			return;
		}
		if (kardex.getCantidadAntes() < kardex.getCantidad()) {
			Messagebox.show("La cantidad a trasladar excede la existente");
			kardex.setCantidad(0);
			return;
		}
		kardex.setCantidadDespues(kardex.getCantidadAntes() - kardex.getCantidad());
		kardex.setObservacion(
				"Traslado Saliente de " + kardex.getCantidad() + " " + kardex.getProducto().getDescripcion()
						+ " de Sucursal " + areaSucursal.getSucursal().getInformacionNegocio().getNombre() + " a "
						+ areaSucursal2.getSucursal().getInformacionNegocio().getNombre());
	}

	@NotifyChange({ "listaKardex", "producto" })
	@Command
	public void limpiar() {
		listaKardex.clear();
		CuadreInventario k = new CuadreInventario();
		k.setProducto(new Producto());
		listaKardex.add(k);
	}

	@Command
	public void salir() {
		winDetalleMovimiento.detach();
	}

	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public List<Sucursal> getListaSucursal() {
		return listaSucursal;
	}

	public void setListaSucursal(List<Sucursal> listaSucursal) {
		this.listaSucursal = listaSucursal;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public List<CuadreInventario> getListaKardex() {
		return listaKardex;
	}

	public void setListaKardex(List<CuadreInventario> listaKardex) {
		this.listaKardex = listaKardex;
	}

	public List<Sucursal> getListaSucursal2() {
		return listaSucursal2;
	}

	public void setListaSucursal2(List<Sucursal> listaSucursal2) {
		this.listaSucursal2 = listaSucursal2;
	}

	public Sucursal getSucursal2() {
		return sucursal2;
	}

	public void setSucursal2(Sucursal sucursal2) {
		this.sucursal2 = sucursal2;
	}

	public List<AreaSucursal> getListaAreaSucursal() {
		return listaAreaSucursal;
	}

	public void setListaAreaSucursal(List<AreaSucursal> listaAreaSucursal) {
		this.listaAreaSucursal = listaAreaSucursal;
	}

	public AreaSucursal getAreaSucursal() {
		return areaSucursal;
	}

	public void setAreaSucursal(AreaSucursal areaSucursal) {
		this.areaSucursal = areaSucursal;
	}

	public Timestamp getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Timestamp fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Timestamp getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Timestamp fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getDimensionWidth() {
		return dimensionWidth;
	}

	public void setDimensionWidth(String dimensionWidth) {
		this.dimensionWidth = dimensionWidth;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isEstadoVisualComponente() {
		return estadoVisualComponente;
	}

	public void setEstadoVisualComponente(boolean estadoVisualComponente) {
		this.estadoVisualComponente = estadoVisualComponente;
	}

}
