package ec.com.exodo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.dao.ie.CompraCabeceraDAO;
import ec.com.exodo.dao.ie.CompraDetalleDAO;
import ec.com.exodo.dao.ie.ConceptoMovimientoDAO;
import ec.com.exodo.dao.ie.CuadreInventarioDAO;
import ec.com.exodo.dao.ie.NotaCompraDetalleDAO;
import ec.com.exodo.dao.ie.ProductoDAO;
import ec.com.exodo.dao.ie.ProductoProveedorDAO;
import ec.com.exodo.dao.ie.ProveedorDAO;
import ec.com.exodo.entity.CompraCabecera;
import ec.com.exodo.entity.CompraDetalle;
import ec.com.exodo.entity.ConceptoMovimiento;
import ec.com.exodo.entity.CuadreInventario;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.entity.NotaCompraCabecera;
import ec.com.exodo.entity.NotaCompraDetalle;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.ProductoProveedor;
import ec.com.exodo.entity.Proveedor;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.models.Modelos;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassCompra {

	/*
	 * Instancias de DAO's
	 */

	private ProductoProveedorDAO productoProveedorDAO = InstanciaDAO.getInstanciaProductoProveedorDAO();
	private CompraCabeceraDAO compraCabeceraDAO = InstanciaDAO.getInstanciaCompraCabeceraDAO();
	private NotaCompraDetalleDAO notaCompraDetalleDAO = InstanciaDAO.getInstanciaNotaCompraDetalleDAO();
	private CompraDetalleDAO compraDetalleDAO = InstanciaDAO.getInstanciaCompraDetalleDAO();
	private ProductoDAO productoDAO = InstanciaDAO.getInstanciaProductoDAO();
	private CuadreInventarioDAO cuadreInventarioDAO = InstanciaDAO.getInstanciaCuadreInventarioDAO();
	private ConceptoMovimientoDAO conceptoMovimientoDAO = InstanciaDAO.getInstanciaConceptoMovimientoDAO();
	private ProveedorDAO proveedorDAO = InstanciaDAO.getInstanciaProveedorDAO();

	/*
	 * Variables que interantuan con la Vista
	 */
	private List<Modelos<CompraDetalle, NotaCompraDetalle>> listaCompra = new ArrayList<>();
	private List<NotaCompraDetalle> listaNotaCompraDetalle = new ArrayList<>();

	private NotaCompraCabecera notaCompraCabecera;
	private CompraCabecera compraCabecera;

	private boolean estadoAnular = true;
	private boolean estadoGuardar = true;
	private boolean estadoNotaCompra = false;
	private boolean estadoModificar = false;
	private String tipo = "compraDirecta";

	private boolean banderaValidarProducto = true;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("compraCabecera") CompraCabecera compraCabecera,
			@ExecutionArgParam("listaCompra") List<Modelos<CompraDetalle, NotaCompraDetalle>> listaCompra) {
		Selectors.wireComponents(view, this, false);
		if (compraCabecera != null) {
			this.compraCabecera = compraCabecera;
			this.listaCompra = new ArrayList<>(listaCompra.size());
			this.listaCompra.addAll(listaCompra);
		} else {
			nuevo();
		}
	}

	@Command
	public void agregarProveedor() {
		if (compraCabecera.getProveedor().getId() != null) {
			mensajeCambiarProveedor();
		} else {
			abrirVentajaProveedor();
		}
	}

	private void mensajeCambiarProveedor() {
		Messagebox.show("Si escoje otro proveedor se limpiaran los registros actuales", "Advertencia",
				Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						abrirVentajaProveedor();
					}
				});
	}

	private void abrirVentajaProveedor() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("clase", "Compra");
		Executions.createComponents("/view/formProveedor.zul", null, parameters);
	}

	@NotifyChange("compraCabecera")
	@Command
	public void agregarProveedorCtrl(@BindingParam("item") String item) {
		if (item.equals("") || item.isEmpty()) {
			Messagebox.show("Ingresar ruc o nombre");
		} else {
			final Proveedor proveedor = proveedorDAO.getProveedorByEmpresaOrRuc(item.toUpperCase());
			if (proveedor != null) {
				nuevo();
				compraCabecera.setProveedor(proveedor);
			} else
				Messagebox.show("Proveedor no encontrado");
		}
	}

	@GlobalCommand
	@NotifyChange({ "estadoModificar", "listaCompra", "notaCompraCabecera", "compraCabecera", "estadoGuardar",
			"estadoAnular" })
	public void cargarDatos(@BindingParam("objeto") Proveedor proveedor) {
		if (compraCabecera.getProveedor().getId() != null) {
			nuevo();
		}
		compraCabecera.setProveedor(proveedor);
	}

	@Command
	@NotifyChange("listaCompra")
	public void agregarProducto() {
		if (compraCabecera.getProveedor().getId() == null) {
			Messagebox.show("Debe ingresar un Proveedor");
			return;
		}
		if (listaCompra.size() > 1 && listaCompra.get(listaCompra.size() - 1).getObjetoT().getCantidad() == 0) {
			Messagebox.show("El ultimo registro no tiene Cantidad");
			return;
		}
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("filtroProducto", "");
		parameters.put("clase", "compra");
		parameters.put("proveedor", compraCabecera.getProveedor());
		Executions.createComponents("/view/formProducto.zul", null, parameters);
	}

	boolean repetido = false;

	@GlobalCommand
	@NotifyChange({ "listaCompra", "estadoGuardar", "estadoGuardar", "compraCabecera" })
	public void cargarProducto(@BindingParam("objetoProducto") Producto producto) {
		repetido = false;
		listaCompra.stream().filter(compra -> (compra.getObjetoT().getProducto().getId() == producto.getId()))
				.forEach((x) -> repetido = true);
		if (repetido) {
			Messagebox.show("El producto " + producto.getDescripcion() + " ya ha sido ingresado");
		} else {
			agregarProducto(producto);
		}
	}

	@Command
	@NotifyChange({ "listaCompra", "estadoGuardar", "listaCompra", "estadoModificar", "btGuardarEditar",
			"compraCabecera" })
	public void cargarProductoCodigo(@BindingParam("item") String codigo) {
		if (compraCabecera.getProveedor().getId() == null) {
			Messagebox.show("Debe ingresar un Proveedor");
			nuevo();
			return;
		}
		if (codigo == null || codigo.trim().equals("")) {
			Messagebox.show("Ingresar Codigo");
			return;
		}
		if (!cargarProducto(codigo)) {
			Messagebox.show("Codigo no encontrado");
		}
	}

	@NotifyChange({ "listaCompra", "estadoGuardar", "estadoGuardar", "estadoModificar", "btGuardarEditar",
			"compraCabecera" })
	@Command
	public void cargarProductoDescripcion(@BindingParam("item") String descripcion) {
		if (compraCabecera.getProveedor().getId() == null) {
			Messagebox.show("Debe ingresar un Proveedor");
			nuevo();
			return;
		}
		if (descripcion == null || descripcion.trim().equals("")) {
			Messagebox.show("Ingresar Nombre del producto");
			return;
		}
		if (!cargarProducto(descripcion)) {
			abrirVentanaProducto(descripcion);
		}
	}

	private void abrirVentanaProducto(String descripcion) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("clase", "compra");
		parameters.put("proveedor", compraCabecera.getProveedor());
		parameters.put("filtroProducto", descripcion);
		Executions.createComponents("/view/formProducto.zul", null, parameters);
	}

	/*
	 * Metodos que se usa al cargar los productos desde el text de codigo y
	 * desripcion
	 */

	private boolean cargarProducto(String dato) {
		final ProductoProveedor productoProveedor = productoProveedorDAO
				.getProductoByCodigoDescripcionProveedor(dato.toUpperCase(), compraCabecera.getProveedor().getId());
		if (productoProveedor != null) {
			if (validarProductoRepetido(productoProveedor.getProducto())) {
				agregarProducto(productoProveedor.getProducto());
			}
		}
		return (productoProveedor != null);
	}

	private boolean validarProductoRepetido(final Producto producto) {
		banderaValidarProducto = true;
		listaCompra.stream().filter(com -> com.getObjetoT().getProducto().getId() == producto.getId())
				.forEach(this::logicaProductoRepetido);
		return banderaValidarProducto;
	}

	private void logicaProductoRepetido(Modelos<CompraDetalle, NotaCompraDetalle> com) {
		com.getObjetoT().setCantidad(com.getObjetoT().getCantidad() + 1);
		confirmar(com);
		listaCompra.get(0).getObjetoT().getProducto().setCodigo("");
		listaCompra.get(0).getObjetoT().getProducto().setDescripcion("");
		banderaValidarProducto = false;
	}

	private void agregarProducto(Producto producto) {
		CompraDetalle cd = getCompraDetalleNuevo();
		cd.setProducto(producto);
		cd.setPrecioCompra(producto.getPrecioCompra());
		cd.setCantidad(1);
		List<Modelos<CompraDetalle, NotaCompraDetalle>> listaCompraProvisional = new ArrayList<>();
		listaCompraProvisional.addAll(listaCompra);
		listaCompra.clear();
		Proveedor p = compraCabecera.getProveedor();
		nuevo();
		compraCabecera.setProveedor(p);
		listaCompra.add(new Modelos<CompraDetalle, NotaCompraDetalle>(cd, null, false));
		listaCompraProvisional.remove(0);
		listaCompra.addAll(listaCompraProvisional);
		confirmar(listaCompra.get(1));
	}

	@Command
	public void agregarNotaCompra() {
		Executions.createComponents("/view/formNotaCompraLista.zul", null, null);
	}

	@Command
	@NotifyChange({ "notaCompraCabecera", "compraCabecera", "listaCompra", "estadoGuardar", "estadoNotaCompra" })
	public void agregarNotaCompraCtrl(@BindingParam("item") String item) {
		if (item.equals("")) {
			Messagebox.show("Codigo de Nota de Compra no encontrados");
			return;
		}

		final NotaCompraCabecera notaCompraCabecera = InstanciaDAO.getInstanciaNotaCompraCabeceraDAO()
				.getNotaCompraCabeceraByCodio(item);
		if (notaCompraCabecera != null) {
			cargarNotaCompra(notaCompraCabecera);
		} else {
			Messagebox.show("Nota de Compra no encontrado");
		}
	}

	@GlobalCommand
	@NotifyChange({ "notaCompraCabecera", "compraCabecera", "listaCompra", "estadoGuardar", "estadoNotaCompra" })
	public void cargarNotaCompra(@BindingParam("objetoNotaCompra") NotaCompraCabecera notaCompraCabecera) {
		this.notaCompraCabecera = notaCompraCabecera;
		compraCabecera.setProveedor(notaCompraCabecera.getProveedor());
		listaCompra.clear();
		listaCompra.add(new Modelos<CompraDetalle, NotaCompraDetalle>(getCompraDetalleNuevo(), null, true));
		agregarDetalleNotaCompra(notaCompraCabecera.getId());

		estadoGuardar = false;
		estadoNotaCompra = true;
	}

	private void agregarDetalleNotaCompra(int idNotaCompraCabecera) {
		for (NotaCompraDetalle notaDetalle : notaCompraDetalleDAO.getNotaCompraDetallesByCabecera(idNotaCompraCabecera,
				Sistema.ESTADO_INGRESADO)) {
			CompraDetalle compraDetalle = new CompraDetalle();
			compraDetalle.setProducto(notaDetalle.getProducto());
			compraDetalle.setCantidad(notaDetalle.getCantidad());
			compraDetalle.setPrecioCompra(notaDetalle.getPrecioCompra());
			compraDetalle.setSubtotal(notaDetalle.getSubtotal());
			compraDetalle.setDescuentoValor(notaDetalle.getDescuentoValor());
			compraDetalle.setDescuentoPorcentaje(notaDetalle.getDescuentoPorcentaje());
			compraDetalle.setValorIva(notaDetalle.getValorIva());
			compraDetalle.setTotal(notaDetalle.getTotal());
			listaCompra.add(new Modelos<CompraDetalle, NotaCompraDetalle>(compraDetalle, notaDetalle, false));
		}
	}

	@Command
	public void buscar() {
		Executions.createComponents("/view/formCompraLista.zul", null, null);
	}

	@GlobalCommand
	@NotifyChange({ "estadoModificar", "estadoAnular", "compraCabecera", "listaCompra", "estadoGuardar" })
	public void cargarCompra(@BindingParam("objetoCompra") CompraCabecera compraCabecera) {
		this.compraCabecera = compraCabecera;
		listaCompra.clear();
		List<CompraDetalle> listaCompraDetalle = compraDetalleDAO.getCompraDetallesByCabecera(compraCabecera.getId());
		listaCompraDetalle
				.forEach(compra -> listaCompra.add(new Modelos<CompraDetalle, NotaCompraDetalle>(compra, null, true)));
		estadoAnular = false;
		estadoGuardar = true;
		estadoModificar = true;
	}

	@Command
	@NotifyChange({ "listaCompra", "estadoGuardar", "compraCabecera" })
	public void confirmar(@BindingParam("item") Modelos<CompraDetalle, NotaCompraDetalle> compra) {
		validarConfirmar(compra);
		calcularItem(compra.getObjetoT());
		calcularListatotal();
		estadoGuardar = false;
	}

	private void validarConfirmar(Modelos<CompraDetalle, NotaCompraDetalle> compra) {
		if (compra.getObjetoT().getProducto().getId() == null) {
			Messagebox.show("Debe escoger un producto");
			compra.getObjetoT().setPrecioCompra(0.0);
			return;
		}
		if (compra.getObjetoT().getCantidad() < 1) {
			Messagebox.show("Debe ingresar una cantidad");
			compra.getObjetoT().setCantidad(1);
		}
		if (compra.getObjetoT1() != null) {
			if (compra.getObjetoT().getCantidad() > compra.getObjetoT1().getCantidad()) {
			}
		}
	}

	private void calcularItem(CompraDetalle compraDetalle) {
		compraDetalle.setCantidad(Sistema.redondear(compraDetalle.getCantidad()));

		compraDetalle.setSubtotal(Sistema.redondear(compraDetalle.getCantidad() * compraDetalle.getPrecioCompra(), 4));

		//if (compraDetalle.getDescuentoPorcentaje() > 0) {
			/* Calculamos el valor descuento por precio Unitario */
			compraDetalle.setDescuentoValor(
					(compraDetalle.getPrecioCompra() * (compraDetalle.getDescuentoPorcentaje() / 100)));
			/*
			 * Seteamos el nuevo valor de subtotal restandole el resultado de descuento
			 * multiplicado cantidad
			 */
			compraDetalle.setSubtotal(Sistema.redondear(
					compraDetalle.getSubtotal() - (compraDetalle.getDescuentoValor() * compraDetalle.getCantidad()),
					4));
		//}
		if (compraDetalle.getProducto().getImpuesto().getPorcentaje() > 0) {
			/* Calculamos el valor de iva por precio Unitario menos descuento */
			compraDetalle
					.setValorIva(Sistema.redondear((compraDetalle.getPrecioCompra() - compraDetalle.getDescuentoValor())
							* (((double) compraDetalle.getProducto().getImpuesto().getPorcentaje()) / 100), 4));
		}
		compraDetalle
				.setTotal(compraDetalle.getSubtotal() + (compraDetalle.getValorIva() * compraDetalle.getCantidad()));
	}

	public void calcularListatotal() {
		cargarTotales();
		if (listaCompra.size() > 0) {
			listaCompra.forEach(item -> {
				if (item.getObjetoT().getCantidad() > 0) {
					compraCabecera.setSubtotal(
							compraCabecera.getSubtotal() + Sistema.redondear(item.getObjetoT().getSubtotal(), 2));
					compraCabecera.setIva(compraCabecera.getIva()
							+ Sistema.redondear(item.getObjetoT().getValorIva() * item.getObjetoT().getCantidad(), 2));
					compraCabecera.setDescuento(compraCabecera.getDescuento() + Sistema
							.redondear(item.getObjetoT().getDescuentoValor() * item.getObjetoT().getCantidad(), 2));
				}
			});
			compraCabecera.setTotal(Sistema.redondear((compraCabecera.getSubtotal() + compraCabecera.getIva()), 2));
		}
	}

	private void cargarTotales() {
		compraCabecera.setSubtotal(0.0);
		compraCabecera.setTotal(0.0);
		compraCabecera.setIva(0.0);
		compraCabecera.setDescuento(0.0);
		compraCabecera.setTotal(0.0);
		compraCabecera.setDescuento(0.0);
	}

	@Command
	@NotifyChange({ "estadoModificar", "listaCompra", "notaCompraCabecera", "compraCabecera", "estadoGuardar",
			"estadoAnular", })
	public void eliminar(@BindingParam("item") Modelos<CompraDetalle, NotaCompraDetalle> compra) {
		listaCompra.remove(compra);
		if (listaCompra.size() == 0 || listaCompra == null) {
			nuevo();
		}
		calcularListatotal();
	}

	@Command
	public void verPrecio(@BindingParam("item") Modelos<CompraDetalle, NotaCompraDetalle> compra) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("clase", "Compra");
		List<Proveedor> listaProveedor = new ArrayList<>();
		cuadreInventarioDAO
				.getListaCuadreInventarioByProductoAndConceptoMovmiento(compra.getObjetoT().getProducto().getId(),
						Sistema.ID_COMPRA)
				.stream().map(this::tranformarAProveedor).forEach(listaProveedor::add);

//		productoProveedorDAO.getListaProductoProveedorByProducto(compra.getObjetoT().getProducto().getId()).stream()
//				.map(this::tr).forEach(listaProveedor::add);
		parameters.put("lista", listaProveedor);
		Executions.createComponents("/view/formProveedor.zul", null, parameters);
	}

	private Proveedor tranformarAProveedor(CuadreInventario x) {
		final  CompraDetalle compraDetalle = compraDetalleDAO.getCompraDetallesById(x.getIdDetalle());
		final  Proveedor proveedor = compraDetalle.getCompraCabecera().getProveedor();
		proveedor.setDescuentoValor(compraDetalle.getDescuentoValor());
		proveedor.setDescuentoPorcentaje(compraDetalle.getDescuentoPorcentaje());
		proveedor.setCosto(compraDetalle.getPrecioCompra());
/*		x.getProveedor().setDescuentoValor(x.getDescuentoValor());
		x.getProveedor().setDescuentoPorcentaje(x.getDescuentoPorcentaje());
		x.getProveedor().setCosto(x.getPrecioCosto());*/
		return compraDetalle.getCompraCabecera().getProveedor();
	}

	@Command
	@NotifyChange({ "listaCompra", "compraCabecera", "estadoGuardar" })
	public void guardar(@BindingParam("actualizar") boolean acutalizar) {
		if (validarGuardar()) {
			final CompraCabecera compraCabeceraReferencia = guardarCabecera();
			listaCompra.remove(0);
			compraCabeceraDAO.saveorUpdateDetalle(listaCompra, compraCabeceraReferencia, acutalizar);
			verificarNotaCompra();
			Clients.showNotification("Ingresado Satisfactorio ", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center",
					1500);
			estadoGuardar = true;
			compraCabecera = compraCabeceraReferencia;
			nuevo();
		}
	}

	private boolean validarGuardar() {
		boolean devolver = true;
		if ((compraCabecera.getProveedor() == null)) {
			Messagebox.show("Debe seleccionar un Proveedor", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
			devolver = false;
		}
		if (estadoNotaCompra && notaCompraCabecera == null) {
			Messagebox.show("Debe seleccionar una Nota de Compra", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
			devolver = false;
		}
		if (listaCompra.isEmpty() == true) {
			Messagebox.show("Debe seleccionar Al menos un Producto", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
			devolver = false;
		}
		if (Sistema.validarRepetido(
				InstanciaDAO.getInstanciaCompraCabeceraDAO().getCompraCabeceras(Sistema.ESTADO_INGRESADO),
				compraCabecera)) {
			devolver = false;
		}
		return devolver;
	}

	private CompraCabecera guardarCabecera() {
		compraCabecera.setEstado(new Estado(Sistema.ESTADO_INGRESADO));
		compraCabecera.setUsuario(Sistema.getUsuario());
		compraCabecera.setFecha(new Timestamp(new Date().getTime()));
		return compraCabeceraDAO.saveorUpdate(compraCabecera);
	}

	private void verificarNotaCompra() {
		if (estadoNotaCompra) {
			listaNotaCompraDetalle.clear();
			listaNotaCompraDetalle = notaCompraDetalleDAO.getNotaCompraDetallesByCabecera(notaCompraCabecera.getId(),
					Sistema.ESTADO_INGRESADO);
			if (listaNotaCompraDetalle == null || listaNotaCompraDetalle.size() == 0) {
				notaCompraCabecera.setEstado(new Estado(Sistema.ESTADO_CONSUMIDO));
				InstanciaDAO.getInstanciaNotaCompraCabeceraDAO().saveorUpdate(notaCompraCabecera);
			}
		}
	}

	@Command
	@NotifyChange({ "estadoModificar", "listaCompra", "compraCabecera", "estadoGuardar", "estadoAnular", "total",
			"subtotal", "iva", "descuento" })
	public void anular() {
		Messagebox.show(getIdioma().getMensajeEliminar(compraCabecera.getCodigo()), "Advertencia",
				Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						estadoAnular = true;
						compraCabecera.setEstado(new Estado(Sistema.ESTADO_ANULADO));
						compraCabecera.setUsuario(Sistema.getUsuario());

						if (compraCabeceraDAO.saveorUpdate(compraCabecera) != null) {
							estadoGuardar = true;
							estadoModificar = true;
							listaCompra.stream().map(x -> x.getObjetoT()).forEach(this::guardarCuadreInventario);
							Clients.showNotification("El registro ha sido anulado", Clients.NOTIFICATION_TYPE_INFO,
									null, "middle_center", 1500);
							nuevo();
							BindUtils.postNotifyChange(null, null, ClassCompra.this, "estadoAnular");
						}
					}
				});
	}

	private void guardarCuadreInventario(CompraDetalle compraDetalle) {
		final Producto producto = productoDAO.getProducto(compraDetalle.getProducto().getId());
		CuadreInventario cuadreInventario = new CuadreInventario();

		final ConceptoMovimiento conceptoMovimiento = conceptoMovimientoDAO
				.getConceptoMovimientoById(Sistema.ID_ANULAR_COMPRA);

		final double valorCompra = Sistema.redondear(
				compraDetalle.getPrecioCompra() - compraDetalle.getDescuentoValor() + compraDetalle.getValorIva());
		cuadreInventario.setValorCompra(valorCompra * -1);
		cuadreInventario.setTotalCompra(Sistema.redondear(compraDetalle.getTotal()) * -1);
		cuadreInventario.setFecha(new Timestamp(new Date().getTime()));
		cuadreInventario.setConceptoMovimiento(conceptoMovimiento);
		cuadreInventario.setCantidadAntes(producto.getCantidad());
		cuadreInventario.setCantidad(compraDetalle.getCantidad());
		cuadreInventario.setCantidadDespues(producto.getCantidad() - compraDetalle.getCantidad());
		producto.setCantidad(producto.getCantidad() - compraDetalle.getCantidad());
		cuadreInventario.setProducto(producto);
		cuadreInventario
				.setObservacion("Anulacion Compra de " + compraDetalle.getCantidad() + " " + producto.getDescripcion());
		cuadreInventarioDAO.saveorUpdate(cuadreInventario);
	}

	@Command
	public void generarPdf() {
		if (this.listaCompra.isEmpty()) {
			Messagebox.show("No existen registros en el Detalle");
			return;
		}
		try {
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("compraCabecera", compraCabecera);
			parametros.put("listaCompra", listaCompra);
			Executions.createComponents("/reportes/impresionCompra.zul", null, parametros);
		} catch (Exception e) {
			System.out.println("Que anda mal");
			Messagebox.show("Falta datos en la compra");
		}
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		iframe.setContent(Sistema.reportePDF("facturacion.jasper",
				Sistema.getMapReport(compraCabecera.getProveedor().getNombre(), compraCabecera.getProveedor().getRuc(),
						compraCabecera.getProveedor().getDireccion(), compraCabecera.getProveedor().getTelefono(),
						Sistema.getListaInformacionNegocio().get(0).getNombre(),
						Sistema.getListaInformacionNegocio().get(0).getRuc(), compraCabecera.getFecha(),
						compraCabecera.getUsuario().getNombre(), Sistema.redondear(compraCabecera.getSubtotal()),
						Sistema.redondear(compraCabecera.getIva()), 0.0, 0.0,
						Sistema.redondear(compraCabecera.getTotal()), compraCabecera.getEstado()),
				Sistema.getCompraDataSource(listaCompra)));
	}

	@Command
	@NotifyChange({ "notaCompraCabecera", "tipo", "estadoProducto", "estadoNotaCompra", "listaCompra", "compraCabecera",
			"estadoGuardar", "estadoModificar", "listaCompra", "notaCompraCabecera", "estadoAnular", })
	public void select() {
		if (tipo.trim().equals("compraDirecta")) {
			estadoNotaCompra = false;
		}
		if (tipo.trim().equals("notaCompra")) {
			estadoNotaCompra = true;
		}
	}

	@Command
	@NotifyChange({ "estadoModificar", "listaCompra", "notaCompraCabecera", "compraCabecera", "estadoGuardar",
			"estadoAnular", })
	public void nuevo() {
		compraCabecera = new CompraCabecera();
		compraCabecera.setProveedor(new Proveedor());
		listaCompra = new ArrayList<>();
		notaCompraCabecera = new NotaCompraCabecera();
		compraCabecera.setFecha(new Timestamp(new Date().getTime()));
		estadoGuardar = true;
		estadoAnular = true;
		estadoModificar = false;
		listaCompra.add(new Modelos<CompraDetalle, NotaCompraDetalle>(getCompraDetalleNuevo(), null, true));
		cargarTotales();
	}

	private CompraDetalle getCompraDetalleNuevo() {
		CompraDetalle compraDetalle = new CompraDetalle();
		compraDetalle.setCantidad(0);
		compraDetalle.setProducto(new Producto());
		return compraDetalle;
	}

	/*
	 * Metodos GET AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public boolean isEstadoNotaCompra() {
		return estadoNotaCompra;
	}

	public void setEstadoNotaCompra(boolean estadoNotaCompra) {
		this.estadoNotaCompra = estadoNotaCompra;
	}

	public NotaCompraCabecera getNotaCompraCabecera() {
		return notaCompraCabecera;
	}

	public void setNotaCompraCabecera(NotaCompraCabecera notaCompraCabecera) {
		this.notaCompraCabecera = notaCompraCabecera;
	}

	public CompraCabecera getCompraCabecera() {
		return compraCabecera;
	}

	public void setCompraCabecera(CompraCabecera CompraCabecera) {
		this.compraCabecera = CompraCabecera;
	}

	public boolean isEstadoGuardar() {
		return estadoGuardar;
	}

	public void setEstadoGuardar(boolean estadoGuardar) {
		this.estadoGuardar = estadoGuardar;
	}

	public List<Modelos<CompraDetalle, NotaCompraDetalle>> getListaCompra() {
		return listaCompra;
	}

	public void setListaCompra(List<Modelos<CompraDetalle, NotaCompraDetalle>> listaCompra) {
		this.listaCompra = listaCompra;
	}

	public boolean isEstadoAnular() {
		return estadoAnular;
	}

	public void setEstadoAnular(boolean estadoAnular) {
		this.estadoAnular = estadoAnular;
	}

	public boolean isEstadoModificar() {
		return estadoModificar;
	}

	public void setEstadoModificar(boolean estadoModificar) {
		this.estadoModificar = estadoModificar;
	}

}
