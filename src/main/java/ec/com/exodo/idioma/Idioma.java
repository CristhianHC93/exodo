package ec.com.exodo.idioma;

import ec.com.exodo.entity.Producto;

public interface Idioma {
	//Para Modulos
	public String getInventario();
	public String getCompra();
	public String getVenta();
	public String getAbrirCaja();
	public String getUsuario();
	public String getConfiguracion();
	public String getReporte();
	public String getCerrarSesion();
	//Para item's de Modulos
	
	public String getCompraMin();
	public String getVentaMin();
	
	public String getSistemaFacturacion();
	public String getProducto();
	public String getCategoria();
	public String getProveedor();
	public String getNotaCompra();
	public String getCliente();
	public String getModulo();
	public String getRol();
	public String getDetalleModulo();
	public String getRolModulo();
	public String getEmail();
	public String getReporteVenta();
	public String getConceptoMovimiento();
	
	public String getHost();
	public String getPuerto();	
	public String getGuardar();
	public String getEditar();
	public String getNuevo();
	public String getAceptar();
	public String getSalir();
	public String getSalida();
	public String getEntrada();
	public String getGenerarPdf();
	public String getGenerarExcel();
	public String getCodigo();
	public String getDescripcion();
	public String getBuscar();
	public String getOpcion();
	public String getProveedorMin();
	public String getCategoriaMin();
	public String getImpuesto();
	public String getPrecioVenta();
	public String getPrecioCompra();
	public String getStock();
	public String getStockReal();
	public String getStockDiferencia();
	public String getStockAlerta();
	public String getStockCritico();
	public String getNoExisteInformacion();	
	public String getEmpresa();
	public String getRuc();
	public String getNombre();
	public String getDireccion();
	public String getTelefono();
	public String getCelular();
	public String getCorreoPersonal();
	public String getCorreoEmpresarial();
	public String getObservacion();
	public String getAnular();	
	public String getNotaCompraMin();
	public String getHora();
	public String getFecha();
	public String getFechaRecepcion();
	public String getCantidad();
	public String getPrecio();
	public String getSubTotalMin();
	public String getAgregarItem();
	public String getSubTotal();
	public String getIva();
	public String getTotal();
	public String getFechaEmision();
	public String getCedula();
	public String getCorreo();
	public String getProforma();
	public String getClienteMin();
	public String getDescuentoPorcentaje();
	public String getPorcentaje();
	public String getDescuentoValor();
	public String getFuncion();
	public String getCargo();
	public String getRolMin();
	public String getUsuarioMin();
	public String getClave();
	public String getActivo();
	public String getFechaCreacion();
	public String getEstado();	
	public String getAbreviacion();
	public String getModuloMin();
	public String getRuta();
	public String getIvaMin();
	public String getTotalMin();
	public String getListaCompra();
	public String getListaNotaCompra();
	public String getListaVenta();
	public String getCaja();
	
	/*
	 * Mensajes de Pantalla
	 */

	public String getMensageCodigoRepetido();
	public String getMensageRucRepetido();
	public String getMensageCedulaRepetido();
	
	/*
	 * 
	 */
	public String getArea();
	public String getAreaMin();
	public String getSucursal();
	public String getSucursalMin();
	public String getCuadreInventario();
	public String getAlimentarInventario();
	

	public String getIngresarCodigo();
	public String getIngresarDescripcion();
	public String getCodigoNoEncontrado();

	
	/*METODOS DE MENSAJES*/
	
	public String getIngresoSatisfactorio();
	public String getCajaAbierta();
	public String getNoExistenElementos();
	public String getEscojerProducto();
	public String getIngresarCantidad();
	public String getRegistroEliminadoSatifactoriamente();
	public String getImposibleEliminarRegistro();
	public String getProcesoRealizadoExitosamente();
	
	/*METODOS DE MENSAJES PARAMETRIZADOS*/
	
	public String getMensajeEliminar(String registro);
	public String getMensajeAlimentarInventario(Producto producto);
	public String getMensajeImposibleRealizarAccion(String descripcion);
	public String getMensajeProductoIngresado(String producto);
}
