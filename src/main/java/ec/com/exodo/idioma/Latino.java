package ec.com.exodo.idioma;

import ec.com.exodo.entity.Producto;

public class Latino implements Idioma {

	private Latino() {
	}

	private static Latino instancia;

	public static Latino getInstancia() {
		if (instancia == null) {
			instancia = new Latino();
		}
		return instancia;
	}

	// Label Nombre del Sistema
	private final String SISTEMA_FACTURACION = "EXODO";
	// Label de Menu's
	private final String INVENTARIO = "INVENTARIO";
	private final String COMPRA = "COMPRA";
	private final String VENTA = "VENTA";
	private final String USUARIO = "USUARIO";
	private final String CONFIGURACION = "CONFIGURACION";
	private final String REPORTE = "REPORTE";
	private final String CERRAR_SESION = "CERRAR SESION";
	// Label SubMenu's
	private final String CUADRE_INVENTARIO = "CUADRE INVENTARIO";
	private final String PRODUCTO = "PRODUCTO";
	private final String CATEGORIA = "CATEGORIA";
	private final String PROVEEDOR = "PROVEEDOR";
	private final String NOTA_COMPRA = "NOTA COMPRA";
	private final String CLIENTE = "CLIENTE";
	private final String MODULO = "MODULO";
	private final String ROL = "ROL";
	private final String DETALLE_MODULO = "DETALLE MODULO";
	private final String ROL_MODULO = "ROL MODULO";
	private final String EMAIL = "EMAIL";
	private final String REPORTE_VENTA = "REPORTE VENTA";
	private final String CONCEPTO_MOVIMIENTO = "CONCEPTO MOVIMIENTO";
	private final String ALIMENTAR_INVENTARIO = "ALIMENTAR INVENTARIO";
	// Label Botones
	private final String HOST = "Host";
	private final String COMPRA_MIN = "Compra";
	private final String VENTA_MIN = "Venta";

	private final String CAJA = "Caja";

	private final String PUERTO = "Puerto";
	private final String GUARDAR = "Guardar";
	private final String EDITAR = "Guardar Cambios";
	private final String NUEVO = "Nuevo";
	private final String ACEPTAR = "Aceptar";
	private final String SALIR = "Salir";
	private final String GENERAR_PDF = "Generar PDF";
	private final String GENERAR_EXCEL = "Generar EXCEL";
	// Label generales
	private final String CODIGO = "Código *";
	private final String DESCRIPCION = "Descripción *";
	private final String BUSCAR = "Buscar";
	private final String OPCION = "Opción";
	private final String PROVEEDOR_MIN = "Proveedor *";
	private final String CATEGORIA_MIN = "Categoría *";
	private final String IMPUESTO = "Impuesto *";
	private final String PRECIO_VENTA = "Precio Venta *";
	private final String PRECIO_COMPRA = "Precio Compra *";
	private final String STOCK = "Stock *";
	private final String STOCK_ALERTA = "StockAlerta *";
	private final String STOCK_CRITICO = "StockCritico *";
	private final String NO_EXISTE_INFORMACION = "No existe Información";
	private final String NO_EXISTEN_ELEMENTOS = "No existen elementos";
	private final String INGRESAR_CODIGO = "Ingresar Código";
	private final String INGRESAR_DESCRIPCION = "Ingresar Descripción";
	private final String CODIGO_NO_ENCONTRADO = "Código no encontrado";
	private final String ESCOJER_PRODUCTO = "Escojer Producto";
	private final String INGRESAR_CANTIDAD = "Ingresar Cantidad";
	private final String REGISTRO_ELIMINADO_SATISFACTORIAMENTE = "Registro eliminado satisfactoriamente";
	private final String IMPOSIBLE_ELIMINAR_REGISTRO = "Imposible Eliminar registro";
	private final String PROCESO_REALIZADO_EXITOSAMENTE = "Proceso Realizado Exitosamente";
	private final String EMPRESA = "Empresa *";
	private final String RUC = "RUC *";
	private final String NOMBRE = "Nombre *";
	private final String DIRECCION = "Dirección";
	private final String TELEFONO = "Teléfono";
	private final String CELULAR = "Celular";
	private final String CORREO_PERSONAL = "Correo Personal";
	private final String CORREO_EMPRESARIAL = "Correo Empresarial";
	private final String OBSERVACION = "Observación";
	private final String ANULAR = "Anular";
	private final String NOTA_COMPRA_MIN = "Nota Compra";
	private final String HORA = "Hora";
	private final String FECHA = "Fecha";
	private final String FECHA_RECEPCION = "Fecha Recepción";
	private final String FECHA_EMISION = "Fecha Emisión";
	private final String CANTIDAD = "Cantidad";
	private final String PRECIO = "Precio";
	private final String SUB_TOTAL_MIN = "SubTotal";
	private final String AGREGAR_ITEM = "Agregar Item";
	private final String SUB_TOTAL = "SUBTOTAL";
	private final String IVA = "IVA";
	private final String TOTAL = "TOTAL";
	private final String CEDULA = "Cédula *";
	private final String CORREO = "Correo ";
	private final String PROFORMA = "Proforma";
	private final String CLIENTE_MIN = "Cliente";
	private final String DESCUENTO_PORCENTAJE = "Desc %";
	private final String DESCUENTO_VALOR = "Desc $";
	private final String FUNCION = "Función";
	private final String CARGO = "Cargo";
	private final String ROL_MIN = "Rol *";
	private final String USUARIO_MIN = "Usuario *";
	private final String CLAVE = "Clave *";
	private final String ACTIVO = "Activo";
	private final String FECHA_CREACION = "Fecha Creación";
	private final String ESTADO = "Estado";
	private final String ABREVIACION = "Abreviación";
	private final String MODULO_MIN = "Módulo";
	private final String RUTA = "Ruta";
	private final String IVA_MIN = "Iva";
	private final String TOTAL_MIN = "Total";

	private final String ENTRADA = "Entrada";
	private final String SALIDA = "Salida";

	private final String ABRIR_CAJA = "Abrir Caja";

	private final String LISTA_COMPRA = "LISTA COMPRA";
	private final String LISTA_NOTA_COMPRA = "LISTA NOTA COMPRA";
	private final String LISTA_VENTA = "LISTA VENTA";

	private final String MENSAJE_CODIGO_REPETIDO = "Este Código ya ha sido asignado";
	private final String MENSAJE_RUC_REPETIDO = "Este Ruc ya ha sido asignado";
	private final String MENSAJE_CEDULA_REPETIDO = "Esta Cedula ya ha sido asignado";
	private final String PORCENTAJE = "Porcentaje";
	private final String INGRESO_SATISFACTORIO = "Ingresado Satisfactorio";
	private final String CAJA_ABIERTA = "La caja ya esta abierta";

	@Override
	public String getAbrirCaja() {
		return ABRIR_CAJA;
	}

	@Override
	public String getInventario() {
		return INVENTARIO;
	}

	@Override
	public String getCompra() {
		return COMPRA;
	}

	@Override
	public String getVenta() {
		return VENTA;
	}

	@Override
	public String getUsuario() {
		return USUARIO;
	}

	@Override
	public String getConfiguracion() {
		return CONFIGURACION;
	}

	@Override
	public String getReporte() {
		return REPORTE;
	}

	@Override
	public String getCerrarSesion() {
		return CERRAR_SESION;
	}

	@Override
	public String getSistemaFacturacion() {
		return SISTEMA_FACTURACION;
	}

	@Override
	public String getProducto() {
		return PRODUCTO;
	}

	public String getCategoria() {
		return CATEGORIA;
	}

	@Override
	public String getProveedor() {
		return PROVEEDOR;
	}

	@Override
	public String getNotaCompra() {
		return NOTA_COMPRA;
	}

	@Override
	public String getCliente() {
		return CLIENTE;
	}

	@Override
	public String getModulo() {
		return MODULO;
	}

	@Override
	public String getRol() {
		return ROL;
	}

	@Override
	public String getDetalleModulo() {
		return DETALLE_MODULO;
	}

	@Override
	public String getRolModulo() {
		return ROL_MODULO;
	}

	@Override
	public String getEmail() {
		return EMAIL;
	}

	@Override
	public String getHost() {
		return HOST;
	}

	@Override
	public String getPuerto() {
		return PUERTO;
	}

	@Override
	public String getGuardar() {
		return GUARDAR;
	}

	@Override
	public String getEditar() {
		return EDITAR;
	}

	@Override
	public String getNuevo() {
		return NUEVO;
	}

	@Override
	public String getAceptar() {
		return ACEPTAR;
	}

	@Override
	public String getSalir() {
		return SALIR;
	}

	@Override
	public String getGenerarPdf() {
		return GENERAR_PDF;
	}

	@Override
	public String getGenerarExcel() {
		return GENERAR_EXCEL;
	}

	@Override
	public String getCodigo() {
		return CODIGO;
	}

	@Override
	public String getDescripcion() {
		return DESCRIPCION;
	}

	@Override
	public String getBuscar() {
		return BUSCAR;
	}

	@Override
	public String getOpcion() {
		return OPCION;
	}

	@Override
	public String getProveedorMin() {
		return PROVEEDOR_MIN;
	}

	@Override
	public String getCategoriaMin() {
		return CATEGORIA_MIN;
	}

	@Override
	public String getImpuesto() {
		return IMPUESTO;
	}

	@Override
	public String getPrecioVenta() {
		return PRECIO_VENTA;
	}

	@Override
	public String getPrecioCompra() {
		return PRECIO_COMPRA;
	}

	@Override
	public String getStock() {
		return STOCK;
	}

	@Override
	public String getStockReal() {
		return "Stock Real";
	}

	@Override
	public String getStockDiferencia() {
		return "Stock Diferencia";
	}

	@Override
	public String getStockAlerta() {
		return STOCK_ALERTA;
	}

	@Override
	public String getStockCritico() {
		return STOCK_CRITICO;
	}

	@Override
	public String getNoExisteInformacion() {
		return NO_EXISTE_INFORMACION;
	}

	@Override
	public String getEmpresa() {
		return EMPRESA;
	}

	@Override
	public String getRuc() {
		return RUC;
	}

	@Override
	public String getNombre() {
		return NOMBRE;
	}

	@Override
	public String getDireccion() {
		return DIRECCION;
	}

	@Override
	public String getTelefono() {
		return TELEFONO;
	}

	@Override
	public String getCelular() {
		return CELULAR;
	}

	@Override
	public String getCorreoPersonal() {
		return CORREO_PERSONAL;
	}

	@Override
	public String getCorreoEmpresarial() {
		return CORREO_EMPRESARIAL;
	}

	@Override
	public String getObservacion() {
		return OBSERVACION;
	}

	@Override
	public String getAnular() {
		return ANULAR;
	}

	@Override
	public String getNotaCompraMin() {
		return NOTA_COMPRA_MIN;
	}

	@Override
	public String getHora() {
		return HORA;
	}

	@Override
	public String getFecha() {
		return FECHA;
	}

	@Override
	public String getFechaRecepcion() {
		return FECHA_RECEPCION;
	}

	@Override
	public String getCantidad() {
		return CANTIDAD;
	}

	@Override
	public String getPrecio() {
		return PRECIO;
	}

	@Override
	public String getSubTotalMin() {
		return SUB_TOTAL_MIN;
	}

	@Override
	public String getAgregarItem() {
		return AGREGAR_ITEM;
	}

	@Override
	public String getSubTotal() {
		return SUB_TOTAL;
	}

	@Override
	public String getIva() {
		return IVA;
	}

	@Override
	public String getTotal() {
		return TOTAL;
	}

	@Override
	public String getFechaEmision() {
		return FECHA_EMISION;
	}

	@Override
	public String getCedula() {
		return CEDULA;
	}

	@Override
	public String getCorreo() {
		return CORREO;
	}

	@Override
	public String getProforma() {
		return PROFORMA;
	}

	@Override
	public String getClienteMin() {
		return CLIENTE_MIN;
	}

	@Override
	public String getDescuentoPorcentaje() {
		return DESCUENTO_PORCENTAJE;
	}

	@Override
	public String getFuncion() {
		return FUNCION;
	}

	@Override
	public String getCargo() {
		return CARGO;
	}

	@Override
	public String getRolMin() {
		return ROL_MIN;
	}

	@Override
	public String getUsuarioMin() {
		return USUARIO_MIN;
	}

	@Override
	public String getClave() {
		return CLAVE;
	}

	@Override
	public String getActivo() {
		return ACTIVO;
	}

	@Override
	public String getFechaCreacion() {
		return FECHA_CREACION;
	}

	@Override
	public String getEstado() {
		return ESTADO;
	}

	@Override
	public String getAbreviacion() {
		return ABREVIACION;
	}

	@Override
	public String getModuloMin() {
		return MODULO_MIN;
	}

	@Override
	public String getRuta() {
		return RUTA;
	}

	@Override
	public String getIvaMin() {
		return IVA_MIN;
	}

	@Override
	public String getTotalMin() {
		return TOTAL_MIN;
	}

	@Override
	public String getListaCompra() {
		return LISTA_COMPRA;
	}

	@Override
	public String getListaNotaCompra() {
		return LISTA_NOTA_COMPRA;
	}

	@Override
	public String getListaVenta() {
		return LISTA_VENTA;
	}

	@Override
	public String getMensageCodigoRepetido() {
		return MENSAJE_CODIGO_REPETIDO;
	}

	@Override
	public String getMensageRucRepetido() {
		return MENSAJE_RUC_REPETIDO;
	}

	@Override
	public String getMensageCedulaRepetido() {
		return MENSAJE_CEDULA_REPETIDO;
	}

	@Override
	public String getReporteVenta() {
		return REPORTE_VENTA;
	}

	@Override
	public String getArea() {
		return "AREA";
	}

	@Override
	public String getAreaMin() {
		return "Area";
	}

	@Override
	public String getSucursal() {
		return "SUCURSAL";
	}

	@Override
	public String getSucursalMin() {
		return "sucursal";
	}

	@Override
	public String getSalida() {
		return SALIDA;
	}

	@Override
	public String getEntrada() {
		return ENTRADA;
	}

	@Override
	public String getConceptoMovimiento() {
		return CONCEPTO_MOVIMIENTO;
	}

	@Override
	public String getCuadreInventario() {
		return CUADRE_INVENTARIO;
	}

	@Override
	public String getCompraMin() {
		return COMPRA_MIN;
	}

	@Override
	public String getVentaMin() {
		return VENTA_MIN;
	}

	@Override
	public String getDescuentoValor() {
		return DESCUENTO_VALOR;
	}

	@Override
	public String getPorcentaje() {
		return PORCENTAJE;
	}

	@Override
	public String getIngresoSatisfactorio() {
		return INGRESO_SATISFACTORIO;
	}

	@Override
	public String getAlimentarInventario() {
		return ALIMENTAR_INVENTARIO;
	}

	@Override
	public String getCaja() {
		return CAJA;
	}

	/* METODOS DE MENSAJES */
	@Override
	public String getCajaAbierta() {
		return CAJA_ABIERTA;
	}

	@Override
	public String getNoExistenElementos() {
		return NO_EXISTEN_ELEMENTOS;
	}

	@Override
	public String getIngresarCodigo() {
		return INGRESAR_CODIGO;
	}

	@Override
	public String getIngresarDescripcion() {
		return INGRESAR_DESCRIPCION;
	}

	@Override
	public String getCodigoNoEncontrado() {
		return CODIGO_NO_ENCONTRADO;
	}

	@Override
	public String getEscojerProducto() {
		return ESCOJER_PRODUCTO;
	}

	@Override
	public String getIngresarCantidad() {
		return INGRESAR_CANTIDAD;
	}

	@Override
	public String getRegistroEliminadoSatifactoriamente() {
		return REGISTRO_ELIMINADO_SATISFACTORIAMENTE;
	}

	@Override
	public String getImposibleEliminarRegistro() {
		return IMPOSIBLE_ELIMINAR_REGISTRO;
	}
	
	@Override
	public String getProcesoRealizadoExitosamente() {
		return PROCESO_REALIZADO_EXITOSAMENTE;
	}

	/* METODOS DE MENSAJES PARAMETRIZADOS */

	@Override
	public String getMensajeEliminar(String registro) {
		final String msj = "Esta seguro(a) que desea eliminar el registro \"" + registro
				+ "\" esta accion no podra deshacerse si se confirma";
		return msj;
	}

	@Override
	public String getMensajeAlimentarInventario(Producto producto) {
		return "Alimentar Inventario de ".concat(producto.getDescripcion().concat(" - ".concat(producto.getCodigo())));
	}

	@Override
	public String getMensajeImposibleRealizarAccion(String descripcion) {
		return "Imposible realizar accion con ".concat(descripcion);
	}

	@Override
	public String getMensajeProductoIngresado(String producto) {
		return "El producto ".concat(producto).concat(" ya ha sido ingresado");
	}

}
