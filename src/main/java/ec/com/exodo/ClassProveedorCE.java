package ec.com.exodo;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.ProveedorDAO;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.entity.Proveedor;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassProveedorCE {
	@Wire("#winFormProveedorCE")
	private Window winFormProveedor;

	private String rucProveedor;

	private Proveedor proveedor;
	private ProveedorDAO proveedorDAO = InstanciaDAO.getInstanciaProveedorDAO();
	private boolean bandera = false;
	private boolean isNew = true;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") Proveedor proveedor) {
		Selectors.wireComponents(view, this, false);
		this.proveedor = (proveedor == null) ? new Proveedor() : proveedor;
		this.isNew = (proveedor == null);
	}

	@Command
	@NotifyChange("proveedor")
	public void guardarEditar() {
		String rucProvicional = proveedor.getRuc();
		proveedor.setRuc(rucProveedor);
		if (Sistema.validarRepetido(proveedorDAO.getProveedores(Sistema.ESTADO_INGRESADO), proveedor)) {
			proveedor.setRuc(rucProvicional);
			return;
		} else {
			if (isNew) {
				proveedor.setEstado(new Estado(Sistema.ESTADO_INGRESADO));
				proveedorDAO.saveorUpdate(proveedor);
				Clients.showNotification("Proveedor ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
						"middle_center", 1500);
				proveedor = new Proveedor();
			} else {
				proveedorDAO.saveorUpdate(proveedor);
				Clients.showNotification("Proveedor Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
						"middle_center", 1500);
			}
			bandera = true;
		}
	}

	@Command
	public void validar(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("empresa")) {
			proveedor.setEmpresa(cadena.toUpperCase());
		}
		if (campo.equals("nombre")) {
			proveedor.setNombre(cadena.toUpperCase());
		}
		if (campo.equals("direccion")) {
			proveedor.setDireccion(cadena.toUpperCase());
		}
		if (campo.equals("observacion")) {
			proveedor.setObservacion(cadena.toUpperCase());
		}
		if (campo.equals("telefono")) {
			if (!Sistema.validarNumero(cadena)) {
				proveedor.setTelefono("");
				Clients.showNotification("Celular Invalido", Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center",
						1500);
			}
		}
		if (campo.equals("celular")) {
			if (!Sistema.validarNumero(cadena)) {
				proveedor.setCelular("");
				Clients.showNotification("Celular Invalido", Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center",
						1500);
			}
		}
		if (campo.equals("correo")) {
			if (!Sistema.validarCorreo(cadena)) {
				proveedor.setCorreoPersonal("");
				Clients.showNotification("Correo Invalido", Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center",
						1500);
			}
		}
		if (campo.equals("correoEmpresarial")) {
			if (!Sistema.validarCorreo(cadena)) {
				proveedor.setCorreoEmpresarial("");
				Clients.showNotification("Correo Invalido", Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center",
						1500);
			}
		}
		if (campo.equals("telefono")) {
			if (!Sistema.validarNumero(cadena)) {
				proveedor.setTelefono("");
				Clients.showNotification("Telefono Invalido", Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center",
						1500);
			}
		}
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winFormProveedor.detach();
	}

	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public String getRucProveedor() {
		return rucProveedor;
	}

	public void setRucProveedor(String rucProveedor) {
		this.rucProveedor = rucProveedor;
	}

}
