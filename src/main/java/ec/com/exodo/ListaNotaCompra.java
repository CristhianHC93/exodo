package ec.com.exodo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.NotaCompraCabeceraDAO;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.entity.NotaCompraCabecera;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ListaNotaCompra {
	@Wire("#winListaNotaCompra")
	private Window winListaNotaCompra;

	private String selectCombo = getComboBusqueda().get(0);
	private Date fechaBusqueda;
	private String datoBusqueda;
	private boolean buscarVisible = true;

	private NotaCompraCabecera notaCompraCabecera;
	private List<NotaCompraCabecera> listaNotaCompraCabecera;
	private List<NotaCompraCabecera> listaNotaCompraCabeceraProvicional;
	NotaCompraCabeceraDAO notaCompraCabeceraDAO = InstanciaDAO.getInstanciaNotaCompraCabeceraDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		listaNotaCompraCabeceraProvicional = notaCompraCabeceraDAO.getListaNotaCompraCabeceraByEstado(Sistema.ESTADO_INGRESADO);
		listaNotaCompraCabecera = new ArrayList<>(listaNotaCompraCabeceraProvicional.size());
		listaNotaCompraCabecera.addAll(listaNotaCompraCabeceraProvicional);
	}

	@Command
	@NotifyChange("buscarVisible")
	public void cambioFecha() {
		if (selectCombo.trim().equals("Fecha")) {
			buscarVisible = false;
		} else {
			buscarVisible = true;
		}
	}

	@Command
	@NotifyChange("listaNotaCompraCabecera")
	public void filtrar() {
		listaNotaCompraCabecera.clear();
		if (selectCombo.trim().equals("Codigo/Proveedor")) {
			if (datoBusqueda.trim().equals("")) {
				// notaCompraCabeceraDAO
				listaNotaCompraCabecera.addAll(listaNotaCompraCabeceraProvicional);
			} else {
				for (NotaCompraCabecera item : listaNotaCompraCabeceraProvicional) {
					if (item.getCodigo().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0
							|| item.getProveedor().getNombre().trim().toLowerCase()
									.indexOf(datoBusqueda.trim().toLowerCase()) == 0) {
						listaNotaCompraCabecera.add(item);
					}
				}
			}
		} else if (selectCombo.trim().equals("Fecha")) {
			for (NotaCompraCabecera item : listaNotaCompraCabeceraProvicional) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String dateIni = sdf.format(fechaBusqueda);
				String dateFin = sdf.format(item.getFechaEmision());
				if (dateIni.toString().equals(dateFin.toString())) {
					listaNotaCompraCabecera.add(item);
				}
			}
		}

	}

	@Command
	public void salir() {
		winListaNotaCompra.detach();
	}

	@Command
	@NotifyChange({ "notaCompraCabecera", "listaNotaCompraCabecera" })
	public void aceptar(@BindingParam("item") final NotaCompraCabecera notaCompraCabecera) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objetoNotaCompra", notaCompraCabecera);
		BindUtils.postGlobalCommand(null, null, "cargarNotaCompra", parametros);
		salir();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command
	@NotifyChange("listaNotaCompraCabecera")
	public void anular(@BindingParam("item") final NotaCompraCabecera notaCompraCabecera) {
		String msj = "Esta seguro(a) que desea anular el registro \"" + notaCompraCabecera.getCodigo()
				+ "\" esta accion no podra deshacerse si se confirma";

		Messagebox.show(msj, "Advertencia", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				new EventListener() {
					@Override
					public void onEvent(Event event) throws Exception {
						if (((Integer) event.getData()).intValue() == Messagebox.OK) {
							try {
								notaCompraCabecera.setEstado(new Estado(Sistema.ESTADO_ANULADO));
								notaCompraCabeceraDAO.saveorUpdate(notaCompraCabecera);
								listaNotaCompraCabecera.clear();
								listaNotaCompraCabecera = notaCompraCabeceraDAO
										.getListaNotaCompraCabeceraByEstado(Sistema.ESTADO_INGRESADO);
							} catch (Exception e) {
								e.printStackTrace();
							}
							Clients.showNotification("Registro anulado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ListaNotaCompra.this, "listaNotaCompraCabecera");
						}
					}
				});
	}

	public String getFooter() {
		return String.format(ParametroZul.getInstancia().getFooterMessage(), listaNotaCompraCabecera.size());
	}

	/*
	 * Llenar comboBox para buscar
	 */
	public final List<String> getComboBusqueda() {
		return Sistema.getComboCompra();
	}

	/*
	 * Metodos get and set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public String getSelectCombo() {
		return selectCombo;
	}

	public void setSelectCombo(String selectCombo) {
		this.selectCombo = selectCombo;
	}

	public NotaCompraCabecera getNotaCompraCabecera() {
		return notaCompraCabecera;
	}

	public void setNotaCompraCabecera(NotaCompraCabecera notaCompraCabecera) {
		this.notaCompraCabecera = notaCompraCabecera;
	}

	public List<NotaCompraCabecera> getListaNotaCompraCabecera() {
		return listaNotaCompraCabecera;
	}

	public void setListaNotaCompraCabecera(List<NotaCompraCabecera> listaNotaCompraCabecera) {
		this.listaNotaCompraCabecera = listaNotaCompraCabecera;
	}

	public Date getFechaBusqueda() {
		return fechaBusqueda;
	}

	public void setFechaBusqueda(Date fechaBusqueda) {
		this.fechaBusqueda = fechaBusqueda;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public boolean isBuscarVisible() {
		return buscarVisible;
	}

	public void setBuscarVisible(boolean buscarVisible) {
		this.buscarVisible = buscarVisible;
	}

}
