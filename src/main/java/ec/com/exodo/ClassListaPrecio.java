package ec.com.exodo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.PrecioDAO;
import ec.com.exodo.entity.Precio;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassListaPrecio {
	@Wire("#winFormListaPrecio")
	private Window winFormListaPrecio;

	private PrecioDAO precioDAO = InstanciaDAO.getInstanciaPrecioDAO();
	private List<Precio> listaPrecio;

	private Producto producto;
	private boolean banderaValidarAceptar = true;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("lista") List<Precio> listaPrecio, @ExecutionArgParam("producto") Producto producto) {
		Selectors.wireComponents(view, this, false);
		this.listaPrecio = listaPrecio;
		this.producto = producto;
	}

	@Command
	public void aceptar() {
		if (validarAceptar()) {
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("listaPrecio", listaPrecio);
			BindUtils.postGlobalCommand(null, null, "cargarListaPrecio", parametros);
			salir();
		}

	}

	private boolean validarAceptar() {
		banderaValidarAceptar = true;
		listaPrecio.forEach(x -> {
			if (x.getValor() < producto.getPrecioCompra()) {
				banderaValidarAceptar = false;
			}
		});

		if (!banderaValidarAceptar) {
			Messagebox.show("No puede ingresar un PVP menor al precio de compra");
		}

		return banderaValidarAceptar;
	}

	@Command
	@NotifyChange("listaPrecio")
	public void nuevo() {
		listaPrecio.add(new Precio("PVP ".concat(String.valueOf(listaPrecio.size() + 1)), 0, 0));
	}

	@Command
	@NotifyChange("listaPrecio")
	public void eliminar(@BindingParam("item") Precio precio) {
		if (precio.getProducto() == null) {
			listaPrecio.remove(precio);
		} else {
			String msj = "Esta seguro(a) que desea eliminar el registro \"" + precio.getDescripcion()
					+ "\" esta accion no podra deshacerse si se confirma";
			Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
					(EventListener<Event>) event -> {
						if (((Integer) event.getData()).intValue() == Messagebox.OK) {
							int a = precioDAO.eliminar(precio.getId());
							if (a == 1) {
								listaPrecio.remove(precio);
								Clients.showNotification("Registro eliminado satisfactoriamente",
										Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassListaPrecio.this, "listaPrecio");
							} else {
								Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
										null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassListaPrecio.this, "listaPrecio");
							}
						}
					});
		}

	}

	@Command
	@NotifyChange("listaPrecio")
	public void calculoPorcentaje(@BindingParam("item") Precio precio) {
		double precioCompra = Sistema.redondear(producto.getPrecioCompra()
				+ (producto.getPrecioCompra() * ((double) producto.getImpuesto().getPorcentaje() / 100)),3);
		double valor = precioCompra + (precioCompra * (precio.getPorcentaje() / 100));
		precio.setValor(Sistema.redondear(valor, 3));
	}

	@Command
	@NotifyChange("listaPrecio")
	public void calculoValor(@BindingParam("item") Precio precio) {
		double precioCompra = Sistema.redondear(producto.getPrecioCompra()
				+ (producto.getPrecioCompra() * ((double) producto.getImpuesto().getPorcentaje() / 100)),3);
		double valor = precio.getValor() - precioCompra;
		precio.setPorcentaje(Sistema.redondear(((valor) * 100) / precioCompra, 3));
	}

	@Command
	public void salir() {
		// BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		winFormListaPrecio.detach();
	}

	/**/

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public List<Precio> getListaPrecio() {
		return listaPrecio;
	}

	public void setListaPrecio(List<Precio> listaPrecio) {
		this.listaPrecio = listaPrecio;
	}

}
