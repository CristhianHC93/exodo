package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the venta_detalle database table.
 * 
 */
@Entity
@Table(name="venta_detalle")
@NamedQuery(name="VentaDetalle.findAll", query="SELECT v FROM VentaDetalle v")
public class VentaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private double cantidad;

	@Column(name="descuento_porcentaje")
	private Integer descuentoPorcentaje;

	@Column(name="descuento_valor")
	private double descuentoValor;

	@Column(name="precio_subtotal")
	private double precioSubtotal;

	@Column(name="precio_venta")
	private double precioVenta;

	//bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name="id_estado")
	private Estado estado;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name="id_producto")
	private Producto producto;

	//bi-directional many-to-one association to VentaCabecera
	@ManyToOne
	@JoinColumn(name="id_venta_cabecera")
	private VentaCabecera ventaCabecera;

	public VentaDetalle() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getDescuentoPorcentaje() {
		return this.descuentoPorcentaje;
	}

	public void setDescuentoPorcentaje(Integer descuentoPorcentaje) {
		this.descuentoPorcentaje = descuentoPorcentaje;
	}

	public double getDescuentoValor() {
		return this.descuentoValor;
	}

	public void setDescuentoValor(double descuentoValor) {
		this.descuentoValor = descuentoValor;
	}

	public double getPrecioSubtotal() {
		return this.precioSubtotal;
	}

	public void setPrecioSubtotal(double precioSubtotal) {
		this.precioSubtotal = precioSubtotal;
	}

	public double getPrecioVenta() {
		return this.precioVenta;
	}

	public void setPrecioVenta(double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public VentaCabecera getVentaCabecera() {
		return this.ventaCabecera;
	}

	public void setVentaCabecera(VentaCabecera ventaCabecera) {
		this.ventaCabecera = ventaCabecera;
	}

}