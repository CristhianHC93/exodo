package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the "producto_proveedor" database table.
 * 
 */
@Entity
@Table(name = "\"producto_proveedor\"")
@NamedQuery(name = "ProductoProveedor.findAll", query = "SELECT p FROM ProductoProveedor p")
public class ProductoProveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "\"precio_costo\"")
	private double precioCosto;

	@Column(name = "descuento_porcentaje")
	private double descuentoPorcentaje;

	@Column(name = "descuento_valor")
	private double descuentoValor;

	// bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name = "id_producto")
	private Producto producto;

	// bi-directional many-to-one association to Proveedor
	@ManyToOne
	@JoinColumn(name = "id_proveedor")
	private Proveedor proveedor;

	public ProductoProveedor() {
	}

	public ProductoProveedor(double precioCosto, Producto producto, Proveedor proveedor) {
		this.precioCosto = precioCosto;
		this.producto = producto;
		this.proveedor = proveedor;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrecioCosto() {
		return this.precioCosto;
	}

	public void setPrecioCosto(double precioCosto) {
		this.precioCosto = precioCosto;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public double getDescuentoPorcentaje() {
		return descuentoPorcentaje;
	}

	public void setDescuentoPorcentaje(double descuentoPorcentaje) {
		this.descuentoPorcentaje = descuentoPorcentaje;
	}

	public double getDescuentoValor() {
		return descuentoValor;
	}

	public void setDescuentoValor(double descuentoValor) {
		this.descuentoValor = descuentoValor;
	}

}