package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * The persistent class for the cuadre_inventario database table.
 * 
 */
@Entity
@Table(name = "cuadre_inventario")
@NamedQuery(name = "CuadreInventario.findAll", query = "SELECT c FROM CuadreInventario c")
public class CuadreInventario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "cantidad_antes", nullable = false)
	private double cantidadAntes;

	@Column(name = "cantidad_despues", nullable = false)
	private double cantidadDespues;

	@Column(nullable = false)
	private double cantidad;

	@Column(name = "valor_compra")
	private double valorCompra;

	@Column(name = "valor_venta")
	private double valorVenta;

	private String observacion;

	@Column(name = "total_compra")
	private double totalCompra;

	@Column(name = "total_venta")
	private double totalVenta;

	private Timestamp fecha;

	@Column(name = "id_detalle")
	private Integer idDetalle;

	// bi-directional many-to-one association to CuadreInventarioCabecera
	@ManyToOne
	@JoinColumn(name = "id_cuadre_inventario_cabecera")
	private CuadreInventarioCabecera cuadreInventarioCabecera;

	// bi-directional many-to-one association to Producto
	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_producto", nullable = false)
	private Producto producto;

	@ManyToOne
	@JoinColumn(name = "id_concepto_movimiento", nullable = false)
	private ConceptoMovimiento conceptoMovimiento;

	public CuadreInventario() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public double getCantidadAntes() {
		return cantidadAntes;
	}

	public void setCantidadAntes(double cantidadAntes) {
		this.cantidadAntes = cantidadAntes;
	}

	public double getCantidadDespues() {
		return cantidadDespues;
	}

	public void setCantidadDespues(double cantidadDespues) {
		this.cantidadDespues = cantidadDespues;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public ConceptoMovimiento getConceptoMovimiento() {
		return conceptoMovimiento;
	}

	public void setConceptoMovimiento(ConceptoMovimiento conceptoMovimiento) {
		this.conceptoMovimiento = conceptoMovimiento;
	}

	public CuadreInventarioCabecera getCuadreInventarioCabecera() {
		return this.cuadreInventarioCabecera;
	}

	public void setCuadreInventarioCabecera(CuadreInventarioCabecera cuadreInventarioCabecera) {
		this.cuadreInventarioCabecera = cuadreInventarioCabecera;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public double getValorCompra() {
		return valorCompra;
	}

	public void setValorCompra(double valorCompra) {
		this.valorCompra = valorCompra;
	}

	public double getValorVenta() {
		return valorVenta;
	}

	public void setValorVenta(double valorVenta) {
		this.valorVenta = valorVenta;
	}

	public double getTotalCompra() {
		return totalCompra;
	}

	public void setTotalCompra(double totalCompra) {
		this.totalCompra = totalCompra;
	}

	public double getTotalVenta() {
		return totalVenta;
	}

	public void setTotalVenta(double totalVenta) {
		this.totalVenta = totalVenta;
	}

	public Integer getIdDetalle() {
		return idDetalle;
	}

	public void setIdDetalle(Integer idDetalle) {
		this.idDetalle = idDetalle;
	}
}