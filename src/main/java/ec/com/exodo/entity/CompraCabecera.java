package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;

import ec.com.exodo.sistema.Sistema;

import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the compra_cabecera database table.
 * 
 */
@Entity
@Table(name = "compra_cabecera")
@NamedQuery(name = "CompraCabecera.findAll", query = "SELECT c FROM CompraCabecera c")
public class CompraCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String codigo;

	@Column(nullable = false)
	private Timestamp fecha;

	private double descuento;
	private double iva;
	private double subtotal;
	private double total;

	// bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name = "id_estado")
	private Estado estado;

	// bi-directional many-to-one association to Proveedor
	@ManyToOne
	@JoinColumn(name = "id_proveedor")
	private Proveedor proveedor;

	// bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;

	// bi-directional many-to-one association to CompraDetalle
	@OneToMany(mappedBy = "compraCabecera")
	private List<CompraDetalle> compraDetalles;

	// bi-directional many-to-one association to DevolucionCompra
	@OneToMany(mappedBy = "compraCabecera")
	private List<DevolucionCompra> devolucionCompras;

	public CompraCabecera() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public double getIva() {
		return Sistema.redondear(this.iva,2);
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getSubtotal() {
		return Sistema.redondear(this.subtotal,2);
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getTotal() {
		return Sistema.redondear(this.total,2);
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}