package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the compra_detalle database table.
 * 
 */
@Entity
@Table(name = "compra_detalle")
@NamedQuery(name = "CompraDetalle.findAll", query = "SELECT c FROM CompraDetalle c")
public class CompraDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
	private double cantidad;

	@Column(name = "precio_compra", nullable = false)
	private double precioCompra;

	@Column(nullable = false)
	private double subtotal;

	@Column(name = "valor_iva")
	private double valorIva;

	@Column(name = "descuento_porcentaje")
	private double descuentoPorcentaje;

	@Column(name = "descuento_valor")
	private double descuentoValor;

	@Column(nullable = false)
	private double total;

	// bi-directional many-to-one association to CompraCabecera
	@ManyToOne
	@JoinColumn(name = "id_compra_cabecera", nullable = false)
	private CompraCabecera compraCabecera;

	// bi-directional many-to-one association to Producto
	@ManyToOne()
	@JoinColumn(name = "id_producto", nullable = false)
	private Producto producto;

	public CompraDetalle() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecioCompra() {
		return this.precioCompra;
	}

	public void setPrecioCompra(double precioCompra) {
		this.precioCompra = precioCompra;
	}

	public double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public CompraCabecera getCompraCabecera() {
		return this.compraCabecera;
	}

	public void setCompraCabecera(CompraCabecera compraCabecera) {
		this.compraCabecera = compraCabecera;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public double getValorIva() {
		return valorIva;
	}

	public void setValorIva(double valorIva) {
		this.valorIva = valorIva;
	}

	public double getDescuentoPorcentaje() {
		return descuentoPorcentaje;
	}

	public void setDescuentoPorcentaje(double descuentoPorcentaje) {
		this.descuentoPorcentaje = descuentoPorcentaje;
	}

	public double getDescuentoValor() {
		return descuentoValor;
	}

	public void setDescuentoValor(double descuentoValor) {
		this.descuentoValor = descuentoValor;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

}