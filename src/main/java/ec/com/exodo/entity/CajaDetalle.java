package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the caja_detalle database table.
 * 
 */
@Entity
@Table(name="caja_detalle")
@NamedQuery(name="CajaDetalle.findAll", query="SELECT c FROM CajaDetalle c")
public class CajaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	//bi-directional many-to-one association to Caja
	@ManyToOne
	@JoinColumn(name="id_caja")
	private Caja caja;

	//bi-directional many-to-one association to VentaCabecera
	@ManyToOne
	@JoinColumn(name="id_venta_cabecera")
	private VentaCabecera ventaCabecera;

	public CajaDetalle() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Caja getCaja() {
		return this.caja;
	}

	public void setCaja(Caja caja) {
		this.caja = caja;
	}

	public VentaCabecera getVentaCabecera() {
		return this.ventaCabecera;
	}

	public void setVentaCabecera(VentaCabecera ventaCabecera) {
		this.ventaCabecera = ventaCabecera;
	}

}