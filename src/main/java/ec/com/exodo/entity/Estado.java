package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the estado database table.
 * 
 */
@Entity
@NamedQuery(name = "Estado.findAll", query = "SELECT e FROM Estado e")
public class Estado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Integer codigo;

	private String descripcion;

	private String tabla;

	/*// bi-directional many-to-one association to Cliente
	@OneToMany(mappedBy = "estado")
	private List<Cliente> clientes;

	// bi-directional many-to-one association to CompraCabecera
	@OneToMany(mappedBy = "estado")
	private List<CompraCabecera> compraCabeceras;

	// bi-directional many-to-one association to NotaCompraCabecera
	@OneToMany(mappedBy = "estado")
	private List<NotaCompraCabecera> notaCompraCabeceras;

	// bi-directional many-to-one association to NotaCompraDetalle
	@OneToMany(mappedBy = "estado")
	private List<NotaCompraDetalle> notaCompraDetalles;

	// bi-directional many-to-one association to Producto
	@OneToMany(mappedBy = "estado")
	private List<Producto> productos;

	// bi-directional many-to-one association to Proveedor
	@OneToMany(mappedBy = "estado")
	private List<Proveedor> proveedors;

	// bi-directional many-to-one association to Rol
	@OneToMany(mappedBy = "estado")
	private List<Rol> rols;

	// bi-directional many-to-one association to VentaCabecera
	@OneToMany(mappedBy = "estado")
	private List<VentaCabecera> ventaCabeceras;

	// bi-directional many-to-one association to VentaDetalle
	@OneToMany(mappedBy = "estado")
	private List<VentaDetalle> ventaDetalles;*/

	public Estado() {
	}

	public Estado(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTabla() {
		return this.tabla;
	}

	public void setTabla(String tabla) {
		this.tabla = tabla;
	}
/*
	public List<Cliente> getClientes() {
		return this.clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public Cliente addCliente(Cliente cliente) {
		getClientes().add(cliente);
		cliente.setEstado(this);

		return cliente;
	}

	public Cliente removeCliente(Cliente cliente) {
		getClientes().remove(cliente);
		cliente.setEstado(null);

		return cliente;
	}

	public List<CompraCabecera> getCompraCabeceras() {
		return this.compraCabeceras;
	}

	public void setCompraCabeceras(List<CompraCabecera> compraCabeceras) {
		this.compraCabeceras = compraCabeceras;
	}

	public CompraCabecera addCompraCabecera(CompraCabecera compraCabecera) {
		getCompraCabeceras().add(compraCabecera);
		compraCabecera.setEstado(this);

		return compraCabecera;
	}

	public CompraCabecera removeCompraCabecera(CompraCabecera compraCabecera) {
		getCompraCabeceras().remove(compraCabecera);
		compraCabecera.setEstado(null);

		return compraCabecera;
	}

	public List<NotaCompraCabecera> getNotaCompraCabeceras() {
		return this.notaCompraCabeceras;
	}

	public void setNotaCompraCabeceras(List<NotaCompraCabecera> notaCompraCabeceras) {
		this.notaCompraCabeceras = notaCompraCabeceras;
	}

	public NotaCompraCabecera addNotaCompraCabecera(NotaCompraCabecera notaCompraCabecera) {
		getNotaCompraCabeceras().add(notaCompraCabecera);
		notaCompraCabecera.setEstado(this);

		return notaCompraCabecera;
	}

	public NotaCompraCabecera removeNotaCompraCabecera(NotaCompraCabecera notaCompraCabecera) {
		getNotaCompraCabeceras().remove(notaCompraCabecera);
		notaCompraCabecera.setEstado(null);

		return notaCompraCabecera;
	}

	public List<NotaCompraDetalle> getNotaCompraDetalles() {
		return this.notaCompraDetalles;
	}

	public void setNotaCompraDetalles(List<NotaCompraDetalle> notaCompraDetalles) {
		this.notaCompraDetalles = notaCompraDetalles;
	}

	public NotaCompraDetalle addNotaCompraDetalle(NotaCompraDetalle notaCompraDetalle) {
		getNotaCompraDetalles().add(notaCompraDetalle);
		notaCompraDetalle.setEstado(this);

		return notaCompraDetalle;
	}

	public NotaCompraDetalle removeNotaCompraDetalle(NotaCompraDetalle notaCompraDetalle) {
		getNotaCompraDetalles().remove(notaCompraDetalle);
		notaCompraDetalle.setEstado(null);

		return notaCompraDetalle;
	}

	public List<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public Producto addProducto(Producto producto) {
		getProductos().add(producto);
		producto.setEstado(this);

		return producto;
	}

	public Producto removeProducto(Producto producto) {
		getProductos().remove(producto);
		producto.setEstado(null);

		return producto;
	}

	public List<Proveedor> getProveedors() {
		return this.proveedors;
	}

	public void setProveedors(List<Proveedor> proveedors) {
		this.proveedors = proveedors;
	}

	public Proveedor addProveedor(Proveedor proveedor) {
		getProveedors().add(proveedor);
		proveedor.setEstado(this);

		return proveedor;
	}

	public Proveedor removeProveedor(Proveedor proveedor) {
		getProveedors().remove(proveedor);
		proveedor.setEstado(null);

		return proveedor;
	}

	public List<Rol> getRols() {
		return this.rols;
	}

	public void setRols(List<Rol> rols) {
		this.rols = rols;
	}

	public Rol addRol(Rol rol) {
		getRols().add(rol);
		rol.setEstado(this);

		return rol;
	}

	public Rol removeRol(Rol rol) {
		getRols().remove(rol);
		rol.setEstado(null);

		return rol;
	}

	public List<VentaCabecera> getVentaCabeceras() {
		return this.ventaCabeceras;
	}

	public void setVentaCabeceras(List<VentaCabecera> ventaCabeceras) {
		this.ventaCabeceras = ventaCabeceras;
	}

	public VentaCabecera addVentaCabecera(VentaCabecera ventaCabecera) {
		getVentaCabeceras().add(ventaCabecera);
		ventaCabecera.setEstado(this);

		return ventaCabecera;
	}

	public VentaCabecera removeVentaCabecera(VentaCabecera ventaCabecera) {
		getVentaCabeceras().remove(ventaCabecera);
		ventaCabecera.setEstado(null);

		return ventaCabecera;
	}

	public List<VentaDetalle> getVentaDetalles() {
		return this.ventaDetalles;
	}

	public void setVentaDetalles(List<VentaDetalle> ventaDetalles) {
		this.ventaDetalles = ventaDetalles;
	}

	public VentaDetalle addVentaDetalle(VentaDetalle ventaDetalle) {
		getVentaDetalles().add(ventaDetalle);
		ventaDetalle.setEstado(this);

		return ventaDetalle;
	}

	public VentaDetalle removeVentaDetalle(VentaDetalle ventaDetalle) {
		getVentaDetalles().remove(ventaDetalle);
		ventaDetalle.setEstado(null);

		return ventaDetalle;
	}*/

}