package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the cuadre_inventario_cabecera database table.
 * 
 */
@Entity
@Table(name="cuadre_inventario_cabecera")
@NamedQuery(name="CuadreInventarioCabecera.findAll", query="SELECT c FROM CuadreInventarioCabecera c")
public class CuadreInventarioCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Boolean estado;

	@Column(name="fecha_apertura")
	private Timestamp fechaApertura;

	@Column(name="fecha_cierre")
	private Timestamp fechaCierre;

	//bi-directional many-to-one association to CuadreInventario
	@OneToMany(mappedBy="cuadreInventarioCabecera")
	private List<CuadreInventario> cuadreInventarios;

	//bi-directional many-to-one association to AreaSucursal
	@ManyToOne
	@JoinColumn(name="id_area_sucursal")
	private AreaSucursal areaSucursal;

	public CuadreInventarioCabecera() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Timestamp getFechaApertura() {
		return this.fechaApertura;
	}

	public void setFechaApertura(Timestamp fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public Timestamp getFechaCierre() {
		return this.fechaCierre;
	}

	public void setFechaCierre(Timestamp fechaCierre) {
		this.fechaCierre = fechaCierre;
	}

	public List<CuadreInventario> getCuadreInventarios() {
		return this.cuadreInventarios;
	}

	public void setCuadreInventarios(List<CuadreInventario> cuadreInventarios) {
		this.cuadreInventarios = cuadreInventarios;
	}

	public CuadreInventario addCuadreInventario(CuadreInventario cuadreInventario) {
		getCuadreInventarios().add(cuadreInventario);
		cuadreInventario.setCuadreInventarioCabecera(this);

		return cuadreInventario;
	}

	public CuadreInventario removeCuadreInventario(CuadreInventario cuadreInventario) {
		getCuadreInventarios().remove(cuadreInventario);
		cuadreInventario.setCuadreInventarioCabecera(null);

		return cuadreInventario;
	}

	public AreaSucursal getAreaSucursal() {
		return this.areaSucursal;
	}

	public void setAreaSucursal(AreaSucursal areaSucursal) {
		this.areaSucursal = areaSucursal;
	}

}