package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the area_sucursal database table.
 * 
 */
@Entity
@Table(name = "area_sucursal")
@NamedQuery(name = "AreaSucursal.findAll", query = "SELECT a FROM AreaSucursal a")
public class AreaSucursal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Boolean estado;

	// bi-directional many-to-one association to Area
	@ManyToOne
	@JoinColumn(name = "id_area")
	private Area area;

	// bi-directional many-to-one association to Sucursal
	@ManyToOne
	@JoinColumn(name = "id_sucursal")
	private Sucursal sucursal;

	// bi-directional many-to-one association to Caja
	@OneToMany(mappedBy = "areaSucursal")
	private List<Caja> cajas;

	// bi-directional many-to-one association to CuadreInventarioCabecera
	@OneToMany(mappedBy = "areaSucursal")
	private List<CuadreInventarioCabecera> cuadreInventarioCabeceras;

	// bi-directional many-to-one association to Producto
	@OneToMany(mappedBy = "areaSucursal")
	private List<Producto> productos;

	// bi-directional many-to-one association to Usuario
	@OneToMany(mappedBy = "areaSucursal")
	private List<Usuario> usuarios;

	// bi-directional many-to-one association to VentaCabecera
	@OneToMany(mappedBy = "areaSucursal")
	private List<VentaCabecera> ventaCabeceras;

	public AreaSucursal() {
	}

	public AreaSucursal(int id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Area getArea() {
		return this.area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Sucursal getSucursal() {
		return this.sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public List<Caja> getCajas() {
		return this.cajas;
	}

	public void setCajas(List<Caja> cajas) {
		this.cajas = cajas;
	}

	public Caja addCaja(Caja caja) {
		getCajas().add(caja);
		caja.setAreaSucursal(this);

		return caja;
	}

	public Caja removeCaja(Caja caja) {
		getCajas().remove(caja);
		caja.setAreaSucursal(null);

		return caja;
	}

	public List<CuadreInventarioCabecera> getCuadreInventarioCabeceras() {
		return this.cuadreInventarioCabeceras;
	}

	public void setCuadreInventarioCabeceras(List<CuadreInventarioCabecera> cuadreInventarioCabeceras) {
		this.cuadreInventarioCabeceras = cuadreInventarioCabeceras;
	}

	public CuadreInventarioCabecera addCuadreInventarioCabecera(CuadreInventarioCabecera cuadreInventarioCabecera) {
		getCuadreInventarioCabeceras().add(cuadreInventarioCabecera);
		cuadreInventarioCabecera.setAreaSucursal(this);

		return cuadreInventarioCabecera;
	}

	public CuadreInventarioCabecera removeCuadreInventarioCabecera(CuadreInventarioCabecera cuadreInventarioCabecera) {
		getCuadreInventarioCabeceras().remove(cuadreInventarioCabecera);
		cuadreInventarioCabecera.setAreaSucursal(null);

		return cuadreInventarioCabecera;
	}

	public List<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public Producto addProducto(Producto producto) {
		getProductos().add(producto);
		producto.setAreaSucursal(this);

		return producto;
	}

	public Producto removeProducto(Producto producto) {
		getProductos().remove(producto);
		producto.setAreaSucursal(null);

		return producto;
	}

	public List<Usuario> getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Usuario addUsuario(Usuario usuario) {
		getUsuarios().add(usuario);
		usuario.setAreaSucursal(this);

		return usuario;
	}

	public Usuario removeUsuario(Usuario usuario) {
		getUsuarios().remove(usuario);
		usuario.setAreaSucursal(null);

		return usuario;
	}

	public List<VentaCabecera> getVentaCabeceras() {
		return this.ventaCabeceras;
	}

	public void setVentaCabeceras(List<VentaCabecera> ventaCabeceras) {
		this.ventaCabeceras = ventaCabeceras;
	}

	public VentaCabecera addVentaCabecera(VentaCabecera ventaCabecera) {
		getVentaCabeceras().add(ventaCabecera);
		ventaCabecera.setAreaSucursal(this);

		return ventaCabecera;
	}

	public VentaCabecera removeVentaCabecera(VentaCabecera ventaCabecera) {
		getVentaCabeceras().remove(ventaCabecera);
		ventaCabecera.setAreaSucursal(null);

		return ventaCabecera;
	}

}