package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the venta_cabecera database table.
 * 
 */
@Entity
@Table(name="venta_cabecera")
@NamedQuery(name="VentaCabecera.findAll", query="SELECT v FROM VentaCabecera v")
public class VentaCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String codigo;

	private double compensacion;

	@Column(name="descuento_porcentaje")
	private double descuentoPorcentaje;

	@Column(name="descuento_valor")
	private double descuentoValor;

	@Column(name="fecha_emision")
	private Timestamp fechaEmision;

	private double iva;

	private double subtotal;

	private double total;

	//bi-directional many-to-one association to CajaDetalle
	@OneToMany(mappedBy="ventaCabecera")
	private List<CajaDetalle> cajaDetalles;

	//bi-directional many-to-one association to DevolucionVenta
	@OneToMany(mappedBy="ventaCabecera")
	private List<DevolucionVenta> devolucionVentas;

	//bi-directional many-to-one association to AreaSucursal
	@ManyToOne
	@JoinColumn(name="id_area_sucursal")
	private AreaSucursal areaSucursal;

	//bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;

	//bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name="id_estado")
	private Estado estado;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;

	//bi-directional many-to-one association to VentaDetalle
	@OneToMany(mappedBy="ventaCabecera")
	private List<VentaDetalle> ventaDetalles;

	public VentaCabecera() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public double getCompensacion() {
		return this.compensacion;
	}

	public void setCompensacion(double compensacion) {
		this.compensacion = compensacion;
	}

	public double getDescuentoPorcentaje() {
		return this.descuentoPorcentaje;
	}

	public void setDescuentoPorcentaje(double descuentoPorcentaje) {
		this.descuentoPorcentaje = descuentoPorcentaje;
	}

	public double getDescuentoValor() {
		return this.descuentoValor;
	}

	public void setDescuentoValor(double descuentoValor) {
		this.descuentoValor = descuentoValor;
	}

	public Timestamp getFechaEmision() {
		return this.fechaEmision;
	}

	public void setFechaEmision(Timestamp fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public double getIva() {
		return this.iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getTotal() {
		return this.total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public List<CajaDetalle> getCajaDetalles() {
		return this.cajaDetalles;
	}

	public void setCajaDetalles(List<CajaDetalle> cajaDetalles) {
		this.cajaDetalles = cajaDetalles;
	}

	public CajaDetalle addCajaDetalle(CajaDetalle cajaDetalle) {
		getCajaDetalles().add(cajaDetalle);
		cajaDetalle.setVentaCabecera(this);

		return cajaDetalle;
	}

	public CajaDetalle removeCajaDetalle(CajaDetalle cajaDetalle) {
		getCajaDetalles().remove(cajaDetalle);
		cajaDetalle.setVentaCabecera(null);

		return cajaDetalle;
	}

	public List<DevolucionVenta> getDevolucionVentas() {
		return this.devolucionVentas;
	}

	public void setDevolucionVentas(List<DevolucionVenta> devolucionVentas) {
		this.devolucionVentas = devolucionVentas;
	}

	public DevolucionVenta addDevolucionVenta(DevolucionVenta devolucionVenta) {
		getDevolucionVentas().add(devolucionVenta);
		devolucionVenta.setVentaCabecera(this);

		return devolucionVenta;
	}

	public DevolucionVenta removeDevolucionVenta(DevolucionVenta devolucionVenta) {
		getDevolucionVentas().remove(devolucionVenta);
		devolucionVenta.setVentaCabecera(null);

		return devolucionVenta;
	}

	public AreaSucursal getAreaSucursal() {
		return this.areaSucursal;
	}

	public void setAreaSucursal(AreaSucursal areaSucursal) {
		this.areaSucursal = areaSucursal;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<VentaDetalle> getVentaDetalles() {
		return this.ventaDetalles;
	}

	public void setVentaDetalles(List<VentaDetalle> ventaDetalles) {
		this.ventaDetalles = ventaDetalles;
	}

	public VentaDetalle addVentaDetalle(VentaDetalle ventaDetalle) {
		getVentaDetalles().add(ventaDetalle);
		ventaDetalle.setVentaCabecera(this);

		return ventaDetalle;
	}

	public VentaDetalle removeVentaDetalle(VentaDetalle ventaDetalle) {
		getVentaDetalles().remove(ventaDetalle);
		ventaDetalle.setVentaCabecera(null);

		return ventaDetalle;
	}

}