package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the caja database table.
 * 
 */
@Entity
@NamedQuery(name = "Caja.findAll", query = "SELECT c FROM Caja c")
public class Caja implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "caja_chica")
	private double cajaChica;

	@Column(name = "cierre_caja")
	private double cierreCaja;

	private double diferencia;

	private Boolean estado;

	@Column(name = "fecha_apertura")
	private Timestamp fechaApertura;

	@Column(name = "fecha_cierre")
	private Timestamp fechaCierre;

	@Column(name = "observacion_apertura")
	private String observacionApertura;

	@Column(name = "observacion_cierre")
	private String observacionCierre;

	@Column(name = "valor_entregado")
	private double valorEntregado;

	// bi-directional many-to-one association to AreaSucursal
	@ManyToOne
	@JoinColumn(name = "id_area_sucursal")
	private AreaSucursal areaSucursal;

	// bi-directional many-to-one association to CajaDetalle
	@OneToMany(mappedBy = "caja")
	private List<CajaDetalle> cajaDetalles;

	public Caja() {
		estado = true;
	}

	public Caja(double cajaChica, AreaSucursal areaSucursal) {
		estado = true;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getCajaChica() {
		return this.cajaChica;
	}

	public void setCajaChica(double cajaChica) {
		this.cajaChica = cajaChica;
	}

	public double getCierreCaja() {
		return this.cierreCaja;
	}

	public void setCierreCaja(double cierreCaja) {
		this.cierreCaja = cierreCaja;
	}

	public double getDiferencia() {
		return this.diferencia;
	}

	public void setDiferencia(double diferencia) {
		this.diferencia = diferencia;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Timestamp getFechaApertura() {
		return this.fechaApertura;
	}

	public void setFechaApertura(Timestamp fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public Timestamp getFechaCierre() {
		return this.fechaCierre;
	}

	public void setFechaCierre(Timestamp fechaCierre) {
		this.fechaCierre = fechaCierre;
	}

	public String getObservacionApertura() {
		return this.observacionApertura;
	}

	public void setObservacionApertura(String observacionApertura) {
		this.observacionApertura = observacionApertura;
	}

	public String getObservacionCierre() {
		return this.observacionCierre;
	}

	public void setObservacionCierre(String observacionCierre) {
		this.observacionCierre = observacionCierre;
	}

	public double getValorEntregado() {
		return this.valorEntregado;
	}

	public void setValorEntregado(double valorEntregado) {
		this.valorEntregado = valorEntregado;
	}

	public AreaSucursal getAreaSucursal() {
		return this.areaSucursal;
	}

	public void setAreaSucursal(AreaSucursal areaSucursal) {
		this.areaSucursal = areaSucursal;
	}

	public List<CajaDetalle> getCajaDetalles() {
		return this.cajaDetalles;
	}

	public void setCajaDetalles(List<CajaDetalle> cajaDetalles) {
		this.cajaDetalles = cajaDetalles;
	}

	public CajaDetalle addCajaDetalle(CajaDetalle cajaDetalle) {
		getCajaDetalles().add(cajaDetalle);
		cajaDetalle.setCaja(this);

		return cajaDetalle;
	}

	public CajaDetalle removeCajaDetalle(CajaDetalle cajaDetalle) {
		getCajaDetalles().remove(cajaDetalle);
		cajaDetalle.setCaja(null);

		return cajaDetalle;
	}

}