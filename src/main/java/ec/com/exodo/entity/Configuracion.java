package ec.com.exodo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Configuracion.findAll", query = "SELECT c FROM Configuracion c")
public class Configuracion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique = true,nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Integer precios;

	public Configuracion() {
		super();
	}

	public Configuracion(Integer id, Integer precios) {
		this.id = id;
		this.precios = precios;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPrecios() {
		return precios;
	}

	public void setPrecios(Integer precios) {
		this.precios = precios;
	}

}
