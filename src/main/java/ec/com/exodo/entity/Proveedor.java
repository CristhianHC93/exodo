package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the proveedor database table.
 * 
 */
@Entity
@NamedQuery(name = "Proveedor.findAll", query = "SELECT p FROM Proveedor p")
public class Proveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String celular;

	@Column(name = "correo_empresarial")
	private String correoEmpresarial;

	@Column(name = "correo_personal")
	private String correoPersonal;

	private String direccion;

	private String empresa;

	private String nombre;

	private String observacion;

	private String ruc;

	private String telefono;

	// bi-directional many-to-one association to CompraCabecera
	@OneToMany(mappedBy = "proveedor")
	private List<CompraCabecera> compraCabeceras;

	// bi-directional many-to-one association to NotaCompraCabecera
	@OneToMany(mappedBy = "proveedor")
	private List<NotaCompraCabecera> notaCompraCabeceras;

	// bi-directional many-to-one association to Producto
	@OneToMany(mappedBy = "proveedor")
	private List<Producto> productos;

	// bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name = "id_estado")
	private Estado estado;

	// bi-directional many-to-one association to ProductoProveedor
	@OneToMany(mappedBy = "proveedor", fetch = FetchType.EAGER)
	private List<ProductoProveedor> productoProveedors;

	//

	@Transient
	private boolean checkProducto;

	@Transient
	private double costo;
	
	@Transient
	private double descuentoValor;
	
	@Transient
	private double descuentoPorcentaje;

	public Proveedor() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCorreoEmpresarial() {
		return this.correoEmpresarial;
	}

	public void setCorreoEmpresarial(String correoEmpresarial) {
		this.correoEmpresarial = correoEmpresarial;
	}

	public String getCorreoPersonal() {
		return this.correoPersonal;
	}

	public void setCorreoPersonal(String correoPersonal) {
		this.correoPersonal = correoPersonal;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getRuc() {
		return this.ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<CompraCabecera> getCompraCabeceras() {
		return this.compraCabeceras;
	}

	public void setCompraCabeceras(List<CompraCabecera> compraCabeceras) {
		this.compraCabeceras = compraCabeceras;
	}

	public CompraCabecera addCompraCabecera(CompraCabecera compraCabecera) {
		getCompraCabeceras().add(compraCabecera);
		compraCabecera.setProveedor(this);

		return compraCabecera;
	}

	public CompraCabecera removeCompraCabecera(CompraCabecera compraCabecera) {
		getCompraCabeceras().remove(compraCabecera);
		compraCabecera.setProveedor(null);

		return compraCabecera;
	}

	public List<NotaCompraCabecera> getNotaCompraCabeceras() {
		return this.notaCompraCabeceras;
	}

	public void setNotaCompraCabeceras(List<NotaCompraCabecera> notaCompraCabeceras) {
		this.notaCompraCabeceras = notaCompraCabeceras;
	}

	public NotaCompraCabecera addNotaCompraCabecera(NotaCompraCabecera notaCompraCabecera) {
		getNotaCompraCabeceras().add(notaCompraCabecera);
		notaCompraCabecera.setProveedor(this);

		return notaCompraCabecera;
	}

	public NotaCompraCabecera removeNotaCompraCabecera(NotaCompraCabecera notaCompraCabecera) {
		getNotaCompraCabeceras().remove(notaCompraCabecera);
		notaCompraCabecera.setProveedor(null);

		return notaCompraCabecera;
	}

	public List<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public Producto addProducto(Producto producto) {
		getProductos().add(producto);
		producto.setProveedor(this);

		return producto;
	}

	public Producto removeProducto(Producto producto) {
		getProductos().remove(producto);
		producto.setProveedor(null);

		return producto;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public boolean isCheckProducto() {
		return checkProducto;
	}

	public void setCheckProducto(boolean checkProducto) {
		this.checkProducto = checkProducto;
	}

	public double getCosto() {
		return costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}

	public List<ProductoProveedor> getProductoProveedors() {
		return this.productoProveedors;
	}

	public void setProductoProveedors(List<ProductoProveedor> productoProveedors) {
		this.productoProveedors = productoProveedors;
	}

	public ProductoProveedor addProductoProveedor(ProductoProveedor productoProveedor) {
		getProductoProveedors().add(productoProveedor);
		productoProveedor.setProveedor(this);

		return productoProveedor;
	}

	public ProductoProveedor removeProductoProveedor(ProductoProveedor productoProveedor) {
		getProductoProveedors().remove(productoProveedor);
		productoProveedor.setProveedor(null);

		return productoProveedor;
	}

	public double getDescuentoValor() {
		return descuentoValor;
	}

	public void setDescuentoValor(double descuentoValor) {
		this.descuentoValor = descuentoValor;
	}

	public double getDescuentoPorcentaje() {
		return descuentoPorcentaje;
	}

	public void setDescuentoPorcentaje(double descuentoPorcentaje) {
		this.descuentoPorcentaje = descuentoPorcentaje;
	}

}