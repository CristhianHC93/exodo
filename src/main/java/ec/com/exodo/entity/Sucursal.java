package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the sucursal database table.
 * 
 */
@Entity
@NamedQuery(name = "Sucursal.findAll", query = "SELECT s FROM Sucursal s")
public class Sucursal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "codigo_factura")
	private String codigoFactura;

	// bi-directional many-to-one association to AreaSucursal
	@OneToMany(mappedBy = "sucursal")
	private List<AreaSucursal> areaSucursals;

	// bi-directional many-to-one association to InformacionNegocio
	@ManyToOne
	@JoinColumn(name = "id_informacion_negocio")
	private InformacionNegocio informacionNegocio;

	// bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;

	public Sucursal() {
		informacionNegocio = new InformacionNegocio();
	}

	public Sucursal(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigoFactura() {
		return this.codigoFactura;
	}

	public void setCodigoFactura(String codigoFactura) {
		this.codigoFactura = codigoFactura;
	}

	public List<AreaSucursal> getAreaSucursals() {
		return this.areaSucursals;
	}

	public void setAreaSucursals(List<AreaSucursal> areaSucursals) {
		this.areaSucursals = areaSucursals;
	}

	public AreaSucursal addAreaSucursal(AreaSucursal areaSucursal) {
		getAreaSucursals().add(areaSucursal);
		areaSucursal.setSucursal(this);

		return areaSucursal;
	}

	public AreaSucursal removeAreaSucursal(AreaSucursal areaSucursal) {
		getAreaSucursals().remove(areaSucursal);
		areaSucursal.setSucursal(null);

		return areaSucursal;
	}

	public InformacionNegocio getInformacionNegocio() {
		return this.informacionNegocio;
	}

	public void setInformacionNegocio(InformacionNegocio informacionNegocio) {
		this.informacionNegocio = informacionNegocio;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}