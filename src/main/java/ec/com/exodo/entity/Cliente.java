package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;

import ec.com.exodo.sistema.Sistema;

import java.util.List;

/**
 * The persistent class for the cliente database table.
 * 
 */
@Entity
@NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String cedula;

	private String celular;

	private String correo;

	private String direccion;

	private String nombre;

	private String telefono;

	// bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name = "id_estado")
	private Estado estado;

	// bi-directional many-to-one association to VentaCabecera
	@OneToMany(mappedBy = "cliente")
	private List<VentaCabecera> ventaCabeceras;

	public Cliente() {
		this.estado = new Estado(Sistema.ESTADO_INGRESADO);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCedula() {
		return this.cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public List<VentaCabecera> getVentaCabeceras() {
		return this.ventaCabeceras;
	}

	public void setVentaCabeceras(List<VentaCabecera> ventaCabeceras) {
		this.ventaCabeceras = ventaCabeceras;
	}
}