package ec.com.exodo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Precio.findAll", query = "SELECT p FROM Precio p")
public class Precio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false)
	private String descripcion;

	@Column(nullable = false)
	private double valor;

	@Column(nullable = false)
	private double porcentaje;

	@ManyToOne
	@JoinColumn(name = "id_producto", nullable = true)
	private Producto producto;

	public Precio() {
		super();
	}

	public Precio(String descripcion, double valor, double porcentaje, Producto producto) {
		super();
		this.descripcion = descripcion;
		this.valor = valor;
		this.porcentaje = porcentaje;
		this.producto = producto;
	}
	
	public Precio(String descripcion, double valor, double porcentaje) {
		super();
		this.descripcion = descripcion;
		this.valor = valor;
		this.porcentaje = porcentaje;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
}