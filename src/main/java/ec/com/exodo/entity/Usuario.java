package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.List;

/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
@NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String cargo;

	private String cedula;

	private String email;

	private Boolean estado;

	private String funcion;

	@Column(name = "hora_entrada")
	private Time horaEntrada;

	@Column(name = "hora_salida")
	private Time horaSalida;

	private String nombre;

	private String password;

	private String username;

	// bi-directional many-to-one association to CompraCabecera
	@OneToMany(mappedBy = "usuario")
	private List<CompraCabecera> compraCabeceras;

	// bi-directional many-to-one association to DevolucionCompra
	@OneToMany(mappedBy = "usuario")
	private List<DevolucionCompra> devolucionCompras;

	// bi-directional many-to-one association to DevolucionVenta
	@OneToMany(mappedBy = "usuario")
	private List<DevolucionVenta> devolucionVentas;

	// bi-directional many-to-one association to NotaCompraCabecera
	@OneToMany(mappedBy = "usuario")
	private List<NotaCompraCabecera> notaCompraCabeceras;

	// bi-directional many-to-one association to Sucursal
	@OneToMany(mappedBy = "usuario")
	private List<Sucursal> sucursals;

	// bi-directional many-to-one association to AreaSucursal
	@ManyToOne
	@JoinColumn(name = "id_area_sucursal")
	private AreaSucursal areaSucursal;

	// bi-directional many-to-one association to Rol
	@ManyToOne
	@JoinColumn(name = "id_rol")
	private Rol rol;

	// bi-directional many-to-one association to VentaCabecera
	@OneToMany(mappedBy = "usuario")
	private List<VentaCabecera> ventaCabeceras;

	public Usuario() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCargo() {
		return this.cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getCedula() {
		return this.cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getFuncion() {
		return this.funcion;
	}

	public void setFuncion(String funcion) {
		this.funcion = funcion;
	}

	public Time getHoraEntrada() {
		return this.horaEntrada;
	}

	public void setHoraEntrada(Time horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public Time getHoraSalida() {
		return this.horaSalida;
	}

	public void setHoraSalida(Time horaSalida) {
		this.horaSalida = horaSalida;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<CompraCabecera> getCompraCabeceras() {
		return this.compraCabeceras;
	}

	public void setCompraCabeceras(List<CompraCabecera> compraCabeceras) {
		this.compraCabeceras = compraCabeceras;
	}

	public CompraCabecera addCompraCabecera(CompraCabecera compraCabecera) {
		getCompraCabeceras().add(compraCabecera);
		compraCabecera.setUsuario(this);

		return compraCabecera;
	}

	public CompraCabecera removeCompraCabecera(CompraCabecera compraCabecera) {
		getCompraCabeceras().remove(compraCabecera);
		compraCabecera.setUsuario(null);

		return compraCabecera;
	}

	public List<DevolucionCompra> getDevolucionCompras() {
		return this.devolucionCompras;
	}

	public void setDevolucionCompras(List<DevolucionCompra> devolucionCompras) {
		this.devolucionCompras = devolucionCompras;
	}

	public DevolucionCompra addDevolucionCompra(DevolucionCompra devolucionCompra) {
		getDevolucionCompras().add(devolucionCompra);
		devolucionCompra.setUsuario(this);

		return devolucionCompra;
	}

	public DevolucionCompra removeDevolucionCompra(DevolucionCompra devolucionCompra) {
		getDevolucionCompras().remove(devolucionCompra);
		devolucionCompra.setUsuario(null);

		return devolucionCompra;
	}

	public List<DevolucionVenta> getDevolucionVentas() {
		return this.devolucionVentas;
	}

	public void setDevolucionVentas(List<DevolucionVenta> devolucionVentas) {
		this.devolucionVentas = devolucionVentas;
	}

	public DevolucionVenta addDevolucionVenta(DevolucionVenta devolucionVenta) {
		getDevolucionVentas().add(devolucionVenta);
		devolucionVenta.setUsuario(this);

		return devolucionVenta;
	}

	public DevolucionVenta removeDevolucionVenta(DevolucionVenta devolucionVenta) {
		getDevolucionVentas().remove(devolucionVenta);
		devolucionVenta.setUsuario(null);

		return devolucionVenta;
	}

	public List<NotaCompraCabecera> getNotaCompraCabeceras() {
		return this.notaCompraCabeceras;
	}

	public void setNotaCompraCabeceras(List<NotaCompraCabecera> notaCompraCabeceras) {
		this.notaCompraCabeceras = notaCompraCabeceras;
	}

	public NotaCompraCabecera addNotaCompraCabecera(NotaCompraCabecera notaCompraCabecera) {
		getNotaCompraCabeceras().add(notaCompraCabecera);
		notaCompraCabecera.setUsuario(this);

		return notaCompraCabecera;
	}

	public NotaCompraCabecera removeNotaCompraCabecera(NotaCompraCabecera notaCompraCabecera) {
		getNotaCompraCabeceras().remove(notaCompraCabecera);
		notaCompraCabecera.setUsuario(null);

		return notaCompraCabecera;
	}

	public List<Sucursal> getSucursals() {
		return this.sucursals;
	}

	public void setSucursals(List<Sucursal> sucursals) {
		this.sucursals = sucursals;
	}

	public Sucursal addSucursal(Sucursal sucursal) {
		getSucursals().add(sucursal);
		sucursal.setUsuario(this);

		return sucursal;
	}

	public Sucursal removeSucursal(Sucursal sucursal) {
		getSucursals().remove(sucursal);
		sucursal.setUsuario(null);

		return sucursal;
	}

	public AreaSucursal getAreaSucursal() {
		return this.areaSucursal;
	}

	public void setAreaSucursal(AreaSucursal areaSucursal) {
		this.areaSucursal = areaSucursal;
	}

	public Rol getRol() {
		return this.rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public List<VentaCabecera> getVentaCabeceras() {
		return this.ventaCabeceras;
	}

	public void setVentaCabeceras(List<VentaCabecera> ventaCabeceras) {
		this.ventaCabeceras = ventaCabeceras;
	}

	public VentaCabecera addVentaCabecera(VentaCabecera ventaCabecera) {
		getVentaCabeceras().add(ventaCabecera);
		ventaCabecera.setUsuario(this);

		return ventaCabecera;
	}

	public VentaCabecera removeVentaCabecera(VentaCabecera ventaCabecera) {
		getVentaCabeceras().remove(ventaCabecera);
		ventaCabecera.setUsuario(null);

		return ventaCabecera;
	}

}