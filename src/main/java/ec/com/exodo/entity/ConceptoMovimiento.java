package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the concepto_movimiento database table.
 * 
 */
@Entity
@Table(name = "concepto_movimiento")
@NamedQuery(name = "ConceptoMovimiento.findAll", query = "SELECT c FROM ConceptoMovimiento c")
public class ConceptoMovimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Integer codigo;

	private String concepto;

	private String descripcion;

	private Boolean estado;

	// bi-directional many-to-one association to Kardex
	@OneToMany(mappedBy = "conceptoMovimiento")
	private List<CuadreInventario> cuadreInventarios;

	public ConceptoMovimiento() {
	}

	public ConceptoMovimiento(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getConcepto() {
		return this.concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public List<CuadreInventario> getCuadreInventarios() {
		return cuadreInventarios;
	}

	public void setCuadreInventarios(List<CuadreInventario> cuadreInventarios) {
		this.cuadreInventarios = cuadreInventarios;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConceptoMovimiento [id=");
		builder.append(id);
		builder.append(", codigo=");
		builder.append(codigo);
		builder.append(", concepto=");
		builder.append(concepto);
		builder.append(", descripcion=");
		builder.append(descripcion);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", cuadreInventarios=");
		builder.append(cuadreInventarios);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConceptoMovimiento other = (ConceptoMovimiento) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}