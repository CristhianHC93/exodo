package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the nota_compra_detalle database table.
 * 
 */
@Entity
@Table(name = "nota_compra_detalle")
@NamedQuery(name = "NotaCompraDetalle.findAll", query = "SELECT n FROM NotaCompraDetalle n")
public class NotaCompraDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private double cantidad;

	@Column(name = "cantidad_entregada")
	private double cantidadEntregada;

	@Column(name = "precio_compra")
	private double precioCompra;

	@Column(name = "subtotal")
	private double subtotal;

	@Column(name = "valor_iva")
	private double valorIva;

	@Column(name = "descuento_porcentaje")
	private double descuentoPorcentaje;

	@Column(name = "descuento_valor")
	private double descuentoValor;

	@Column(nullable = false)
	private double total;

	// bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name = "id_estado")
	private Estado estado;

	// bi-directional many-to-one association to NotaCompraCabecera
	@ManyToOne
	@JoinColumn(name = "id_nota_compra_cabecera")
	private NotaCompraCabecera notaCompraCabecera;

	// bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name = "id_producto")
	private Producto producto;

	public NotaCompraDetalle() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public double getCantidadEntregada() {
		return this.cantidadEntregada;
	}

	public void setCantidadEntregada(double cantidadEntregada) {
		this.cantidadEntregada = cantidadEntregada;
	}

	public double getPrecioCompra() {
		return this.precioCompra;
	}

	public void setPrecioCompra(double precioCompra) {
		this.precioCompra = precioCompra;
	}

	public double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public NotaCompraCabecera getNotaCompraCabecera() {
		return this.notaCompraCabecera;
	}

	public void setNotaCompraCabecera(NotaCompraCabecera notaCompraCabecera) {
		this.notaCompraCabecera = notaCompraCabecera;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public double getValorIva() {
		return valorIva;
	}

	public void setValorIva(double valorIva) {
		this.valorIva = valorIva;
	}

	public double getDescuentoPorcentaje() {
		return descuentoPorcentaje;
	}

	public void setDescuentoPorcentaje(double descuentoPorcentaje) {
		this.descuentoPorcentaje = descuentoPorcentaje;
	}

	public double getDescuentoValor() {
		return descuentoValor;
	}

	public void setDescuentoValor(double descuentoValor) {
		this.descuentoValor = descuentoValor;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

}