package ec.com.exodo.dao.ie;

import java.util.Date;
import java.util.List;

import ec.com.exodo.entity.CuadreInventario;

public interface CuadreInventarioDAO {	
	public CuadreInventario getCuadreInventarioByProductoAndEstado(int producto,boolean estado);
	public CuadreInventario getCuadreInventarioByProductoCodigoAndAreaSucursal(String codigo, int area);
	public CuadreInventario saveorUpdate(CuadreInventario cuadreInventario);
	public int eliminar(CuadreInventario cuadreInventario);
	public List<CuadreInventario> getListaCuadreInventarioByProductoAndFecha(int idProducto, Date fechaInicial, Date fechaFinal);
	public List<CuadreInventario> getListaCuadreInventarioByProductoAndConceptoMovmiento(int idProducto,int idConceptoMovimiento);
}
