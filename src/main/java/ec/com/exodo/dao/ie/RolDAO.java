package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Rol;

public interface RolDAO {

	public List<Rol> getListaRol();
	public List<Rol> getListaRol(int estado);
	public Rol saveorUpdate(Rol rol);
}
