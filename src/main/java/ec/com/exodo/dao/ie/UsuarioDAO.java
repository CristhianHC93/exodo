package ec.com.exodo.dao.ie;

import java.sql.Time;
import java.util.List;

import ec.com.exodo.entity.Usuario;

public interface UsuarioDAO {

	public Usuario saveorUpdate(Usuario Usuario);
	public Usuario getUsuario(Usuario u,Time t);
	public Usuario getUsuario(String username, String password);
	public List<Usuario> getListaUsuario();
	public List<Usuario> getListaUsuarioEmpleados();
}
