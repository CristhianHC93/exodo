package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Area;

public interface AreaDAO {
	public List<Area> getListaArea();
	public Area saveorUpdate(Area area);
	public int eliminar(Area area);
}
