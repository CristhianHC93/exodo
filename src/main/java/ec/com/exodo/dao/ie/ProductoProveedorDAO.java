package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.ProductoProveedor;

public interface ProductoProveedorDAO {
	
	public List<ProductoProveedor> getListaProductoProveedorByProducto(int idProducto);
	public List<ProductoProveedor> getListaProductoProveedorByProveedorAndSucursal(int idProveedor,String dato, int sucursal);
	public List<ProductoProveedor> getListaProductoProveedorByProveedor(int idProveedor, String dato);
	public ProductoProveedor getProductoByCodigoDescripcionProveedor(String dato, int proveedor);
	
	public ProductoProveedor saveorUpdate(ProductoProveedor ProductoProveedor);
	
	public int eliminar(Producto producto);
	public int eliminarByProductoAndProveedor(int idProducto,int idProveedor);
	public ProductoProveedor getProductoProveedorByProductoAndProveedor(int idProducto, int idProveedor);
}
