package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.AreaSucursal;

public interface AreaSucursalDAO {
	public List<AreaSucursal> getListaAreaSucursal();
	public List<AreaSucursal> getListaAreaSucursalBySucursal(int sucursal);
	public List<AreaSucursal> getListaAreaSucursalBySucursalActivo(int sucursal);
	public List<AreaSucursal> getListaAreaSucursalByArea(int area);
	public AreaSucursal saveorUpdate(AreaSucursal areaSucursal);
	public AreaSucursal getAreaSucursalByAreaAndSucursal(int area,int sucursal);
	public int eliminar(AreaSucursal area);
}
