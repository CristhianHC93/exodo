package ec.com.exodo.dao.ie;

import java.util.Date;
import java.util.List;

import ec.com.exodo.entity.Caja;

public interface CajaDAO {
	public List<Caja> getListaCaja();
	public List<Caja> getListaCaja(int area, boolean estado);
	public List<Caja> getListaCaja(boolean estado,Date fi,Date ff);
	public List<Caja> getListaCaja(int area,boolean estado,Date fi,Date ff);
	public Caja getCaja(int area, boolean estado);
	public Caja saveorUpdate(Caja caja);
}
