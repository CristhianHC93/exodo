package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.VentaDetalle;

public interface VentaDetalleDAO {

	public List<VentaDetalle> getListaVentaDetalleByCabecera(int idVentaCabecera,int estado);
	public VentaDetalle saveorUpdate(VentaDetalle ventaDetalle);
}
