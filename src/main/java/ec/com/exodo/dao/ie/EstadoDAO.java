package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Estado;

public interface EstadoDAO {
	public List<Estado> getListaEstadoByModulo(int modulo);
}
