package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Categoria;

public interface CategoriaDAO {
	public List<Categoria> getListaCategoriaByArea(int idArea);
	public List<Categoria> getListaCategoria();
	public Categoria saveorUpdate(Categoria Categoria);
	public int eliminar(Categoria categoria);
}
