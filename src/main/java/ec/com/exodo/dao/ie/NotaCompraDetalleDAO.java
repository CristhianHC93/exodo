package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.NotaCompraDetalle;

public interface NotaCompraDetalleDAO {	
	public List<NotaCompraDetalle> getNotaCompraDetallesByCabecera(int idNotaCompraCabecera,int estado);
	public NotaCompraDetalle saveorUpdate(NotaCompraDetalle notaCompraDetalle);
}
