package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.NotaCompraCabecera;
import ec.com.exodo.entity.NotaCompraDetalle;
import ec.com.exodo.models.Modelos;

public interface NotaCompraCabeceraDAO {
	
	public List<NotaCompraCabecera> getListaNotaCompraCabeceraByEstado(int estado);
	public NotaCompraCabecera saveorUpdate(NotaCompraCabecera NotaCompraCabecera);
	public List<Modelos<NotaCompraDetalle, Object>> saveorUpdateDetalle(List<Modelos<NotaCompraDetalle, Object>> listaNotaCompra,NotaCompraCabecera notaCompraCabecera);
	public NotaCompraCabecera getNotaCompraCabeceraByCodio(String codigo);
}
