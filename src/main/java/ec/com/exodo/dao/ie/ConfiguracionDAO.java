package ec.com.exodo.dao.ie;

import ec.com.exodo.entity.Configuracion;

public interface ConfiguracionDAO {
	
	public Configuracion getConfiguracion(int id);
	public Configuracion saveOrUpdate(Configuracion configuracion);
}
