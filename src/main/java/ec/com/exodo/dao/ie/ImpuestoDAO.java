package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Impuesto;

public interface ImpuestoDAO {
	public List<Impuesto> getImpuestos();
}
