package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Rol;
import ec.com.exodo.entity.RolModulo;

public interface RolModuloDAO {
	public List<RolModulo> getListaRolModulo(int rol,int estado);
	public List<RolModulo> getListaRolModulo(Rol rol); 
	public List<RolModulo> getListaRolModulo(int estado);
	public RolModulo saveorUpdate(RolModulo rolModulo);
}
