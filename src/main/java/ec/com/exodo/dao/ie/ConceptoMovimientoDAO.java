package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.ConceptoMovimiento;

public interface ConceptoMovimientoDAO {
	public List<ConceptoMovimiento> getListaConceptoMovimiento();
	public List<ConceptoMovimiento> getListaConceptoMovimiento(int codigo,boolean estado);
	public ConceptoMovimiento getConceptoMovimientoById(int id);
	public ConceptoMovimiento saveorUpdate(ConceptoMovimiento conceptoMovimiento);
	public int eliminar(int idConceptoMovimiento);
}
