package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Precio;

public interface PrecioDAO {

	public List<Precio> getListaByProductoIsNull();
	public List<Precio> getListaByProducto(int idProducto);
	public Precio saveOrUpdate(Precio precio);
	public int eliminar(int idPrecio);
}
