package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Modulo;

public interface ModuloDAO {
	public List<Modulo> getListaModulo();
	public Modulo saveorUpdate(Modulo modulo);
}
