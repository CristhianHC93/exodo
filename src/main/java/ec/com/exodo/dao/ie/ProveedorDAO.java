package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Proveedor;

public interface ProveedorDAO {
	public List<Proveedor> getProveedores(int estado);
	public Proveedor saveorUpdate(Proveedor Proveedor);
	public Proveedor getProveedorByEmpresaOrRuc(String dato);
}
