package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Cliente;

public interface ClienteDAO {
	public List<Cliente> getListaClienteByEstado(int idEstado);
	public Cliente saveorUpdate(Cliente cliente);
	public Cliente getClienteByCedula(String cedula);
	public List<Cliente> getFiltroCliente(String dato);

}
