package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.InformacionNegocio;

public interface InformacionNegocioDAO {
	public InformacionNegocio saveorUpdate(InformacionNegocio informacionNegocio);

	public List<InformacionNegocio> getListaInformacionNegocio();
}
