package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.ModuloDetalle;

public interface ModuloDetalleDAO {
	public List<ModuloDetalle> getListaModuloDetallesByModulo(int idModulo);
	public List<ModuloDetalle> getListaModuloDetalle();
	public ModuloDetalle saveorUpdate(ModuloDetalle moduloDetalle);
}
