package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Email;

public interface EmailDAO {
	public Email saveorUpdate(Email email);
	public List<Email> getListaEmail();
}
