package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.CuadreInventarioCabecera;

public interface CuadreInventarioCabeceraDAO {
	
	public List<CuadreInventarioCabecera> getListaCuadreInventarioCabecera();
	public List<CuadreInventarioCabecera> getListaCuadreInventarioCabecera(boolean estado);
	public List<CuadreInventarioCabecera> getListaCuadreInventarioCabecera(int areaSucursal,boolean estado);
	public CuadreInventarioCabecera getCuadreInventarioByAreaSucursal(int areaSucursal,boolean estado);
	public CuadreInventarioCabecera saveorUpdate(CuadreInventarioCabecera cuadreInventarioCabecera);
}
