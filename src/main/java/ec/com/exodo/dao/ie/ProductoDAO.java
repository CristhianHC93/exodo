package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Producto;


public interface ProductoDAO {

	public List<Producto> getListaProducto(int estado);
	public List<Producto> getListaProductoBySucursal(int estado, int sucursal);
	public List<Producto> getListaProductoByAreaSucursal(int estado,int area);
	public List<Producto> getListaProductoByCategoria(int estado,int categoria);
	public Producto saveorUpdate(Producto Producto);
	public Producto getProducto(int idProducto);
	public Producto getProductoByCodigoOrDescripcion(String dato);
	public Producto getProductoByCodigoOrDescripcionAndAreaSucursal(String codigo,int area);
	public Producto getProductoByIdAndAreaSucursal(int idProducto,int area);
	public List<Producto> getListaProductoByDescripcionOrCodigo(String dato);
	public List<Producto> getListaProductoByDescripcionOrCodigoAndAreaSucursal(String dato,int area);
	public List<Producto> getFiltroProductoBySucursal(String dato,int sucursal);
	public List<Producto> getFiltroProductoByAreaSucursalByProveedor(String dato, int area,int proveedor);
}
