package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.CompraCabecera;
import ec.com.exodo.entity.CompraDetalle;
import ec.com.exodo.entity.NotaCompraDetalle;
import ec.com.exodo.models.Modelos;

public interface CompraCabeceraDAO {
	
	public List<CompraCabecera> getCompraCabeceras(int estado);
	public CompraCabecera saveorUpdate(CompraCabecera compraCabecera);
	public void saveorUpdateDetalle(List<Modelos<CompraDetalle,NotaCompraDetalle>> listaNotaCompra,CompraCabecera compraCabecera,boolean actualizarPrecio);
}
