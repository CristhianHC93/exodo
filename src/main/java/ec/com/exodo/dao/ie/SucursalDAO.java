package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.Sucursal;

public interface SucursalDAO {
	public List<Sucursal> getListaSucursal();
	public Sucursal saveorUpdate(Sucursal sucursal);
	public int eliminar(Sucursal Sucursal);
	public Sucursal getSucursal(int id);
}
