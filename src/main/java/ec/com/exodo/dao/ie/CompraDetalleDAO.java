package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.CompraDetalle;

public interface CompraDetalleDAO {	
	public List<CompraDetalle> getCompraDetallesByCabecera(int idCompraCabecera);
	public CompraDetalle getCompraDetallesById(int idCompraDetalle);
	public CompraDetalle saveorUpdate(CompraDetalle CompraDetalle);
}
