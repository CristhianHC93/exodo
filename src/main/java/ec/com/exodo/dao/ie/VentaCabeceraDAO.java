package ec.com.exodo.dao.ie;

import java.util.Date;
import java.util.List;

import ec.com.exodo.entity.VentaCabecera;
import ec.com.exodo.entity.VentaDetalle;
import ec.com.exodo.models.Modelos;

public interface VentaCabeceraDAO {

	public List<VentaCabecera> getListaVentaCabeceraByAreaSucursal(int estado,int areaSucursal);
	public List<VentaCabecera> getListaVentaCabeceraBySucursal(int estado, int sucursal);
	public VentaCabecera saveorUpdate(VentaCabecera ventaDetalle);
	public void saveorUpdateDetalle(List<Modelos<VentaDetalle, Object>> listaVenta,VentaCabecera ventaCabecera);
	public List<VentaCabecera> getListaVentaCabeceraByAreaSucursalByFecha(int areaSucursal,int estado,Date fi,Date ff);
	public VentaCabecera getUltimaVentaCabecera(int estado);
	public List<VentaCabecera> getListaVentaCabecera(int estado);
	public List<VentaCabecera> getListaVentaCabeceraByFecha(int estado, Date fi, Date ff);
}
