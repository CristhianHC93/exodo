package ec.com.exodo.dao.ie;

import java.util.List;

import ec.com.exodo.entity.CajaDetalle;

public interface CajaDetalleDAO {
	
	public List<CajaDetalle> getListaCajaDetalle();
	public List<CajaDetalle> getListaCajaDetalleByCaja(int caja);
	public CajaDetalle saveorUpdate(CajaDetalle cajaDetalle);

}
