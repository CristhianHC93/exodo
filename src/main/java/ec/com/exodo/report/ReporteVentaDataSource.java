package ec.com.exodo.report;

import java.util.ArrayList;
import java.util.List;

import ec.com.exodo.entity.VentaCabecera;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class ReporteVentaDataSource implements JRDataSource {
	private List<VentaCabecera> listaVentaCabecera = new ArrayList<>();
	private int indiceVentaCabecera = -1;

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object valor = null;

		if ("codigo".equals(arg0.getName())) {
			valor = listaVentaCabecera.get(indiceVentaCabecera).getCodigo();
		} else if ("fecha_emision".equals(arg0.getName())) {
			valor = listaVentaCabecera.get(indiceVentaCabecera).getFechaEmision();
		} else if ("nombre".equals(arg0.getName())) {
			valor = listaVentaCabecera.get(indiceVentaCabecera).getCliente().getNombre();
		} else if ("subtotal".equals(arg0.getName())) {
			valor = listaVentaCabecera.get(indiceVentaCabecera).getSubtotal();
		} else if ("descuento".equals(arg0.getName())) {
			valor = listaVentaCabecera.get(indiceVentaCabecera).getDescuentoValor();
		} else if ("iva".equals(arg0.getName())) {
			valor = listaVentaCabecera.get(indiceVentaCabecera).getIva();
		} else if ("total".equals(arg0.getName())) {
			valor = listaVentaCabecera.get(indiceVentaCabecera).getTotal();
		}
		return valor;
	}

	@Override
	public boolean next() throws JRException {
		return ++indiceVentaCabecera < listaVentaCabecera.size();
	}

	public void add(VentaCabecera ventaCabecera) {
		this.listaVentaCabecera.add(ventaCabecera);
	}

}
