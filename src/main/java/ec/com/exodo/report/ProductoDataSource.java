package ec.com.exodo.report;

import java.util.ArrayList;
import java.util.List;

import ec.com.exodo.entity.Producto;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class ProductoDataSource implements JRDataSource {

	private List<Producto> listaProducto = new ArrayList<>();
	private int indiceProducto = -1;

	@Override
	public Object getFieldValue(JRField arg0) throws JRException {
		Object valor = null;

		if ("codigo".equals(arg0.getName())) {
			valor = listaProducto.get(indiceProducto).getCodigo();
		} else if ("descripcion".equals(arg0.getName())) {
			valor = listaProducto.get(indiceProducto).getDescripcion();
		} else if ("cantidad".equals(arg0.getName())) {
			valor = listaProducto.get(indiceProducto).getCantidad();
		} else if ("precio_compra".equals(arg0.getName())) {
			valor = listaProducto.get(indiceProducto).getPrecioCompra();
		} else if ("precio_venta".equals(arg0.getName())) {
			valor = listaProducto.get(indiceProducto).getPrecioVenta();
		} //else if ("descripcionc".equals(arg0.getName())) {
			//valor = listaProducto.get(indiceProducto).getCategoria().getDescripcion();
		else if ("nombre".equals(arg0.getName())) {
			valor = listaProducto.get(indiceProducto).getProveedor().getNombre();
		}
		return valor;
	}

	@Override
	public boolean next() throws JRException {
		return ++indiceProducto < listaProducto.size();
	}

	public void add(Producto producto) {
		this.listaProducto.add(producto);
	}

}
