package ec.com.exodo.report;

import java.util.ArrayList;
import java.util.List;

import ec.com.exodo.entity.VentaDetalle;
import ec.com.exodo.models.Modelos;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class ProformaDataSource implements JRDataSource{
	private List<Modelos<VentaDetalle, Object>> listaVenta = new ArrayList<>();
	private int indicePorforma = -1;

	@Override
	public Object getFieldValue(JRField jrf) throws JRException {

		Object valor = null;

/*		if ("codigo".equals(jrf.getName())) {
			valor = listaVenta.get(indicePorforma).getObjetoT().getProducto().getCodigo();
		} else */if ("descripcion".equals(jrf.getName())) {
			valor = listaVenta.get(indicePorforma).getObjetoT().getProducto().getDescripcionCorta();
		} else if ("cantidad".equals(jrf.getName())) {
			valor = (int) listaVenta.get(indicePorforma).getObjetoT().getCantidad();
		} else if ("precio_venta".equals(jrf.getName())) {
			valor = listaVenta.get(indicePorforma).getObjetoT().getPrecioVenta();
		} else if ("precio_subtotal".equals(jrf.getName())) {
			valor = listaVenta.get(indicePorforma).getObjetoT().getPrecioSubtotal();
		}else if ("descuento_valor".equals(jrf.getName())) {
			valor = listaVenta.get(indicePorforma).getObjetoT().getDescuentoValor();}
			/*}else if ("descuento_porcentaje".equals(jrf.getName())) {
			valor = listaVenta.get(indicePorforma).getObjetoT().getDescuentoPorcentaje();
		}*/
		return valor;
	}

	@Override
	public boolean next() throws JRException {
		return ++indicePorforma < listaVenta.size();
	}
	
	public void addProforma(Modelos<VentaDetalle, Object> venta)
    {
        this.listaVenta.add(venta);
    }
}
