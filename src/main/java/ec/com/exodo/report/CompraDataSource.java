package ec.com.exodo.report;

import java.util.ArrayList;
import java.util.List;

import ec.com.exodo.entity.CompraDetalle;
import ec.com.exodo.entity.NotaCompraDetalle;
import ec.com.exodo.models.Modelos;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class CompraDataSource implements JRDataSource{
	private List<Modelos<CompraDetalle, NotaCompraDetalle>> listaCompra = new ArrayList<>();
	private int indicePorforma = -1;

	@Override
	public Object getFieldValue(JRField jrf) throws JRException {

		Object valor = null;

/*		if ("codigo".equals(jrf.getName())) {
			valor = listaVenta.get(indicePorforma).getObjetoT().getProducto().getCodigo();
		} else */if ("descripcion".equals(jrf.getName())) {
			valor = listaCompra.get(indicePorforma).getObjetoT().getProducto().getDescripcionCorta();
		} else if ("cantidad".equals(jrf.getName())) {
			valor = (int) listaCompra.get(indicePorforma).getObjetoT().getCantidad();
		} else if ("precio_venta".equals(jrf.getName())) {
			valor = listaCompra.get(indicePorforma).getObjetoT().getPrecioCompra();
		} else if ("precio_subtotal".equals(jrf.getName())) {
			valor = listaCompra.get(indicePorforma).getObjetoT().getSubtotal();
		}else if ("descuento_valor".equals(jrf.getName())) {
			valor = 0.0;}
		/*}else if ("descuento_porcentaje".equals(jrf.getName())) {
			valor = listaVenta.get(indicePorforma).getObjetoT().getDescuentoPorcentaje();
		}*/
		return valor;
	}

	@Override
	public boolean next() throws JRException {
		return ++indicePorforma < listaCompra.size();
	}
	
	public void addCompra(Modelos<CompraDetalle, NotaCompraDetalle> compra)
    {
        this.listaCompra.add(compra);
    }
}