package ec.com.exodo.factory;

import ec.com.exodo.dao.ie.AreaDAO;
import ec.com.exodo.dao.ie.AreaSucursalDAO;
import ec.com.exodo.dao.ie.CajaDAO;
import ec.com.exodo.dao.ie.CajaDetalleDAO;
import ec.com.exodo.dao.ie.CategoriaDAO;
import ec.com.exodo.dao.ie.ClienteDAO;
import ec.com.exodo.dao.ie.CompraCabeceraDAO;
import ec.com.exodo.dao.ie.CompraDetalleDAO;
import ec.com.exodo.dao.ie.ConceptoMovimientoDAO;
import ec.com.exodo.dao.ie.ConfiguracionDAO;
import ec.com.exodo.dao.ie.CuadreInventarioCabeceraDAO;
import ec.com.exodo.dao.ie.CuadreInventarioDAO;
import ec.com.exodo.dao.ie.DevolucionCompraDAO;
import ec.com.exodo.dao.ie.EmailDAO;
import ec.com.exodo.dao.ie.EstadoDAO;
import ec.com.exodo.dao.ie.ImpuestoDAO;
import ec.com.exodo.dao.ie.InformacionNegocioDAO;
import ec.com.exodo.dao.ie.ModuloDAO;
import ec.com.exodo.dao.ie.ModuloDetalleDAO;
import ec.com.exodo.dao.ie.NotaCompraCabeceraDAO;
import ec.com.exodo.dao.ie.NotaCompraDetalleDAO;
import ec.com.exodo.dao.ie.PrecioDAO;
import ec.com.exodo.dao.ie.ProductoDAO;
import ec.com.exodo.dao.ie.ProductoProveedorDAO;
import ec.com.exodo.dao.ie.ProveedorDAO;
import ec.com.exodo.dao.ie.RolDAO;
import ec.com.exodo.dao.ie.RolModuloDAO;
import ec.com.exodo.dao.ie.SucursalDAO;
import ec.com.exodo.dao.ie.UsuarioDAO;
import ec.com.exodo.dao.ie.VentaCabeceraDAO;
import ec.com.exodo.dao.ie.VentaDetalleDAO;
import ec.com.exodo.doa.impl.AreaDAOImpl;
import ec.com.exodo.doa.impl.AreaSucursalDAOImpl;
import ec.com.exodo.doa.impl.CajaDAOImpl;
import ec.com.exodo.doa.impl.CajaDetalleDAOImpl;
import ec.com.exodo.doa.impl.CategoriaDAOImpl;
import ec.com.exodo.doa.impl.ClienteDAOImpl;
import ec.com.exodo.doa.impl.CompraCabeceraDAOImpl;
import ec.com.exodo.doa.impl.CompraDetalleDAOImpl;
import ec.com.exodo.doa.impl.ConceptoMovimientoDAOImpl;
import ec.com.exodo.doa.impl.ConfiguracionDAOImpl;
import ec.com.exodo.doa.impl.CuadreInventarioCabeceraDAOImpl;
import ec.com.exodo.doa.impl.CuadreInventarioDAOImpl;
import ec.com.exodo.doa.impl.DevolucionCompraDAOImpl;
import ec.com.exodo.doa.impl.EmailDAOImpl;
import ec.com.exodo.doa.impl.EstadoDAOImpl;
import ec.com.exodo.doa.impl.ImpuestoDAOImpl;
import ec.com.exodo.doa.impl.InformacionNegocioDAOImpl;
import ec.com.exodo.doa.impl.ModuloDAOImpl;
import ec.com.exodo.doa.impl.ModuloDetalleDAOImpl;
import ec.com.exodo.doa.impl.NotaCompraCabeceraDAOImpl;
import ec.com.exodo.doa.impl.NotaCompraDetalleDAOImpl;
import ec.com.exodo.doa.impl.PrecioDAOImpl;
import ec.com.exodo.doa.impl.ProductoDAOImpl;
import ec.com.exodo.doa.impl.ProductoProveedorDAOImpl;
import ec.com.exodo.doa.impl.ProveedorDAOImpl;
import ec.com.exodo.doa.impl.RolDAOImpl;
import ec.com.exodo.doa.impl.RolModuloDAOImpl;
import ec.com.exodo.doa.impl.SucursalDAOImpl;
import ec.com.exodo.doa.impl.UsuarioDAOImpl;
import ec.com.exodo.doa.impl.VentaCabeceraDAOImpl;
import ec.com.exodo.doa.impl.VentaDetalleDAOImpl;

public class InstanciaDAO {

	private static UsuarioDAO usuarioDAO;
	private static EstadoDAO estadoDAO;
	private static ProductoDAO productoDAO;
	private static CategoriaDAO categoriaDAO;
	private static ImpuestoDAO impuestoDAO;
	private static ProveedorDAO proveedorDAO;
	private static ClienteDAO clienteDAO;
	private static NotaCompraCabeceraDAO notaCompraCabeceraDAO;
	private static NotaCompraDetalleDAO notaCompraDetalleDAO;
	private static CompraCabeceraDAO compraCabeceraDAO;
	private static CompraDetalleDAO compraDetalleDAO;
	private static VentaCabeceraDAO ventaCabeceraDAO;
	private static VentaDetalleDAO ventaDetalleDAO;
	private static DevolucionCompraDAO devolucionCompraDAO;
	private static RolModuloDAO rolModuloDAO;
	private static ModuloDAO moduloDAO;
	private static ModuloDetalleDAO moduloDetalleDAO;
	private static RolDAO rolDAO;
	private static EmailDAO emailDAO;
	private static InformacionNegocioDAO informacionNegocioDAO;
	private static AreaDAO areaDAO;
	private static ProductoProveedorDAO productoProveedorDAO;
	private static SucursalDAO sucursalDAO;
	private static AreaSucursalDAO areaSucursalDAO;
	private static ConceptoMovimientoDAO conceptoMovimientoDAO;
	private static CuadreInventarioDAO cuadreInventarioDAO;

	private static CajaDAO cajaDAO;
	private static CajaDetalleDAO cajaDetalleDAO;
	private static CuadreInventarioCabeceraDAO cuadreInventarioCabeceraDAO;
	private static PrecioDAO precioDAO;
	private static ConfiguracionDAO configuracionDAO;

	private static InstanciaDAO instancia;

	public static InstanciaDAO getInstanciaDAO() {
		return (instancia == null) ? new InstanciaDAO() : instancia;
	}

	public static CajaDetalleDAO getInstanciaCajaDetalleDAO() {
		if (cajaDetalleDAO == null) {
			cajaDetalleDAO = new CajaDetalleDAOImpl();
		}
		return cajaDetalleDAO;
	}

	public static CuadreInventarioCabeceraDAO getInstanciaCuadreInventarioCabeceraDAO() {
		if (cuadreInventarioCabeceraDAO == null) {
			cuadreInventarioCabeceraDAO = new CuadreInventarioCabeceraDAOImpl();
		}
		return cuadreInventarioCabeceraDAO;
	}

	public static CajaDAO getInstanciaCajaDAO() {
		if (cajaDAO == null) {
			cajaDAO = new CajaDAOImpl();
		}
		return cajaDAO;
	}

	public static ConceptoMovimientoDAO getInstanciaConceptoMovimientoDAO() {
		if (conceptoMovimientoDAO == null) {
			conceptoMovimientoDAO = new ConceptoMovimientoDAOImpl();
		}
		return conceptoMovimientoDAO;
	}

	public static CuadreInventarioDAO getInstanciaCuadreInventarioDAO() {
		if (cuadreInventarioDAO == null) {
			cuadreInventarioDAO = new CuadreInventarioDAOImpl();
		}
		return cuadreInventarioDAO;
	}

	public static AreaSucursalDAO getInstanciaAreaSucursalDAO() {
		if (areaSucursalDAO == null) {
			areaSucursalDAO = new AreaSucursalDAOImpl();
		}
		return areaSucursalDAO;
	}

	public static SucursalDAO getInstanciaSucursalDAO() {
		if (sucursalDAO == null) {
			sucursalDAO = new SucursalDAOImpl();
		}
		return sucursalDAO;
	}

	public static ProductoDAO getInstanciaProductoDAO() {
		if (productoDAO == null) {
			productoDAO = new ProductoDAOImpl();
		}
		return productoDAO;
	}

	public static ProductoProveedorDAO getInstanciaProductoProveedorDAO() {
		if (productoProveedorDAO == null) {
			productoProveedorDAO = new ProductoProveedorDAOImpl();
		}
		return productoProveedorDAO;
	}

	public static EmailDAO getInstanciaEmailDAO() {
		if (emailDAO == null) {
			emailDAO = new EmailDAOImpl();
		}
		return emailDAO;
	}

	public static InformacionNegocioDAO getInstanciaInformacionNegocioDAO() {
		if (informacionNegocioDAO == null) {
			informacionNegocioDAO = new InformacionNegocioDAOImpl();
		}
		return informacionNegocioDAO;
	}

	public static EstadoDAO getInstanciaEstadoDAO() {
		if (estadoDAO == null) {
			estadoDAO = new EstadoDAOImpl();
		}
		return estadoDAO;
	}

	public static RolDAO getInstanciaRolDAO() {
		if (rolDAO == null) {
			rolDAO = new RolDAOImpl();
		}
		return rolDAO;
	}

	public static AreaDAO getInstanciaAreaDAO() {
		if (areaDAO == null) {
			areaDAO = new AreaDAOImpl();
		}
		return areaDAO;
	}

	public static ModuloDAO getInstanciaModuloDAO() {
		if (moduloDAO == null) {
			moduloDAO = new ModuloDAOImpl();
		}
		return moduloDAO;
	}

	public static ModuloDetalleDAO getInstanciaModuloDetalleDAO() {
		if (moduloDetalleDAO == null) {
			moduloDetalleDAO = new ModuloDetalleDAOImpl();
		}
		return moduloDetalleDAO;
	}

	public static RolModuloDAO getInstanciaRolModuloDAO() {
		if (rolModuloDAO == null) {
			rolModuloDAO = new RolModuloDAOImpl();
		}
		return rolModuloDAO;
	}

	public static UsuarioDAO getInstanciaUsuarioDAO() {
		if (usuarioDAO == null) {
			usuarioDAO = new UsuarioDAOImpl();
		}
		return usuarioDAO;
	}

	public static CategoriaDAO getInstanciaCategoriaDAO() {
		if (categoriaDAO == null) {
			categoriaDAO = new CategoriaDAOImpl();
		}
		return categoriaDAO;
	}

	public static ImpuestoDAO getInstanciaImpuestoDAO() {
		if (impuestoDAO == null) {
			impuestoDAO = new ImpuestoDAOImpl();
		}
		return impuestoDAO;
	}

	public static ProveedorDAO getInstanciaProveedorDAO() {
		if (proveedorDAO == null) {
			proveedorDAO = new ProveedorDAOImpl();
		}
		return proveedorDAO;
	}

	public static ClienteDAO getInstanciaClienteDAO() {
		if (clienteDAO == null) {
			clienteDAO = new ClienteDAOImpl();
		}
		return clienteDAO;
	}

	public static NotaCompraCabeceraDAO getInstanciaNotaCompraCabeceraDAO() {
		if (notaCompraCabeceraDAO == null) {
			notaCompraCabeceraDAO = new NotaCompraCabeceraDAOImpl();
		}
		return notaCompraCabeceraDAO;
	}

	public static NotaCompraDetalleDAO getInstanciaNotaCompraDetalleDAO() {
		if (notaCompraDetalleDAO == null) {
			notaCompraDetalleDAO = new NotaCompraDetalleDAOImpl();
		}
		return notaCompraDetalleDAO;
	}

	public static CompraCabeceraDAO getInstanciaCompraCabeceraDAO() {
		if (compraCabeceraDAO == null) {
			compraCabeceraDAO = new CompraCabeceraDAOImpl();
		}
		return compraCabeceraDAO;
	}

	public static CompraDetalleDAO getInstanciaCompraDetalleDAO() {
		if (compraDetalleDAO == null) {
			compraDetalleDAO = new CompraDetalleDAOImpl();
		}
		return compraDetalleDAO;
	}

	public static VentaCabeceraDAO getInstanciaVentaCabeceraDAO() {
		if (ventaCabeceraDAO == null) {
			ventaCabeceraDAO = new VentaCabeceraDAOImpl();
		}
		return ventaCabeceraDAO;
	}

	public static VentaDetalleDAO getInstanciaVentaDetalleDAO() {
		if (ventaDetalleDAO == null) {
			ventaDetalleDAO = new VentaDetalleDAOImpl();
		}
		return ventaDetalleDAO;
	}

	public static DevolucionCompraDAO getInstanciaDevolucionCompraDAO() {
		if (devolucionCompraDAO == null) {
			devolucionCompraDAO = new DevolucionCompraDAOImpl();
		}
		return devolucionCompraDAO;
	}
	
	public static PrecioDAO getInstanciaPrecioDAO() {
		if (precioDAO == null) {
			precioDAO = new PrecioDAOImpl();
		}
		return precioDAO;
	}
	
	public static ConfiguracionDAO getInstanciaConfiguracionDAO() {
		if (configuracionDAO == null) {
			configuracionDAO = new ConfiguracionDAOImpl();
		}
		return configuracionDAO;
	}

}
