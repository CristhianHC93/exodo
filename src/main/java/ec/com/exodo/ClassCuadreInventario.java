package ec.com.exodo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.dao.ie.AreaSucursalDAO;
import ec.com.exodo.dao.ie.ConceptoMovimientoDAO;
import ec.com.exodo.dao.ie.CuadreInventarioCabeceraDAO;
import ec.com.exodo.dao.ie.CuadreInventarioDAO;
import ec.com.exodo.dao.ie.ProductoDAO;
import ec.com.exodo.entity.AreaSucursal;
import ec.com.exodo.entity.CuadreInventario;
import ec.com.exodo.entity.CuadreInventarioCabecera;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassCuadreInventario {

	private List<AreaSucursal> listaAreaSucursal;
	private AreaSucursal areaSucursal;

	private double totalStock;
	private double totalStockReal;
	private double totalStockDiferencia;
	private double totalCompra;
	private double totalVenta;

	private CuadreInventarioCabecera cuadreInventarioCabecera;

	private boolean banderaValidarProducto = true;

	List<Producto> listaProducto;

	// Bandera para ver que productos estan repetidos
	boolean banderaProducto = false;

	private ProductoDAO productoDAO = InstanciaDAO.getInstanciaProductoDAO();
	private ConceptoMovimientoDAO conceptoMovimientoDAO = InstanciaDAO.getInstanciaConceptoMovimientoDAO();
	private AreaSucursalDAO areaSucursalDAO = InstanciaDAO.getInstanciaAreaSucursalDAO();
	private CuadreInventarioDAO cuadreInventarioDAO = InstanciaDAO.getInstanciaCuadreInventarioDAO();
	private CuadreInventarioCabeceraDAO cuadreInventarioCabeceraDAO = InstanciaDAO
			.getInstanciaCuadreInventarioCabeceraDAO();

	private List<CuadreInventario> listaCuadreInventario = new ArrayList<>();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		selectSucursal();
		limpiar();
	}

	@NotifyChange({ "listaAreaSucursal", "areaSucursal" })
	@Command
	public void selectSucursal() {
		listaAreaSucursal = areaSucursalDAO.getListaAreaSucursalBySucursalActivo(Sistema.getSucursal().getId());
		areaSucursal = (listaAreaSucursal.size() > 0) ? listaAreaSucursal.get(0) : null;
	}

	@NotifyChange({ "listaCuadreInventario", "totalStock", "totalStockReal", "totalStockDiferencia", "totalCompra",
			"totalVenta" })
	@Command
	public void guardar() {
		if (listaCuadreInventario.size() <= 1) {
			Messagebox.show("No existen elementos");
			return;
		}

		cuadreInventarioCabecera.setFechaCierre(new Timestamp(new Date().getTime()));
		cuadreInventarioCabecera.setEstado(false);
		CuadreInventarioCabecera cuadreInventarioCabeceraRef = cuadreInventarioCabeceraDAO
				.saveorUpdate(cuadreInventarioCabecera);
		// Timestamp fecha = new Timestamp(new Date().getTime());
		listaCuadreInventario.forEach(cuadreInventario -> {
			if (cuadreInventario.getProducto().getId() != null) {
				cuadreInventario.setCuadreInventarioCabecera(cuadreInventarioCabeceraRef);
				guardarCuadreInventario(cuadreInventario);
			}
		});
		limpiar();
		Clients.showNotification("Ingresado Satisfactorio ", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center",
				1500);

	}

	private void guardarCuadreInventario(CuadreInventario cuadreInventario) {
		cuadreInventario.setFecha(new Timestamp(new Date().getTime()));
		if (cuadreInventario.getCantidad() < 0) {
			cuadreInventario.setConceptoMovimiento(
					conceptoMovimientoDAO.getConceptoMovimientoById(Sistema.ID_CUADRE_INVENTARIO_SALIDA));
			cuadreInventario.setCantidad(cuadreInventario.getCantidad() * -1);
			if (cuadreInventario.getObservacion() == null) {
				cuadreInventario.setObservacion("CUADRE INVENTARIO - FALTABA");
			} else {
				cuadreInventario.setObservacion(
						cuadreInventario.getObservacion().concat("->").concat("CUADRE INVENTARIO - FALTABA"));
			}
		} else {
			cuadreInventario.setConceptoMovimiento(
					conceptoMovimientoDAO.getConceptoMovimientoById(Sistema.ID_CUADRE_INVENTARIO_ENTRADA));
			cuadreInventario.setCantidad(cuadreInventario.getCantidad());
			if (cuadreInventario.getObservacion() == null) {
				cuadreInventario.setObservacion("CUADRE INVENTARIO - SOBRABA");
			} else {
				cuadreInventario.setObservacion(
						cuadreInventario.getObservacion().concat("->").concat("CUADRE INVENTARIO - SOBRABA"));
			}
		}
		cuadreInventario.getProducto().setCantidad(cuadreInventario.getCantidadDespues());
		cuadreInventarioDAO.saveorUpdate(cuadreInventario);
	}

	@Command
	@NotifyChange({ "listaCuadreInventario", "totalStock", "totalStockReal", "totalStockDiferencia", "totalCompra",
			"totalVenta" })
	public void eliminar(@BindingParam("item") CuadreInventario cuadreInventario) {
		listaCuadreInventario.remove(cuadreInventario);
		calcularTotales();
	}

	@Command
	public void verDetalle(@BindingParam("cuadreInventario") CuadreInventario cuadreInventario) {
		if (areaSucursal == null) {
			return;
		}
		if (cuadreInventario.getProducto() == null) {
			return;
		}
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("clase", "cuadreInventario");
		parameters.put("cuadreInventario", cuadreInventario);
		Executions.createComponents("/view/formDetalleMovimientoProducto.zul", null, parameters);
	}

	@GlobalCommand
	@NotifyChange({ "listaCuadreInventario" })
	public void cargarProducto(@BindingParam("objetoProducto") Producto producto) {
		// Codigo para verificar si es un producto repetido
		listaCuadreInventario.forEach(v -> {
			if (v.getProducto().getId() == producto.getId()) {
				Messagebox.show("El producto " + producto.getDescripcion() + " ya ha sido ingresado");
				banderaProducto = true;
			}
		});

		if (banderaProducto) {
			banderaProducto = false;
			return;
		}
		agregarProducto(producto);
	}

	@Command
	@NotifyChange({ "listaCuadreInventario", "totalStock", "totalStockReal", "totalStockDiferencia", "totalCompra",
			"totalVenta" })
	public void cargarProductoCodigo(@BindingParam("item") String codigo) {
		if (codigo == null || codigo.trim().equals("")) {
			Messagebox.show("Ingresar Codigo");
			return;
		}
		if (!cargarProducto(codigo)) {
			Messagebox.show("Codigo no encontrado");
		}
	}

	@NotifyChange({ "listaCuadreInventario", "totalStock", "totalStockReal", "totalStockDiferencia", "totalCompra",
			"totalVenta" })
	@Command
	public void cargarProductoDescripcion(@BindingParam("item") String descripcion) {
		if (descripcion == null || descripcion.trim().equals("")) {
			Messagebox.show("Ingresar Nombre del producto");
			return;
		}
		if (!cargarProducto(descripcion)) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("clase", "cuadreInventario");
			parameters.put("filtroProducto", descripcion);
			parameters.put("area", areaSucursal);
			Executions.createComponents("/view/formProducto.zul", null, parameters);
		}
	}

	/*
	 * Metodos que se usa al cargar los productos desde el text de codigo y
	 * desripcion
	 */

	private boolean cargarProducto(String dato) {
		final Producto producto = productoDAO.getProductoByCodigoOrDescripcionAndAreaSucursal(dato.toUpperCase(),
				areaSucursal.getId());
		if (producto != null) {
			if (validarProductoRepetido(producto)) {
				agregarProducto(producto);
			}
		}
		return (producto != null);
	}

	private boolean validarProductoRepetido(final Producto producto) {
		banderaValidarProducto = true;
		listaCuadreInventario.forEach(cuadreInventario -> {
			if (cuadreInventario.getProducto().getId() == producto.getId()) {
				cuadreInventario.setCantidadDespues(cuadreInventario.getCantidadDespues() + 1);
				confirmar(cuadreInventario);
				listaCuadreInventario.get(0).getProducto().setCodigo("");
				listaCuadreInventario.get(0).getProducto().setDescripcion("");
				return;
			}
		});
		return banderaValidarProducto;
	}

	private void agregarProducto(Producto producto) {
		CuadreInventario c = new CuadreInventario();
		c.setProducto(producto);
		c.setCantidadAntes(producto.getCantidad());
		c.setCantidadDespues(0.0);
		c.setCuadreInventarioCabecera(verificarCuadreInventarioCabcecra());
		c.setObservacion("");
		List<CuadreInventario> listaCuadreInventarioProvisional = new ArrayList<>();
		listaCuadreInventarioProvisional.addAll(listaCuadreInventario);
		limpiar();
		listaCuadreInventario.add(c);
		listaCuadreInventarioProvisional.remove(0);
		listaCuadreInventario.addAll(listaCuadreInventarioProvisional);
	}

	@Command
	@NotifyChange({ "listaCuadreInventario", "totalStock", "totalStockReal", "totalStockDiferencia", "totalCompra",
			"totalVenta" })
	public void confirmar(@BindingParam("item") CuadreInventario cuadreInventario) {
		if (cuadreInventario.getProducto() == null) {
			Messagebox.show("Escoger un Producto");
			return;
		}
		if (cuadreInventario.getCantidadDespues() < 0) {
			Messagebox.show("Escoger Ingresar Stock");
			return;
		}
		cuadreInventario.setCantidad(cuadreInventario.getCantidadDespues() - cuadreInventario.getCantidadAntes());
		cuadreInventario.setValorCompra(Sistema.redondear(cuadreInventario.getProducto().getPrecioCompra()));
		cuadreInventario.setValorVenta(Sistema.redondear(cuadreInventario.getProducto().getPrecioVenta()));

		cuadreInventario.setTotalCompra(
				Sistema.redondear(cuadreInventario.getProducto().getPrecioCompra() * cuadreInventario.getCantidad()));
		cuadreInventario.setTotalVenta(
				Sistema.redondear(cuadreInventario.getProducto().getPrecioVenta() * cuadreInventario.getCantidad()));
		calcularTotales();
	}

	private void calcularTotales() {
		totalStock = 0;
		totalStockReal = 0;
		totalStockDiferencia = 0;
		totalCompra = 0;
		totalVenta = 0;
		listaCuadreInventario.forEach(x -> {
			totalStock += x.getCantidadAntes();
			totalStockReal += x.getCantidadDespues();
			totalStockDiferencia += x.getCantidad();
			totalCompra += x.getValorCompra();
			totalVenta += x.getValorVenta();
		});
		totalStock = Sistema.redondear(totalStock);
		totalStockReal = Sistema.redondear(totalStockReal);
		totalStockDiferencia = Sistema.redondear(totalStockDiferencia);
		totalCompra = Sistema.redondear(totalCompra);
		totalVenta = Sistema.redondear(totalVenta);
	}

	private CuadreInventarioCabecera verificarCuadreInventarioCabcecra() {
		if (cuadreInventarioCabecera == null) {
			cuadreInventarioCabecera = new CuadreInventarioCabecera();
			cuadreInventarioCabecera.setEstado(true);
			cuadreInventarioCabecera.setFechaApertura(new Timestamp(new Date().getTime()));
			cuadreInventarioCabecera.setAreaSucursal(areaSucursal);
		}
		return cuadreInventarioCabecera;
	}

	@NotifyChange("listaCuadreInventario")
	@Command
	public void aceptar() {
		limpiar();
		listaProducto = productoDAO.getListaProductoByAreaSucursal(Sistema.ESTADO_INGRESADO, areaSucursal.getId());
		listaProducto.forEach(x -> {
			CuadreInventario c = cuadreInventarioDAO.getCuadreInventarioByProductoAndEstado(x.getId(), true);
			if (c == null) {
				c = new CuadreInventario();
				c.setProducto(x);
				c.setCuadreInventarioCabecera(verificarCuadreInventarioCabcecra());
			}
			c.setCantidadAntes(x.getCantidad());
			listaCuadreInventario.add(c);
		});
	}

	@NotifyChange("listaCuadreInventario")
	@Command
	public void limpiar() {
		listaCuadreInventario.clear();
		CuadreInventario c = new CuadreInventario();
		c.setProducto(new Producto());
		listaCuadreInventario.add(c);
	}

	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public List<AreaSucursal> getListaAreaSucursal() {
		return listaAreaSucursal;
	}

	public void setListaAreaSucursal(List<AreaSucursal> listaAreaSucursal) {
		this.listaAreaSucursal = listaAreaSucursal;
	}

	public AreaSucursal getAreaSucursal() {
		return areaSucursal;
	}

	public void setAreaSucursal(AreaSucursal areaSucursal) {
		this.areaSucursal = areaSucursal;
	}

	public List<CuadreInventario> getListaCuadreInventario() {
		return listaCuadreInventario;
	}

	public void setListaCuadreInventario(List<CuadreInventario> listaCuadreInventario) {
		this.listaCuadreInventario = listaCuadreInventario;
	}

	public double getTotalStock() {
		return totalStock;
	}

	public void setTotalStock(double totalStock) {
		this.totalStock = totalStock;
	}

	public double getTotalStockReal() {
		return totalStockReal;
	}

	public void setTotalStockReal(double totalStockReal) {
		this.totalStockReal = totalStockReal;
	}

	public double getTotalStockDiferencia() {
		return totalStockDiferencia;
	}

	public void setTotalStockDiferencia(double totalStockDiferencia) {
		this.totalStockDiferencia = totalStockDiferencia;
	}

	public double getTotalCompra() {
		return totalCompra;
	}

	public void setTotalCompra(double totalCompra) {
		this.totalCompra = totalCompra;
	}

	public double getTotalVenta() {
		return totalVenta;
	}

	public void setTotalVenta(double totalVenta) {
		this.totalVenta = totalVenta;
	}

}
