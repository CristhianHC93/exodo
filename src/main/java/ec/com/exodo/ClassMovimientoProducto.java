package ec.com.exodo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.dao.ie.AreaSucursalDAO;
import ec.com.exodo.dao.ie.ConceptoMovimientoDAO;
import ec.com.exodo.dao.ie.CuadreInventarioDAO;
import ec.com.exodo.dao.ie.ProductoDAO;
import ec.com.exodo.entity.AreaSucursal;
import ec.com.exodo.entity.ConceptoMovimiento;
import ec.com.exodo.entity.CuadreInventario;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.Sucursal;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassMovimientoProducto {

	private String selectCombo = getComboConceptoMovimiento().get(0);
	private List<ConceptoMovimiento> listaConceptoMovimiento;

	private List<Sucursal> listaSucursal;
	private List<AreaSucursal> listaAreaSucursal;
	private Sucursal sucursal;
	private AreaSucursal areaSucursal;

	// Bandera para ver que productos estan repetidos
	boolean banderaProducto = false;

	private boolean banderaValidarProducto = true;

	private ProductoDAO productoDAO = InstanciaDAO.getInstanciaProductoDAO();
	private AreaSucursalDAO areaSucursalDAO = InstanciaDAO.getInstanciaAreaSucursalDAO();
	private ConceptoMovimientoDAO conceptoMovimientoDAO = InstanciaDAO.getInstanciaConceptoMovimientoDAO();
	private CuadreInventarioDAO cuadreInventarioDAO = InstanciaDAO.getInstanciaCuadreInventarioDAO();

	private List<CuadreInventario> listaKardex = new ArrayList<>();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		listaSucursal = Sistema.getListaSucursal();
		sucursal = (listaSucursal.size() > 0) ? listaSucursal.get(0) : null;
		selectSucursal();
		select();
		limpiar();
	}

	@NotifyChange("listaConceptoMovimiento")
	@Command
	public void select() {
		if (selectCombo.equals("SALIDA")) {
			listaConceptoMovimiento = conceptoMovimientoDAO.getListaConceptoMovimiento(Sistema.CODIGO_SALIDA, true);
		}
		if (selectCombo.equals("ENTRADA")) {
			listaConceptoMovimiento = conceptoMovimientoDAO.getListaConceptoMovimiento(Sistema.CODIGO_ENTRADA, true);
		}
	}

	@NotifyChange({ "listaArea", "area" })
	@Command
	public void selectSucursal() {
		listaAreaSucursal = areaSucursalDAO.getListaAreaSucursalBySucursalActivo(sucursal.getId());
		areaSucursal = (listaAreaSucursal.size() > 0) ? listaAreaSucursal.get(0) : null;
	}

	@NotifyChange("listaKardex")
	@Command
	public void selectArea() {
		limpiar();
	}

	private void limpiar() {
		listaKardex.clear();
		CuadreInventario k = new CuadreInventario();
		k.setProducto(new Producto());
		listaKardex.add(k);
	}

	@NotifyChange({ "listaKardex", "totalStock", "totalStockReal", "totalStockDiferencia", "totalCompra",
			"totalVenta" })
	@Command
	public void guardar() {
		if (listaKardex.size() <= 1) {
			Messagebox.show("No existen elementos");
			return;
		}
		Timestamp fecha = new Timestamp(new Date().getTime());
		listaKardex.forEach(x -> {
			if (x.getProducto().getId() != null) {
				x.setFecha(fecha);
				x.getProducto().setCantidad(x.getCantidadDespues());
				cuadreInventarioDAO.saveorUpdate(x);
			}
		});
		limpiar();
		Clients.showNotification("Ingresado Satisfactorio ", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center",
				1500);
	}

	@GlobalCommand
	@NotifyChange({ "listaKardex" })
	public void cargarProducto(@BindingParam("objetoProducto") Producto producto) {
		// Codigo para verificar si es un producto repetido
		listaKardex.forEach(v -> {
			if (v.getProducto().getId() == producto.getId()) {
				Messagebox.show("El producto " + producto.getDescripcion() + " ya ha sido ingresado");
				banderaProducto = true;
			}
		});

		if (banderaProducto) {
			banderaProducto = false;
			return;
		}
		agregarProducto(producto);
	}

	@Command
	@NotifyChange({ "listaKardex" })
	public void cargarProductoCodigo(@BindingParam("item") String codigo) {
		if (codigo == null || codigo.trim().equals("")) {
			Messagebox.show("Ingresar Codigo");
			return;
		}
		if (!cargarProducto(codigo)) {
			Messagebox.show("Codigo no encontrado");
		}
	}

	@NotifyChange({ "listaKardex" })
	@Command
	public void cargarProductoDescripcion(@BindingParam("item") String descripcion) {
		if (descripcion == null || descripcion.trim().equals("")) {
			Messagebox.show("Ingresar Nombre del producto");
			return;
		}
		if (!cargarProducto(descripcion)) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("clase", "kardex");
			parameters.put("filtroProducto", descripcion);
			parameters.put("area", areaSucursal);
			Executions.createComponents("/view/formProducto.zul", null, parameters);

		}
	}

	/*
	 * Metodos que se usa al cargar los productos desde el text de codigo y
	 * desripcion
	 */

	private boolean cargarProducto(String dato) {
		final Producto producto = productoDAO.getProductoByCodigoOrDescripcionAndAreaSucursal(dato.toUpperCase(),
				areaSucursal.getId());
		if (producto != null) {
			if (validarProductoRepetido(producto)) {
				agregarProducto(producto);
			}
		}
		return (producto != null);
	}

	private boolean validarProductoRepetido(final Producto producto) {
		banderaValidarProducto = true;
		listaKardex.forEach(kardex -> {
			if (kardex.getProducto().getId() == producto.getId()) {
				kardex.setCantidadDespues(kardex.getCantidadDespues() + 1);
				confirmar(kardex);
				listaKardex.get(0).getProducto().setCodigo("");
				listaKardex.get(0).getProducto().setDescripcion("");
				return;
			}
		});
		return banderaValidarProducto;
	}

	private void agregarProducto(Producto producto) {
		CuadreInventario k = new CuadreInventario();
		k.setProducto(producto);
		k.setCantidadAntes(producto.getCantidad());
		k.setCantidad(0);
		List<CuadreInventario> listaKardexProvisional = new ArrayList<>();
		listaKardexProvisional.addAll(listaKardex);
		limpiar();
		listaKardex.add(k);
		listaKardexProvisional.remove(0);
		listaKardex.addAll(listaKardexProvisional);
	}

	@Command
	@NotifyChange({ "listaKardex" })
	public void confirmar(@BindingParam("item") CuadreInventario kardex) {
		if (kardex.getProducto().getId() == null) {
			Messagebox.show("Escoger un Producto");
			return;
		}
		if (kardex.getCantidad() < 0) {
			Messagebox.show("Escoger Ingresar Cantidad");
			return;
		}
		if (selectCombo.equals("SALIDA")) {
			kardex.setCantidadDespues(kardex.getCantidadAntes() - kardex.getCantidad());
		}
		if (selectCombo.equals("ENTRADA")) {
			kardex.setCantidadDespues(kardex.getCantidadAntes() + kardex.getCantidad());
		}

	}

	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public final List<String> getComboConceptoMovimiento() {
		return Sistema.getListaConceptoMovimiento();
	}

	public List<Sucursal> getListaSucursal() {
		return listaSucursal;
	}

	public void setListaSucursal(List<Sucursal> listaSucursal) {
		this.listaSucursal = listaSucursal;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public List<CuadreInventario> getListaKardex() {
		return listaKardex;
	}

	public void setListaKardex(List<CuadreInventario> listaKardex) {
		this.listaKardex = listaKardex;
	}

	public String getSelectCombo() {
		return selectCombo;
	}

	public void setSelectCombo(String selectCombo) {
		this.selectCombo = selectCombo;
	}

	public List<ConceptoMovimiento> getListaConceptoMovimiento() {
		return listaConceptoMovimiento;
	}

	public void setListaConceptoMovimiento(List<ConceptoMovimiento> listaConceptoMovimiento) {
		this.listaConceptoMovimiento = listaConceptoMovimiento;
	}

	public List<AreaSucursal> getListaAreaSucursal() {
		return listaAreaSucursal;
	}

	public void setListaAreaSucursal(List<AreaSucursal> listaAreaSucursal) {
		this.listaAreaSucursal = listaAreaSucursal;
	}

	public AreaSucursal getAreaSucursal() {
		return areaSucursal;
	}

	public void setAreaSucursal(AreaSucursal areaSucursal) {
		this.areaSucursal = areaSucursal;
	}

}
