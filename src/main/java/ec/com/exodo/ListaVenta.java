package ec.com.exodo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.VentaCabeceraDAO;
import ec.com.exodo.entity.AreaSucursal;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.entity.VentaCabecera;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ListaVenta {
	@Wire("#winListaVenta")
	private Window winListaVenta;
	
	private boolean buscarVisible = true;

	private VentaCabecera ventaCabecera;
	private List<VentaCabecera> listaVentaCabecera;
	VentaCabeceraDAO ventaCabeceraDAO = InstanciaDAO.getInstanciaVentaCabeceraDAO();
	int idEstado = 0;

	String var = "";

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("estado") Estado estado,
			@ExecutionArgParam("var") String var, @ExecutionArgParam("areaSucursal") AreaSucursal areaSucursal) {
		Selectors.wireComponents(view, this, false);
		listaVentaCabecera = ventaCabeceraDAO.getListaVentaCabecera(estado.getId());
		idEstado = estado.getId();
		if (var == null)
			var = "null";
		this.var = var;
	}

	@Command
	@NotifyChange("buscarVisible")
	public void cambioFecha(@BindingParam("selectCombo") String selectCombo) {
		buscarVisible = selectCombo.trim()
				.equals(getIdioma().getCodigo().concat("/").concat(getIdioma().getClienteMin()));
	}

	@Command
	@NotifyChange("listaVentaCabecera")
	public void filtrarDato(@BindingParam("dato") String datoBusqueda) {
		List<VentaCabecera> listaVentaCabeceraProvicional = ventaCabeceraDAO.getListaVentaCabecera(idEstado);
		listaVentaCabecera.clear();
		if (datoBusqueda.trim().equals(""))
			listaVentaCabecera.addAll(listaVentaCabeceraProvicional);
		else
			listaVentaCabeceraProvicional.stream().filter(logicaFiltro(datoBusqueda)).forEach(listaVentaCabecera::add);
	}

	public void filtrarFecha(@BindingParam("dato") Date fechaBusqueda) {
		List<VentaCabecera> listaVentaCabeceraProvicional = ventaCabeceraDAO.getListaVentaCabecera(idEstado);
		listaVentaCabecera.clear();
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		listaVentaCabeceraProvicional.stream().filter(filtroFecha(fechaBusqueda, sdf)).forEach(listaVentaCabecera::add);
	}

	private Predicate<VentaCabecera> filtroFecha(Date fechaBusqueda, final SimpleDateFormat sdf) {
		return item -> (sdf.format(fechaBusqueda).toString().equals(sdf.format(item.getFechaEmision()).toString()));
	}

	private Predicate<VentaCabecera> logicaFiltro(final String datoBusqueda) {
		return item -> (item.getCodigo().trim().toLowerCase().contains(datoBusqueda.trim().toLowerCase())
				|| item.getCliente().getNombre().trim().toLowerCase().contains(datoBusqueda.trim().toLowerCase()));
	}

	@Command
	public void salir() {
		winListaVenta.detach();
	}

	@Command
	@NotifyChange({ "ventaCabecera", "listaVentaCabecera" })
	public void aceptar(final @BindingParam("item") VentaCabecera ventaCabecera) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objetoVenta", ventaCabecera);
		parametros.put("var", this.var);
		BindUtils.postGlobalCommand(null, null, "cargarVenta", parametros);
		salir();
	}

	public String getFooter() {
		return String.format(ParametroZul.getInstancia().getFooterMessage(), listaVentaCabecera.size());
	}

	/*
	 * Llenar comboBox para buscar
	 */
	public final List<String> getComboBusqueda() {
		return Sistema.getComboVenta();
	}

	/*
	 * Metodos GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public boolean isBuscarVisible() {
		return buscarVisible;
	}

	public void setBuscarVisible(boolean buscarVisible) {
		this.buscarVisible = buscarVisible;
	}

	public VentaCabecera getVentaCabecera() {
		return ventaCabecera;
	}

	public void setVentaCabecera(VentaCabecera ventaCabecera) {
		this.ventaCabecera = ventaCabecera;
	}

	public List<VentaCabecera> getListaVentaCabecera() {
		return listaVentaCabecera;
	}

	public void setListaVentaCabecera(List<VentaCabecera> listaVentaCabecera) {
		this.listaVentaCabecera = listaVentaCabecera;
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}
}
