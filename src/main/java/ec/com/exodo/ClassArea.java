package ec.com.exodo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.AreaDAO;
import ec.com.exodo.dao.ie.AreaSucursalDAO;
import ec.com.exodo.entity.Area;
import ec.com.exodo.entity.AreaSucursal;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassArea {
	@Wire("#winArea")
	private Window winArea;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private boolean estadoRoot = false;

	private List<Area> listaArea;

	private AreaDAO areaDAO = InstanciaDAO.getInstanciaAreaDAO();
	private AreaSucursalDAO areaSucursalDAO = InstanciaDAO.getInstanciaAreaSucursalDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(view, this, false);
		ajustarPantalla(clase);
		listaArea = areaDAO.getListaArea();
		/* Verifica si el usuario es root o admin */
		if (Sistema.getUsuario().getRol().getId() == Sistema.ID_ROL_ROOT
				|| Sistema.getUsuario().getRol().getId() == Sistema.ID_ROL_ADMIN) {
			estadoRoot = true;
		}
	}

	private void ajustarPantalla(String clase) {
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (clase.equals("Venta") || clase.equals("Producto")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "85%";
		}
	}

	@Command
	@NotifyChange("listaArea")
	public void agregarNuevo() {
		listaArea.add(new Area());
	}

	@Command
	@NotifyChange("listaArea")
	public void eliminar(final @BindingParam("area") Area area) {
		if (area.getId() == null) {
			listaArea.remove(area);
		} else {
			Messagebox.show(getIdioma().getMensajeEliminar(area.getDescripcion()), "Confirm",
					Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, event -> {
						if (((Integer) event.getData()).intValue() == Messagebox.OK) {
							procesoEliminar(areaDAO.eliminar(area));
						}
					});
		}
	}

	private void procesoEliminar(int a) {
		if (a == 1) {
			Clients.showNotification(getIdioma().getRegistroEliminadoSatifactoriamente(),
					Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
			BindUtils.postNotifyChange(null, null, this, "listaArea");
			nuevo();
		} else {
			Clients.showNotification(getIdioma().getImposibleEliminarRegistro(), Clients.NOTIFICATION_TYPE_ERROR, null,
					"top_center", 2000);
			BindUtils.postNotifyChange(null, null, this, "listaArea");
		}
	}

	private void nuevo() {
		listaArea.clear();
		listaArea = areaDAO.getListaArea();
	}

	@Command
	public void aceptar(final @BindingParam("area") Area area) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", area);
		BindUtils.postGlobalCommand(null, null, "cargarArea", parametros);
		salir();
	}

	@Command
	@NotifyChange("listaArea")
	public void filtrar(@BindingParam("dato") String datoBusqueda) {
		listaArea.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaArea.addAll(areaDAO.getListaArea());
		} else {
			areaDAO.getListaArea().stream().filter(filter(datoBusqueda)).forEach(listaArea::add);
		}
	}

	private Predicate<Area> filter(String datoBusqueda) {
		return dato -> dato.getDescripcion().toUpperCase().trim().contains(datoBusqueda.toUpperCase().trim());
	}

	@Command
	@NotifyChange("listaArea")
	public void guardarEditar(final @BindingParam("area") Area area) {
		if (validarGuardarEditar(area)) {
			AreaSucursal areaSucursal = (area.getId() != null)
					? areaSucursalDAO.getAreaSucursalByAreaAndSucursal(area.getId(), Sistema.getSucursal().getId())
					: new AreaSucursal();
			if (guardarAreaSucursal(areaSucursal, area)) {
				nuevo();
				Clients.showNotification(
						"Area " + ((area.getId() == null) ? "ingresado" : "modificado") + " correctamente",
						Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
			} else {
				Clients.showNotification(getIdioma().getMensajeImposibleRealizarAccion(area.getDescripcion()),
						Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center", 1500);
			}
		}
	}

	/* Metodo para guardar y editar AreaSucural con la respectiva Area */
	private boolean guardarAreaSucursal(AreaSucursal areaSucursal, final Area area) {
		boolean devolver = true;
		if (areaSucursal != null) {
			areaSucursal.setArea(areaDAO.saveorUpdate(area));
			areaSucursal.setSucursal(Sistema.getSucursal());
			areaSucursal.setEstado(true);
			areaSucursal = areaSucursalDAO.saveorUpdate(areaSucursal);
			if (areaSucursal == null) {
				devolver = false;
			}
		} else {
			devolver = false;
		}
		return devolver;
	}

	private boolean validarGuardarEditar(final Area area) {
		boolean devolver = true;
		if (area.getDescripcion().equals("") || area.getDescripcion().isEmpty()) {
			Messagebox.show(getIdioma().getIngresarDescripcion());
			devolver = false;
		} else {
			area.setDescripcion(area.getDescripcion().toUpperCase().trim());
		}

		if (Sistema.validarRepetido(areaDAO.getListaArea(), area)) {
			devolver = false;
		}
		return devolver;
	}

	@Command
	public void salir() {
		winArea.detach();
	}

	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public List<Area> getListaArea() {
		return listaArea;
	}

	public void setListaArea(List<Area> listaArea) {
		this.listaArea = listaArea;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public boolean isEstadoRoot() {
		return estadoRoot;
	}

	public void setEstadoRoot(boolean estadoRoot) {
		this.estadoRoot = estadoRoot;
	}
}
