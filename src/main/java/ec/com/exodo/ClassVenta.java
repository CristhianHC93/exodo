package ec.com.exodo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.dao.ie.CajaDAO;
import ec.com.exodo.dao.ie.CajaDetalleDAO;
import ec.com.exodo.dao.ie.ClienteDAO;
import ec.com.exodo.dao.ie.ConceptoMovimientoDAO;
import ec.com.exodo.dao.ie.CuadreInventarioDAO;
import ec.com.exodo.dao.ie.PrecioDAO;
import ec.com.exodo.dao.ie.ProductoDAO;
import ec.com.exodo.dao.ie.VentaCabeceraDAO;
import ec.com.exodo.dao.ie.VentaDetalleDAO;
import ec.com.exodo.entity.AreaSucursal;
import ec.com.exodo.entity.Caja;
import ec.com.exodo.entity.CajaDetalle;
import ec.com.exodo.entity.Cliente;
import ec.com.exodo.entity.ConceptoMovimiento;
import ec.com.exodo.entity.CuadreInventario;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.VentaCabecera;
import ec.com.exodo.entity.VentaDetalle;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.models.Modelos;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassVenta {

	private Double subtotal = 0.00;
	private Double total = 0.00;
	private Double iva = 0.00;
	private Double valorTotal = 0.00;
	private Double compensacion = 0.00;

	private Double valorDevolver = 0.00;
	private Double valorAcreditar = 0.00;

	private VentaCabecera ventaCabecera;

	private boolean estadoGuardar = true;
	private boolean estadoAnular = true;
	boolean estadoModificar = false;
	private String txtBuscar;
	private List<VentaDetalle> listaVentaDetalleRemovidos = new ArrayList<>();

	private String tipo = "entregarVenta";
	private String tipoImpresion = "ticket";
	private boolean btBuscar = true;
	private boolean btProducto = true;
	private boolean estadoBT = false;

	private boolean banderaValidarProducto = true;

	// Bandera para ver que productos estan repetidos
	// boolean banderaProducto = false;

	double totalAnterior;

	// variable de bindeo que servira para verificar si se desea o no enviar
	// correo
	private boolean estadoCorreo = false;

	private List<Modelos<VentaDetalle, Object>> listaVenta = new ArrayList<>();

	boolean estadoVerificarSucursal = false;

	private VentaCabeceraDAO ventaCabeceraDAO = InstanciaDAO.getInstanciaVentaCabeceraDAO();
	private VentaDetalleDAO ventaDetalleDAO = InstanciaDAO.getInstanciaVentaDetalleDAO();
	private ProductoDAO productoDAO = InstanciaDAO.getInstanciaProductoDAO();
	private CuadreInventarioDAO cuadreInventarioDAO = InstanciaDAO.getInstanciaCuadreInventarioDAO();
	private CajaDAO cajaDAO = InstanciaDAO.getInstanciaCajaDAO();
	private CajaDetalleDAO cajaDetalleDAO = InstanciaDAO.getInstanciaCajaDetalleDAO();
	private ConceptoMovimientoDAO conceptoMovimientoDAO = InstanciaDAO.getInstanciaConceptoMovimientoDAO();
	private ClienteDAO clienteDAO = InstanciaDAO.getInstanciaClienteDAO();
	private PrecioDAO precioDAO = InstanciaDAO.getInstanciaPrecioDAO();

	private Caja caja;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("ventaCabecera") VentaCabecera ventaCabecera,
			@ExecutionArgParam("listaVenta") List<Modelos<VentaDetalle, Object>> listaVenta) {
		Selectors.wireComponents(view, this, false);
		if (ventaCabecera != null) {
			this.ventaCabecera = ventaCabecera;
			this.listaVenta = new ArrayList<>(listaVenta.size());
			this.listaVenta.addAll(listaVenta);
		} else {
			caja = cajaDAO.getCaja(1, true);
			if (caja == null) {
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("clase", ClassVenta.class.getSimpleName());
				parameters.put("area", new AreaSucursal(1));
				Executions.createComponents("/view/formAbrirCaja.zul", null, parameters);
			}
			nuevo();
		}
	}

	@Command
	public void agregarCliente() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("clase", "Venta");
		Executions.createComponents("/view/formCliente.zul", null, parameters);
	}

	@NotifyChange("ventaCabecera")
	@Command
	public void agregarClienteCtrl(@BindingParam("cedula") String cedula) {
		if (cedula.equals("")) {
			Messagebox.show("Ingresar cedula");
			return;
		}
		final Cliente cliente = clienteDAO.getClienteByCedula(cedula);
		if (cliente != null) {
			ventaCabecera.setCliente(cliente);
		} else {
			Messagebox.show("Cliente no encontrado");
		}
	}

	@GlobalCommand
	@NotifyChange("ventaCabecera")
	public void cargarCliente(@BindingParam("objetoCliente") Cliente cliente) {
		ventaCabecera.setCliente(cliente);
	}

	@Command
	@NotifyChange("listaVenta")
	public void agregarProducto() {
		if (listaVenta.size() > 1 && listaVenta.get(listaVenta.size() - 1).getObjetoT().getCantidad() == 0) {
			Messagebox.show("El último registro no tiene Cantidad");
			return;
		}
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("clase", ClassVenta.class.getSimpleName());
		parameters.put("filtroProducto", "");
		Executions.createComponents("/view/formProducto.zul", null, parameters);
	}

	@GlobalCommand
	@NotifyChange({ "valorDevolver", "valorAcreditar", "listaVenta", "total", "subtotal", "iva", "valorTotal",
			"compensacion", "estadoGuardar", "estadoModificar", "btGuardarEditar" })
	public void cargarProducto(@BindingParam("objetoProducto") Producto producto) {
		banderaValidarProducto = true;
		// Codigo para verificar si es un producto repetido
		listaVenta.forEach(v -> {
			if (v.getObjetoT().getProducto().getId() == producto.getId()) {
				Messagebox.show("El producto " + producto.getDescripcion() + " ya ha sido ingresado");
				banderaValidarProducto = false;
			}
		});

		if (banderaValidarProducto) {
			agregarProducto(producto);
		}
	}

	@Command
	@NotifyChange({ "estadoGuardar", "valorDevolver", "valorAcreditar", "listaVenta", "total", "subtotal", "iva",
			"valorTotal", "compensacion", "estadoModificar", "btGuardarEditar" })
	public void cargarProductoCodigo(@BindingParam("item") String codigo) {
		if (codigo == null || codigo.trim().equals("")) {
			Messagebox.show("Ingresar Codigo");
			return;
		}
		if (!cargarProducto(codigo)) {
			Messagebox.show("Codigo no encontrado");
		}
	}

	@NotifyChange({ "listaVenta", "estadoGuardar", "valorDevolver", "valorAcreditar", "total", "subtotal", "iva",
			"valorTotal", "compensacion", "estadoModificar", "btGuardarEditar" })
	@Command
	public void cargarProductoDescripcion(@BindingParam("item") String descripcion) {
		if (descripcion == null || descripcion.trim().equals("")) {
			Messagebox.show("Ingresar Nombre del producto");
			return;
		}
		if (!cargarProducto(descripcion)) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("clase", ClassVenta.class.getSimpleName());
			parameters.put("filtroProducto", descripcion);
			Executions.createComponents("/view/formProducto.zul", null, parameters);
		}
	}

	private boolean cargarProducto(String dato) {
		final Producto producto = productoDAO.getProductoByCodigoOrDescripcion(dato.toUpperCase());
		if (producto != null) {
			if (validarProductoCantidadAndRepetido(producto)) {
				agregarProducto(producto);
			}
		}
		return (producto != null);
	}

	private boolean validarProductoCantidadAndRepetido(final Producto producto) {
		banderaValidarProducto = true;
		if (producto.getCantidad() <= 0) {
			Messagebox.show("No existe producto en inventario");
			banderaValidarProducto = false;
		}
		if (banderaValidarProducto) {
			listaVenta.forEach(venta -> {
				if (venta.getObjetoT().getProducto().getId() == producto.getId()) {
					venta.getObjetoT().setCantidad(venta.getObjetoT().getCantidad() + 1);
					confirmar(venta);
					listaVenta.get(0).getObjetoT().getProducto().setCodigo("");
					listaVenta.get(0).getObjetoT().getProducto().setDescripcion("");
					banderaValidarProducto = false;
				}
			});
		}
		return banderaValidarProducto;
	}

	private void agregarProducto(Producto producto) {
		VentaDetalle vd = getVentaDetalleNuevo();
		vd.setProducto(producto);
		vd.setCantidad(1);
		vd.getProducto().setPrecios(precioDAO.getListaByProducto(producto.getId()));
		if (vd.getProducto().getPrecios().size() > 0) {
			vd.setPrecioVenta(producto.getPrecios().get(0).getValor());
		}

		List<Modelos<VentaDetalle, Object>> listaVentaProvisional = new ArrayList<>();
		listaVentaProvisional.addAll(listaVenta);
		listaVenta.clear();
		Cliente c = ventaCabecera.getCliente();
		nuevo();
		ventaCabecera.setCliente(c);
		listaVenta.add(new Modelos<VentaDetalle, Object>(vd, null, false));
		listaVentaProvisional.remove(0);
		listaVenta.addAll(listaVentaProvisional);
		confirmar(listaVenta.get(1));
	}

	@Command
	public void buscarThis() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("estado", getEstado());
		parameters.put("var", "this");
		Executions.createComponents("/view/formVentaLista.zul", null, parameters);
	}

	@Command
	public void buscar() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("estado", new Estado(Sistema.ESTADO_ORDEN_VENTA));
		Executions.createComponents("/view/formVentaLista.zul", null, parameters);
	}

	@GlobalCommand
	@NotifyChange({ "estadoAnular", "estadoBT", "btGuardarEditar", "valorTotal", "compensacion", "total", "subtotal",
			"iva", "ventaCabecera", "listaVenta", "estadoGuardar" })
	public void cargarVenta(@BindingParam("objetoVenta") VentaCabecera ventaCabecera, @BindingParam("var") String var) {
		this.ventaCabecera = ventaCabecera;
		total = this.ventaCabecera.getTotal();
		totalAnterior = total;
		subtotal = this.ventaCabecera.getSubtotal();
		compensacion = this.ventaCabecera.getCompensacion();
		valorTotal = this.total - compensacion;
		iva = this.ventaCabecera.getIva();
		listaVenta.clear();
		List<VentaDetalle> listaVentaDetalle;
		if (var.trim().equals("this")) {
			estadoAnular = false;
			estadoBT = true;
			estadoGuardar = true;
			listaVentaDetalle = ventaDetalleDAO.getListaVentaDetalleByCabecera(ventaCabecera.getId(),
					getEstado().getId());
			listaVentaDetalle.forEach(venta -> {
				venta.setEstado(getEstado());
				listaVenta.add(new Modelos<VentaDetalle, Object>(venta, null, true));
			});
		} else {
			estadoAnular = true;
			estadoModificar = true;
			estadoGuardar = false;
			listaVentaDetalle = ventaDetalleDAO.getListaVentaDetalleByCabecera(ventaCabecera.getId(),
					Sistema.ESTADO_ORDEN_VENTA);
			listaVenta.add(new Modelos<VentaDetalle, Object>(getVentaDetalleNuevo(), null, true));
			for (VentaDetalle nota : listaVentaDetalle) {
				nota.setEstado(getEstado());
				listaVenta.add(new Modelos<VentaDetalle, Object>(nota, null, false));
			}
		}
	}

	@Command
	@NotifyChange({ "estadoAnular", "estadoBT", "btGuardarEditar", "listaVenta", "ventaCabecera", "total", "subtotal",
			"iva", "estadoGuardar", "valorAcreditar", "valorDevolver", "valorTotal", "compensacion" })
	public void anular() {
		ConceptoMovimiento cm = new ConceptoMovimiento();
		cm.setId(Sistema.ID_ANULAR_VENTA);
		estadoAnular = true;
		final Estado estado = new Estado(Sistema.ESTADO_ANULADO);
		ventaCabecera.setEstado(estado);
		ventaCabecera.setUsuario(Sistema.getUsuario());
		ventaCabeceraDAO.saveorUpdate(ventaCabecera);
		estadoGuardar = true;
		for (Modelos<VentaDetalle, Object> detalle : listaVenta) {
			Producto producto = productoDAO.getProducto(detalle.getObjetoT().getProducto().getId());
			guardarCuadreInventario(producto, detalle.getObjetoT());
			detalle.getObjetoT().setEstado(estado);
			ventaDetalleDAO.saveorUpdate(detalle.getObjetoT());
		}
		nuevo();
		Clients.showNotification("El registro ha sido anulado", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center",
				1500);
		BindUtils.postNotifyChange(null, null, ClassVenta.this, "estadoAnular");
	}

	private void guardarCuadreInventario(Producto producto, VentaDetalle ventaDetalle) {
		CuadreInventario cuadreInventario = new CuadreInventario();
		cuadreInventario.setValorVenta(Sistema.redondear(ventaDetalle.getPrecioVenta()) * -1);
		cuadreInventario.setTotalVenta(Sistema.redondear(ventaDetalle.getPrecioSubtotal() * -1));
		cuadreInventario.setFecha(new Timestamp(new Date().getTime()));
		cuadreInventario
				.setConceptoMovimiento(conceptoMovimientoDAO.getConceptoMovimientoById(Sistema.ID_ANULAR_VENTA));
		cuadreInventario.setCantidadAntes(producto.getCantidad());
		cuadreInventario.setCantidad(ventaDetalle.getCantidad());
		cuadreInventario.setCantidadDespues(producto.getCantidad() + ventaDetalle.getCantidad());
		producto.setCantidad(cuadreInventario.getCantidadDespues());
		cuadreInventario.setProducto(producto);
		cuadreInventario
				.setObservacion("Anulacion Venta de " + ventaDetalle.getCantidad() + " " + producto.getDescripcion());
		cuadreInventarioDAO.saveorUpdate(cuadreInventario);
	}

	@Command
	@NotifyChange({ "estadoAnular", "tipo", "btBuscar", "btProducto", "listaCompra", "compraCabecera", "estadoGuardar",
			"total", "subtotal", "iva", "btGuardarEditar", "listaVenta", "ventaCabecera", "txtBuscar", "valorAcreditar",
			"valorDevolver" })
	public void select() {
		if (tipo.trim().equals("ordenVenta")) {
			btBuscar = false;
			btProducto = true;
		}
		if (tipo.trim().equals("cobrarVenta")) {
			btBuscar = true;
			btProducto = false;
			txtBuscar = "Buscar Orden Compra";
		}
		if (tipo.trim().equals("entregarVenta")) {
			btBuscar = true;
			btProducto = false;
			txtBuscar = "Buscar Cobrar Venta";
		}
		if (tipo.trim().equals("planVenta")) {
			btBuscar = true;
			btProducto = false;
			txtBuscar = "Buscar Plan Venta";
		}
		// nuevo();
	}

	@Command
	@NotifyChange({ "valorDevolver", "valorAcreditar", "listaVenta", "total", "subtotal", "iva", "valorTotal",
			"compensacion", "estadoGuardar", "estadoModificar", "btGuardarEditar" })
	public void confirmar(@BindingParam("item") Modelos<VentaDetalle, Object> venta) {
		if (venta.getObjetoT().getProducto() == null) {
			Messagebox.show("Favor escoger codigo o agregar producto");
			return;
		}
		if (venta.getObjetoT().getCantidad() == 0) {
			Messagebox.show("Debe ingresar una cantidad");
			venta.getObjetoT().setCantidad(1);
			return;
		}
		if (venta.getObjetoT().getProducto().getCantidad() < venta.getObjetoT().getCantidad()) {
			Messagebox.show("No hay suficiente pedido en el inventario");
			venta.getObjetoT().setCantidad(venta.getObjetoT().getProducto().getCantidad());
		}
		calcularListatotal(listaVenta);
		if (ventaCabecera.getId() == null) {
			estadoGuardar = false;
		} else {
			estadoModificar = true;
			estadoGuardar = false;
			valores(ventaCabecera);
		}
		if (venta.getObjetoT().getCantidad() == 0 && listaVenta.size() == 1) {
			estadoGuardar = true;
		}
	}

	public void calcularListatotal(List<Modelos<VentaDetalle, Object>> listaVenta) {
		double ivaTT = 0.0;
		double compensacionTT = 0.0;
		double subt = 0.0;
		double subTT = 0.0;
		double subtIva = 0.0;
		// double descuento = 0.0;
		if (listaVenta.size() > 0) {
			for (Modelos<VentaDetalle, Object> item : listaVenta) {
				if (item.getObjetoT().getCantidad() > 0) {

					item.getObjetoT().setCantidad(Sistema.redondear(item.getObjetoT().getCantidad()));
					// item.getObjetoT().setPrecioVenta(item.getObjetoT().getProducto().getPrecioVenta());
					item.getObjetoT().setPrecioSubtotal(
							Sistema.redondear(item.getObjetoT().getCantidad() * item.getObjetoT().getPrecioVenta()));
					item.getObjetoT().setEstado(getEstado());

					if (item.getObjetoT().getProducto().getImpuesto().getPorcentaje() == 14) {
						compensacionTT += Sistema.redondear(item.getObjetoT().getPrecioSubtotal() * 0.02);
					}
					double a = item.getObjetoT().getPrecioSubtotal()
							- (item.getObjetoT().getPrecioSubtotal() * ventaCabecera.getDescuentoPorcentaje() / 100);
					double b = ((double) item.getObjetoT().getProducto().getImpuesto().getPorcentaje() + 100) / 100;
					subt = a / b;
					subtIva = a - subt;
					item.getObjetoT().setPrecioSubtotal(Sistema.redondear(a));
					subTT += subt;
					ivaTT += subtIva;
				}
			}
			subtotal = Sistema.redondear(subTT,2);
			iva = Sistema.redondear(ivaTT,2);
			compensacion = Sistema.redondear(compensacionTT,2);
			valorTotal = Sistema.redondear(subtotal + iva,2);
			total = valorTotal - compensacion;
		} else {
			cargarTotales();
		}
	}

	@Command
	@NotifyChange({ "total", "subtotal", "iva", "valorTotal", "compensacion" })
	public void cargarTotales() {
		this.subtotal = 0.0;
		this.iva = 0.0;
		this.total = 0.0;
		this.valorTotal = 0.0;
		this.compensacion = 0.0;
	}

	@Command
	@NotifyChange({ "total", "subtotal", "iva", "valorTotal", "compensacion", "listaVenta", "estadoGuardar",
			"btGuardarEditar", "listaVenta", "ventaCabecera", "valorAcreditar", "valorDevolver" })
	public void eliminar(@BindingParam("item") Modelos<VentaDetalle, Object> venta) {
		if (estadoModificar) {
			listaVentaDetalleRemovidos.add(venta.getObjetoT());
		}
		listaVenta.remove(venta);
		if (listaVenta.size() == 0 || listaVenta == null) {
			nuevo();
		}
		calcularListatotal(listaVenta);
		if (ventaCabecera.getId() == null) {
			estadoGuardar = false;
		} else {
			estadoModificar = true;
			estadoGuardar = false;
			valores(ventaCabecera);
		}
		if (listaVenta.size() == 1) {
			estadoGuardar = true;
		}
	}

	@Command
	@NotifyChange({ "listaVenta", "ventaCabecera", "estadoGuardar", "subtotal", "total", "iva", "valorTotal",
			"compensacion", "estadoCorreo" })
	public void guardar() {
		if (Sistema.validarRepetido(InstanciaDAO.getInstanciaVentaCabeceraDAO().getListaVentaCabeceraBySucursal(
				Sistema.ESTADO_ENTREGADO_VENTA, Sistema.getSucursal().getId()), ventaCabecera)) {
			return;
		}
		if (ventaCabecera.getCliente() == null) {
			Messagebox.show("Debe seleccionar un Cliente", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}

		/*
		 * if (getEstado().getId() == Sistema.ESTADO_ENTREGADO_VENTA &&
		 * (ventaCabecera.getCodigo() == null || ventaCabecera.getCodigo().equals("")))
		 * { Messagebox.show("Ingresar Codigo", "Error", Messagebox.OK,
		 * Messagebox.EXCLAMATION); return; }
		 */

		if (estadoModificar) {
			if (ventaCabecera.getEstado().getId() == Sistema.ESTADO_ENTREGADO_VENTA) {
				for (VentaDetalle ventaDetalle : ventaDetalleDAO.getListaVentaDetalleByCabecera(ventaCabecera.getId(),
						Sistema.ESTADO_ENTREGADO_VENTA)) {
					ventaDetalle.getProducto()
							.setCantidad(ventaDetalle.getProducto().getCantidad() + ventaDetalle.getCantidad());
					productoDAO.saveorUpdate(ventaDetalle.getProducto());
				}
			}

			for (VentaDetalle ventaDetalle : listaVentaDetalleRemovidos) {
				ventaDetalle.setEstado(new Estado(Sistema.ESTADO_ELIMINADO));
				ventaDetalleDAO.saveorUpdate(ventaDetalle);
			}
		} else {
			ventaCabecera.setCodigo(String.valueOf(ThreadLocalRandom.current().nextInt(1, 100000)));
		}
		listaVenta.remove(0);
		ventaCabecera.setIva(iva);
		ventaCabecera.setSubtotal(subtotal);
		ventaCabecera.setTotal(total);
		ventaCabecera.setEstado(getEstado());
		ventaCabecera.setUsuario(Sistema.getUsuario());
		ventaCabecera.setCompensacion(compensacion);
		ventaCabecera.setFechaEmision(new Timestamp(new Date().getTime()));
		ventaCabecera.setAreaSucursal(new AreaSucursal(1));
		// ventaCabecera.setInformacionNegocio(in);
		// ventaCabecera.setAreaSucursal(areaSucursal);
		int ut = 0;
		if (getEstado().getId() == Sistema.ESTADO_ENTREGADO_VENTA) {
			ut = Integer.parseInt(Sistema.getSucursal().getCodigoFactura());
			for (int i = 0; i < listaVenta.size(); i = i + 11) {
				VentaCabecera vc = new VentaCabecera();
				int r = listaVenta.size() - i;
				int a = (r < 10) ? listaVenta.size() : i + 11;
				List<Modelos<VentaDetalle, Object>> listaVenta = new ArrayList<>();
				listaVenta = this.listaVenta.subList(i, a);
				calcularListatotal(listaVenta);
				vc = ventaCabecera;
				vc.setIva(iva);
				vc.setSubtotal(subtotal);
				vc.setTotal(total);
				vc.setCompensacion(compensacion);
				// vc.setDescuentoValor(descuento);
				// vc.setDescuentoValor(Sistema.redondear(vc.getTotal() *
				// vc.getDescuentoPorcentaje() / 100));
				// vc.setTotal(Sistema.redondear(vc.getTotal() - vc.getDescuentoValor()));
				ut = (getEstado().getId() == Sistema.ESTADO_ENTREGADO_VENTA) ? ut + 1
						: ThreadLocalRandom.current().nextInt(1, 100000);
				vc.setCodigo(String.valueOf(ut));
				Sistema.getSucursal().setCodigoFactura(String.valueOf(ut));
				InstanciaDAO.getInstanciaSucursalDAO().saveorUpdate(Sistema.getSucursal());
				VentaCabecera ventaCabeceraReferencia = ventaCabeceraDAO.saveorUpdate(vc);
				ventaCabeceraDAO.saveorUpdateDetalle(listaVenta, ventaCabeceraReferencia);
				CajaDetalle cd = new CajaDetalle();
				cd.setCaja(caja);
				cd.setVentaCabecera(ventaCabeceraReferencia);
				cajaDetalleDAO.saveorUpdate(cd);
			}
		} else {
			VentaCabecera ventaCabeceraReferencia = ventaCabeceraDAO.saveorUpdate(ventaCabecera);
			ventaCabeceraDAO.saveorUpdateDetalle(listaVenta, ventaCabeceraReferencia);
		}

		Clients.showNotification("Ingresado Satisfactorio ", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center",
				1500);
		estadoGuardar = true;
		// ventaCabecera = ventaCabeceraReferencia;
		// generarPdf();
		// nuevo();
	}

	@Command
	@NotifyChange({ "estadoAnular", "estadoBT", "btGuardarEditar", "listaVenta", "ventaCabecera", "total", "subtotal",
			"iva", "estadoGuardar", "valorAcreditar", "valorDevolver", "valorTotal", "compensacion" })
	public void nuevo() {
		estadoAnular = true;
		valorAcreditar = 0.0;
		valorDevolver = 0.0;
		listaVenta.clear();
		ventaCabecera = new VentaCabecera();
		ventaCabecera.setFechaEmision(new Timestamp(new Date().getTime()));
		ventaCabecera.setDescuentoPorcentaje(0);
		ventaCabecera.setCliente(InstanciaDAO.getInstanciaClienteDAO().getClienteByCedula("99"));
		estadoModificar = false;
		estadoGuardar = true;
		estadoBT = false;
		cargarTotales();
		listaVenta.add(new Modelos<VentaDetalle, Object>(getVentaDetalleNuevo(), null, true));
	}

	private VentaDetalle getVentaDetalleNuevo() {
		VentaDetalle ventaDetalle = new VentaDetalle();
		ventaDetalle.setPrecioSubtotal(0);
		ventaDetalle.setCantidad(0);
		ventaDetalle.setDescuentoPorcentaje(0);
		ventaDetalle.setProducto(new Producto());
		ventaDetalle.setEstado(getEstado());
		return ventaDetalle;
	}

	private Estado getEstado() {
		final Estado estado = new Estado();
		if (tipo.trim().equals("ordenVenta")) {
			estado.setId(Sistema.ESTADO_ORDEN_VENTA);
		}
		if (tipo.trim().equals("cobrarVenta")) {
			estado.setId(Sistema.ESTADO_COBRADO_VENTA);
		}
		if (tipo.trim().equals("entregarVenta")) {
			estado.setId(Sistema.ESTADO_ENTREGADO_VENTA);
		}
		if (tipo.trim().equals("planVenta")) {
			estado.setId(Sistema.ESTADO_PLAN_VENTA);
		}
		return estado;
	}

	/*
	 * private Estado getEstadoAnterior() { if (tipo.trim().equals("cobrarVenta")) {
	 * estado.setId(Sistema.ESTADO_ORDEN_VENTA); } if
	 * (tipo.trim().equals("entregarVenta")) {
	 * estado.setId(Sistema.ESTADO_COBRADO_VENTA); } return estado; }
	 */

	private void valores(VentaCabecera ventaCabecera) {
		if (ventaCabecera.getEstado().getId() == Sistema.ESTADO_COBRADO_VENTA
				|| ventaCabecera.getEstado().getId() == Sistema.ESTADO_ENTREGADO_VENTA) {
			valorAcreditar = 0.0;
			valorDevolver = 0.0;

			if (total > totalAnterior) {
				valorAcreditar = total - totalAnterior;
			} else {
				valorDevolver = totalAnterior - total;
			}
		}
	}

	@Command
	public void generarPdf() {
		if (this.listaVenta.isEmpty()) {
			Messagebox.show("No existen registros en el Detalle");
			return;
		}

		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("ventaCabecera", ventaCabecera);
		parametros.put("listaVenta", listaVenta);
		/*
		 * parametros.put("subtotal", subtotal); parametros.put("iva", iva);
		 * parametros.put("compensacion", compensacion); parametros.put("total", total);
		 */// envia archivo para detalle de reporte

		if (estadoCorreo) {
			if (!Sistema.verificarInternet()) {
				estadoCorreo = false;
				Messagebox.show("Imposible enviar correo, Por favor verifique su conexion a internet");
			}
		}
		if (tipoImpresion.equals("ticket")) {
			Executions.createComponents("/reportes/impresionProforma.zul", null, parametros);
		}
		if (tipoImpresion.equals("factura")) {
			Executions.createComponents("/reportes/impresionProforma.zul", null, parametros);
		}
		// proceso para enviar correo
		if (estadoCorreo) {
			// Proceso para guardar el archivo pdf
			Util.uploadFile(
					Sistema.reportePDF("facturacion.jasper", Sistema.getMapReport(
							ventaCabecera.getAreaSucursal().getSucursal().getInformacionNegocio().getNombre(),
							ventaCabecera.getAreaSucursal().getSucursal().getInformacionNegocio().getRuc(),
							ventaCabecera.getAreaSucursal().getSucursal().getInformacionNegocio().getDireccion(),
							ventaCabecera.getAreaSucursal().getSucursal().getInformacionNegocio().getTelefono(),
							this.ventaCabecera.getCliente().getNombre(), this.ventaCabecera.getCliente().getCedula(),
							ventaCabecera.getFechaEmision(), ventaCabecera.getUsuario().getNombre(),
							Sistema.redondear(subtotal), Sistema.redondear(iva), Sistema.redondear(compensacion),
							Sistema.redondear(ventaCabecera.getDescuentoValor()), Sistema.redondear(total),
							ventaCabecera.getEstado()), Sistema.getProformaDataSource(listaVenta)),
					ventaCabecera.getCodigo() + ".pdf", Sistema.carpetaFacturaPDF);
			// Envio correo
			Sistema.enviarCorreo(ventaCabecera.getCliente().getCorreo(),
					ventaCabecera.getAreaSucursal().getSucursal().getInformacionNegocio().getNombre(),
					"Registro para :" + ventaCabecera.getCliente().getNombre(),
					Util.getPath(Sistema.carpetaFacturaPDF) + ventaCabecera.getCodigo() + ".pdf");
		}
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		String jasper = "";
		if (getEstado().getId() == Sistema.ESTADO_ORDEN_VENTA) {
			jasper = "plan_venta.jasper";
		} else if (getEstado().getId() == Sistema.ESTADO_ENTREGADO_VENTA) {
			jasper = "facturacion.jasper";
		} else if (getEstado().getId() == Sistema.ESTADO_PLAN_VENTA) {
			jasper = "plan_venta.jasper";
		}
		// double val = (ventaCabecera.getDescuentoValor() * 100)
		// / (ventaCabecera.getTotal() + ventaCabecera.getDescuentoValor());
		/*
		 * listaVenta.forEach(x -> { x.getObjetoT().setDescuentoValor(
		 * (x.getObjetoT().getPrecioSubtotal() * ventaCabecera.getDescuentoPorcentaje())
		 * / 100); x.getObjetoT().setPrecioSubtotal(Sistema.redondear(x.getObjetoT().
		 * getPrecioSubtotal())); });
		 */
		iframe.setContent(Sistema.reportePDF(jasper,
				Sistema.getMapReport(ventaCabecera.getAreaSucursal().getSucursal().getInformacionNegocio().getNombre(),
						ventaCabecera.getAreaSucursal().getSucursal().getInformacionNegocio().getRuc(),
						ventaCabecera.getAreaSucursal().getSucursal().getInformacionNegocio().getDireccion(),
						ventaCabecera.getAreaSucursal().getSucursal().getInformacionNegocio().getTelefono(),
						this.ventaCabecera.getCliente().getNombre(), this.ventaCabecera.getCliente().getCedula(),
						ventaCabecera.getFechaEmision(), ventaCabecera.getUsuario().getNombre(),
						Sistema.redondear(ventaCabecera.getSubtotal()), Sistema.redondear(ventaCabecera.getIva()),
						Sistema.redondear(ventaCabecera.getCompensacion()),
						Sistema.redondear(ventaCabecera.getDescuentoPorcentaje()),
						Sistema.redondear(ventaCabecera.getTotal()), ventaCabecera.getEstado()),
				Sistema.getProformaDataSource(listaVenta)));
	}

	@NotifyChange({ "valorDevolver", "valorAcreditar", "listaVenta", "total", "subtotal", "iva", "valorTotal",
			"compensacion", "estadoGuardar", "estadoModificar", "btGuardarEditar" })
	@Command
	public void calcularDescuento(@BindingParam("item") Modelos<VentaDetalle, Object> venta) {
		if (venta.getObjetoT().getPrecioVenta() < venta.getObjetoT().getProducto().getPrecioCompra()) {
			Messagebox.show("Esta marcando perdida");
			venta.getObjetoT().setPrecioVenta(venta.getObjetoT().getProducto().getPrecioCompra());
		}
		double descuento = venta.getObjetoT().getProducto().getPrecioVenta() - venta.getObjetoT().getPrecioVenta();
		double valor = (descuento * 100) / venta.getObjetoT().getProducto().getPrecioVenta();
		venta.getObjetoT().setDescuentoPorcentaje((int) valor);
		confirmar(venta);
	}

	@GlobalCommand
	@NotifyChange({ "valorDevolver", "valorAcreditar", "listaVenta", "total", "subtotal", "iva", "valorTotal",
			"compensacion", "estadoGuardar", "estadoModificar", "btGuardarEditar" })
	public void cargarCaja() {
		caja = cajaDAO.getCaja(1, true);
	}

	@Command
	@NotifyChange({ "valorDevolver", "valorAcreditar", "listaVenta", "total", "subtotal", "iva", "valorTotal",
			"compensacion", "estadoGuardar", "estadoModificar", "btGuardarEditar", "ventaCabecera" })
	public void descuento(@BindingParam("valor") String valor) {
		if (total <= 0.0) {
			ventaCabecera.setDescuentoPorcentaje(0);
			ventaCabecera.setDescuentoValor(0.0);
			Messagebox.show("No valores para aplicar descuento");
			return;
		}
		/*
		 * if (ventaCabecera.getDescuentoPorcentaje() == null) {
		 * ventaCabecera.setDescuentoPorcentaje(0); return; }
		 */
		if (valor.trim().equals("descuentoPorcentaje")) {
			if (ventaCabecera.getDescuentoPorcentaje() >= 100) {
				ventaCabecera.setDescuentoPorcentaje(0);
				ventaCabecera.setDescuentoValor(total * ventaCabecera.getDescuentoPorcentaje() / 100);
			} else {
				ventaCabecera
						.setDescuentoValor(Sistema.redondear(total * ventaCabecera.getDescuentoPorcentaje() / 100));
			}
		}
		if (valor.trim().equals("descuentoValor")) {
			if (ventaCabecera.getDescuentoValor() >= total) {
				ventaCabecera.setDescuentoValor(total * ventaCabecera.getDescuentoPorcentaje() / 100);
			} else {
				double val = (ventaCabecera.getDescuentoValor() * 100) / total;
				ventaCabecera.setDescuentoPorcentaje(Sistema.redondear(val));
			}
		}
		calcularListatotal(listaVenta);
		// this.total = Sistema.redondear(total - ventaCabecera.getDescuentoValor());
	}

	@Command
	@NotifyChange("ventaCabecera")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("codigoVenta")) {
			ventaCabecera.setCodigo(cadena.toUpperCase());
		}
	}

	/*
	 * Metodos GET AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public VentaCabecera getVentaCabecera() {
		return ventaCabecera;
	}

	public void setVentaCabecera(VentaCabecera ventaCabecera) {
		this.ventaCabecera = ventaCabecera;
	}

	public boolean isEstadoGuardar() {
		return estadoGuardar;
	}

	public void setEstadoGuardar(boolean estadoGuardar) {
		this.estadoGuardar = estadoGuardar;
	}

	public List<Modelos<VentaDetalle, Object>> getListaVenta() {
		return listaVenta;
	}

	public void setListaVenta(List<Modelos<VentaDetalle, Object>> listaVenta) {
		this.listaVenta = listaVenta;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public Double getIva() {
		return iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public boolean isBtBuscar() {
		return btBuscar;
	}

	public void setBtBuscar(boolean btBuscar) {
		this.btBuscar = btBuscar;
	}

	public boolean isBtProducto() {
		return btProducto;
	}

	public void setBtProducto(boolean btProducto) {
		this.btProducto = btProducto;
	}

	public boolean isEstadoBT() {
		return estadoBT;
	}

	public void setEstadoBT(boolean estadoBT) {
		this.estadoBT = estadoBT;
	}

	public String getTxtBuscar() {
		return txtBuscar;
	}

	public void setTxtBuscar(String txtBuscar) {
		this.txtBuscar = txtBuscar;
	}

	public Double getValorDevolver() {
		return valorDevolver;
	}

	public void setValorDevolver(Double valorDevolver) {
		this.valorDevolver = valorDevolver;
	}

	public Double getValorAcreditar() {
		return valorAcreditar;
	}

	public void setValorAcreditar(Double valorAcreditar) {
		this.valorAcreditar = valorAcreditar;
	}

	public boolean isEstadoAnular() {
		return estadoAnular;
	}

	public void setEstadoAnular(boolean estadoAnular) {
		this.estadoAnular = estadoAnular;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Double getCompensacion() {
		return compensacion;
	}

	public void setCompensacion(Double compensacion) {
		this.compensacion = compensacion;
	}

	public boolean isEstadoCorreo() {
		return estadoCorreo;
	}

	public void setEstadoCorreo(boolean estadoCoreo) {
		this.estadoCorreo = estadoCoreo;
	}

	public String getTipoImpresion() {
		return tipoImpresion;
	}

	public void setTipoImpresion(String tipoImpresion) {
		this.tipoImpresion = tipoImpresion;
	}
}
