package ec.com.exodo;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.Menuitem;

import ec.com.exodo.entity.ModuloDetalle;
import ec.com.exodo.entity.RolModulo;
import ec.com.exodo.entity.Usuario;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

//esta clase interactua con dos vista index.zul y /template/banner.zul
public class Index {
	// estado para mostrar u ocultar los modulos
	private boolean estadoInventario = false;
	private boolean estadoCompra = false;
	private boolean estadoVenta = false;
	private boolean estadoUsuario = false;
	private boolean estadoConfiguracion = false;
	private boolean estadoReporte = false;
	// variables donde se almacenara los detalles de cada modulo
	private List<ModuloDetalle> listaInventario = new ArrayList<>();
	private List<ModuloDetalle> listaCompra = new ArrayList<>();
	private List<ModuloDetalle> listaVenta = new ArrayList<>();
	private List<ModuloDetalle> listaUsuario = new ArrayList<>();
	private List<ModuloDetalle> listaConfiguracion = new ArrayList<>();
	private List<ModuloDetalle> listaReporte = new ArrayList<>();
	// lista de moduloDetalle para llenar listbox que mostrara los detalles
	private List<ModuloDetalle> listaModuloDetalle = new ArrayList<>();
	private ModuloDetalle moduloDetalle = new ModuloDetalle();
	// variable para la ruta
	private String src = "main.zul";
	// variables para efecto de ocultar bara
	private String anchoBarra = "190px";
	private boolean visibleBarra = true;
	private String btDesplegarBarra = "z-icon-angle-double-left";

	private Usuario usuario;


	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		usuario = (Usuario) Sessions.getCurrent().getAttribute("Usuario");
		List<RolModulo> listaRolModulo = InstanciaDAO.getInstanciaRolModuloDAO()
				.getListaRolModulo(usuario.getRol().getId(), Sistema.ESTADO_ACTIVAR_USUARIO);
		String valor = "";
		for (RolModulo rolModulo : listaRolModulo) {
			if (rolModulo.getModulo() != null) {
				valor = rolModulo.getModulo().getCodigo();
			}
			if (valor.trim().equals(Sistema.INVENTARIO.trim())) {
				estadoInventario = true;
				List<ModuloDetalle> listaModuloDetalle = InstanciaDAO.getInstanciaModuloDetalleDAO()
						.getListaModuloDetallesByModulo(rolModulo.getModulo().getId());
				for (ModuloDetalle moduloDetalle : listaModuloDetalle) {
					listaInventario.add(moduloDetalle);
				}
			} else if (valor.trim().equals(Sistema.COMPRA.trim())) {
				estadoCompra = true;
				List<ModuloDetalle> listaModuloDetalle = InstanciaDAO.getInstanciaModuloDetalleDAO()
						.getListaModuloDetallesByModulo(rolModulo.getModulo().getId());
				for (ModuloDetalle moduloDetalle : listaModuloDetalle) {
					listaCompra.add(moduloDetalle);
				}
			} else if (valor.trim().equals(Sistema.VENTA.trim())) {
				estadoVenta = true;
				List<ModuloDetalle> listaModuloDetalle = InstanciaDAO.getInstanciaModuloDetalleDAO()
						.getListaModuloDetallesByModulo(rolModulo.getModulo().getId());
				for (ModuloDetalle moduloDetalle : listaModuloDetalle) {
					listaVenta.add(moduloDetalle);
				}
			} else if (valor.trim().equals(Sistema.GESTION_USUARIO.trim())) {
				estadoUsuario = true;
				List<ModuloDetalle> listaModuloDetalle = InstanciaDAO.getInstanciaModuloDetalleDAO()
						.getListaModuloDetallesByModulo(rolModulo.getModulo().getId());
				for (ModuloDetalle moduloDetalle : listaModuloDetalle) {
					listaUsuario.add(moduloDetalle);
				}
			} else if (valor.trim().equals(Sistema.CONFIGURACION.trim())) {
				estadoConfiguracion = true;
				List<ModuloDetalle> listaModuloDetalle = InstanciaDAO.getInstanciaModuloDetalleDAO()
						.getListaModuloDetallesByModulo(rolModulo.getModulo().getId());
				for (ModuloDetalle moduloDetalle : listaModuloDetalle) {
					listaConfiguracion.add(moduloDetalle);
				}

			} else if (valor.trim().equals(Sistema.REPORTE.trim())) {
				estadoReporte = true;
				List<ModuloDetalle> listaModuloDetalle = InstanciaDAO.getInstanciaModuloDetalleDAO()
						.getListaModuloDetallesByModulo(rolModulo.getModulo().getId());
				for (ModuloDetalle moduloDetalle : listaModuloDetalle) {
					listaReporte.add(moduloDetalle);
				}
			}
		}

	}

	@Command
	@NotifyChange("listaModuloDetalle")
	public void llenarLista(@BindingParam("bt") String bt) {
		this.listaModuloDetalle.clear();
		switch (bt) {
		case "inventario":
			this.listaModuloDetalle = InstanciaDAO.getInstanciaModuloDetalleDAO().getListaModuloDetallesByModulo(1);
			break;
		case "compra":
			this.listaModuloDetalle = InstanciaDAO.getInstanciaModuloDetalleDAO().getListaModuloDetallesByModulo(2);
			break;
		case "venta":
			this.listaModuloDetalle = InstanciaDAO.getInstanciaModuloDetalleDAO().getListaModuloDetallesByModulo(3);
			break;
		case "usuario":
			this.listaModuloDetalle = InstanciaDAO.getInstanciaModuloDetalleDAO().getListaModuloDetallesByModulo(4);
			break;
		default:
			break;
		}

	}

	@Command
	@NotifyChange({ "anchoBarra", "visibleBarra", "btDesplegarBarra" })
	public void ocultarBarra() {
		if (visibleBarra) {
			visibleBarra = false;
			anchoBarra = "20px";
			btDesplegarBarra = "z-icon-angle-double-right";
		} else {
			visibleBarra = true;
			anchoBarra = "190px";
			btDesplegarBarra = "z-icon-angle-double-left";
		}
	}

	// Metodo que se activa cuando damos click en uno de los detalles y cambiara
	// la variable src la cual esta bindeada
	// en index.zul y nos redirije a la ruta del detalle
	@Command
	@NotifyChange("src")
	public void select(@BindingParam("item") String ruta) {
		src = ruta;
	}

	@Command
	public void acercaDe() {
		Executions.createComponents("/view/formAcercaDe.zul", null, null);
	}

	@Command
	@NotifyChange("src")
	public void src(@BindingParam("item") Menuitem item) {
		setSrc(item.getId());
	}
	/*
	 * Metodos GET and SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public boolean isEstadoCompra() {
		return estadoCompra;
	}

	public void setEstadoCompra(boolean estadoCompra) {
		this.estadoCompra = estadoCompra;
	}

	public boolean isEstadoVenta() {
		return estadoVenta;
	}

	public void setEstadoVenta(boolean estadoVenta) {
		this.estadoVenta = estadoVenta;
	}

	public boolean isEstadoInventario() {
		return estadoInventario;
	}

	public void setEstadoInventario(boolean estadoInventario) {
		this.estadoInventario = estadoInventario;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public List<ModuloDetalle> getListaInventario() {
		return listaInventario;
	}

	public void setListaInventario(List<ModuloDetalle> listaInventario) {
		this.listaInventario = listaInventario;
	}

	public List<ModuloDetalle> getListaCompra() {
		return listaCompra;
	}

	public void setListaCompra(List<ModuloDetalle> listaCompra) {
		this.listaCompra = listaCompra;
	}

	public List<ModuloDetalle> getListaVenta() {
		return listaVenta;
	}

	public void setListaVenta(List<ModuloDetalle> listaVenta) {
		this.listaVenta = listaVenta;
	}

	public boolean isEstadoUsuario() {
		return estadoUsuario;
	}

	public void setEstadoUsuario(boolean estadoUsuario) {
		this.estadoUsuario = estadoUsuario;
	}

	public List<ModuloDetalle> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(List<ModuloDetalle> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

	public boolean isEstadoConfiguracion() {
		return estadoConfiguracion;
	}

	public void setEstadoConfiguracion(boolean estadoConfiguracion) {
		this.estadoConfiguracion = estadoConfiguracion;
	}

	public List<ModuloDetalle> getListaConfiguracion() {
		return listaConfiguracion;
	}

	public void setListaConfiguracion(List<ModuloDetalle> listaConfiguracion) {
		this.listaConfiguracion = listaConfiguracion;
	}

	public boolean isEstadoReporte() {
		return estadoReporte;
	}

	public void setEstadoReporte(boolean estadoReporte) {
		this.estadoReporte = estadoReporte;
	}

	public List<ModuloDetalle> getListaReporte() {
		return listaReporte;
	}

	public void setListaReporte(List<ModuloDetalle> listaReporte) {
		this.listaReporte = listaReporte;
	}

	public List<ModuloDetalle> getListaModuloDetalle() {
		return listaModuloDetalle;
	}

	public void setListaModuloDetalle(List<ModuloDetalle> listaModuloDetalle) {
		this.listaModuloDetalle = listaModuloDetalle;
	}

	public ModuloDetalle getModuloDetalle() {
		return moduloDetalle;
	}

	public void setModuloDetalle(ModuloDetalle moduloDetalle) {
		this.moduloDetalle = moduloDetalle;
	}

	public String getAnchoBarra() {
		return anchoBarra;
	}

	public void setAnchoBarra(String anchoBarra) {
		this.anchoBarra = anchoBarra;
	}

	public boolean isVisibleBarra() {
		return visibleBarra;
	}

	public void setVisibleBarra(boolean visibleBarra) {
		this.visibleBarra = visibleBarra;
	}

	public String getBtDesplegarBarra() {
		return btDesplegarBarra;
	}

	public void setBtDesplegarBarra(String btDesplegarBarra) {
		this.btDesplegarBarra = btDesplegarBarra;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getMensajeUsuario() {
		return usuario.getNombre();
	}
}
