package ec.com.exodo;

import java.sql.Time;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.entity.Rol;
import ec.com.exodo.entity.Usuario;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassUsuario {
	@Wire("#winUsuario")
	private Window winUsuario;

	private String btGuardarEditar = Sistema.getIdioma().getGuardar();
	private List<Usuario> listaUsuario;
	private Usuario usuario;
	private List<Rol> listaRol;
	private String password;
	private String passwordRepetir;
	private String claveRepetida;
	boolean validarRepetido = false;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;

	// private Estado estado = InstanciaEntity.getInstanciaEstado();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(view, this, false);
		if (clase == null) {
			clase = "this";
		}

		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "80%";
		}
		listaUsuario = InstanciaDAO.getInstanciaUsuarioDAO().getListaUsuario();
		listaRol = InstanciaDAO.getInstanciaRolDAO().getListaRol(Sistema.ESTADO_ACTIVAR_USUARIO);
		nuevo();
	}

	@Command
	@NotifyChange({ "listaUsuario", "claveRepetida", "password", "passwordRepetir", "listaUsuario", "usuario",
			"btGuardarEditar" })
	public void guardarEditar() {
		if (Sistema.validarRepetido(InstanciaDAO.getInstanciaUsuarioDAO().getListaUsuario(), usuario)) {
			return;
		}
		validarPassword();
		if (!validarRepetido) {
			Messagebox.show("Claves no coinciden");
			return;
		}
		if (usuario.getId() == null) {
			usuario.setPassword(Sistema.Encriptar(password));
			usuario.setEstado(true);
			listaUsuario.clear();
			InstanciaDAO.getInstanciaUsuarioDAO().saveorUpdate(usuario);
			Clients.showNotification("Usuario ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		} else {
			usuario.setPassword(Sistema.Encriptar(password));
			InstanciaDAO.getInstanciaUsuarioDAO().saveorUpdate(usuario);
			Clients.showNotification("Usuario Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
		listaUsuario = InstanciaDAO.getInstanciaUsuarioDAO().getListaUsuario();
		nuevo();
	}

	@SuppressWarnings("deprecation")
	@Command
	@NotifyChange({ "password", "passwordRepetir", "listaUsuario", "usuario", "btGuardarEditar" })
	public void nuevo() {
		usuario = new Usuario();
		usuario.setHoraEntrada(new Time(8, 0, 0));
		usuario.setHoraSalida(new Time(15, 0, 0));
		btGuardarEditar = Sistema.getIdioma().getGuardar();
		password = "";
		passwordRepetir = "";
	}

	@Command
	@NotifyChange({ "password", "passwordRepetir", "btGuardarEditar" })
	public void desencriptar(@BindingParam("item") Usuario usuario) {
		try {
			password = Sistema.desencriptar(usuario.getPassword());
			passwordRepetir = password;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btGuardarEditar = Sistema.getIdioma().getEditar();
	}

	@Command
	public void check(@BindingParam("item") Usuario usuario) {
		InstanciaDAO.getInstanciaUsuarioDAO().saveorUpdate(usuario);
	}

	@Command
	public void admin(@BindingParam("item") Usuario usuario, @BindingParam("check") Checkbox check) {
		if (usuario.getId() == 1) {
			check.setDisabled(true);
		}
	}

	@NotifyChange("claveRepetida")
	@Command
	public void validarPassword() {
		if (passwordRepetir != null && password != null && !passwordRepetir.trim().equals("")
				&& !password.trim().equals("")) {
			if (!password.trim().equals(passwordRepetir.trim())) {
				validarRepetido = false;
				claveRepetida = "Claves no coinciden";
			} else {
				validarRepetido = true;
				claveRepetida = "";
			}
		}
	}
	
	@Command
	public void aceptar(@BindingParam("usuario") Usuario usuario) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", usuario);
		BindUtils.postGlobalCommand(null, null, "cargarUsuario", parametros);
		salir();
	}
	
	@Command
	public void salir() {
		winUsuario.detach();
	}

	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public List<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Rol> getListaRol() {
		return listaRol;
	}

	public void setListaRol(List<Rol> listaRol) {
		this.listaRol = listaRol;
	}

	public String getPasswordRepetir() {
		return passwordRepetir;
	}

	public void setPasswordRepetir(String passwordRepetir) {
		this.passwordRepetir = passwordRepetir;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBtGuardarEditar() {
		return btGuardarEditar;
	}

	public void setBtGuardarEditar(String btGuardarEditar) {
		this.btGuardarEditar = btGuardarEditar;
	}

	public String getClaveRepetida() {
		return claveRepetida;
	}

	public void setClaveRepetida(String claveRepetida) {
		this.claveRepetida = claveRepetida;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

}
