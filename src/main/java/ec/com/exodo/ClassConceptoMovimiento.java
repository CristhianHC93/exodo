package ec.com.exodo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.ConceptoMovimientoDAO;
import ec.com.exodo.entity.ConceptoMovimiento;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassConceptoMovimiento {
	@Wire("#winConceptoMovimiento")
	private Window winConceptoMovimiento;

	private String selectCombo = getComboConceptoMovimiento().get(0);
	private List<ConceptoMovimiento> listaConceptoMovimiento;

	private ConceptoMovimientoDAO conceptoMovimientoDAO = InstanciaDAO.getInstanciaConceptoMovimientoDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		select();
	}

	@NotifyChange("listaConceptoMovimiento")
	@Command
	public void select() {
		if (selectCombo.equals("SALIDA")) {
			listaConceptoMovimiento = conceptoMovimientoDAO.getListaConceptoMovimiento(Sistema.CODIGO_SALIDA, true);
		}
		if (selectCombo.equals("ENTRADA")) {
			listaConceptoMovimiento = conceptoMovimientoDAO.getListaConceptoMovimiento(Sistema.CODIGO_ENTRADA, true);
		}
	}

	@Command
	@NotifyChange("listaConceptoMovimiento")
	public void nuevo() {
		if (listaConceptoMovimiento == null) {
			listaConceptoMovimiento = new ArrayList<ConceptoMovimiento>();
		}
		listaConceptoMovimiento.add(new ConceptoMovimiento());
	}

	@Command
	@NotifyChange("listaConceptoMovimiento")
	public void eliminar(@BindingParam("conceptoMovimiento") final ConceptoMovimiento conceptoMovimiento) {
		if (conceptoMovimiento.getId() == null) {
			listaConceptoMovimiento.remove(conceptoMovimiento);
			return;
		}
		Messagebox.show(getIdioma().getMensajeEliminar(conceptoMovimiento.getDescripcion()), "Confirm",
				Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						gestionEliminar(conceptoMovimientoDAO.eliminar(conceptoMovimiento.getId()));
					}
				});

	}

	private void gestionEliminar(int a) {
		if (a == 1) {
			Clients.showNotification("Registro eliminado satisfactoriamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"top_center", 2000);
			BindUtils.postNotifyChange(null, null, ClassConceptoMovimiento.this, "listaConceptoMovimiento");
			listaConceptoMovimiento.clear();
			select();

		} else {
			Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR, null, "top_center",
					2000);
			BindUtils.postNotifyChange(null, null, ClassConceptoMovimiento.this, "listaConceptoMovimiento");
		}
	}

	@Command
	@NotifyChange("listaConceptoMovimiento")
	public void guardarEditar(@BindingParam("conceptoMovimiento") final ConceptoMovimiento conceptoMovimiento) {
		conceptoMovimiento.setEstado(true);
		conceptoMovimiento.setCodigo((selectCombo.equals("SALIDA")) ? Sistema.CODIGO_SALIDA : Sistema.CODIGO_ENTRADA);
		conceptoMovimiento.setConcepto(selectCombo.toUpperCase());
		conceptoMovimientoDAO.saveorUpdate(conceptoMovimiento);

		Clients.showNotification("ConceptoMovimiento "
				+ ((conceptoMovimiento.getId() == null) ? "ingresado" : "modificado") + " correctamente",
				Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
		listaConceptoMovimiento.clear();
		select();
	}

	@GlobalCommand
	@NotifyChange("listaConceptoMovimiento")
	public void refrescarlista() {
		select();
	}

	@Command
	public void editar(@BindingParam("conceptoMovimiento") ConceptoMovimiento conceptoMovimiento) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", conceptoMovimiento);
		Executions.createComponents("/view/formConceptoMovimientoCE.zul", null, parameters);
	}

	// @Command
	// public void salir() {
	// winConceptoMovimiento.detach();
	// }

	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public final List<String> getComboConceptoMovimiento() {
		return Sistema.getListaConceptoMovimiento();
	}

	public List<ConceptoMovimiento> getListaConceptoMovimiento() {
		return listaConceptoMovimiento;
	}

	public void setListaConceptoMovimiento(List<ConceptoMovimiento> listaConceptoMovimiento) {
		this.listaConceptoMovimiento = listaConceptoMovimiento;
	}

	public String getSelectCombo() {
		return selectCombo;
	}

	public void setSelectCombo(String selectCombo) {
		this.selectCombo = selectCombo;
	}
}
