package ec.com.exodo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.AreaSucursalDAO;
import ec.com.exodo.dao.ie.ProductoDAO;
import ec.com.exodo.dao.ie.ProductoProveedorDAO;
import ec.com.exodo.entity.AreaSucursal;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.ProductoProveedor;
import ec.com.exodo.entity.Proveedor;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassProducto {
	@Wire("#winFormProducto")
	private Window winFormProducto;

	private String clase;
	private Producto producto;

	private String dimensionPantalla;
	private String dimensionWidth;
	private String estadoPantalla;
	private boolean btAceptar;
	private boolean btGuardar;
	private boolean estadoStock;
	private String datoBusqueda;
	private String datoBusquedaStock;
	private String codigoProducto;
	private String stockAviso = "todos";

	private Proveedor proveedor;
	private List<Producto> listaProducto = new ArrayList<>();
	private ProductoDAO productoDAO = InstanciaDAO.getInstanciaProductoDAO();
	private AreaSucursalDAO areaSucursalDAO = InstanciaDAO.getInstanciaAreaSucursalDAO();

	private List<AreaSucursal> listaAreaSucursal;
	private AreaSucursal areaSucursal;
	private boolean estadoArea = false;
	private boolean verTodosProductos = false;

	boolean estadoVerificarSucursal = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("clase") String clase,
			@ExecutionArgParam("filtroProducto") String filtroProducto,
			@ExecutionArgParam("area") AreaSucursal areaSucursal, @ExecutionArgParam("lista") List<Producto> lista,
			@ExecutionArgParam("proveedor") Proveedor proveedor, @ExecutionArgParam("producto") Producto producto) {
		Selectors.wireComponents(view, this, false);
		if (producto != null) {
			this.producto = producto;
		} else if (lista != null) {
			listaProducto = new ArrayList<>(lista.size());
			listaProducto.addAll(lista);
		} else {
			if (clase == null) {
				clase = "this";
			}
			this.clase = clase;
			if (clase.equals("this")) {
				estadoPantalla = "embedded";
				btAceptar = false;
				dimensionPantalla = "100%";
				dimensionWidth = "100%";
				estadoArea = true;

				listaAreaSucursal = areaSucursalDAO.getListaAreaSucursalBySucursalActivo(Sistema.getSucursal().getId());
				this.areaSucursal = (areaSucursal == null)
						? ((listaAreaSucursal.size() > 0) ? listaAreaSucursal.get(0) : null)
						: areaSucursal;

				obtenerTodos(clase);
			}
			if (clase.equals("notaCompra") || clase.equals("compra")) {
				estadoPantalla = "modal";
				btAceptar = true;
				dimensionPantalla = "95%";
				dimensionWidth = "75%";
				this.proveedor = proveedor;
				this.areaSucursal = Sistema.getAreaSucursal();
				estadoArea = false;
				filtrarLetra(filtroProducto);
				this.datoBusqueda = filtroProducto;
			}
			if (clase.equals("cuadreInventario") || clase.equals("kardex") || clase.equals("traspasoProducto")
					|| clase.equals(ClassVenta.class.getSimpleName())) {
				estadoPantalla = "modal";
				btAceptar = true;
				dimensionPantalla = "95%";
				dimensionWidth = "75%";
				this.areaSucursal = areaSucursal;
				estadoArea = false;
				filtrarLetra(filtroProducto);
				this.datoBusqueda = filtroProducto;
			}
		}
	}

	private void obtenerTodos(String clase) {
		if (clase.equals("this")) {
			if (Sistema.verificarUsuarioEmpleado(Sistema.getUsuario())) {
				Sistema.getListaSucursal().forEach(x -> {
					if (x.getUsuario().getId() == Sistema.getUsuario().getId()) {
						estadoVerificarSucursal = true;
					}
				});
				if (estadoVerificarSucursal) {
					if (verTodosProductos) {
						listaProducto = productoDAO.getListaProducto(Sistema.ESTADO_INGRESADO);
					} else {
						listaProducto = productoDAO.getListaProductoBySucursal(Sistema.ESTADO_INGRESADO,
								Sistema.getSucursal().getId());
					}
				} else {
					if (Sistema.getAreaSucursal() != null) {
						listaProducto = productoDAO.getListaProductoByAreaSucursal(Sistema.ESTADO_INGRESADO,
								Sistema.getAreaSucursal().getId());
					}
				}
			} else {
				if (verTodosProductos) {
					listaProducto = productoDAO.getListaProducto(Sistema.ESTADO_INGRESADO);
				} else {
					listaProducto = productoDAO.getListaProductoByAreaSucursal(Sistema.ESTADO_INGRESADO,
							areaSucursal.getId());
				}
			}
		}
		if (clase.equals("notaCompra") || clase.equals("compra")) {
			if (listaProducto == null) {
				listaProducto = new ArrayList<>();
			} else {
				listaProducto.clear();
			}
			List<ProductoProveedor> lista = productoProveedorDAO.getListaProductoProveedorByProveedor(proveedor.getId(),
					"");
			lista.forEach(x -> {
				listaProducto.add(x.getProducto());
			});

			// listaProducto =
			// productoDAO.getProductosByProveedorByAreaSucursal(proveedor.getId(),
			// Sistema.ESTADO_INGRESADO, areaSucursal.getId());

		}
		if (clase.equals(ClassVenta.class.getSimpleName())) {
			listaProducto = productoDAO.getListaProducto(Sistema.ESTADO_INGRESADO);
		}
		if (clase.equals("cuadreInventario") || clase.equals("kardex")) {
			listaProducto = productoDAO.getListaProductoByAreaSucursal(Sistema.ESTADO_INGRESADO, areaSucursal.getId());
		}
		// listaProducto = new ArrayList<>(listaProductoProvisional.size());
		// listaProducto.addAll(listaProductoProvisional);
		for (Producto p : listaProducto) {
			Sistema.calorimetria(p);
		}
	}

	@NotifyChange("listaProducto")
	@Command
	public void checkTodos() {
		obtenerTodos(clase);
	}

	@NotifyChange({ "listaProducto", "listaAreaSucursal", "areaSucursal" })
	@Command
	public void selectSucursal() {
		listaAreaSucursal = areaSucursalDAO.getListaAreaSucursalBySucursalActivo(Sistema.getSucursal().getId());
		areaSucursal = (listaAreaSucursal.size() > 0) ? listaAreaSucursal.get(0) : null;
		obtenerTodos(clase);
	}

	@NotifyChange({ "listaProducto", "listaAreaSucursal", "areaSucursal" })
	@Command
	public void selectArea() {
		obtenerTodos(clase);
	}

	@GlobalCommand
	@NotifyChange("listaProducto")
	public void refrescarlista() {
		obtenerTodos(clase);
	}

	@Command
	public void nuevo() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("area", areaSucursal.getArea());
		Executions.createComponents("/view/formProductoCE.zul", null, parametros);
	}

	@Command
	@NotifyChange("listaProducto")
	public void deleteProducto(@BindingParam("producto") final Producto producto) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + producto.getDescripcion()
				+ "\" esta accion no podra deshacerse si se confirma";

		Messagebox.show(msj, "Advertencia", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						try {
							producto.setEstado(new Estado(Sistema.ESTADO_ELIMINADO));
							productoDAO.saveorUpdate(producto);
							listaProducto.clear();
							obtenerTodos(clase);
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassProducto.this, "listaProducto");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	@Command
	@NotifyChange("producto")
	public void aceptar(@BindingParam("producto") Producto producto) {
		if (producto.getCantidad() <= 0) {
			if (clase.equals("cuadreInventario") || clase.equals("kardex") || clase.equals("notaCompra")
					|| clase.equals("compra")) {

			} else {
				Messagebox.show("No existe producto en inventario");
				return;
			}
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objetoProducto", producto);
		BindUtils.postGlobalCommand(null, null, "cargarProducto", parametros);
		salir();
	}

	@Command
	public void editar(@BindingParam("producto") Producto producto) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", producto);
		Executions.createComponents("/view/formProductoCE.zul", null, parametros);
	}

	private ProductoProveedorDAO productoProveedorDAO = InstanciaDAO.getInstanciaProductoProveedorDAO();

	@Command
	@NotifyChange({ "listaProducto", "listaSucursal", "sucursal", "listaAreaSucursal", "areaSucursal", "estadoArea",
			"estadoSucursal" })
	public void filtrarLetra(@BindingParam("datoBusqueda") String datoBusqueda) {
		if (filtrar(datoBusqueda)) {
			if (clase.equals("notaCompra") || clase.equals("compra")) {
				if (listaProducto == null) {
					listaProducto = new ArrayList<>();
				} else {
					listaProducto.clear();
				}
				List<ProductoProveedor> lista = productoProveedorDAO
						.getListaProductoProveedorByProveedor(proveedor.getId(), datoBusqueda.toUpperCase());
				lista.forEach(x -> {
					listaProducto.add(x.getProducto());
				});

//						listaProducto = productoDAO.getFiltroProductoByAreaSucursalByProveedor(
//								datoBusqueda.toUpperCase(), areaSucursal.getId(), proveedor.getId());
			} else if (clase.equals(ClassVenta.class.getSimpleName())) {
				listaProducto = productoDAO.getListaProductoByDescripcionOrCodigo(datoBusqueda.toUpperCase());
			} else {
				if (Sistema.verificarUsuarioEmpleado(Sistema.getUsuario())) {
					Sistema.getListaSucursal().forEach(x -> {
						if (x.getUsuario().getId() == Sistema.getUsuario().getId()) {
							estadoVerificarSucursal = true;
						}
					});
					if (estadoVerificarSucursal) {
						listaProducto = productoDAO.getFiltroProductoBySucursal(datoBusqueda.toUpperCase(),
								Sistema.getSucursal().getId());
					} else {
						if (Sistema.getAreaSucursal() != null) {
							listaProducto = productoDAO.getListaProductoByDescripcionOrCodigoAndAreaSucursal(
									datoBusqueda.toUpperCase(), areaSucursal.getId());
						}
					}
				} else {
					if (verTodosProductos) {
						listaProducto = productoDAO.getListaProductoByDescripcionOrCodigo(datoBusqueda.toUpperCase());
					} else {
						listaProducto = productoDAO.getListaProductoByDescripcionOrCodigoAndAreaSucursal(
								datoBusqueda.toUpperCase(), areaSucursal.getId());
					}
				}
			}
		}
	}

	@Command
	@NotifyChange("listaProducto")
	public void filtrarNumero() {
		if (filtrar(datoBusquedaStock)) {
			for (Producto item : listaProducto) {
				if (Double.parseDouble(datoBusquedaStock) >= item.getCantidad()) {
					listaProducto.add(item);
				}
			}
		}
	}

	private boolean filtrar(String datoBusqueda) {
		listaProducto.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			stockAviso();
			return false;
		} else {
			return true;
		}
	}

	@Command
	@NotifyChange("listaProducto")
	public void stockAviso() {
		obtenerTodos(clase);
		List<Producto> listaProvisional = new ArrayList<>(listaProducto.size());
		listaProvisional.addAll(listaProducto);
		listaProducto.clear();
		if (stockAviso.trim().equals("alerta")) {
			listaProvisional.forEach(x -> {
				if (x.getColor() != null && x.getColor().trim().equals("background-color:yellow;")) {
					listaProducto.add(x);
				}
			});
		}
		if (stockAviso.trim().equals("critico")) {
			listaProvisional.forEach(x -> {
				if (x.getColor() != null && x.getColor().trim().equals("background-color:red;")) {
					listaProducto.add(x);
				}
			});
		}
		if (stockAviso.trim().equals("criticoAlerta")) {
			listaProvisional.forEach(x -> {
				if (x.getColor() != null && !x.getColor().trim().equals("")) {
					listaProducto.add(x);
				}
			});
		}
		if (stockAviso.trim().equals("todos")) {
			listaProducto.addAll(listaProvisional);
		}
	}

	@Command
	public void generarExcel() {
		if (listaProducto.size() == 0) {
			Messagebox.show("No hay productos para el reporte");
			return;
		}
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("empresa", Sistema.getListaInformacionNegocio().get(0).getNombre());
		mapreporte.put("direccion", Sistema.getListaInformacionNegocio().get(0).getDireccion());
		mapreporte.put("telefono", Sistema.getListaInformacionNegocio().get(0).getTelefono());
		mapreporte.put("usuario", Sistema.getUsuario().getNombre());
		Sistema.reporteEXCEL(Sistema.inventarioExcelJasper, "Inventario", mapreporte,
				Sistema.getProductoDataSource(listaProducto));
	}

	@Command
	public void generarPdf() {
		if (listaProducto.size() == 0) {
			Messagebox.show("No hay productos para el reporte");
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("lista", listaProducto);
		Executions.createComponents("/reportes/impresionProducto.zul", null, parametros);
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("empresa",
				listaProducto.get(0).getAreaSucursal().getSucursal().getInformacionNegocio().getNombre());
		mapreporte.put("direccion",
				listaProducto.get(0).getAreaSucursal().getSucursal().getInformacionNegocio().getDireccion());
		mapreporte.put("telefono",
				listaProducto.get(0).getAreaSucursal().getSucursal().getInformacionNegocio().getTelefono());
		mapreporte.put("usuario", Sistema.getUsuario().getNombre());
		iframe.setContent(
				Sistema.reportePDF(Sistema.inventarioJasper, mapreporte, Sistema.getProductoDataSource(listaProducto)));
	}

	@Command
	public void generarCodigoBarra(@BindingParam("producto") Producto producto) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("producto", producto);
		Executions.createComponents("/reportes/impresionCodigoBarra.zul", null, parametros);
	}

	@Command
	public void showReportCodigoBarra(@BindingParam("item") Iframe iframe) {
		List<Producto> lista = new ArrayList<>();
		for (int i = 0; i < 23; i++) {
			lista.add(producto);
		}
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("producto", producto.getDescripcion());
		iframe.setContent(
				Sistema.reportePDF(Sistema.codigoBarraJasper, mapreporte, Sistema.getProductoDataSource(lista)));
	}

	@Command
	public void salir() {
		winFormProducto.detach();
	}

	/*
	 * Metodos Get And Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public String getDatoBusquedaStock() {
		return datoBusquedaStock;
	}

	public void setDatoBusquedaStock(String datoBusquedaStock) {
		this.datoBusquedaStock = datoBusquedaStock;
	}

	public List<Producto> getListaProducto() {
		return listaProducto;
	}

	public void setListaProducto(List<Producto> listaProducto) {
		this.listaProducto = listaProducto;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public boolean isBtGuardar() {
		return btGuardar;
	}

	public boolean isEstadoStock() {
		return estadoStock;
	}

	public void setEstadoStock(boolean estadoStock) {
		this.estadoStock = estadoStock;
	}

	public void setBtGuardar(boolean btGuardar) {
		this.btGuardar = btGuardar;
	}

	public String getDimensionWidth() {
		return dimensionWidth;
	}

	public void setDimensionWidth(String dimensionWidth) {
		this.dimensionWidth = dimensionWidth;
	}

	public String getStockAviso() {
		return stockAviso;
	}

	public void setStockAviso(String stockAviso) {
		this.stockAviso = stockAviso;
	}

	public List<AreaSucursal> getListaAreaSucursal() {
		return listaAreaSucursal;
	}

	public void setListaAreaSucursal(List<AreaSucursal> listaAreaSucursal) {
		this.listaAreaSucursal = listaAreaSucursal;
	}

	public AreaSucursal getAreaSucursal() {
		return areaSucursal;
	}

	public void setAreaSucursal(AreaSucursal areaSucursal) {
		this.areaSucursal = areaSucursal;
	}

	public boolean isEstadoArea() {
		return estadoArea;
	}

	public void setEstadoArea(boolean estadoArea) {
		this.estadoArea = estadoArea;
	}

	public boolean isVerTodosProductos() {
		return verTodosProductos;
	}

	public void setVerTodosProductos(boolean verTodosProductos) {
		this.verTodosProductos = verTodosProductos;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

}
