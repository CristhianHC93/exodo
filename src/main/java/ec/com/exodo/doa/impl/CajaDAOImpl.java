package ec.com.exodo.doa.impl;

import java.util.Date;
import java.util.List;

import ec.com.exodo.dao.ie.CajaDAO;
import ec.com.exodo.entity.Caja;

public class CajaDAOImpl extends DAOImplements implements CajaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Caja> getListaCaja() {
		return (List<Caja>) super.getListaRegistro("SELECT c FROM Caja c");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Caja> getListaCaja(int areaSucursal, boolean estado) {
		return (List<Caja>) super.getListaRegistro(areaSucursal, "areaSucursal", estado, "estado",
				"SELECT c FROM Caja c where c.areaSucursal.id = :areaSucursal and c.estado = :estado");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Caja> getListaCaja(boolean estado, Date fi, Date ff) {
		String sql = "SELECT c FROM Caja c where c.fecha between :fechaInicial and :fechaFinal and c.estado = :estado ";
		return (List<Caja>) super.getListaRegistro(estado, "estado", fi, "fechaInicial", ff, "fechaFinal", sql);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Caja> getListaCaja(int areaSucursal, boolean estado, Date fi, Date ff) {
		String sql = "SELECT c FROM Caja c where c.fecha between :fechaInicial and :fechaFinal and c.estado = :estado and c.areaSucursal.id = :areaSucursal";
		return (List<Caja>) super.getListaRegistro(areaSucursal, "areaSucursal", estado, "estado", fi, "fechaInicial",
				ff, "fechaFinal", sql);
	}

	@Override
	public Caja getCaja(int areaSucursal, boolean estado) {
		final Object object = super.getRegistro(areaSucursal, "areaSucursal", estado, "estado",
				"SELECT c FROM Caja c where c.areaSucursal.id = :areaSucursal and c.estado = :estado");
		return (object != null) ? ((Caja) object) : null;
	}

	@Override
	public Caja saveorUpdate(Caja caja) {
		return (Caja) super.saveorUpdate(caja);
	}

}
