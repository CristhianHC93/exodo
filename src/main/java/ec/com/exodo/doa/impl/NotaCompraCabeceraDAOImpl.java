package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.NotaCompraCabeceraDAO;
import ec.com.exodo.entity.NotaCompraCabecera;
import ec.com.exodo.entity.NotaCompraDetalle;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.models.Modelos;

public class NotaCompraCabeceraDAOImpl extends DAOImplements implements NotaCompraCabeceraDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<NotaCompraCabecera> getListaNotaCompraCabeceraByEstado(int estado) {
		return (List<NotaCompraCabecera>) super.getListaRegistro(estado, "estado",
				"SELECT n FROM NotaCompraCabecera n where n.estado.id = :estado ");
	}

	@Override
	public NotaCompraCabecera saveorUpdate(NotaCompraCabecera notaCompraCabecera) {
		return (NotaCompraCabecera) super.saveorUpdate(notaCompraCabecera);
	}

	@Override
	public NotaCompraCabecera getNotaCompraCabeceraByCodio(String codigo) {
		final Object object = super.getRegistro(codigo, "codigo",
				"SELECT n FROM NotaCompraCabecera n where n.codigo = :codigo");
		return (object != null) ? ((NotaCompraCabecera) object) : null;
	}

	@Override
	public List<Modelos<NotaCompraDetalle, Object>> saveorUpdateDetalle(
			List<Modelos<NotaCompraDetalle, Object>> listaNotaCompra, NotaCompraCabecera notaCompraCabecera) {
		try {
			for (Modelos<NotaCompraDetalle, Object> lnc : listaNotaCompra) {
				lnc.getObjetoT().setNotaCompraCabecera(notaCompraCabecera);
				lnc.setObjetoT(InstanciaDAO.getInstanciaNotaCompraDetalleDAO().saveorUpdate(lnc.getObjetoT()));
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return listaNotaCompra;
	}
}
