package ec.com.exodo.doa.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import ec.com.exodo.jpa.JPAPersistenceManager;

public class DAOImplements {
	private EntityManagerFactory emf = null;

	public DAOImplements() {
		this.emf = JPAPersistenceManager.getInstance().getEntityManagerFactory();
	}

	protected EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	public Object saveorUpdate(Object object) {
		EntityManager em = getEntityManager();
		EntityTransaction t = em.getTransaction();
		Object objectDevuelto = null;
		try {
			t.begin();
			objectDevuelto = em.merge(object);
			t.commit();
		} catch (RuntimeException e) {
			if (t.isActive()) {
				System.out.println(this.getClass().getSimpleName().concat(" *** saveorUpdate(Object object) ---> ")
						.concat(object.toString()).concat(" --->").concat(e.getMessage()));
				t.rollback();
			}
		} finally {
			em.close();
		}
		return objectDevuelto;
	}

	public int eliminar(int parametro, String var, String sql) {
		EntityManager em = getEntityManager();
		EntityTransaction t = em.getTransaction();
		int enteroDevuelto = 0;
		try {
			t.begin();
			Query query = em.createQuery(sql);
			query.setParameter(var, parametro);
			query.executeUpdate();
			t.commit();
			enteroDevuelto = 1;
		} catch (RuntimeException e) {
			if (t.isActive()) {
				t.rollback();
				System.out.println(this.getClass().getSimpleName()
						.concat(" *** eliminar(int parametro, String var, String sql) ---> ").concat(sql)
						.concat(" ---> ").concat(e.getMessage()));
				enteroDevuelto = 2;
			}
		} finally {
			em.close();
		}
		return enteroDevuelto;
	}
	
	public int eliminar(int parametro, String var,int parametro2, String var2, String sql) {
		EntityManager em = getEntityManager();
		EntityTransaction t = em.getTransaction();
		int enteroDevuelto = 0;
		try {
			t.begin();
			Query query = em.createQuery(sql);
			query.setParameter(var, parametro);
			query.setParameter(var2, parametro2);
			query.executeUpdate();
			t.commit();
			enteroDevuelto = 1;
		} catch (RuntimeException e) {
			if (t.isActive()) {
				t.rollback();
				System.out.println(this.getClass().getSimpleName()
						.concat(" *** eliminar(int parametro, String var, String sql) ---> ").concat(sql)
						.concat(" ---> ").concat(e.getMessage()));
				enteroDevuelto = 2;
			}
		} finally {
			em.close();
		}
		return enteroDevuelto;
	}

	public Object getRegistro(Object parametro, String var, String sql) {
		EntityManager em = getEntityManager();
		Object object = null;
		try {
			Query query = em.createQuery(sql);
			query.setParameter(var, parametro);
			object = query.getSingleResult();
		} catch (Exception e) {
			System.out.print(this.getClass().getSimpleName().concat(" *** ").concat(sql).concat(" ---> ")
					.concat(e.getMessage()));
			return object;
		} finally {
			em.close();
		}
		return object;
	}

	public Object getRegistro(Object parametro, String var, Object parametro2, String var2, String sql) {
		EntityManager em = getEntityManager();
		Object object = null;
		try {
			Query query = em.createQuery(sql);
			query.setParameter(var, parametro);
			query.setParameter(var2, parametro2);
			object = query.getSingleResult();
		} catch (Exception e) {
			System.out.print(this.getClass().getSimpleName().concat(" *** ").concat(sql).concat(" ---> ")
					.concat(e.getMessage()));
			return object;
		} finally {
			em.close();
		}
		return object;
	}

	public Object getRegistro(Object parametro, String var, Object parametro2, String var2, Object parametro3,
			String var3, String sql) {
		EntityManager em = getEntityManager();
		Object object = null;
		try {
			Query query = em.createQuery(sql);
			query.setParameter(var, parametro);
			query.setParameter(var2, parametro2);
			query.setParameter(var3, parametro3);
			object = query.getSingleResult();
		} catch (Exception e) {
			System.out.print(this.getClass().getSimpleName().concat(" *** ").concat(sql).concat(" ---> ")
					.concat(e.getMessage()));
			return object;
		} finally {
			em.close();
		}
		return object;
	}

	public List<?> getListaRegistro(String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistro(Object parametro, String var, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistro(Object parametro, String var, Object parametro2, String var2, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setParameter(var2, parametro2);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistro(Object parametro, String var, Object parametro2, String var2, Object parametro3,
			String var3, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setParameter(var2, parametro2);
		query.setParameter(var3, parametro3);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistro(Object parametro, String var, Object parametro2, String var2, Object parametro3,
			String var3, Object parametro4, String var4, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setParameter(var2, parametro2);
		query.setParameter(var3, parametro3);
		query.setParameter(var4, parametro4);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistroFiltro(String parametro, String var, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, '%' + parametro + '%');
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistroFiltro(String parametro, String var, Object parametro2, String var2, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, '%' + parametro + '%');
		query.setParameter(var2, parametro2);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistroFiltro(String parametro, String var, Object parametro2, String var2,
			Object parametro3, String var3, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, '%' + parametro + '%');
		query.setParameter(var2, parametro2);
		query.setParameter(var3, parametro3);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public Object getRegistroUltimo(int parametro, String var, String sql) {
		Object object = null;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setMaxResults(1);
		try {
			object = query.getSingleResult();
		} catch (Exception e) {
			System.out.print(this.getClass().getSimpleName().concat("->").concat(e.getMessage()));
		} finally {
			em.close();
		}

		return object;
	}
}
