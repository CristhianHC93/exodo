package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.ConceptoMovimientoDAO;
import ec.com.exodo.entity.ConceptoMovimiento;

public class ConceptoMovimientoDAOImpl extends DAOImplements implements ConceptoMovimientoDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<ConceptoMovimiento> getListaConceptoMovimiento() {
		return (List<ConceptoMovimiento>) super.getListaRegistro("SELECT c FROM ConceptoMovimiento c");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConceptoMovimiento> getListaConceptoMovimiento(int codigo,boolean estado) {
		return (List<ConceptoMovimiento>) super.getListaRegistro(codigo, "codigo",estado,"estado",
				"SELECT c FROM ConceptoMovimiento c where c.codigo = :codigo and c.estado = :estado");
	}

	@Override
	public ConceptoMovimiento getConceptoMovimientoById(int id) {
		final Object object =  super.getRegistro(id, "id",
				"SELECT c FROM ConceptoMovimiento c where c.id = :id");
		return (object != null) ? ((ConceptoMovimiento) object) : null;
	}

	@Override
	public ConceptoMovimiento saveorUpdate(ConceptoMovimiento conceptoMovimiento) {
		return (ConceptoMovimiento) super.saveorUpdate(conceptoMovimiento);
	}

	@Override
	public int eliminar(int idConceptoMovimiento) {
		return super.eliminar(idConceptoMovimiento, "id", "DELETE FROM ConceptoMovimiento where id = :id");
	}

}
