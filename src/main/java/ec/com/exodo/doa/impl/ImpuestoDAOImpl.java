package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.ImpuestoDAO;
import ec.com.exodo.entity.Impuesto;

public class ImpuestoDAOImpl extends DAOImplements implements ImpuestoDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Impuesto> getImpuestos() {
		return (List<Impuesto>) super.getListaRegistro("SELECT i FROM Impuesto i");
	}
}
