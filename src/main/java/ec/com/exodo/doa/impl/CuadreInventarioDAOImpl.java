package ec.com.exodo.doa.impl;

import java.util.Date;
import java.util.List;

import ec.com.exodo.dao.ie.CuadreInventarioDAO;
import ec.com.exodo.entity.CuadreInventario;

public class CuadreInventarioDAOImpl extends DAOImplements implements CuadreInventarioDAO {

	@Override
	public CuadreInventario getCuadreInventarioByProductoAndEstado(int producto, boolean estado) {
		final Object object = super.getRegistro(producto, "producto", estado, "estado",
				"SELECT c FROM CuadreInventario c where c.producto.id = :producto and c.cuadreInventarioCabecera.estado = :estado order by c.id desc");
		return (object != null) ? ((CuadreInventario) object) : null;
	}

	public CuadreInventario getCuadreInventarioByProductoCodigoAndAreaSucursal(String codigo, int area) {
		final Object object = super.getRegistro(codigo, "codigo", area, "area",
				"SELECT c FROM CuadreInventario c where c.producto.codigo = :codigo and c.cuadreInventarioCabecera.areaSucursal.id = :area");
		return (object != null) ? ((CuadreInventario) object) : null;
	}

	@Override
	public CuadreInventario saveorUpdate(CuadreInventario cuadreInventario) {
		return (CuadreInventario) super.saveorUpdate(cuadreInventario);
	}

	@Override
	public int eliminar(CuadreInventario cuadreInventario) {
		return super.eliminar(cuadreInventario.getId(), "id", "DELETE FROM CuadreInventario where id = :id");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CuadreInventario> getListaCuadreInventarioByProductoAndFecha(int idProducto, Date fechaInicial,
			Date fechaFinal) {
		String sql = "SELECT c FROM CuadreInventario c where c.fecha between :fechaInicial and :fechaFinal and c.producto.id = :idProducto";
		return (List<CuadreInventario>) super.getListaRegistro(idProducto, "idProducto", fechaInicial, "fechaInicial",
				fechaFinal, "fechaFinal", sql);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CuadreInventario> getListaCuadreInventarioByProductoAndConceptoMovmiento(int idProducto, int idConceptoMovimiento) {
		String sql = "SELECT c FROM CuadreInventario c where c.producto.id = :idProducto and c.conceptoMovimiento.id = :idConceptoMovimiento";
		return (List<CuadreInventario>) super.getListaRegistro(idProducto, "idProducto", idConceptoMovimiento,
				"idConceptoMovimiento", sql);
	}
}
