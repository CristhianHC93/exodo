package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.ClienteDAO;
import ec.com.exodo.entity.Cliente;

public class ClienteDAOImpl extends DAOImplements implements ClienteDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> getListaClienteByEstado(int idEstado) {
		return (List<Cliente>) super.getListaRegistro(idEstado, "idEstado",
				"SELECT c FROM Cliente c where c.estado.id = :idEstado");
	}

	@Override
	public Cliente saveorUpdate(Cliente cliente) {
		return (Cliente) super.saveorUpdate(cliente);
	}

	@Override
	public Cliente getClienteByCedula(String cedula) {
		final Object object = super.getRegistro(cedula, "cedula",
				"SELECT c FROM Cliente c where c.estado.id = 1 and c.cedula = :cedula");
		return (object != null) ? ((Cliente) object) : null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> getFiltroCliente(String dato) {
		return (List<Cliente>) super.getListaRegistro("%".concat(dato).concat("%"), "dato",
				"SELECT c FROM Cliente c where c.estado.id = 1 and c.cedula like :dato or c.nombre like :dato");
	}
}
