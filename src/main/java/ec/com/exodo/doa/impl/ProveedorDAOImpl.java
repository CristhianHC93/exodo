package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.ProveedorDAO;
import ec.com.exodo.entity.Proveedor;

public class ProveedorDAOImpl extends DAOImplements implements ProveedorDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Proveedor> getProveedores(int estado) {
		return (List<Proveedor>) super.getListaRegistro(estado, "estado",
				"SELECT p FROM Proveedor p where p.estado.id = :estado ");
	}

	@Override
	public Proveedor saveorUpdate(Proveedor proveedor) {
		return (Proveedor) super.saveorUpdate(proveedor);
	}

	@Override
	public Proveedor getProveedorByEmpresaOrRuc(String dato) {
		final Object object = super.getRegistro(dato, "dato",
				"SELECT p FROM Proveedor p where (p.ruc = :dato or p.empresa = :dato)");
		return (object != null) ? ((Proveedor) object) : null;
	}

}
