package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.NotaCompraDetalleDAO;
import ec.com.exodo.entity.NotaCompraDetalle;

public class NotaCompraDetalleDAOImpl extends DAOImplements implements NotaCompraDetalleDAO {

	@Override
	public NotaCompraDetalle saveorUpdate(NotaCompraDetalle notaCompraDetalle) {
		return (NotaCompraDetalle) super.saveorUpdate(notaCompraDetalle);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NotaCompraDetalle> getNotaCompraDetallesByCabecera(int idNotaCompraCabecera, int estado) {
		String sql = "SELECT n FROM NotaCompraDetalle n where n.notaCompraCabecera.id = :idNotaCompraCabecera and n.estado.id = :estado";
		return (List<NotaCompraDetalle>) super.getListaRegistro(idNotaCompraCabecera, "idNotaCompraCabecera", estado,
				"estado", sql);
	}
}
