package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.InformacionNegocioDAO;
import ec.com.exodo.entity.InformacionNegocio;

public class InformacionNegocioDAOImpl extends DAOImplements implements InformacionNegocioDAO {
	@Override
	public InformacionNegocio saveorUpdate(InformacionNegocio informacionNegocio) {
		return (InformacionNegocio) super.saveorUpdate(informacionNegocio);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InformacionNegocio> getListaInformacionNegocio() {
		return (List<InformacionNegocio>) super.getListaRegistro("SELECT e FROM InformacionNegocio e");
	}
}
