package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.ProductoProveedorDAO;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.ProductoProveedor;

public class ProductoProveedorDAOImpl extends DAOImplements implements ProductoProveedorDAO {

	@Override
	public ProductoProveedor saveorUpdate(ProductoProveedor ProductoProveedor) {
		return (ProductoProveedor) super.saveorUpdate(ProductoProveedor);
	}

	@Override
	public int eliminar(Producto producto) {
		return super.eliminar(producto.getId(), "id", "DELETE FROM ProductoProveedor p where p.producto.id = :id");
	}

	@Override
	public int eliminarByProductoAndProveedor(int idProducto, int idProveedor) {
		return super.eliminar(idProducto, "idProducto", idProveedor, "idProveedor",
				"DELETE FROM ProductoProveedor p where p.producto.id = :idProducto and p.proveedor.id = :idProveedor");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductoProveedor> getListaProductoProveedorByProducto(int idProducto) {
		return (List<ProductoProveedor>) super.getListaRegistro(idProducto, "idProducto",
				"SELECT p FROM ProductoProveedor p where p.producto.id = :idProducto order by p.id desc");
	}

	@Override
	public ProductoProveedor getProductoProveedorByProductoAndProveedor(int idProducto, int idProveedor) {
		final Object object =  super.getRegistro(idProducto, "idProducto", idProveedor, "idProveedor",
				"SELECT p FROM ProductoProveedor p where p.producto.id = :idProducto and p.proveedor.id = :idProveedor");
		return (object != null) ? ((ProductoProveedor) object) : null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductoProveedor> getListaProductoProveedorByProveedorAndSucursal(int idProveedor, String dato,
			int sucursal) {
		return (List<ProductoProveedor>) super.getListaRegistro(idProveedor, "idProveedor", sucursal, "sucursal",
				"%" + dato + "%", "dato",
				"SELECT p FROM ProductoProveedor p where p.proveedor.id = :idProveedor  and p.producto.areaSucursal.sucursal.id = :sucursal and "
						+ "(UPPER(p.producto.descripcion) like :dato or UPPER(p.producto.codigo) like :dato) and p.producto.estado.id = 1 "
						+ "order by p.producto.id desc");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductoProveedor> getListaProductoProveedorByProveedor(int idProveedor, String dato) {
		return (List<ProductoProveedor>) super.getListaRegistro(idProveedor, "idProveedor", "%" + dato + "%", "dato",
				"SELECT p FROM ProductoProveedor p where p.proveedor.id = :idProveedor and "
						+ "(UPPER(p.producto.descripcion) like :dato or UPPER(p.producto.codigo) like :dato) and p.producto.estado.id = 1 "
						+ "order by p.producto.id desc");
	}

	@Override
	public ProductoProveedor getProductoByCodigoDescripcionProveedor(String dato, int proveedor) {
		final Object object = super.getRegistro(dato, "dato", proveedor, "proveedor",
				"SELECT p FROM ProductoProveedor p where p.proveedor.id = :proveedor and (p.producto.descripcion = :dato or p.producto.codigo = :dato) and p.producto.estado.id = 1");
		return (object != null) ? ((ProductoProveedor) object) : null;
	}

}
