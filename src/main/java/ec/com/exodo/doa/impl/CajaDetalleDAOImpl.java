package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.CajaDetalleDAO;
import ec.com.exodo.entity.CajaDetalle;

public class CajaDetalleDAOImpl extends DAOImplements implements CajaDetalleDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<CajaDetalle> getListaCajaDetalle() {
		return (List<CajaDetalle>) super.getListaRegistro("SELECT c FROM CajaDetalle c");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CajaDetalle> getListaCajaDetalleByCaja(int idCaja) {
		return (List<CajaDetalle>) super.getListaRegistro(idCaja, "idCaja",
				"SELECT c FROM CajaDetalle c where c.caja.id = :idCaja");
	}

	@Override
	public CajaDetalle saveorUpdate(CajaDetalle cajaDetalle) {
		return (CajaDetalle) super.saveorUpdate(cajaDetalle);
	}
}
