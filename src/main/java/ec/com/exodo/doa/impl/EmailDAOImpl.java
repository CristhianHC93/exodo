package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.EmailDAO;
import ec.com.exodo.entity.Email;

public class EmailDAOImpl extends DAOImplements implements EmailDAO {

	@Override
	public Email saveorUpdate(Email email) {
		return (Email) super.saveorUpdate(email);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Email> getListaEmail() {
		return (List<Email>) super.getListaRegistro("SELECT e FROM Email e");
	}

}
