package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.ModuloDetalleDAO;
import ec.com.exodo.entity.ModuloDetalle;

public class ModuloDetalleDAOImpl extends DAOImplements implements ModuloDetalleDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<ModuloDetalle> getListaModuloDetallesByModulo(int idModulo) {
		String sql = "SELECT m FROM ModuloDetalle m where m.modulo.id = :idModulo";
		return (List<ModuloDetalle>) super.getListaRegistro(idModulo, "idModulo", sql);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ModuloDetalle> getListaModuloDetalle() {
		return (List<ModuloDetalle>) super.getListaRegistro("SELECT m FROM ModuloDetalle m");
	}

	@Override
	public ModuloDetalle saveorUpdate(ModuloDetalle moduloDetalle) {
		return (ModuloDetalle) super.saveorUpdate(moduloDetalle);
	}

}
