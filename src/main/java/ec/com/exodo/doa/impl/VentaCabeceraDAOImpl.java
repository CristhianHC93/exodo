package ec.com.exodo.doa.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import ec.com.exodo.dao.ie.CuadreInventarioDAO;
import ec.com.exodo.dao.ie.VentaCabeceraDAO;
import ec.com.exodo.entity.ConceptoMovimiento;
import ec.com.exodo.entity.CuadreInventario;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.VentaCabecera;
import ec.com.exodo.entity.VentaDetalle;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.models.Modelos;
import ec.com.exodo.sistema.Sistema;

public class VentaCabeceraDAOImpl extends DAOImplements implements VentaCabeceraDAO {

	private CuadreInventarioDAO cuadreInventarioDAO = InstanciaDAO.getInstanciaCuadreInventarioDAO();

	@SuppressWarnings("unchecked")
	@Override
	public List<VentaCabecera> getListaVentaCabeceraByAreaSucursal(int estado, int areaSucursal) {
		return (List<VentaCabecera>) super.getListaRegistro(estado, "estado", areaSucursal, "areaSucursal",
				"SELECT v FROM VentaCabecera v where v.estado.id = :estado and v.areaSucursal.id = :areaSucursal");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VentaCabecera> getListaVentaCabecera(int estado) {
		return (List<VentaCabecera>) super.getListaRegistro(estado, "estado",
				"SELECT v FROM VentaCabecera v where v.estado.id = :estado");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VentaCabecera> getListaVentaCabeceraBySucursal(int estado, int sucursal) {
		return (List<VentaCabecera>) super.getListaRegistro(estado, "estado", sucursal, "sucursal",
				"SELECT v FROM VentaCabecera v where v.estado.id = :estado and v.areaSucursal.sucursal.id = :sucursal");
	}

	@Override
	public VentaCabecera saveorUpdate(VentaCabecera ventaCabecera) {
		return (VentaCabecera) super.saveorUpdate(ventaCabecera);
	}

	@Override
	public void saveorUpdateDetalle(List<Modelos<VentaDetalle, Object>> listaVenta, VentaCabecera ventaCabecera) {
		EntityManager em = getEntityManager();
		EntityTransaction t = em.getTransaction();
		try {
			ConceptoMovimiento cm = new ConceptoMovimiento();
			cm.setId(Sistema.ID_VENTA);

			for (Modelos<VentaDetalle, Object> detalle : listaVenta) {
				t.begin();
				// VentaDetalle detalle = lc.getObjetoT();// new VentaDetalle();
				if (detalle.getObjetoT().getEstado().getId() == Sistema.ESTADO_ENTREGADO_VENTA
						|| detalle.getObjetoT().getEstado().getId() == Sistema.ESTADO_PLAN_VENTA) {
					guardarCuadreInventario(detalle.getObjetoT());
				}
				// detalle = lc.getObjetoT();
				detalle.getObjetoT().setVentaCabecera(ventaCabecera);
				detalle.setObjetoT(em.merge(detalle.getObjetoT()));
				t.commit();
			}
		} catch (RuntimeException e) {
			if (t.isActive()) {
				t.rollback();
			}
			e.printStackTrace();
		} finally {
			em.close();
		}
	}

	private void guardarCuadreInventario(VentaDetalle ventaDetalle) {
		final CuadreInventario cuadreInventario = new CuadreInventario();
		final Producto producto = ventaDetalle.getProducto();

		cuadreInventario.setValorVenta(Sistema.redondear(ventaDetalle.getPrecioVenta()));
		cuadreInventario.setTotalVenta(Sistema.redondear(ventaDetalle.getPrecioSubtotal()));

		cuadreInventario.setFecha(new Timestamp(new Date().getTime()));
		cuadreInventario.setConceptoMovimiento(new ConceptoMovimiento(Sistema.ID_VENTA));

		cuadreInventario.setCantidadAntes(producto.getCantidad());
		cuadreInventario.setCantidad(ventaDetalle.getCantidad());
		cuadreInventario.setCantidadDespues(producto.getCantidad() - ventaDetalle.getCantidad());

		producto.setCantidad(cuadreInventario.getCantidadDespues());

		cuadreInventario.setProducto(producto);
		cuadreInventario.setObservacion("Venta de " + ventaDetalle.getCantidad() + " " + producto.getDescripcion());
		/* Guarda Cuadre de Inventario y cambios del Producto */
		cuadreInventarioDAO.saveorUpdate(cuadreInventario);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VentaCabecera> getListaVentaCabeceraByAreaSucursalByFecha(int areaSucursal, int estado, Date fi,
			Date ff) {
		String sql = "SELECT v FROM VentaCabecera v where v.fechaEmision between :fechaInicial and :fechaFinal and v.estado.id = :estado and v.areaSucursal.id = :areaSucursal";
		return (List<VentaCabecera>) super.getListaRegistro(areaSucursal, "areaSucursal", estado, "estado", fi,
				"fechaInicial", ff, "fechaFinal", sql);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VentaCabecera> getListaVentaCabeceraByFecha(int estado, Date fi, Date ff) {
		String sql = "SELECT v FROM VentaCabecera v where v.fechaEmision between :fechaInicial and :fechaFinal and v.estado.id = :estado";
		return (List<VentaCabecera>) super.getListaRegistro(estado, "estado", fi, "fechaInicial", ff, "fechaFinal",
				sql);
	}

	@Override
	public VentaCabecera getUltimaVentaCabecera(int estado) {
		return (VentaCabecera) super.getRegistroUltimo(estado, "estado",
				"SELECT v FROM VentaCabecera v where v.estado.id = :estado  or v.estado.id = 3 ORDER BY v.id desc ");
	}

}
