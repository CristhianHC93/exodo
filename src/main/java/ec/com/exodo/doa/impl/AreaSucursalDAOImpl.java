package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.AreaSucursalDAO;
import ec.com.exodo.entity.AreaSucursal;

public class AreaSucursalDAOImpl extends DAOImplements implements AreaSucursalDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<AreaSucursal> getListaAreaSucursal() {
		return (List<AreaSucursal>) super.getListaRegistro("SELECT a FROM Area a");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AreaSucursal> getListaAreaSucursalBySucursal(int sucursal) {
		return (List<AreaSucursal>) super.getListaRegistro(sucursal, "sucursal",
				"SELECT a FROM AreaSucursal a where a.sucursal.id = :sucursal order by a.area.id asc");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AreaSucursal> getListaAreaSucursalBySucursalActivo(int sucursal) {
		return (List<AreaSucursal>) super.getListaRegistro(sucursal, "sucursal",
				"SELECT a FROM AreaSucursal a where a.sucursal.id = :sucursal and a.estado = true order by a.area.id asc");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AreaSucursal> getListaAreaSucursalByArea(int area) {
		return (List<AreaSucursal>) super.getListaRegistro(area, "area",
				"SELECT a FROM AreaSucursal a where a.area.id = :area order by a.area.id asc");
	}

	@Override
	public AreaSucursal getAreaSucursalByAreaAndSucursal(int area, int sucursal) {
		final Object object = super.getRegistro(area, "area", sucursal, "sucursal",
				"SELECT a FROM AreaSucursal a where a.area.id = :area and a.sucursal.id = :sucursal ");
		return (object != null) ? ((AreaSucursal) object) : null;
	}

	@Override
	public AreaSucursal saveorUpdate(AreaSucursal areaSucursal) {
		return (AreaSucursal) super.saveorUpdate(areaSucursal);
	}

	@Override
	public int eliminar(AreaSucursal areaSucursal) {
		return super.eliminar(areaSucursal.getId(), "id", "DELETE FROM Area where id = :id");
	}
}
