package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.SucursalDAO;
import ec.com.exodo.entity.Sucursal;

public class SucursalDAOImpl extends DAOImplements implements SucursalDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Sucursal> getListaSucursal() {
		return (List<Sucursal>) super.getListaRegistro("SELECT s FROM Sucursal s order by s.id asc");
	}

	@Override
	public Sucursal getSucursal(int id) {
		final Object object = super.getRegistro(id, "id",
				"SELECT s FROM Sucursal s where s.id = :id order by s.id asc");
		return (object != null) ? ((Sucursal) object) : null;
	}

	@Override
	public Sucursal saveorUpdate(Sucursal sucursal) {
		return (Sucursal) super.saveorUpdate(sucursal);
	}

	@Override
	public int eliminar(Sucursal sucursal) {
		return super.eliminar(sucursal.getId(), "id", "DELETE FROM Sucursal where id = :id");
	}

}
