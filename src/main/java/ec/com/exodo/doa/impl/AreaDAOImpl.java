package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.AreaDAO;
import ec.com.exodo.entity.Area;

public class AreaDAOImpl extends DAOImplements implements AreaDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Area> getListaArea() {
		return (List<Area>) super.getListaRegistro("SELECT a FROM Area a");
	}

	@Override
	public Area saveorUpdate(Area area) {
		return (Area)super.saveorUpdate(area);
	}

	@Override
	public int eliminar(Area area) {
		return super.eliminar(area.getId(), "id", "DELETE FROM Area where id = :id");
	}
}
