package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.CategoriaDAO;
import ec.com.exodo.entity.Categoria;

public class CategoriaDAOImpl extends DAOImplements implements CategoriaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Categoria> getListaCategoria() {
		return (List<Categoria>) super.getListaRegistro("SELECT c FROM Categoria c order by c.id");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Categoria> getListaCategoriaByArea(int idArea) {
		return (List<Categoria>) super.getListaRegistro(idArea, "idArea",
				"SELECT c FROM Categoria c where c.area.id = :idArea order by c.id");
	}

	@Override
	public Categoria saveorUpdate(Categoria categoria) {
		return (Categoria) super.saveorUpdate(categoria);
	}

	@Override
	public int eliminar(Categoria categoria) {
		return super.eliminar(categoria.getId(), "id", "DELETE FROM Categoria where id = :id");
	}
}
