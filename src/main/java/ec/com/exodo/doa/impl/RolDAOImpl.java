package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.RolDAO;
import ec.com.exodo.entity.Rol;

public class RolDAOImpl extends DAOImplements implements RolDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Rol> getListaRol() {
		return (List<Rol>) super.getListaRegistro("SELECT r FROM Rol r where r.id !=1");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Rol> getListaRol(int estado) {
		return (List<Rol>) super.getListaRegistro(estado, "estado", "SELECT r FROM Rol r where r.estado.id = :estado and r.id !=1");
	}

	@Override
	public Rol saveorUpdate(Rol rol) {
		return (Rol) super.saveorUpdate(rol);
	}

}
