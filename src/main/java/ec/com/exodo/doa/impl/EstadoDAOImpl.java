package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.EstadoDAO;
import ec.com.exodo.entity.Estado;

public class EstadoDAOImpl extends DAOImplements implements EstadoDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Estado> getListaEstadoByModulo(int modulo) {
		return (List<Estado>) super.getListaRegistro(modulo, "modulo",
				"SELECT e FROM Estado e where e.codigo = :modulo");
	}

}
