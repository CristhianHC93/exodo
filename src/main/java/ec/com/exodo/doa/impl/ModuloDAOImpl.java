package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.ModuloDAO;
import ec.com.exodo.entity.Modulo;

public class ModuloDAOImpl extends DAOImplements implements ModuloDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Modulo> getListaModulo() {
		return (List<Modulo>) super.getListaRegistro("SELECT m FROM Modulo m");
	}

	@Override
	public Modulo saveorUpdate(Modulo modulo) {
		return (Modulo) super.saveorUpdate(modulo);
	}

}
