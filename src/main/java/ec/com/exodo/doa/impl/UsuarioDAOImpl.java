package ec.com.exodo.doa.impl;

import java.sql.Time;
import java.util.List;

import ec.com.exodo.dao.ie.UsuarioDAO;
import ec.com.exodo.entity.Usuario;

public class UsuarioDAOImpl extends DAOImplements implements UsuarioDAO {

	@Override
	public Usuario saveorUpdate(Usuario usuario) {
		return (Usuario) super.saveorUpdate(usuario);
	}

	@Override
	public Usuario getUsuario(Usuario usuario, Time t) {
		String sql = "SELECT u FROM Usuario u where u.id = :id and u.estado = true and :t BETWEEN u.horaEntrada AND u.horaSalida";
		final Object object = super.getRegistro(usuario.getId(), "id", t, "t", sql);
		return (object != null) ? ((Usuario) object) : null;
	}

	@Override
	public Usuario getUsuario(String username, String password) {
		String sql = "SELECT u FROM Usuario u where u.username = :username and u.password = :password and u.estado = true";
		final Object object = super.getRegistro(username, "username", password, "password", sql);
		return (object != null) ? ((Usuario) object) : null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> getListaUsuario() {
		return (List<Usuario>) super.getListaRegistro("SELECT u FROM Usuario u  where u.rol.id != 1");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> getListaUsuarioEmpleados() {
		return (List<Usuario>) super.getListaRegistro("SELECT u FROM Usuario u  where u.rol.id <> 1 and u.rol.id <>2");
	}

}
