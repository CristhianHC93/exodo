package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.ProductoDAO;
import ec.com.exodo.entity.Producto;

public class ProductoDAOImpl extends DAOImplements implements ProductoDAO {

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Producto> getListaProducto(int estado) {
		return (List<Producto>) super.getListaRegistro(estado, "estado",
				"SELECT p FROM Producto p where p.estado.id = :estado order by p.id desc");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Producto> getListaProductoBySucursal(int estado, int sucursal) {
		return (List<Producto>) super.getListaRegistro(estado, "estado", sucursal, "sucursal",
				"SELECT p FROM Producto p where p.estado.id = :estado and p.areaSucursal.sucursal.id = :sucursal order by p.id desc");
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Producto> getListaProductoByAreaSucursal(int estado, int area) {
		return (List<Producto>) super.getListaRegistro(estado, "estado", area, "area",
				"SELECT p FROM Producto p where p.estado.id = :estado and p.areaSucursal.id = :area order by p.id desc");
	}

	public Producto saveorUpdate(Producto producto) {
		return (Producto) super.saveorUpdate(producto);
	}

	@Override
	public Producto getProducto(int idProducto) {
		final Object object = super.getRegistro(idProducto, "idProducto",
				"SELECT p FROM Producto p where p.id = :idProducto");
		return (object != null) ? ((Producto) object) : null;
	}

	@Override
	public Producto getProductoByCodigoOrDescripcion(String dato) {
		final Object object = super.getRegistro(dato, "dato",
				"SELECT p FROM Producto p where (UPPER(p.codigo) = :dato or UPPER(p.descripcion) = :dato) and p.estado.id = 1");
		return (object != null) ? ((Producto) object) : null;
	}

	@Override
	public Producto getProductoByCodigoOrDescripcionAndAreaSucursal(String dato, int area) {
		final Object object = super.getRegistro(dato, "dato", area, "area",
				"SELECT p FROM Producto p where (UPPER(p.codigo) = :dato or UPPER(p.descripcion) = :dato) and p.estado.id = 1 and p.areaSucursal.id = :area");
		return (object != null) ? ((Producto) object) : null;
	}

	@Override
	public Producto getProductoByIdAndAreaSucursal(int idProducto, int area) {
		final Object object = super.getRegistro(idProducto, "idProducto", area, "area",
				"SELECT p FROM Producto p where p.id = :idProducto and p.estado.id = 1 and p.areaSucursal.id = :area");
		return (object != null) ? ((Producto) object) : null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Producto> getListaProductoByDescripcionOrCodigo(String dato) {
		return (List<Producto>) super.getListaRegistro("%".concat(dato).concat("%"), "dato",
				"SELECT p FROM Producto p where p.estado.id = 1 and (UPPER(p.descripcion) like :dato or UPPER(p.codigo) like :dato)");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Producto> getListaProductoByDescripcionOrCodigoAndAreaSucursal(String dato, int area) {
		return (List<Producto>) super.getListaRegistroFiltro(dato, "dato", area, "area",
				"SELECT p FROM Producto p where (UPPER(p.descripcion) like :dato or UPPER(p.codigo) like :dato) and p.areaSucursal.id = :area ");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Producto> getFiltroProductoBySucursal(String dato, int sucursal) {
		return (List<Producto>) super.getListaRegistroFiltro(dato, "dato", sucursal, "sucursal",
				"SELECT p FROM Producto p where (UPPER(p.descripcion) like :dato or UPPER(p.codigo) like :dato) and p.areaSucursal.sucursal.id = :sucursal");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Producto> getFiltroProductoByAreaSucursalByProveedor(String dato, int area, int proveedor) {
		return (List<Producto>) super.getListaRegistroFiltro(dato, "dato", area, "area", proveedor, "proveedor",
				"SELECT p FROM Producto p where (UPPER(p.descripcion) like :dato or UPPER(p.codigo) like :dato) and p.areaSucursal.id = :area and p.proveedor.id = :proveedor ");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Producto> getListaProductoByCategoria(int estado, int categoria) {
		return (List<Producto>) super.getListaRegistro(estado, "estado", categoria, "categoria",
				"SELECT p FROM Producto p where p.estado.id = :estado and p.categoria.id = :categoria order by p.id desc");
	}

}
