package ec.com.exodo.doa.impl;

import ec.com.exodo.dao.ie.ConfiguracionDAO;

import ec.com.exodo.entity.Configuracion;

public class ConfiguracionDAOImpl extends DAOImplements implements ConfiguracionDAO {

	@Override
	public Configuracion getConfiguracion(int id) {
		final Object object = super.getRegistro(id, "id", "SELECT c FROM Configuracion c where c.id = :id");
		return (object != null) ? ((Configuracion) object) : null;
	}

	@Override
	public Configuracion saveOrUpdate(Configuracion configuracion) {
		return (Configuracion) super.saveorUpdate(configuracion);
	}
}
