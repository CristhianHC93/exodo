package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.VentaDetalleDAO;
import ec.com.exodo.entity.VentaDetalle;

public class VentaDetalleDAOImpl extends DAOImplements implements VentaDetalleDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<VentaDetalle> getListaVentaDetalleByCabecera(int idVentaCabecera, int estado) {
		return (List<VentaDetalle>) super.getListaRegistro(idVentaCabecera, "idVentaCabecera", estado, "estado",
				"SELECT v FROM VentaDetalle v where v.ventaCabecera.id = :idVentaCabecera AND v.estado.id = :estado");
	}

	@Override
	public VentaDetalle saveorUpdate(VentaDetalle ventaDetalle) {
		return (VentaDetalle) super.saveorUpdate(ventaDetalle);
	}

}
