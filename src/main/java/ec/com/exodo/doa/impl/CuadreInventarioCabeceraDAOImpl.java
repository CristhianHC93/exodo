package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.CuadreInventarioCabeceraDAO;
import ec.com.exodo.entity.CuadreInventarioCabecera;

public class CuadreInventarioCabeceraDAOImpl extends DAOImplements implements CuadreInventarioCabeceraDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<CuadreInventarioCabecera> getListaCuadreInventarioCabecera() {
		return (List<CuadreInventarioCabecera>) super.getListaRegistro("SELECT c FROM CuadreInventarioCabecera c");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CuadreInventarioCabecera> getListaCuadreInventarioCabecera(boolean estado) {
		return (List<CuadreInventarioCabecera>) super.getListaRegistro(estado, "estado",
				"SELECT c FROM CuadreInventarioCabecera c where c.estado = :estado");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CuadreInventarioCabecera> getListaCuadreInventarioCabecera(int areaSucursal, boolean estado) {
		return (List<CuadreInventarioCabecera>) super.getListaRegistro(areaSucursal, "areaSucursal", estado, "estado",
				"SELECT c FROM CuadreInventarioCabecera c where c.estado = :estado and c.areaSucursal.id :areaSucursal");
	}

	@Override
	public CuadreInventarioCabecera getCuadreInventarioByAreaSucursal(int areaSucursal, boolean estado) {
		final Object object = super.getRegistro(areaSucursal, "areaSucursal", estado, "estado",
				"SELECT c FROM CuadreInventarioCabecera c where c.estado = :estado and c.areaSucursal.id = :areaSucursal");
		return (object != null) ? ((CuadreInventarioCabecera) object) : null;
	}

	@Override
	public CuadreInventarioCabecera saveorUpdate(CuadreInventarioCabecera cuadreInventarioCabecera) {
		return (CuadreInventarioCabecera) super.saveorUpdate(cuadreInventarioCabecera);
	}

}
