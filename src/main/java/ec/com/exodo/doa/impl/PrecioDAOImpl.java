package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.PrecioDAO;
import ec.com.exodo.entity.Precio;

public class PrecioDAOImpl extends DAOImplements implements PrecioDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Precio> getListaByProducto(int idProducto) {
		return ((List<Precio>) super.getListaRegistro(idProducto, "idProducto",
				"SELECT p FROM Precio p where p.producto.id = :idProducto order by p.descripcion asc"));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Precio> getListaByProductoIsNull() {
		return ((List<Precio>) super.getListaRegistro(
				"SELECT p FROM Precio p where p.producto is null order by p.descripcion asc"));
	}

	@Override
	public Precio saveOrUpdate(Precio precio) {
		return (Precio) super.saveorUpdate(precio);
	}

	@Override
	public int eliminar(int idPrecio) {
		return super.eliminar(idPrecio, "idPrecio", "DELETE FROM Precio where id = :idPrecio");
	}

}
