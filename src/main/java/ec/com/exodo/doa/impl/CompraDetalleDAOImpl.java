package ec.com.exodo.doa.impl;

import java.util.List;

import ec.com.exodo.dao.ie.CompraDetalleDAO;
import ec.com.exodo.entity.CompraDetalle;

public class CompraDetalleDAOImpl extends DAOImplements implements CompraDetalleDAO {

	@Override
	public CompraDetalle saveorUpdate(CompraDetalle CompraDetalle) {
		return (CompraDetalle) super.saveorUpdate(CompraDetalle);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CompraDetalle> getCompraDetallesByCabecera(int idCompraCabecera) {
		return (List<CompraDetalle>) super.getListaRegistro(idCompraCabecera, "idCompraCabecera",
				"SELECT n FROM CompraDetalle n where n.compraCabecera.id = :idCompraCabecera");
	}

	@Override
	public CompraDetalle getCompraDetallesById(int idCompraDetalle) {
		final Object object = super.getRegistro(idCompraDetalle, "idCompraDetalle",
				"SELECT n FROM CompraDetalle n where n.id = :idCompraDetalle");
		return (object != null) ? ((CompraDetalle) object) : null;
	}
}
