package ec.com.exodo.doa.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import ec.com.exodo.dao.ie.CompraCabeceraDAO;
import ec.com.exodo.dao.ie.CompraDetalleDAO;
import ec.com.exodo.dao.ie.CuadreInventarioDAO;
import ec.com.exodo.dao.ie.PrecioDAO;
import ec.com.exodo.dao.ie.ProductoProveedorDAO;
import ec.com.exodo.entity.CompraCabecera;
import ec.com.exodo.entity.CompraDetalle;
import ec.com.exodo.entity.ConceptoMovimiento;
import ec.com.exodo.entity.CuadreInventario;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.entity.NotaCompraDetalle;
import ec.com.exodo.entity.Precio;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.ProductoProveedor;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.models.Modelos;
import ec.com.exodo.sistema.Sistema;

public class CompraCabeceraDAOImpl extends DAOImplements implements CompraCabeceraDAO {

	private CuadreInventarioDAO cuadreInventarioDAO = InstanciaDAO.getInstanciaCuadreInventarioDAO();
	private ProductoProveedorDAO productoProveedorDAO = InstanciaDAO.getInstanciaProductoProveedorDAO();
	private CompraDetalleDAO compraDetalleDAO = InstanciaDAO.getInstanciaCompraDetalleDAO();

	@SuppressWarnings("unchecked")
	@Override
	public List<CompraCabecera> getCompraCabeceras(int estado) {
		return (List<CompraCabecera>) super.getListaRegistro(estado, "estado",
				"SELECT c FROM CompraCabecera c where c.estado.id = :estado ");
	}

	@Override
	public CompraCabecera saveorUpdate(CompraCabecera compraCabecera) {
		return (CompraCabecera) super.saveorUpdate(compraCabecera);

	}

	@Override
	public void saveorUpdateDetalle(List<Modelos<CompraDetalle, NotaCompraDetalle>> listaCompra,
			CompraCabecera compraCabecera, boolean actualizarPrecio) {
		try {
			for (Modelos<CompraDetalle, NotaCompraDetalle> detalle : listaCompra) {
				if (detalle.getObjetoT1() != null) {
					gestionNotaCompra(detalle);
				}
				detalle.getObjetoT().setCompraCabecera(compraCabecera);
				final ProductoProveedor productoProveedor = productoProveedorDAO
						.getProductoProveedorByProductoAndProveedor(detalle.getObjetoT().getProducto().getId(),
								compraCabecera.getProveedor().getId());
				if (productoProveedor != null) {
					productoProveedor.setPrecioCosto(detalle.getObjetoT().getPrecioCompra());
					productoProveedor.setDescuentoPorcentaje(detalle.getObjetoT().getDescuentoPorcentaje());
					productoProveedor.setDescuentoValor(detalle.getObjetoT().getDescuentoValor());
					productoProveedor.setPrecioCosto(detalle.getObjetoT().getPrecioCompra());
					productoProveedorDAO.saveorUpdate(productoProveedor);
					// detalle = lc.getObjetoT();
					// Metodo donde tambien se gusrda el detalle
					guardarCuadreInventario(detalle.getObjetoT(), actualizarPrecio, detalle);
				}
			}

		} catch (RuntimeException e) {
			System.out.print(e.getMessage());
		}
	}

	private void gestionNotaCompra(Modelos<CompraDetalle, NotaCompraDetalle> detalleCompra) {
		/* Setear Cantidades resultantes en Nota de Compra */
		detalleCompra.getObjetoT1().setCantidadEntregada(
				detalleCompra.getObjetoT1().getCantidadEntregada() + detalleCompra.getObjetoT().getCantidad());
		detalleCompra.getObjetoT1()
				.setCantidad(detalleCompra.getObjetoT1().getCantidad() - detalleCompra.getObjetoT().getCantidad());
		/*
		 * Verifica que si no existen cantidad en Nota de Compra para cambiar en estado
		 * consumido
		 */
		if (detalleCompra.getObjetoT1().getCantidad() == 0) {
			detalleCompra.getObjetoT1().setEstado(new Estado(Sistema.ESTADO_CONSUMIDO));
		}
		InstanciaDAO.getInstanciaNotaCompraDetalleDAO().saveorUpdate(detalleCompra.getObjetoT1());
	}

	private void guardarCuadreInventario(CompraDetalle compraDetalle, boolean actualizarPrecio,
			Modelos<CompraDetalle, NotaCompraDetalle> detalle) {
		final CuadreInventario cuadreInventario = new CuadreInventario();
		final Producto producto = compraDetalle.getProducto();
		final double valorCompra = Sistema.redondear(
				compraDetalle.getPrecioCompra() - compraDetalle.getDescuentoValor() + compraDetalle.getValorIva());
		cuadreInventario.setValorCompra(valorCompra);
		cuadreInventario.setTotalCompra(Sistema.redondear(compraDetalle.getTotal()));

		cuadreInventario.setFecha(new Timestamp(new Date().getTime()));
		cuadreInventario.setConceptoMovimiento(new ConceptoMovimiento(Sistema.ID_COMPRA));

		cuadreInventario.setCantidadAntes(producto.getCantidad());
		cuadreInventario.setCantidad(compraDetalle.getCantidad());
		cuadreInventario.setCantidadDespues(producto.getCantidad() + compraDetalle.getCantidad());

		producto.setCantidad(cuadreInventario.getCantidadDespues());
		producto.setProveedor(compraDetalle.getCompraCabecera().getProveedor());
		producto.setPrecioCompra(compraDetalle.getPrecioCompra());
		if (actualizarPrecio) {
			actualizarPrecio(producto, valorCompra);
		}
		cuadreInventario.setProducto(producto);
		cuadreInventario.setObservacion("Compra de " + compraDetalle.getCantidad() + " " + producto.getDescripcion());
		/* Guarda Cuadre de Inventario y cambios del Producto */
		final CompraDetalle comDetalle = compraDetalleDAO.saveorUpdate(detalle.getObjetoT());
		cuadreInventario.setIdDetalle(comDetalle.getId());
		cuadreInventarioDAO.saveorUpdate(cuadreInventario);
	}

	private void actualizarPrecio(Producto producto, double valorCompra) {
		final PrecioDAO precioDAO = InstanciaDAO.getInstanciaPrecioDAO();
		List<Precio> listaPrecio = precioDAO.getListaByProducto(producto.getId());
		listaPrecio.forEach(x -> {
			x.setValor(Sistema.redondear(valorCompra + (valorCompra * (x.getPorcentaje() / 100)), 4));
			precioDAO.saveOrUpdate(x);
		});
		producto.setPrecioVenta(listaPrecio.get(0).getValor());
	}
}
