package ec.com.exodo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.DoubleConsumer;
import java.util.function.Predicate;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.dao.ie.AreaSucursalDAO;
import ec.com.exodo.dao.ie.CategoriaDAO;
import ec.com.exodo.dao.ie.ConceptoMovimientoDAO;
import ec.com.exodo.dao.ie.CuadreInventarioDAO;
import ec.com.exodo.dao.ie.ProductoDAO;
import ec.com.exodo.entity.AreaSucursal;
import ec.com.exodo.entity.Categoria;
import ec.com.exodo.entity.CuadreInventario;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassAlimentarInventario {

	private List<AreaSucursal> listaAreaSucursal;
	private AreaSucursal areaSucursal;

	private List<Categoria> listaCategoria;
	private Categoria categoria;

	private Map<String, Double> totales = new HashMap<>();

	private List<Producto> listaProducto;

	/* Bandera para verificar si existen productos estan repetidos */
	boolean banderaValidarProducto = false;

	private ProductoDAO productoDAO = InstanciaDAO.getInstanciaProductoDAO();
	private AreaSucursalDAO areaSucursalDAO = InstanciaDAO.getInstanciaAreaSucursalDAO();
	private CuadreInventarioDAO cuadreInventarioDAO = InstanciaDAO.getInstanciaCuadreInventarioDAO();
	private ConceptoMovimientoDAO conceptoMovimientoDAO = InstanciaDAO.getInstanciaConceptoMovimientoDAO();
	private CategoriaDAO categoriaDAO = InstanciaDAO.getInstanciaCategoriaDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		selectAreaSucursal();
		limpiar();
	}

	private void selectAreaSucursal() {
		listaAreaSucursal = areaSucursalDAO.getListaAreaSucursalBySucursalActivo(Sistema.getSucursal().getId());
		areaSucursal = (listaAreaSucursal.size() > 0) ? listaAreaSucursal.get(0) : null;
	}

	private void selectCategory() {
		listaCategoria = categoriaDAO.getListaCategoriaByArea(areaSucursal.getArea().getId());
		categoria = (listaCategoria.size() > 0) ? listaCategoria.get(0) : null;
	}

	@NotifyChange({ "listaCategoria", "categoria", "listaProducto", "totales" })
	@Command
	public void limpiar() {
		selectCategory();
		if (listaProducto == null) {
			listaProducto = new ArrayList<>();
		}
		listaProducto.clear();
		listaProducto.add(new Producto());
		totales.put("totalStock", 0.0);		
	}

	@NotifyChange({ "listaProducto", "totales" })
	@Command
	public void guardar() {
		if (validarGuardar()) {
			listaProducto.stream().filter(filtrarGuardar).forEach(this::guardarCuadreInventario);
			limpiar();
			Clients.showNotification(getIdioma().getIngresoSatisfactorio(), Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	private boolean validarGuardar() {
		boolean devolver = true;
		if (listaProducto.size() <= 1) {
			Messagebox.show(getIdioma().getNoExistenElementos());
			devolver = false;
		}
		return devolver;
	}

	private Predicate<Producto> filtrarGuardar = producto -> (producto != null && producto.getCantidad() > 0);

	private void guardarCuadreInventario(Producto producto) {
		Producto productoRef = productoDAO.getProducto(producto.getId());
		if (productoRef != null) {
			CuadreInventario cuadreInventario = new CuadreInventario();
			cuadreInventario.setCantidad(producto.getCantidad());
			cuadreInventario.setCantidadAntes(productoRef.getCantidad());
			cuadreInventario.setCantidadDespues(productoRef.getCantidad() + producto.getCantidad());
			cuadreInventario.setFecha(new Timestamp(new Date().getTime()));
			cuadreInventario.setConceptoMovimiento(
					conceptoMovimientoDAO.getConceptoMovimientoById(Sistema.ID_CUADRE_INVENTARIO_ENTRADA));
			cuadreInventario.setObservacion(getIdioma().getMensajeAlimentarInventario(productoRef));

			productoRef.setCantidad(cuadreInventario.getCantidadDespues());
			cuadreInventario.setProducto(productoRef);

			if (cuadreInventarioDAO.saveorUpdate(cuadreInventario) == null)
				Messagebox.show(getIdioma().getMensajeImposibleRealizarAccion(producto.getDescripcion()));
		} else
			Messagebox.show(getIdioma().getMensajeImposibleRealizarAccion(producto.getDescripcion()));
	}

	@GlobalCommand
	@NotifyChange({ "listaProducto" })
	public void cargarProducto(final @BindingParam("objetoProducto") Producto producto) {
		if (validarCargarProducto(producto)) {
			banderaValidarProducto = true;
			listaProducto.stream().filter(filtrarCargarProducto(producto.getId()))
					.forEach(item -> banderaValidarProducto = false);
			if (banderaValidarProducto)
				agregarProducto(producto);
			else
				Messagebox.show(getIdioma().getMensajeProductoIngresado(producto.getDescripcion()));
		}
	}

	private boolean validarCargarProducto(final Producto producto) {
		boolean devolver = true;
		if (producto.getCantidad() > 0) {
			Messagebox.show(
					"Producto ya existe en inventario, Si desea Ajustar la cantidad por favor dirijase a Cuadre de Inventario");
			devolver = false;
		}
		return devolver;
	}

	private Predicate<Producto> filtrarCargarProducto(final int id) {
		return item -> (item.getId() != null && item.getId() == id);
	}

	@Command
	@NotifyChange({ "listaProducto", "totales" })
	public void cargarProductoCodigo(@BindingParam("item") String codigo) {
		if (codigo == null || codigo.trim().equals("")) {
			Messagebox.show(getIdioma().getIngresarCodigo());
			return;
		}
		if (!cargarProducto(codigo))
			Messagebox.show(getIdioma().getCodigoNoEncontrado());
	}

	@NotifyChange({ "listaProducto", "totales" })
	@Command
	public void cargarProductoDescripcion(@BindingParam("item") String descripcion) {
		if (descripcion == null || descripcion.trim().equals("")) {
			Messagebox.show(getIdioma().getIngresarDescripcion());
			return;
		}
		/*
		 * Si cargar producto retorna falso, y abrira la ventana para seleccionar
		 * producto
		 */
		if (!cargarProducto(descripcion)) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("clase", "cuadreInventario");
			parameters.put("filtroProducto", descripcion);
			parameters.put("area", areaSucursal);
			Executions.createComponents("/view/formProducto.zul", null, parameters);
		}
	}

	/*
	 * Metodos que se usa al cargar los productos desde el text de codigo y
	 * desripcion
	 */
	private boolean cargarProducto(String dato) {
		final Producto producto = productoDAO.getProductoByCodigoOrDescripcionAndAreaSucursal(dato.toUpperCase(),
				areaSucursal.getId());
		if (producto != null) {
			if (validarProductoCantidadAndRepetido(producto)) {
				agregarProducto(producto);
			}
		}
		return (producto != null);
	}

	private boolean validarProductoCantidadAndRepetido(final Producto producto) {
		banderaValidarProducto = validarCargarProducto(producto);
		if (banderaValidarProducto) {
			listaProducto.stream().filter(filtrarCargarProducto(producto.getId())).forEach(this::sumarProductoRepetido);
		}
		return banderaValidarProducto;
	}

	private void sumarProductoRepetido(final Producto producto) {
		listaProducto.get(0).setCodigo("");
		listaProducto.get(0).setDescripcion("");
		producto.setCantidad(producto.getCantidad() + 1);
		confirmar(producto);
		banderaValidarProducto = false;
	}

	private void agregarProducto(Producto producto) {
		List<Producto> listaProvisional = new ArrayList<>(listaProducto);
		limpiar();
		listaProducto.add(producto);
		listaProvisional.remove(0);
		listaProducto.addAll(listaProvisional);
	}

	@Command
	@NotifyChange({ "listaProducto", "totales" })
	public void confirmar(final @BindingParam("item") Producto producto) {
		if (producto == null || producto.getCodigo() == null) {
			Messagebox.show(getIdioma().getEscojerProducto());
			return;
		}
		if (producto.getCantidad() < 0) {
			Messagebox.show(getIdioma().getIngresarCantidad());
			return;
		}
		calcularTotales();
	}

	@NotifyChange({ "listaProducto", "totales" })
	@Command
	public void eliminar(final @BindingParam("item") Producto producto) {
		listaProducto.remove(producto);
		calcularTotales();
	}

	private void calcularTotales() {
		totales.clear();
		listaProducto.stream().mapToDouble(producto -> producto.getCantidad()).forEach(sumarTotal);
	}

	private DoubleConsumer sumarTotal = x -> totales.merge("totalStock", x, Double::sum);

	@NotifyChange("listaProducto")
	@Command
	public void aceptar() {
		limpiar();
		productoDAO.getListaProductoByCategoria(Sistema.ESTADO_INGRESADO, categoria.getId()).stream()
				.filter(filterProducto).forEach(listaProducto::add);
	}

	private Predicate<Producto> filterProducto = producto -> (producto.getCantidad() <= 0);

	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public List<AreaSucursal> getListaAreaSucursal() {
		return listaAreaSucursal;
	}

	public void setListaAreaSucursal(List<AreaSucursal> listaAreaSucursal) {
		this.listaAreaSucursal = listaAreaSucursal;
	}

	public AreaSucursal getAreaSucursal() {
		return areaSucursal;
	}

	public void setAreaSucursal(AreaSucursal areaSucursal) {
		this.areaSucursal = areaSucursal;
	}

	public List<Producto> getListaProducto() {
		return listaProducto;
	}

	public void setListaProducto(List<Producto> listaProducto) {
		this.listaProducto = listaProducto;
	}

	public Map<String, Double> getTotales() {
		return totales;
	}

	public void setTotales(Map<String, Double> totales) {
		this.totales = totales;
	}

	public List<Categoria> getListaCategoria() {
		return listaCategoria;
	}

	public void setListaCategoria(List<Categoria> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
}