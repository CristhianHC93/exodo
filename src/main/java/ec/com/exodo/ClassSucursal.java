package ec.com.exodo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.SucursalDAO;
import ec.com.exodo.entity.Sucursal;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassSucursal {
	@Wire("#winSucursal")
	private Window winSucursal;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private boolean estadoRoot = false;

	private List<Sucursal> listaSucursal;
	private SucursalDAO sucursalDAO = InstanciaDAO.getInstanciaSucursalDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			final @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(view, this, false);
		ajustarPantalla(clase);
		cargaInicial();
		estadoRoot = (Sistema.getUsuario().getRol().getId() == Sistema.ID_ROL_ROOT);
	}

	private void ajustarPantalla(final String clase) {
		if (clase == null) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		} else if (clase.equals("Venta") || clase.equals("Producto")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "85%";
		}
	}

	private void cargaInicial() {
		if (listaSucursal == null)
			listaSucursal = new ArrayList<>();
		listaSucursal.clear();
		listaSucursal = sucursalDAO.getListaSucursal();
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/view/formSucursalCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaSucursal")
	public void eliminar(@BindingParam("sucursal") final Sucursal sucursal) {
		Messagebox.show(getIdioma().getMensajeEliminar(sucursal.getInformacionNegocio().getNombre()), "Confirm",
				Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						gestionEliminar(sucursalDAO.eliminar(sucursal));
					}
				});
	}

	private void gestionEliminar(final int a) {
		if (a == 1) {
			Clients.showNotification("Registro eliminado satisfactoriamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"top_center", 2000);
			BindUtils.postNotifyChange(null, null, ClassSucursal.this, "listaSucursal");
		} else {
			Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR, null, "top_center",
					2000);
			BindUtils.postNotifyChange(null, null, ClassSucursal.this, "listaSucursal");
		}
	}

	@Command
	public void aceptar(@BindingParam("sucursal") Sucursal sucursal) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", sucursal);
		BindUtils.postGlobalCommand(null, null, "cargarSucursal", parametros);
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaSucursal")
	public void refrescarlista() {
		listaSucursal = sucursalDAO.getListaSucursal();
	}

	@Command
	public void editar(@BindingParam("sucursal") Sucursal sucursal) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", sucursal);
		Executions.createComponents("/view/formSucursalCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaSucursal")
	public void filtrar(final @BindingParam("dato") String datoBusqueda) {
		listaSucursal.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaSucursal.addAll(sucursalDAO.getListaSucursal());
		} else {
			sucursalDAO.getListaSucursal().stream().filter(item -> item.getInformacionNegocio().getNombre().trim()
					.toUpperCase().contains(datoBusqueda.trim().toUpperCase())).forEach(listaSucursal::add);
		}
	}

	@Command
	public void salir() {
		winSucursal.detach();
	}

	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public List<Sucursal> getListaSucursal() {
		return listaSucursal;
	}

	public void setListaSucursal(List<Sucursal> listaSucursal) {
		this.listaSucursal = listaSucursal;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public boolean isEstadoRoot() {
		return estadoRoot;
	}

	public void setEstadoRoot(boolean estadoRoot) {
		this.estadoRoot = estadoRoot;
	}
}
