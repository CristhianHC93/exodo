package ec.com.exodo;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.ClienteDAO;
import ec.com.exodo.entity.Cliente;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassClienteCE {
	@Wire("#winClienteCE")
	private Window winCliente;

	private Cliente cliente;
	private ClienteDAO clienteDAO = InstanciaDAO.getInstanciaClienteDAO();

	private boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") Cliente cliente) {
		Selectors.wireComponents(view, this, false);
		this.cliente = (cliente == null) ? new Cliente() : cliente;
	}

	@Command
	@NotifyChange("cliente")
	public void guardarEditar() {
		if (validarGuardarEditar()) {
			Cliente clienteRef = clienteDAO.saveorUpdate(cliente);
			if (clienteRef != null) {
				Clients.showNotification(getIdioma().getProcesoRealizadoExitosamente(), Clients.NOTIFICATION_TYPE_INFO,
						null, "middle_center", 1500);
				if (cliente.getId() == null)
					cliente = new Cliente();
				bandera = true;
			} else {
				Clients.showNotification(getIdioma().getMensajeImposibleRealizarAccion(cliente.getNombre()),
						Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center", 1500);
			}
		}
	}

	private boolean validarGuardarEditar() {
		boolean devolver = true;
		final String cedulaProvicional = cliente.getCedula();
		if (Sistema.validarRepetido(clienteDAO.getListaClienteByEstado(Sistema.ESTADO_INGRESADO), cliente)) {
			cliente.setCedula(cedulaProvicional);
			devolver = false;
		}
		if (cliente.getCorreo() == null || cliente.getCorreo().trim().equals("")) {
			cliente.setCorreo("demo@demo.com");
		}
		return devolver;
	}

	@Command
	public void validar(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		/*
		 * if (campo.equals("cedula")) { if (!Sistema.validarCedula(cadena)) {
		 * cliente.setCedula(""); Clients.showNotification("Cedula Invalida",
		 * Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center", 1500); } }
		 */
		if (campo.equals("nombre")) {
			cliente.setNombre(cadena.toUpperCase());
		}
		if (campo.equals("direccion")) {
			cliente.setDireccion(cadena.toUpperCase());
		}
		if (campo.equals("celular")) {
			if (!Sistema.validarNumero(cadena)) {
				cliente.setCelular("");
				Clients.showNotification("Celular Invalido", Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center",
						1500);
			}
		}
		if (campo.equals("telefono")) {
			if (!Sistema.validarNumero(cadena)) {
				cliente.setTelefono("");
				Clients.showNotification("Telefono Invalido", Clients.NOTIFICATION_TYPE_ERROR, null, "middle_center",
						1500);
			}
		}

	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winCliente.detach();
	}

	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
