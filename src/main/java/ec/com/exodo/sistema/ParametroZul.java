package ec.com.exodo.sistema;

public class ParametroZul {

	private static ParametroZul parametroZul;

	public static ParametroZul getInstancia() {
		if (parametroZul == null) {
			parametroZul = new ParametroZul();
		}
		return parametroZul;
	}

	private  final String FOOTER_MESSAGE = "Un Total de %d Items";
	private  final String FORMAT_CANTIDAD = "#,###";
	private  final String FORMAT_NUMERO = "#########";
	private  final String FORMAT_PRECIO_TOTAL = "#,##0.00##";
	private  final String FORMAT_PRECIO = "#,##0.000###";
	private  final String LOCALE = "es_MX";
	private  final String CONSTRAINT_PRECIO = "no empty,/^[0-9]*.[0-9]*";

	/* Get and set */

	public  String getFooterMessage() {
		return FOOTER_MESSAGE;
	}

	public  String getFormatCantidad() {
		return FORMAT_CANTIDAD;
	}

	public  String getFormatNumero() {
		return FORMAT_NUMERO;
	}

	public  String getFormatPrecio() {
		return FORMAT_PRECIO;
	}

	public  String getLocale() {
		return LOCALE;
	}

	public  String getConstraintPrecio() {
		return CONSTRAINT_PRECIO;
	}

	public String getFormatPrecioTotal() {
		return FORMAT_PRECIO_TOTAL;
	}
	
	

}
