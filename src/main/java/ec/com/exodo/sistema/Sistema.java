package ec.com.exodo.sistema;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.entity.Area;
import ec.com.exodo.entity.AreaSucursal;
import ec.com.exodo.entity.Categoria;
import ec.com.exodo.entity.Cliente;
import ec.com.exodo.entity.CompraCabecera;
import ec.com.exodo.entity.CompraDetalle;
import ec.com.exodo.entity.Email;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.entity.Impuesto;
import ec.com.exodo.entity.InformacionNegocio;
import ec.com.exodo.entity.NotaCompraDetalle;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.Proveedor;
import ec.com.exodo.entity.Sucursal;
import ec.com.exodo.entity.Usuario;
import ec.com.exodo.entity.VentaCabecera;
import ec.com.exodo.entity.VentaDetalle;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.idioma.Ingles;
import ec.com.exodo.idioma.Latino;
import ec.com.exodo.models.Modelos;
import ec.com.exodo.report.CompraDataSource;
import ec.com.exodo.report.NotaCompraDataSource;
import ec.com.exodo.report.ProductoDataSource;
import ec.com.exodo.report.ProformaDataSource;
import ec.com.exodo.report.ReporteVentaDataSource;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

public class Sistema {

	private Sistema() {
	}

	public static Usuario getUsuario() {
		return (Usuario) Sessions.getCurrent().getAttribute("Usuario");
	}

	private static Idioma idioma = getIdioma();

	public static Idioma getIdioma() {
		if (idioma == null) {
			idioma = Latino.getInstancia();
		}
		return idioma;
	}

	public static void setIdioma(String idiomaSelect) {
		if (idioma == null) {
			switch (idiomaSelect.trim()) {
			case "Latino":
				idioma = Latino.getInstancia();
				break;
			case "Ingles":
				idioma = new Ingles();
			case "LogOut":
				idioma = null;
				break;
			default:
				idioma = null;
				break;
			}
		}

	}

	private static AreaSucursal areaSucursal;
	private static Sucursal sucursal;

	public static void setAreaSucursal(AreaSucursal areaSucursal) {
		Sistema.areaSucursal = areaSucursal;
	}

	public static AreaSucursal getAreaSucursal() {
		if (areaSucursal == null) {
			areaSucursal = new AreaSucursal(1);
		}
		return areaSucursal;
	}

	public static Sucursal getSucursal() {
		if (sucursal == null) {
			sucursal = InstanciaDAO.getInstanciaSucursalDAO().getSucursal(1);
		}
		return sucursal;
	}

	public static void setSucursal(Sucursal sucursal) {
		Sistema.sucursal = sucursal;
	}

	/* Metodo para Verificar si Usuario es rol Admin o Root */
	public static boolean verificarUsuarioEmpleado(Usuario usuario) {
		if (usuario.getRol().getId() != ID_ROL_ROOT && usuario.getRol().getId() != ID_ROL_ADMIN) {
			return true;
		} else {
			return false;
		}
	}

	public static final String carpetaImagen = "imagen";
	public static final String carpetaFacturaPDF = "facturaPDF";

	public static final double COMPENSACION_PORCENTAJE = 0.02;

	// Estaticos Parametros zul

	/*
	 * Para Calorimetria de Productos
	 */
	private static final String STOCK_ALERTA = "background-color:yellow;";
	private static final String STOCK_CRITICO = "background-color:red;";

	public static final int ID_CONFIGURACION = 1;

	/*
	 * Estados del Sistema
	 */

	public static final int ESTADO_INGRESADO = 1;
	public static final int ESTADO_ELIMINADO = 2;
	public static final int ESTADO_ANULADO = 3;
	public static final int ESTADO_CONSUMIDO = 4;
	public static final int ESTADO_ORDEN_VENTA = 5;
	public static final int ESTADO_COBRADO_VENTA = 6;
	public static final int ESTADO_ENTREGADO_VENTA = 7;
	public static final int ESTADO_PLAN_VENTA = 10;
	public static final int ESTADO_ACTIVAR_USUARIO = 8;
	public static final int ESTADO_DESACTIVAR_USUARIO = 9;

	/*
	 * Filtron para Estado
	 */

	public static final int FILTRO_ESTADO_TODAS = 1;
	public static final int FILTRO_ESTADO_VENTA = 2;
	public static final int FILTRO_ESTADO_USUARIO = 3;

	/*
	 * ID's Roles
	 */
	public static final int ID_ROL_ROOT = 1;
	public static final int ID_ROL_ADMIN = 2;
	public static final int ID_ROL_VENDEDOR = 3;
	public static final int ID_ROL_COADMINISTRADOR = 4;

	/*
	 * Codigos Concepto Movimiento
	 */
	public static final int CODIGO_SALIDA = 1;
	public static final int CODIGO_ENTRADA = 2;

	/*
	 * Codigos Concepto Movimiento
	 */
	public static final int ID_TRASPASO_SALIDA = 1;
	public static final int ID_TRASPASO_ENTRADA = 2;
	public static final int ID_VENTA = 3;
	public static final int ID_COMPRA = 4;
	public static final int ID_ANULAR_COMPRA = 5;
	public static final int ID_ANULAR_VENTA = 6;
	public static final int ID_CUADRE_INVENTARIO_SALIDA = 7;
	public static final int ID_CUADRE_INVENTARIO_ENTRADA = 8;

	/*
	 * Tab del Menu
	 */

	public static final String INVENTARIO = "tabInventario";
	public static final String COMPRA = "tabCompra";
	public static final String VENTA = "tabVenta";
	public static final String GESTION_USUARIO = "tabGestionUsuario";
	public static final String CONFIGURACION = "tabConfiguracion";
	public static final String REPORTE = "tabReporte";

	/*
	 * Metodo para encriptar
	 */

	public static String Encriptar(String texto) {

		String secretKey = "qualityinfosolutions";
		String base64EncryptedString = "";

		try {

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
			byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

			SecretKey key = new SecretKeySpec(keyBytes, "DESede");
			Cipher cipher = Cipher.getInstance("DESede");
			cipher.init(Cipher.ENCRYPT_MODE, key);

			byte[] plainTextBytes = texto.getBytes("utf-8");
			byte[] buf = cipher.doFinal(plainTextBytes);
			byte[] base64Bytes = Base64.encodeBase64(buf);
			base64EncryptedString = new String(base64Bytes);

		} catch (Exception ex) {
		}
		return base64EncryptedString;
	}
	/*
	 * Metodo para Desencriptar
	 */

	public static String desencriptar(String textoEncriptado) throws Exception {

		String secretKey = "qualityinfosolutions";
		String base64EncryptedString = "";

		try {
			byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
			byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
			SecretKey key = new SecretKeySpec(keyBytes, "DESede");

			Cipher decipher = Cipher.getInstance("DESede");
			decipher.init(Cipher.DECRYPT_MODE, key);

			byte[] plainText = decipher.doFinal(message);

			base64EncryptedString = new String(plainText, "UTF-8");

		} catch (Exception ex) {
		}
		return base64EncryptedString;
	}

	public static List<Estado> getListaEstadoUsuario() {
		return InstanciaDAO.getInstanciaEstadoDAO().getListaEstadoByModulo(FILTRO_ESTADO_USUARIO);
	}

	public static List<Impuesto> getListaImpuesto() {
		return InstanciaDAO.getInstanciaImpuestoDAO().getImpuestos();
	}

	public static List<InformacionNegocio> getListaInformacionNegocio() {
		return InstanciaDAO.getInstanciaInformacionNegocioDAO().getListaInformacionNegocio();
	}

	public static List<Email> getListaEmail() {
		return InstanciaDAO.getInstanciaEmailDAO().getListaEmail();
	}

	public static List<Sucursal> getListaSucursal() {
		return InstanciaDAO.getInstanciaSucursalDAO().getListaSucursal();
	}

	@SuppressWarnings("unchecked")
	public static boolean validarRepetido(List<?> lista, Object objecto) {
		boolean devuelto = false;
		for (Object o : lista) {
			if (objecto instanceof Modelos<?, ?>) {
				if (((Modelos<VentaDetalle, Object>) objecto).getObjetoT().getProducto().getCodigo().trim()
						.equals(((Modelos<VentaDetalle, Object>) o).getObjetoT().getProducto().getCodigo().trim())) {
					Messagebox.show(idioma.getMensageCodigoRepetido());
					devuelto = true;
				}
			}
			if (objecto instanceof Producto) {
				if (((Producto) objecto).getId() != ((Producto) o).getId()
						&& ((Producto) objecto).getCodigo().trim().equals(((Producto) o).getCodigo().trim())) {
					Messagebox.show(idioma.getMensageCodigoRepetido());
					devuelto = true;
				}
			}
			if (objecto instanceof Categoria) {
				if (((Categoria) objecto).getId() != ((Categoria) o).getId() && ((Categoria) objecto).getDescripcion()
						.trim().toUpperCase().equals(((Categoria) o).getDescripcion().trim().toUpperCase())) {
					Messagebox.show("Esta Categoria ya ha sido asignada");
					devuelto = true;
				}
			}
			if (objecto instanceof Area) {
				if (((Area) objecto).getId() != ((Area) o).getId() && ((Area) objecto).getDescripcion().trim()
						.toUpperCase().equals(((Area) o).getDescripcion().trim().toUpperCase())) {
					Messagebox.show("Esta Area ya ha sido asignada");
					devuelto = true;
				}
			}

			if (objecto instanceof Proveedor) {
				if (((Proveedor) objecto).getId() != ((Proveedor) o).getId()
						&& ((Proveedor) objecto).getRuc().trim().equals(((Proveedor) o).getRuc().trim())) {
					Messagebox.show(idioma.getMensageRucRepetido());
					devuelto = true;
				}
			}
			if (objecto instanceof Cliente) {
				if (((Cliente) objecto).getId() != ((Cliente) o).getId()
						&& (((Cliente) objecto).getCedula().trim().equals(((Cliente) o).getCedula().trim()))) {
					Messagebox.show(idioma.getMensageCedulaRepetido());
					devuelto = true;
				}
			}
			if (objecto instanceof VentaCabecera) {
				if (((VentaCabecera) objecto).getId() != ((VentaCabecera) o).getId() && (((VentaCabecera) objecto)
						.getCodigo().trim().equals(((VentaCabecera) o).getCodigo().trim()))) {
					Messagebox.show(idioma.getMensageCodigoRepetido());
					devuelto = true;
				}
			}
			if (objecto instanceof CompraCabecera) {
				if (((CompraCabecera) objecto).getId() != ((CompraCabecera) o).getId() && (((CompraCabecera) objecto)
						.getCodigo().trim().equals(((CompraCabecera) o).getCodigo().trim()))) {
					Messagebox.show(idioma.getMensageCodigoRepetido());
					devuelto = true;
				}
			}
			if (objecto instanceof Usuario) {
				if (((Usuario) objecto).getId() != ((Usuario) o).getId()
						&& (((Usuario) objecto).getUsername().trim().equals(((Usuario) o).getUsername().trim()))) {
					Messagebox.show("Username repetido");
					devuelto = true;
				}
			}
		}
		return devuelto;
	}

	public static double redondear(double numero) {
		double resultado;
		resultado = numero * Math.pow(10, 4);
		resultado = Math.round(resultado);
		resultado = resultado / Math.pow(10, 4);
		return resultado;
	}

	public static double redondear(double numero, int redondeo) {
		double resultado;
		resultado = numero * Math.pow(10, redondeo);
		resultado = Math.round(resultado);
		resultado = resultado / Math.pow(10, redondeo);
		return resultado;
	}

	/*
	 * public static double redondear3(double numero) { double resultado; resultado
	 * = numero * Math.pow(10, 3); resultado = Math.round(resultado); resultado =
	 * resultado / Math.pow(10, 3); return resultado; }
	 */

	/*
	 * Calorimetria
	 */

	public static void calorimetria(Producto p) {
		if (p.getCantidad() <= p.getCantidadAlerta()) {
			p.setColor(Sistema.STOCK_ALERTA);
			// listaProductoStock.add(p);
		}
		if (p.getCantidad() <= p.getCantidadCritica()) {
			p.setColor(Sistema.STOCK_CRITICO);
			// listaProductoStock.add(p);
		}
		if (p.getCantidad() > p.getCantidadAlerta() && p.getCantidad() > p.getCantidadCritica()) {
			p.setColor("");
		}
	}

	/*
	 * Para ComboxBox
	 */

	private static final List<String> buscarCompra = new ArrayList<String>();
	private static final List<String> buscarVenta = new ArrayList<String>();
	private static final List<String> listaIdioma = new ArrayList<String>();
	private static final List<String> listaConceptoMovimiento = new ArrayList<String>();

	static {
		buscarCompra.add(idioma.getCodigo().concat("/").concat(idioma.getProveedorMin()));
		buscarCompra.add(idioma.getFecha());
		buscarVenta.add(idioma.getCodigo().concat("/").concat(idioma.getClienteMin()));
		buscarVenta.add(idioma.getFecha());
		listaIdioma.add("Latino");
		listaIdioma.add("Ingles");
		listaConceptoMovimiento.add(idioma.getSalida().toUpperCase());
		listaConceptoMovimiento.add(idioma.getEntrada().toUpperCase());
	}

	public static final List<String> getComboCompra() {
		return new ArrayList<String>(buscarCompra);
	}

	public static final List<String> getComboVenta() {
		return new ArrayList<String>(buscarVenta);
	}

	public static final List<String> getListaIdioma() {
		return new ArrayList<String>(listaIdioma);
	}

	public static final List<String> getListaConceptoMovimiento() {
		return new ArrayList<String>(listaConceptoMovimiento);
	}

	/*
	 * Metodos para todos los reportes
	 */

	private static String getFileName(String nombreJasper) {
		String fileName = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/viewReport").toString()
				+ System.getProperty("file.separator") + nombreJasper;
		return fileName;
	}

	public static AMedia reportePDF(String nombreJaspre, Map<String, Object> map, JRDataSource dataSource) {
		AMedia amedia = null;
		try {
			final byte[] buf = net.sf.jasperreports.engine.JasperRunManager
					.runReportToPdf(new java.io.FileInputStream(getFileName(nombreJaspre)), map, dataSource);
			final java.io.InputStream mediais = new java.io.ByteArrayInputStream(buf);
			amedia = new AMedia(nombreJaspre.replace("jasper", "pdf"), "pdf", "application/pdf", mediais);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
		return amedia;
	}

	public static void reporteEXCEL(String nombreJaspre, String nombreArchivo, Map<String, Object> map,
			JRDataSource dataSource) {
		AMedia amedia = null;
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(getFileName(nombreJaspre), map, dataSource);

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();

			// coding For Excel:
			JRXlsExporter exporterXLS = new JRXlsExporter();
			exporterXLS.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporterXLS.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
			// configuration.setOnePagePerSheet(true);
			configuration.setDetectCellType(false);
			configuration.setWhitePageBackground(false);
			configuration.setRemoveEmptySpaceBetweenRows(true);
			// //agregadas
			configuration.setIgnorePageMargins(true);
			configuration.setWhitePageBackground(false);
			configuration.setIgnoreCellBorder(false);
			configuration.setRemoveEmptySpaceBetweenColumns(true);
			configuration.setRemoveEmptySpaceBetweenRows(true);
			configuration.setCollapseRowSpan(false);
			exporterXLS.setConfiguration(configuration);
			exporterXLS.exportReport();
			Date fechaActual = new Date();
			String nombre = "";
			String hora = "";
			SimpleDateFormat format = new SimpleDateFormat("MMM-dd-yyyy-HH:mm:ss");
			nombre = format.format(fechaActual);
			hora = nombre.replace(":", "-");
			amedia = new AMedia(nombreArchivo + "-" + hora + ".xls", "xls", "application/file", out.toByteArray());
			Filedownload.save(amedia);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	/*
	 * Para reportes Productos
	 */

	public static final String inventarioJasper = "inventario.jasper";
	public static final String inventarioExcelJasper = "inventario_excel.jasper";
	public static final String codigoBarraJasper = "codigo_barra.jasper";
	public static final String reporteVentaJasper = "reporte_venta.jasper";

	public static ProductoDataSource getProductoDataSource(List<Producto> listap) {
		ProductoDataSource datasource = new ProductoDataSource();
		listap.forEach(x -> datasource.add(x));
		return datasource;
	}
	/*
	 * Para reportes Proforma
	 */

	// genera el datadource para la factura
	public static ProformaDataSource getProformaDataSource(List<Modelos<VentaDetalle, Object>> listaVenta) {
		ProformaDataSource datasource = new ProformaDataSource();
		for (Modelos<VentaDetalle, Object> venta : listaVenta) {
			datasource.addProforma(venta);
		}
		return datasource;
	}

	// genera datos de envio para detalles de factura

	public static Map<String, Object> getMapReport(String nombre, String ruc, String direccion, String telefono,
			String cliente, String cedula, Timestamp fechaHora, String usuario, Double subtotal, Double iva,
			Double compensacion, Double descuento, Double total, Estado estado) {
		Map<String, Object> mapreporte = new HashMap<String, Object>();

		mapreporte.put("nombre", nombre);
		mapreporte.put("ruc", ruc);
		mapreporte.put("direccion", direccion);
		mapreporte.put("telefono", telefono);
		mapreporte.put("fecha", fechaHora);

		mapreporte.put("cedula", cedula);
		mapreporte.put("cliente", cliente);
		mapreporte.put("usuario", usuario);

		mapreporte.put("subtotal", subtotal);
		mapreporte.put("iva", iva);
		mapreporte.put("compensacion", compensacion);
		mapreporte.put("descuento", descuento);
		mapreporte.put("total", total);

		if (estado.getId() == Sistema.ESTADO_ORDEN_VENTA) {
			mapreporte.put("proforma", "Valido por 8 dias");
		} else if (estado.getId() == Sistema.ESTADO_ENTREGADO_VENTA) {
			mapreporte.put("proforma", "");
		} else if (estado.getId() == Sistema.ESTADO_PLAN_VENTA) {
			mapreporte.put("proforma", "");
		} else if (estado.getId() == Sistema.ESTADO_INGRESADO) {
			mapreporte.put("proforma", "Compra");
		}
		return mapreporte;
	}

	/*
	 * Para reportes Compra
	 */

	// genera el datadource para la factura
	public static CompraDataSource getCompraDataSource(List<Modelos<CompraDetalle, NotaCompraDetalle>> listaCompra) {
		CompraDataSource datasource = new CompraDataSource();
		for (Modelos<CompraDetalle, NotaCompraDetalle> compra : listaCompra) {
			datasource.addCompra(compra);
		}
		return datasource;
	}

	public static NotaCompraDataSource getNotaCompraDataSource(
			List<Modelos<NotaCompraDetalle, Object>> listaNotaCompra) {
		NotaCompraDataSource datasource = new NotaCompraDataSource();
		for (Modelos<NotaCompraDetalle, Object> compra : listaNotaCompra) {
			datasource.addCompra(compra);
		}
		return datasource;
	}
	/*
	 * Reporte Venta
	 */

	public static ReporteVentaDataSource getReporteVentaDataSource(List<VentaCabecera> listap) {
		ReporteVentaDataSource datasource = new ReporteVentaDataSource();
		listap.forEach(x -> datasource.add(x));
		/*
		 * for (VentaCabecera ventaCabera : listap) {
		 * datasource.addProducto(ventaCabera); }
		 */
		return datasource;
	}

	public static boolean validarCorreo(String cadena) {
		if (cadena == null || !cadena.matches(".+@.+\\.[a-z]+")) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean validarNumero(String cadena) {
		if (cadena.matches("\\d*"))
			return true;
		else
			return false;
	}

	public static boolean validarCedula(String cedula) {
		boolean cedulaCorrecta = false;

		try {

			if (cedula.length() == 10) // ConstantesApp.LongitudCedula
			{
				int tercerDigito = Integer.parseInt(cedula.substring(2, 3));
				if (tercerDigito < 6) {
					// Coeficientes de validación cédula
					// El decimo digito se lo considera dígito verificador
					int[] coefValCedula = { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
					int verificador = Integer.parseInt(cedula.substring(9, 10));
					int suma = 0;
					int digito = 0;
					for (int i = 0; i < (cedula.length() - 1); i++) {
						digito = Integer.parseInt(cedula.substring(i, i + 1)) * coefValCedula[i];
						suma += ((digito % 10) + (digito / 10));
					}

					if ((suma % 10 == 0) && (suma % 10 == verificador)) {
						cedulaCorrecta = true;
					} else if ((10 - (suma % 10)) == verificador) {
						cedulaCorrecta = true;
					} else {
						cedulaCorrecta = false;
					}
				} else {
					cedulaCorrecta = false;
				}
			} else {
				cedulaCorrecta = false;
			}
		} catch (NumberFormatException nfe) {
			cedulaCorrecta = false;
		} catch (Exception err) {
			System.out.println("Una excepcion ocurrio en el proceso de validadcion");
			cedulaCorrecta = false;
		}
		if (!cedulaCorrecta) {
			System.out.println("La Cédula ingresada es Incorrecta");
		}
		return cedulaCorrecta;
	}
	/*
	 * Codigo para enviar correo
	 */

	// Credenciales para correo
	public static String correoEnvia;
	public static String claveCorreo;
	public static String host;
	public static String puerto;

	public static void datosEmail(String correoEnvia, String claveCorreo, String host, String puerto) {
		Sistema.correoEnvia = correoEnvia;
		Sistema.claveCorreo = claveCorreo;
		Sistema.host = host;
		Sistema.puerto = puerto;
	}

	public static void enviarCorreo(String correo, String asunto, String mensaje, String archivo) {
		// La configuración para enviar correo
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.port", puerto);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.user", correoEnvia);
		properties.put("mail.password", claveCorreo);

		// Obtener la sesion
		Session session = Session.getInstance(properties, null);

		try {
			// Crear el cuerpo del mensaje
			MimeMessage mimeMessage = new MimeMessage(session);

			// Agregar quien envía el correo
			mimeMessage.setFrom(new InternetAddress(correoEnvia, "Sistema Realizado por DesigntTechx"));

			// Los destinatarios
			InternetAddress[] internetAddresses = { new InternetAddress(correo) };

			// Agregar los destinatarios al mensaje
			mimeMessage.setRecipients(Message.RecipientType.TO, internetAddresses);

			// Agregar el asunto al correo
			mimeMessage.setSubject(asunto);

			// Creo la parte del mensaje
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(mensaje);
			mimeBodyPart.attachFile(archivo);

			// Crear el multipart para agregar la parte del mensaje anterior
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);

			// Agregar el multipart al cuerpo del mensaje
			mimeMessage.setContent(multipart);

			// Enviar el mensaje
			Transport transport = session.getTransport("smtp");
			transport.connect(correoEnvia, claveCorreo);
			transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
			transport.close();
			System.out.println("Correo enviado");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void enviarCorreoPrueba(String correo, String asunto, String mensaje) {
		// La configuración para enviar correo
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.port", puerto);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.user", correoEnvia);
		properties.put("mail.password", claveCorreo);

		// Obtener la sesion
		Session session = Session.getInstance(properties, null);

		try {
			// Crear el cuerpo del mensaje
			MimeMessage mimeMessage = new MimeMessage(session);

			// Agregar quien envía el correo
			mimeMessage.setFrom(new InternetAddress(correoEnvia, "Sistema Realizado por DesigntTechx"));

			// Los destinatarios
			InternetAddress[] internetAddresses = { new InternetAddress(correo) };

			// Agregar los destinatarios al mensaje
			mimeMessage.setRecipients(Message.RecipientType.TO, internetAddresses);

			// Agregar el asunto al correo
			mimeMessage.setSubject(asunto);

			// Creo la parte del mensaje
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(mensaje);
			// mimeBodyPart.attachFile(archivo);

			// Crear el multipart para agregar la parte del mensaje anterior
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);

			// Agregar el multipart al cuerpo del mensaje
			mimeMessage.setContent(multipart);

			// Enviar el mensaje
			Transport transport = session.getTransport("smtp");
			transport.connect(correoEnvia, claveCorreo);
			transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
			transport.close();
			System.out.println("Correo enviado");
			Messagebox.show("Correo Enviado con Exito");
		} catch (Exception ex) {
			ex.printStackTrace();
			Messagebox.show("Imposible enviar Correo");
		}
	}

	// Codigo para comprbar si existe conexion ha internet

	public static boolean verificarInternet() {
		boolean respuesta = false;
		String dirWeb = "www.google.com";
		int puerto = 80;
		try {
			if (extracted(dirWeb, puerto).isConnected()) {
				System.out.println(
						"Conexión establecida con la dirección: " + dirWeb + " a travéz del puerto: " + puerto);
				respuesta = true;
			}
		} catch (Exception e) {
			respuesta = false;
			System.err.println("No se pudo establecer conexión con: " + dirWeb + " a travez del puerto: " + puerto);
		}
		return respuesta;
	}

	private static Socket extracted(String dirWeb, int puerto) throws UnknownHostException, IOException {
		return new Socket(dirWeb, puerto);
	}
}
