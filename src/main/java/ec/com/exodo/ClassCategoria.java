package ec.com.exodo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.AreaDAO;
import ec.com.exodo.dao.ie.CategoriaDAO;
import ec.com.exodo.entity.Area;
import ec.com.exodo.entity.Categoria;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassCategoria {
	@Wire("#winCategoria")
	private Window winCategoria;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;

	private List<Categoria> listaCategoria;
	private List<Area> listaArea;

	private Area area;
	
	private boolean enableCombo= true;

	private CategoriaDAO categoriaDAO = InstanciaDAO.getInstanciaCategoriaDAO();
	private AreaDAO areaDAO = InstanciaDAO.getInstanciaAreaDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("clase") String clase,
			@ExecutionArgParam("area") Area area) {
		Selectors.wireComponents(view, this, false);
		ajustarPantalla(clase);
		this.area = area;
		enableCombo = (area != null);
		cargaInicial();
	}

	private void ajustarPantalla(String clase) {
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (clase.equals("Venta") || clase.equals("Producto")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "75%";
		}
	}

	private void cargaInicial() {
		listaArea = areaDAO.getListaArea();
		if (listaArea != null && listaArea.size() > 0) {
			area = (area == null) ? listaArea.get(0) : area;
			filtroArea(area.getId());
		} else {
			Messagebox.show("Es necesario agregar areas al sistema");
			salir();
		}
	}

	@Command
	@NotifyChange({ "listaCategoria" })
	public void filtroArea(@BindingParam("idArea") final int idArea) {
		listaCategoria = categoriaDAO.getListaCategoriaByArea(idArea);
	}

	@Command
	@NotifyChange("listaCategoria")
	public void agregarNuevo() {
		listaCategoria.add(new Categoria(area));
	}

	@Command
	@NotifyChange("listaCategoria")
	public void eliminar(@BindingParam("categoria") final Categoria categoria) {
		if (categoria.getId() == null) {
			listaCategoria.remove(categoria);
		} else {
			Messagebox.show(getIdioma().getMensajeEliminar(categoria.getDescripcion()), "Confirm",
					Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, event -> {
						if (((Integer) event.getData()).intValue() == Messagebox.OK) {
							procesoEliminar(categoriaDAO.eliminar(categoria));
						}
					});
		}
	}

	private void procesoEliminar(int a) {
		if (a == 1) {
			Clients.showNotification("Registro eliminado satisfactoriamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"top_center", 2000);
			BindUtils.postNotifyChange(null, null, ClassCategoria.this, "listaCategoria");
			// Recargar ListaCategoria
			filtroArea(area.getId());
		} else {
			Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR, null, "top_center",
					2000);
			BindUtils.postNotifyChange(null, null, ClassCategoria.this, "listaCategoria");
		}
	}

	@Command
	public void aceptar(@BindingParam("categoria") Categoria categoria) {
		if (categoria == null) {
			Messagebox.show("Debe seleccionar un elemento de la lista ", "Error", Messagebox.OK,
					Messagebox.EXCLAMATION);
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", categoria);
		BindUtils.postGlobalCommand(null, null, "cargarCategoria", parametros);
		salir();
	}

	@Command
	@NotifyChange("listaCategoria")
	public void guardarEditar(@BindingParam("categoria") Categoria categoria) {
		if (validarGuardarEditar(categoria)) {
			if (Sistema.validarRepetido(categoriaDAO.getListaCategoria(), categoria)) {
				return;
			}
			if (categoriaDAO.saveorUpdate(categoria) != null) {
				Clients.showNotification(
						"Categoria " + ((categoria.getId() == null) ? "ingresado" : "modificado") + " correctamente",
						Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
				filtroArea(area.getId());
			} else {
				Clients.showNotification("Imposible realizar esta accion", Clients.NOTIFICATION_TYPE_WARNING, null,
						"middle_center", 1500);
			}
		}
	}

	private boolean validarGuardarEditar(final Categoria categoria) {
		boolean banderaDevolver = true;
		if (categoria.getDescripcion().equals("") || categoria.getDescripcion().isEmpty()) {
			Messagebox.show(getIdioma().getIngresarDescripcion());
			banderaDevolver = false;
		} else {
			categoria.setDescripcion(categoria.getDescripcion().toUpperCase().trim());
		}
		if (categoria.getArea() == null) {
			Clients.showNotification("Es necesario Asignar Area", Clients.NOTIFICATION_TYPE_WARNING, null,
					"middle_center", 1500);
			banderaDevolver = false;
		}
		return banderaDevolver;
	}

	@Command
	@NotifyChange("listaCategoria")
	public void filtrar(final @BindingParam("dato") String datoBusqueda) {
		listaCategoria.clear();
		final List<Categoria> listaProvicional = categoriaDAO.getListaCategoriaByArea(area.getId());
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaCategoria.addAll(categoriaDAO.getListaCategoriaByArea(area.getId()));
		} else {
			listaProvicional.stream().filter(filter(datoBusqueda)).forEach(listaCategoria::add);
		}
	}

	private Predicate<Categoria> filter(String datoBusqueda) {
		return dato -> dato.getDescripcion().trim().toUpperCase().contains(datoBusqueda.toUpperCase().trim());
	}

	@Command
	public void salir() {
		winCategoria.detach();
	}

	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public List<Categoria> getListaCategoria() {
		return listaCategoria;
	}

	public void setListaCategoria(List<Categoria> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public List<Area> getListaArea() {
		return listaArea;
	}

	public void setListaArea(List<Area> listaArea) {
		this.listaArea = listaArea;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public boolean isEnableCombo() {
		return enableCombo;
	}

	public void setEnableCombo(boolean enableCombo) {
		this.enableCombo = enableCombo;
	}
	
	

}
