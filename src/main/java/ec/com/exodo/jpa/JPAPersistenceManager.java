package ec.com.exodo.jpa;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAPersistenceManager {

	public static final boolean DEBUG = true;
	private static JPAPersistenceManager singleton;
	protected EntityManagerFactory emf;
	// private static String vDBMS = (String)
	// Sessions.getCurrent().getAttribute("PostgreSQL"); //PostgreSQL MSSQL

	public static JPAPersistenceManager getInstance() {
		if (singleton == null) {
			singleton = new JPAPersistenceManager();
		}
		return singleton;
	}

	public JPAPersistenceManager() {

	}

	public EntityManagerFactory getEntityManagerFactory() {
		if (emf == null)
			createEntityManagerFactory();
		return emf;
	}

	@SuppressWarnings("rawtypes")
	protected void createEntityManagerFactory() {
		this.emf = Persistence.createEntityManagerFactory("exodo", new java.util.HashMap());
		if (DEBUG)
			System.out.println(new java.util.Date());
	}
}
