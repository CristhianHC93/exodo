package ec.com.exodo;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.SucursalDAO;
import ec.com.exodo.entity.Sucursal;
import ec.com.exodo.entity.Usuario;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassSucursalCE {
	@Wire("#winSucursalCE")
	private Window winSucursalCE;

	private Sucursal sucursal;
	private boolean bandera = false;
	boolean isNew = true;
	private SucursalDAO sucursalDAO = InstanciaDAO.getInstanciaSucursalDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") Sucursal sucursal) {
		Selectors.wireComponents(view, this, false);
		this.sucursal = (sucursal == null) ? new Sucursal() : sucursal;
		this.isNew = (sucursal == null);
		/*
		 * if (sucursal == null) { this.sucursal = new Sucursal(); // Realiza Instancia
		 * desde las Entity // this.sucursal.setInformacionNegocio(new
		 * InformacionNegocio()); // this.sucursal.getInformacionNegocio().setEmail(new
		 * Email()); } else { this.sucursal = sucursal; }
		 */
	}

	@Command
	@NotifyChange("sucursal")
	public void guardarEditar() {
		if (sucursal.getUsuario() == null) {
			Messagebox.show("Debe escoger un Usuario Encargado");
			return;
		}
		bandera = true;
		if (isNew) {
			sucursalDAO.saveorUpdate(sucursal);
			Clients.showNotification("Sucursal ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			// sucursal = new Sucursal();
		} else {
			sucursalDAO.saveorUpdate(sucursal);
			Clients.showNotification("SucursalCE Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	public void agregarUsuario() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("clase", "sucursal");
		Executions.createComponents("/view/formUsuario.zul", null, parameters);
	}

	@GlobalCommand
	@NotifyChange("sucursal")
	public void cargarUsuario(@BindingParam("objeto") Usuario usuario) {
		sucursal.setUsuario(usuario);
	}

	@Command
	@NotifyChange("sucursalCE")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		// if (campo.equals("descripcion")) {
		// sucursal.setDescripcion(cadena.toUpperCase());
		// }
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winSucursalCE.detach();
	}

	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursalCE) {
		this.sucursal = sucursalCE;
	}
}
