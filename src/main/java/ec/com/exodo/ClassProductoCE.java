package ec.com.exodo;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.AreaSucursalDAO;
import ec.com.exodo.dao.ie.PrecioDAO;
import ec.com.exodo.dao.ie.ProductoDAO;
import ec.com.exodo.dao.ie.ProductoProveedorDAO;
import ec.com.exodo.dao.ie.ProveedorDAO;
import ec.com.exodo.entity.Area;
import ec.com.exodo.entity.Categoria;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.entity.Impuesto;
import ec.com.exodo.entity.Precio;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.ProductoProveedor;
import ec.com.exodo.entity.Proveedor;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassProductoCE {
	@Wire("#winFormProductoCE")
	private Window winFormProducto;

	// private String codigoProducto;
	private Producto producto;
	private List<Impuesto> listaImpuesto;

	private ProductoDAO productoDAO = InstanciaDAO.getInstanciaProductoDAO();
	private ProductoProveedorDAO productoProveedorDAO = InstanciaDAO.getInstanciaProductoProveedorDAO();
	private ProveedorDAO proveedorDAO = InstanciaDAO.getInstanciaProveedorDAO();
	private AreaSucursalDAO areaSucursalDAO = InstanciaDAO.getInstanciaAreaSucursalDAO();
	private PrecioDAO precioDAO = InstanciaDAO.getInstanciaPrecioDAO();

	// private Estado estado = InstanciaEntity.getInstanciaEstado();
	private List<Proveedor> listaProveedor;

	private List<Precio> listaPrecio;
	private List<Precio> listaPrecioConfiguracion;

	boolean estadoVerificarSucursal = false;
	boolean banderaPrecios = false;
	
	private Area area;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") Producto producto,@ExecutionArgParam("area") Area area) {
		Selectors.wireComponents(view, this, false);
		this.area = area;
		listaPrecioConfiguracion = precioDAO.getListaByProductoIsNull();
		validarlistaPrecioConfiguracion(listaPrecioConfiguracion);
		cargarDatosInicio(producto);
	}

	private void validarlistaPrecioConfiguracion(final List<Precio> listaPrecioConfiguracion) {
		if (listaPrecioConfiguracion == null || listaPrecioConfiguracion.size() < 1) {
			Messagebox.show("Debe Ingresar Parametros de Precios");
			salir();
			return;
		}
	}

	private void cargarDatosInicio(Producto producto) {
		listaImpuesto = Sistema.getListaImpuesto();
		if (producto == null) {
			nuevo();
		} else {
			this.producto = producto;
			listaProveedor = new ArrayList<>();
			productoProveedorDAO.getListaProductoProveedorByProducto(producto.getId()).forEach(x -> {
				x.getProveedor().setCosto(x.getPrecioCosto());
				listaProveedor.add(x.getProveedor());
			});
			listaPrecio = precioDAO.getListaByProducto(producto.getId());
		}
	}

	private void nuevo() {
		this.producto = new Producto();
		this.producto.setImpuesto((producto == null) ? listaImpuesto.get(0) : producto.getImpuesto());
		producto.setEstado(new Estado(Sistema.ESTADO_INGRESADO));
		if (listaProveedor != null) {
			listaProveedor.clear();
		}
		listaPrecio = listaPrecioConfiguracion;
		producto.setImpuesto(listaImpuesto.get(0));
	}

	@NotifyChange("producto")
	@Command
	public void guardarEditar() {
		if (validarGuardarEditar()) {
			/* Setear Calorimetria */
			Sistema.calorimetria(producto);
			producto.setCantidad(Sistema.redondear(producto.getCantidad()));
			producto.setAreaSucursal(areaSucursalDAO.getAreaSucursalByAreaAndSucursal(
					producto.getCategoria().getArea().getId(), Sistema.getSucursal().getId()));
			Producto referenciaProducto = productoDAO.saveorUpdate(producto);
			if (referenciaProducto != null) {
				if (producto.getId() == null) {
					guardarListados(referenciaProducto);
					nuevo();
					Clients.showNotification("Producto Ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
							"middle_center", 1500);
				} else {
					// actualizarCategoria(productoDAO.getProducto(producto.getId()));
					productoProveedorDAO.eliminar(referenciaProducto);
					guardarListados(referenciaProducto);
					listaPrecio = precioDAO.getListaByProducto(referenciaProducto.getId());
					Clients.showNotification("Producto Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
							"middle_center", 1500);
				}
			} else {
				Clients.showNotification("Imposible realizar accion", Clients.NOTIFICATION_TYPE_WARNING, null,
						"middle_center", 1500);
			}
		}
	}

	
	private void guardarListados(Producto referenciaProducto) {
		listaProveedor.forEach(prov -> {
			productoProveedorDAO.saveorUpdate(new ProductoProveedor(prov.getCosto(), referenciaProducto, prov));
		});
		listaPrecio.forEach(precio -> {
			if (precio.getProducto() == null) {
				precio.setId(null);
			}
			precio.setProducto(referenciaProducto);
			precio = precioDAO.saveOrUpdate(precio);
		});
	}

	private boolean validarGuardarEditar() {
		boolean devolver = true;
		final String codigoProvicional = producto.getCodigo();
		if (producto.getProveedor() == null) {
			Messagebox.show("Debe ingresar proveedor");
			devolver = false;
		}
		if (devolver && producto.getCategoria() == null) {
			Messagebox.show("Debe Seleccionar una Categoria");
			devolver = false;
		}
		if (devolver && (listaProveedor == null || listaProveedor.size() < 1)) {
			Messagebox.show("Debe Seleccionar al menos un Porveedor");
			devolver = false;
		}
		if (devolver && producto.getPrecioVenta() <= 0) {
			banderaPrecios = false;
			String msj = "Debe Agregar Precios, Desea que se agreguen automanticamente con ajustes por defecto";
			Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
					(EventListener<Event>) event -> {
						if (((Integer) event.getData()).intValue() == Messagebox.OK) {
							generarPreciosPorDefecto();
						}
						if (((Integer) event.getData()).intValue() == Messagebox.CANCEL) {
							banderaPrecios = true;
						}
					});
			devolver = false;
		}
		if (devolver && Sistema.validarRepetido(
				productoDAO.getListaProductoBySucursal(Sistema.ESTADO_INGRESADO, Sistema.getSucursal().getId()),
				producto)) {
			producto.setCodigo(codigoProvicional);
			devolver = false;
		}
		return devolver;
	}

	private void generarPreciosPorDefecto() {
		listaPrecio.forEach(x -> {
			double valor = Sistema.redondear(
					producto.getPrecioCompra() + (producto.getPrecioCompra() * (x.getPorcentaje() / 100)), 4);
			x.setValor(valor);
		});
		producto.setPrecioVenta(listaPrecio.get(0).getValor());
		guardarEditar();
	}

	@Command
	public void agregarProveedor() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("clase", "Producto");
		List<Proveedor> listaProveedor = proveedorDAO.getProveedores(Sistema.ESTADO_INGRESADO);
		if (producto.getId() == null) {
			if (this.listaProveedor != null) {
				this.listaProveedor.forEach(x -> {
					listaProveedor.forEach(y -> {
						if (x.getId() == y.getId()) {
							y.setCheckProducto(true);
							y.setCosto(x.getCosto());
						}
					});
				});
			}
			parameters.put("lista", listaProveedor);
		} else {
			productoProveedorDAO.getListaProductoProveedorByProducto(producto.getId()).forEach(x -> {
				listaProveedor.forEach(y -> {
					if (x.getProveedor().getId() == y.getId()) {
						y.setCheckProducto(true);
						y.setCosto(x.getPrecioCosto());
					}
				});
			});
			parameters.put("lista", listaProveedor);
		}

		Executions.createComponents("/view/formProveedor.zul", null, parameters);
	}

	@Command
	public void agregarCategoria() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("clase", "Producto");
		parameters.put("area", area);
		Executions.createComponents("/view/formCategoria.zul", null, parameters);
	}

	@GlobalCommand
	@NotifyChange("producto")
	public void cargarDatos(@BindingParam("objeto") Object object, @BindingParam("lista") List<Proveedor> lista) {
		listaProveedor = lista;
		if (object instanceof Proveedor) {
			producto.setProveedor((Proveedor) object);
			producto.setPrecioCompra(((Proveedor) object).getCosto());
		}
	}

	@GlobalCommand
	@NotifyChange("producto")
	public void cargarListaPrecio(@BindingParam("listaPrecio") List<Precio> listaPrecio) {
		this.listaPrecio = listaPrecio;
		producto.setPrecioVenta(Sistema.redondear(listaPrecio.get(0).getValor(), 4));
	}

	@Command
	public void agregarPrecio() {
		if (producto.getPrecioCompra() < 0) {
			Messagebox.show("Debe Ingresar valor compra para asignar precios");
			return;
		}
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("lista", setValoresListaPrecio(this.listaPrecio));
		parameters.put("producto", this.producto);
		Executions.createComponents("/view/formListaPrecio.zul", null, parameters);
	}

	private List<Precio> setValoresListaPrecio(List<Precio> listaPrecios) {
		listaPrecios.forEach(x -> {
			final double precioCompra = producto.getPrecioCompra()
					+ (producto.getPrecioCompra() * ((double) producto.getImpuesto().getPorcentaje() / 100));
			final double valor = Sistema.redondear((precioCompra + (precioCompra * (x.getPorcentaje() / 100))), 3);
			x.setValor(valor);
		});
		return listaPrecios;
	}

	@GlobalCommand
	@NotifyChange("producto")
	public void cargarCategoria(@BindingParam("objeto") Object object) {
		if (object instanceof Categoria) {
			producto.setCategoria((Categoria) object);
		}
	}

	@Command
	@NotifyChange("producto")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("descripcion")) {
			producto.setDescripcion(cadena.toUpperCase());
		}
		if (campo.equals("descripcionCorta")) {
			producto.setDescripcionCorta(cadena.toUpperCase());
		}
		if (campo.equals("codigo")) {
			producto.setCodigo(cadena.toUpperCase());
		}
	}

	@NotifyChange("producto")
	@Command
	public void uploadFile(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) {
		if (producto.getCodigo() == null || producto.getCodigo().equals("")) {
			Messagebox.show("Es necesario ingresar codigo");
			return;
		}
		Media media;
		media = event.getMedia();
		int aleatorio = ThreadLocalRandom.current().nextInt(1, 100000);
		if (Util.uploadFile(media, producto.getCodigo() + aleatorio, Sistema.carpetaImagen)) {
			new File(Util.getPathRaiz() + producto.getImagen()).delete();
			producto.setImagen("/" + Sistema.carpetaImagen + "/" + producto.getCodigo() + aleatorio);
		}
	}

	@Command
	public void salir() {
		BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		winFormProducto.detach();
	}

	/*
	 * Metodos Get And Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto Producto) {
		this.producto = Producto;
	}

	public List<Impuesto> getListaImpuesto() {
		return listaImpuesto;
	}

	public void setListaImpuesto(List<Impuesto> listaImpuesto) {
		this.listaImpuesto = listaImpuesto;
	}
}
