package ec.com.exodo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.ProveedorDAO;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.entity.Proveedor;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassProveedor {
	@Wire("#winFormProveedor")
	private Window winFormProveedor;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private boolean checkMulti = false;
	private boolean soloLectura = false;
	private String datoBusqueda;

	private List<Proveedor> listaProveedor;
	// private List<Proveedor> listaProveedorProvisional;
	private ProveedorDAO proveedorDAO = InstanciaDAO.getInstanciaProveedorDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("clase") String clase,
			@ExecutionArgParam("lista") List<Proveedor> lista) {
		Selectors.wireComponents(view, this, false);
		if (clase == null) {
			clase = "this";
		}

		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (clase.equals("NotaCompra") || clase.equals("Compra") || clase.equals("Producto")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "90%";
		}

		if (lista != null) {
			checkMulti = true;
			listaProveedor = lista;
			soloLectura = (clase.equals("NotaCompra") || clase.equals("Compra"));
		} else {
			listaProveedor = proveedorDAO.getProveedores(Sistema.ESTADO_INGRESADO);
		}
	}

	@Command
	@NotifyChange({ "listaProveedor", "proveedor", "btGuardarEditar" })
	public void nuevo() {
		Executions.createComponents("/view/formProveedorCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaProveedor")
	public void deleteProveedor(@BindingParam("proveedor") final Proveedor proveedor) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + proveedor.getNombre()
				+ "\" esta accion no podra deshacerse si se confirma";

		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						try {
							proveedor.setEstado(new Estado(Sistema.ESTADO_ELIMINADO));
							proveedorDAO.saveorUpdate(proveedor);
							listaProveedor.clear();
							listaProveedor = proveedorDAO.getProveedores(Sistema.ESTADO_INGRESADO);
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassProveedor.this, "listaProveedor");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	@Command
	@NotifyChange("listaProveedor")
	public void aceptar(@BindingParam("proveedor") Proveedor proveedor) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", proveedor);
		if (aceptarProveedorMulti(proveedor, parametros)) {
			BindUtils.postGlobalCommand(null, null, "cargarDatos", parametros);
			salir();
		}
	}

	private boolean aceptarProveedorMulti(final Proveedor proveedor, Map<String, Object> parametros) {
		boolean devolver = true;
		if (checkMulti) {
			if (proveedor.getCosto() <= 0.0) {
				Messagebox.show("Debe ingreasr Costo");
				devolver = false;
			}
			proveedor.setCheckProducto(true);
			List<Proveedor> listaDevolver = new ArrayList<>();
			listaProveedor.forEach(x -> {
				if (x.isCheckProducto()) {
					listaDevolver.add(x);
				}
			});
			parametros.put("lista", listaDevolver);
		}
		return devolver;
	}

	@Command
	@NotifyChange("proveedor")
	public void editar(@BindingParam("proveedor") Proveedor proveedor) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", proveedor);
		Executions.createComponents("/view/formProveedorCE.zul", null, parametros);
	}

	@Command
	@NotifyChange("listaProveedor")
	public void filtrar() {
		listaProveedor.clear();
		if (datoBusqueda.trim().equals("")) {
			listaProveedor.addAll(proveedorDAO.getProveedores(Sistema.ESTADO_INGRESADO));
		} else {
			for (Proveedor item : proveedorDAO.getProveedores(Sistema.ESTADO_INGRESADO)) {
				if (item.getEmpresa().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0
						|| item.getNombre().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0
						|| item.getRuc().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0) {
					listaProveedor.add(item);
				}
			}
		}
	}

	@GlobalCommand
	@NotifyChange("listaProveedor")
	public void refrescarlista() {
		listaProveedor = proveedorDAO.getProveedores(Sistema.ESTADO_INGRESADO);
	}

	@Command
	public void salir() {
		winFormProveedor.detach();
	}

	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public List<Proveedor> getListaProveedor() {
		return listaProveedor;
	}

	public void setListaProveedor(List<Proveedor> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public boolean isCheckMulti() {
		return checkMulti;
	}

	public void setCheckMulti(boolean checkMulti) {
		this.checkMulti = checkMulti;
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public boolean isSoloLectura() {
		return soloLectura;
	}

	public void setSoloLectura(boolean soloLectura) {
		this.soloLectura = soloLectura;
	}

}
