package ec.com.exodo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.dao.ie.CompraDetalleDAO;
import ec.com.exodo.dao.ie.CuadreInventarioDAO;
import ec.com.exodo.dao.ie.NotaCompraCabeceraDAO;
import ec.com.exodo.dao.ie.NotaCompraDetalleDAO;
import ec.com.exodo.dao.ie.ProductoProveedorDAO;
import ec.com.exodo.dao.ie.ProveedorDAO;
import ec.com.exodo.entity.CompraDetalle;
import ec.com.exodo.entity.CuadreInventario;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.entity.NotaCompraCabecera;
import ec.com.exodo.entity.NotaCompraDetalle;
import ec.com.exodo.entity.Producto;
import ec.com.exodo.entity.ProductoProveedor;
import ec.com.exodo.entity.Proveedor;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.models.Modelos;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassNotaCompra {

	private Double subtotal = 0.00;
	private Double total = 0.00;
	private Double iva = 0.00;
	private Double descuento = 0.00;

	private NotaCompraCabecera notaCompraCabecera;

	private boolean estadoGuardar = true;
	private boolean estadoModificar = false;
	private boolean estadoAnular = true;

	private List<NotaCompraDetalle> listaNotaCompraDetalleRemovidos = new ArrayList<>();

	private List<Modelos<NotaCompraDetalle, Object>> listaNotaCompra;

	private boolean banderaValidarProducto = true;

	private ProductoProveedorDAO productoProveedorDAO = InstanciaDAO.getInstanciaProductoProveedorDAO();
	private NotaCompraCabeceraDAO notaCompraCabeceraDAO = InstanciaDAO.getInstanciaNotaCompraCabeceraDAO();
	private NotaCompraDetalleDAO notaCompraDetalleDAO = InstanciaDAO.getInstanciaNotaCompraDetalleDAO();
	private ProveedorDAO proveedorDAO = InstanciaDAO.getInstanciaProveedorDAO();
	private CuadreInventarioDAO cuadreInventarioDAO = InstanciaDAO.getInstanciaCuadreInventarioDAO();

	private CompraDetalleDAO compraDetalleDAO = InstanciaDAO.getInstanciaCompraDetalleDAO();

	boolean estadoVerificarSucursal = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("notaCompraCabecera") NotaCompraCabecera notaCompraCabecera,
			@ExecutionArgParam("listaNotaCompra") List<Modelos<NotaCompraDetalle, Object>> listaNotaCompra,
			@ExecutionArgParam("subtotal") Double subtotal, @ExecutionArgParam("iva") Double iva,
			@ExecutionArgParam("total") Double total) {
		Selectors.wireComponents(view, this, false);
		if (notaCompraCabecera != null) {
			this.notaCompraCabecera = notaCompraCabecera;
			this.listaNotaCompra = new ArrayList<>(listaNotaCompra.size());
			this.listaNotaCompra.addAll(listaNotaCompra);
		} else {
			nuevo();
		}
	}

	@Command
	public void agregarProveedor() {
		if (notaCompraCabecera.getProveedor().getId() != null) {
			Messagebox.show("Si escoje otro proveedor se limpiaran los registros actuales", "Advertencia",
					Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, (EventListener<Event>) event -> {
						if (((Integer) event.getData()).intValue() == Messagebox.OK) {
							try {
								Map<String, Object> parameters = new HashMap<String, Object>();
								parameters.put("clase", "NotaCompra");
								Executions.createComponents("/view/formProveedor.zul", null, parameters);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
		} else {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("clase", "NotaCompra");
			Executions.createComponents("/view/formProveedor.zul", null, parameters);
		}
	}

	@NotifyChange("notaCompraCabecera")
	@Command
	public void agregarProveedorCtrl(@BindingParam("item") String item) {
		if (item.equals("")) {
			Messagebox.show("Ingresar ruc o nombre");
			return;
		}
		final Proveedor proveedor = proveedorDAO.getProveedorByEmpresaOrRuc(item.toUpperCase());
		if (proveedor != null) {
			nuevo();
			notaCompraCabecera.setProveedor(proveedor);
		} else {
			Messagebox.show("Proveedor no encontrado");
		}
	}

	@GlobalCommand
	@NotifyChange({ "btGuardarEditar", "listaNotaCompra", "notaCompraCabecera", "total", "subtotal", "iva", "descuento",
			"estadoGuardar", "estadoModificar" })
	public void cargarDatos(@BindingParam("objeto") Proveedor proveedor) {
		if (notaCompraCabecera.getProveedor().getId() != null) {
			nuevo();
		}
		notaCompraCabecera.setProveedor(proveedor);
	}

	@Command
	@NotifyChange("listaNotaCompra")
	public void agregarProducto() {
		if (notaCompraCabecera.getProveedor().getId() == null) {
			Messagebox.show("Debe ingresar un Proveedor");
			return;
		}
		if (listaNotaCompra.size() > 1
				&& listaNotaCompra.get(listaNotaCompra.size() - 1).getObjetoT().getCantidad() == 0) {
			Messagebox.show("El ultimo registro no tiene Cantidad");
			return;
		}
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("filtroProducto", "");
		parameters.put("clase", "notaCompra");
		parameters.put("proveedor", notaCompraCabecera.getProveedor());
		Executions.createComponents("/view/formProducto.zul", null, parameters);
	}

	@GlobalCommand
	@NotifyChange({ "notaCompraCabecera", "listaNotaCompra", "estadoGuardar" })
	public void cargarProducto(@BindingParam("objetoProducto") Producto producto) {
		boolean repetido = false;
		for (Modelos<NotaCompraDetalle, Object> notaCompra : listaNotaCompra) {
			if (notaCompra.getObjetoT().getProducto().getId() == producto.getId()) {
				repetido = true;
			}
		}
		if (repetido) {
			Messagebox.show("El producto " + producto.getDescripcion() + " ya ha sido ingresado");
		} else {
			NotaCompraDetalle notaCompraDetalle = new NotaCompraDetalle();
			notaCompraDetalle.setProducto(producto);
			notaCompraDetalle.setCantidad(1);
			notaCompraDetalle.setPrecioCompra(producto.getPrecioCompra());
			notaCompraDetalle.setCantidadEntregada(0);
			notaCompraDetalle.setEstado(new Estado(Sistema.ESTADO_INGRESADO));
			listaNotaCompra.add(new Modelos<NotaCompraDetalle, Object>(notaCompraDetalle, null, false));
			confirmar(listaNotaCompra.get(listaNotaCompra.size() - 1));
			listaNotaCompra.get(0).setEstado(true);
		}
	}

	@Command
	@NotifyChange({ "listaNotaCompra", "estadoGuardar", "listaCompra", "total", "subtotal", "descuento", "iva",
			"valorTotal", "compensacion", "estadoModificar", "btGuardarEditar" })
	public void cargarProductoCodigo(@BindingParam("item") String codigo) {
		if (notaCompraCabecera.getProveedor().getId() == null) {
			Messagebox.show("Debe ingresar un Proveedor");
			nuevo();
			return;
		}
		if (codigo == null || codigo.trim().equals("")) {
			Messagebox.show("Ingresar Codigo");
			return;
		}
		if (!cargarProducto(codigo)) {
			Messagebox.show("Codigo no encontrado");
		}
	}

	@NotifyChange({ "listaNotaCompra", "estadoGuardar", "total", "subtotal", "iva", "descuento", "estadoGuardar",
			"valorTotal", "estadoModificar", "btGuardarEditar" })
	@Command
	public void cargarProductoDescripcion(@BindingParam("item") String descripcion) {
		if (notaCompraCabecera.getProveedor().getId() == null) {
			Messagebox.show("Debe ingresar un Proveedor");
			nuevo();
			return;
		}
		if (descripcion == null || descripcion.trim().equals("")) {
			Messagebox.show("Ingresar Nombre del producto");
			return;
		}
		if (!cargarProducto(descripcion)) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("clase", "notaCompra");
			parameters.put("filtroProducto", descripcion);
			parameters.put("proveedor", notaCompraCabecera.getProveedor());
			Executions.createComponents("/view/formProducto.zul", null, parameters);
		}
	}

	/*
	 * Metodos que se usa al cargar los productos desde el text de codigo y
	 * desripcion
	 */

	private boolean cargarProducto(String dato) {
		final ProductoProveedor productoProveedor = productoProveedorDAO
				.getProductoByCodigoDescripcionProveedor(dato.toUpperCase(), notaCompraCabecera.getProveedor().getId());
		if (productoProveedor != null) {
			if (validarProductoRepetido(productoProveedor.getProducto())) {
				agregarProducto(productoProveedor.getProducto());
			}
		}
		return (productoProveedor != null);
	}

	private boolean validarProductoRepetido(final Producto producto) {
		banderaValidarProducto = true;
		listaNotaCompra.forEach(com -> {
			if (com.getObjetoT().getProducto().getId() == producto.getId()) {
				com.getObjetoT().setCantidad(com.getObjetoT().getCantidad() + 1);
				confirmar(com);
				listaNotaCompra.get(0).getObjetoT().getProducto().setCodigo("");
				listaNotaCompra.get(0).getObjetoT().getProducto().setDescripcion("");
				banderaValidarProducto = false;
			}
		});
		return banderaValidarProducto;
	}

	private void agregarProducto(final Producto producto) {
		NotaCompraDetalle ncd = getNotaCompraDetalleNuevo();
		ncd.setProducto(producto);
		ncd.setEstado(new Estado(Sistema.ESTADO_INGRESADO));
		ncd.setCantidad(1);
		ncd.setPrecioCompra(producto.getPrecioCompra());
		listaNotaCompra.add(new Modelos<NotaCompraDetalle, Object>(ncd, null, false));
		confirmar(listaNotaCompra.get(listaNotaCompra.size() - 1));
		listaNotaCompra.get(0).getObjetoT().getProducto().setCodigo("");
		listaNotaCompra.get(0).getObjetoT().getProducto().setDescripcion("");
	}

	@Command
	@NotifyChange({ "listaNotaCompra", "total", "subtotal", "iva", "descuento", "estadoGuardar" })
	public void confirmar(@BindingParam("item") Modelos<NotaCompraDetalle, Object> notaCompra) {
		validarConfirmar(notaCompra.getObjetoT());
		calcularItem(notaCompra.getObjetoT());
		calcularListatotal();
		estadoGuardar = false;
	}

	private void validarConfirmar(NotaCompraDetalle notaCompraDetalle) {
		if (notaCompraDetalle.getProducto() == null) {
			Messagebox.show("Debe escoger un producto");
			return;
		}
		if (notaCompraDetalle.getCantidad() == 0) {
			Messagebox.show("Debe ingresar una cantidad");
			notaCompraDetalle.setCantidad(1);
		}
	}

	private void calcularItem(NotaCompraDetalle notaCompraDetalle) {
		notaCompraDetalle.setCantidad(Sistema.redondear(notaCompraDetalle.getCantidad()));

		notaCompraDetalle
				.setSubtotal(Sistema.redondear(notaCompraDetalle.getCantidad() * notaCompraDetalle.getPrecioCompra()));

//		if (notaCompraDetalle.getDescuentoPorcentaje() > 0) {
			/* Calculamos el valor descuento por precio Unitario */
			notaCompraDetalle.setDescuentoValor(Sistema.redondear(
					notaCompraDetalle.getPrecioCompra() * (notaCompraDetalle.getDescuentoPorcentaje() / 100)));
			/*
			 * Seteamos el nuevo valor de subtotal restandole el resultado de descuento
			 * multiplicado cantidad
			 */
			notaCompraDetalle.setSubtotal(Sistema.redondear(notaCompraDetalle.getSubtotal()
					- (notaCompraDetalle.getDescuentoValor() * notaCompraDetalle.getCantidad())));

//		}
		if (notaCompraDetalle.getProducto().getImpuesto().getPorcentaje() > 0) {
			/* Calculamos el valor de iva por precio Unitario menos descuento */
			notaCompraDetalle.setValorIva(
					Sistema.redondear((notaCompraDetalle.getPrecioCompra() - notaCompraDetalle.getDescuentoValor())
							* (((double) notaCompraDetalle.getProducto().getImpuesto().getPorcentaje()) / 100)));
		}
		notaCompraDetalle.setTotal(
				notaCompraDetalle.getSubtotal() + (notaCompraDetalle.getValorIva() * notaCompraDetalle.getCantidad()));
	}

	public void calcularListatotal() {
		subtotal = 0.0;
		iva = 0.0;
		descuento = 0.0;
		if (listaNotaCompra.size() > 0) {
			listaNotaCompra.forEach(item -> {
				if (item.getObjetoT().getCantidad() > 0) {
					subtotal += Sistema.redondear(item.getObjetoT().getSubtotal(), 2);
					iva += Sistema.redondear(item.getObjetoT().getValorIva() * item.getObjetoT().getCantidad(), 2);
					descuento += Sistema
							.redondear(item.getObjetoT().getDescuentoValor() * item.getObjetoT().getCantidad(), 2);
				}
			});
			total = Sistema.redondear((subtotal + iva), 2);
		} else {
			cargarTotales();
		}
	}

	public void cargarTotales() {
		this.subtotal = 0.0;
		this.iva = 0.0;
		this.total = 0.0;
		this.descuento = 0.0;
	}

	@Command
	@NotifyChange({ "btGuardarEditar", "listaNotaCompra", "notaCompraCabecera", "total", "subtotal", "iva", "descuento",
			"estadoGuardar", "estadoModificar" })
	public void eliminar(@BindingParam("item") Modelos<NotaCompraDetalle, Object> notaCompra) {
		if (estadoModificar) {
			listaNotaCompraDetalleRemovidos.add(notaCompra.getObjetoT());
		}
		listaNotaCompra.remove(notaCompra);
		if (listaNotaCompra.size() == 0 || listaNotaCompra == null) {
			nuevo();
		}
		calcularListatotal();
	}

	@Command
	public void verPrecio(@BindingParam("item") Modelos<NotaCompraDetalle, Object> notaCompra) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("clase", "NotaCompra");
		// List<Proveedor> listaProveedor =
		// InstanciaDAO.getInstanciaProveedorDAO().getProveedores(Sistema.ESTADO_INGRESADO);
		List<Proveedor> listaProveedor = new ArrayList<>();
		cuadreInventarioDAO
				.getListaCuadreInventarioByProductoAndConceptoMovmiento(notaCompra.getObjetoT().getProducto().getId(),
						Sistema.ID_COMPRA)
				.stream().map(this::tranformarAProveedor).forEach(listaProveedor::add);
		parameters.put("lista", listaProveedor);

		Executions.createComponents("/view/formProveedor.zul", null, parameters);
	}

	private Proveedor tranformarAProveedor(CuadreInventario x) {
		final CompraDetalle compraDetalle = compraDetalleDAO.getCompraDetallesById(x.getIdDetalle());
		final Proveedor proveedor = compraDetalle.getCompraCabecera().getProveedor();
		proveedor.setDescuentoValor(compraDetalle.getDescuentoValor());
		proveedor.setDescuentoPorcentaje(compraDetalle.getDescuentoPorcentaje());
		proveedor.setCosto(compraDetalle.getPrecioCompra());
		/*
		 * x.getProveedor().setDescuentoValor(x.getDescuentoValor());
		 * x.getProveedor().setDescuentoPorcentaje(x.getDescuentoPorcentaje());
		 * x.getProveedor().setCosto(x.getPrecioCosto());
		 */
		return compraDetalle.getCompraCabecera().getProveedor();
	}

	@Command
	@NotifyChange({ "btGuardarEditar", "listaNotaCompra", "notaCompraCabecera", "total", "subtotal", "iva", "descuento",
			"estadoGuardar", "estadoModificar" })
	public void guardar() {
		if ((notaCompraCabecera.getProveedor().getId() == null)) {
			Messagebox.show("Debe seleccionar un Proveedor", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}
		if (listaNotaCompra.isEmpty() == true) {
			Messagebox.show("Debe seleccionar Al menos un Producto", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}
		if (estadoModificar) {
			for (NotaCompraDetalle notaCompraDetalle : listaNotaCompraDetalleRemovidos) {
				notaCompraDetalle.setEstado(new Estado(Sistema.ESTADO_ELIMINADO));
				notaCompraDetalleDAO.saveorUpdate(notaCompraDetalle);
			}
		}
		notaCompraCabecera.setIva(iva);
		notaCompraCabecera.setSubtotal(subtotal);
		notaCompraCabecera.setTotal(total);
		notaCompraCabecera.setDescuento(descuento);
		notaCompraCabecera.setEstado(new Estado(Sistema.ESTADO_INGRESADO));
		notaCompraCabecera.setUsuario(Sistema.getUsuario());
		notaCompraCabecera.setFechaEmision(new Timestamp(new Date().getTime()));

		NotaCompraCabecera notaCompraCabeceraReferencia = notaCompraCabeceraDAO.saveorUpdate(notaCompraCabecera);
		listaNotaCompra.remove(0);
		listaNotaCompra = notaCompraCabeceraDAO.saveorUpdateDetalle(listaNotaCompra, notaCompraCabeceraReferencia);

		Clients.showNotification("Ingresado Satisfactorio ", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center",
				1500);
		estadoGuardar = true;
		notaCompraCabecera = notaCompraCabeceraReferencia;
		nuevo();
	}

	@Command
	@NotifyChange({ "btGuardarEditar", "listaNotaCompra", "notaCompraCabecera", "total", "subtotal", "iva", "descuento",
			"estadoGuardar", "estadoModificar", "estadoAnular" })
	public void nuevo() {
		listaNotaCompra = new ArrayList<>();
		notaCompraCabecera = new NotaCompraCabecera();
		notaCompraCabecera.setProveedor(new Proveedor());
		notaCompraCabecera.setFechaEmision(new Timestamp(new Date().getTime()));
		estadoModificar = false;
		estadoGuardar = true;
		estadoAnular = true;
		cargarTotales();
		listaNotaCompra.add(new Modelos<NotaCompraDetalle, Object>(getNotaCompraDetalleNuevo(), null, true));
	}

	private NotaCompraDetalle getNotaCompraDetalleNuevo() {
		NotaCompraDetalle notaCompraDetalle = new NotaCompraDetalle();
		notaCompraDetalle.setCantidad(0);
		notaCompraDetalle.setProducto(new Producto());
		return notaCompraDetalle;
	}

	@Command
	public void generarPdf() {
		if (this.listaNotaCompra.isEmpty()) {
			Messagebox.show("No existen registros en el Detalle");
			return;
		}
		try {
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("notaCompraCabecera", notaCompraCabecera);
			parametros.put("listaNotaCompra", listaNotaCompra);
			Executions.createComponents("/reportes/impresionNotaCompra.zul", null, parametros);
		} catch (Exception e) {
			System.out.println("Que anda mal");
			Messagebox.show("Falta datos en la compra");
		}
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {

		iframe.setContent(Sistema.reportePDF("facturacion.jasper", Sistema.getMapReport(
				notaCompraCabecera.getProveedor().getNombre(), notaCompraCabecera.getProveedor().getRuc(),
				notaCompraCabecera.getProveedor().getDireccion(), notaCompraCabecera.getProveedor().getTelefono(),
				Sistema.getListaInformacionNegocio().get(0).getNombre(),
				Sistema.getListaInformacionNegocio().get(0).getRuc(), notaCompraCabecera.getFechaEmision(),
				notaCompraCabecera.getUsuario().getNombre(), Sistema.redondear(notaCompraCabecera.getSubtotal()),
				Sistema.redondear(notaCompraCabecera.getIva()), 0.0, 0.0,
				Sistema.redondear(notaCompraCabecera.getTotal()), notaCompraCabecera.getEstado()),
				Sistema.getNotaCompraDataSource(listaNotaCompra)));
	}

	@Command
	@NotifyChange({ "btGuardarEditar", "listaNotaCompra", "notaCompraCabecera", "total", "subtotal", "iva", "descuento",
			"estadoGuardar", "estadoModificar", "estadoAnular" })
	public void anular() {

		String msj = "Esta seguro(a) que desea anular la Nota de Compra \"" + notaCompraCabecera.getCodigo()
				+ "\" esta accion no podra deshacerse si se confirma";

		Messagebox.show(msj, "Advertencia", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						notaCompraCabecera.setEstado(new Estado(Sistema.ESTADO_ANULADO));
						notaCompraCabecera.setUsuario(Sistema.getUsuario());
						notaCompraCabeceraDAO.saveorUpdate(notaCompraCabecera);
						estadoAnular = true;
						nuevo();
						Clients.showNotification("El registro ha sido anulado", Clients.NOTIFICATION_TYPE_INFO, null,
								"middle_center", 1500);
						BindUtils.postNotifyChange(null, null, ClassNotaCompra.this, "estadoAnular");
					}
				});
	}

	@GlobalCommand
	@NotifyChange({ "btGuardarEditar", "total", "subtotal", "descuento", "iva", "descuento", "notaCompraCabecera",
			"listaNotaCompra", "estadoGuardar", "estadoModificar", "estadoAnular" })
	public void cargarNotaCompra(@BindingParam("objetoNotaCompra") NotaCompraCabecera notaCompraCabecera) {
		this.notaCompraCabecera = notaCompraCabecera;
		total = this.notaCompraCabecera.getTotal();
		subtotal = this.notaCompraCabecera.getSubtotal();
		iva = this.notaCompraCabecera.getIva();
		descuento = this.notaCompraCabecera.getDescuento();
		listaNotaCompra.clear();

		notaCompraDetalleDAO.getNotaCompraDetallesByCabecera(notaCompraCabecera.getId(), Sistema.ESTADO_INGRESADO)
				.forEach(nota -> listaNotaCompra.add(new Modelos<NotaCompraDetalle, Object>(nota, null, true)));
		estadoModificar = true;
		estadoGuardar = true;
		estadoAnular = false;
		// calcularListatotal();
	}

	@Command
	public void buscar() {
		Executions.createComponents("/view/formNotaCompraLista.zul", null, null);
	}

	/*
	 * Metodos GET AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public ParametroZul getParametroZul() {
		return ParametroZul.getInstancia();
	}

	public NotaCompraCabecera getNotaCompraCabecera() {
		return notaCompraCabecera;
	}

	public void setNotaCompraCabecera(NotaCompraCabecera notaCompraCabecera) {
		this.notaCompraCabecera = notaCompraCabecera;
	}

	public boolean isEstadoGuardar() {
		return estadoGuardar;
	}

	public void setEstadoGuardar(boolean estadoGuardar) {
		this.estadoGuardar = estadoGuardar;
	}

	public List<Modelos<NotaCompraDetalle, Object>> getListaNotaCompra() {
		return listaNotaCompra;
	}

	public void setListaNotaCompra(List<Modelos<NotaCompraDetalle, Object>> listaNotaCompra) {
		this.listaNotaCompra = listaNotaCompra;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public Double getIva() {
		return iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public boolean isEstadoModificar() {
		return estadoModificar;
	}

	public void setEstadoModificar(boolean estadoModificar) {
		this.estadoModificar = estadoModificar;
	}

	public boolean isEstadoAnular() {
		return estadoAnular;
	}

	public void setEstadoAnular(boolean estadoAnular) {
		this.estadoAnular = estadoAnular;
	}

	public Double getDescuento() {
		return descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

}
