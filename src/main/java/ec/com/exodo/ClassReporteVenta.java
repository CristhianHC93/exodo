package ec.com.exodo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.dao.ie.SucursalDAO;
import ec.com.exodo.dao.ie.VentaCabeceraDAO;
import ec.com.exodo.entity.Sucursal;
import ec.com.exodo.entity.VentaCabecera;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.ParametroZul;
import ec.com.exodo.sistema.Sistema;

public class ClassReporteVenta {

	private double total;
	private double subTotal;
	private double totalIva;
	private double totalDescuento;

	private Date fechaInicial = new Date();
	private Date fechaFinal = new Date();

	private VentaCabecera ventaCabecera;
	private List<VentaCabecera> listaVentaCabecera = new ArrayList<>();
	private List<VentaCabecera> listaVentaCabeceraProvicional;

	private VentaCabeceraDAO VentaCabeceraDAO = InstanciaDAO.getInstanciaVentaCabeceraDAO();
	private SucursalDAO sucursalDAO = InstanciaDAO.getInstanciaSucursalDAO();

	private String footer = String.format(ParametroZul.getInstancia().getFooterMessage(), listaVentaCabecera.size());

	private String tipo = "entregarVenta";

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("lista") List<VentaCabecera> lista) {
		Selectors.wireComponents(view, this, false);
		if (lista != null) {
			listaVentaCabecera = new ArrayList<>(lista.size());
			listaVentaCabecera.addAll(lista);
		}
	}

	@SuppressWarnings("deprecation")
	@NotifyChange({ "listaVentaCabecera", "total", "subTotal", "totalIva", "totalDescuento", "footer" })
	@Command
	public void calcularReporte() {
		fechaInicial.setHours(0);
		fechaFinal.setHours(24);
		switch (tipo) {
		case "ordenVenta":
			listaVentaCabeceraProvicional = VentaCabeceraDAO.getListaVentaCabeceraByFecha(Sistema.ESTADO_ORDEN_VENTA,
					fechaInicial, fechaFinal);
			break;
		case "EntregarVenta":
			listaVentaCabeceraProvicional = VentaCabeceraDAO
					.getListaVentaCabeceraByFecha(Sistema.ESTADO_ENTREGADO_VENTA, fechaInicial, fechaFinal);
			break;
		case "anularVenta":
			listaVentaCabeceraProvicional = VentaCabeceraDAO.getListaVentaCabeceraByFecha(Sistema.ESTADO_ANULADO,
					fechaInicial, fechaFinal);
			break;
		case "planVenta":
			listaVentaCabeceraProvicional = VentaCabeceraDAO.getListaVentaCabeceraByFecha(Sistema.ESTADO_PLAN_VENTA,
					fechaInicial, fechaFinal);
			break;
		default:
			listaVentaCabeceraProvicional = VentaCabeceraDAO
					.getListaVentaCabeceraByFecha(Sistema.ESTADO_ENTREGADO_VENTA, fechaInicial, fechaFinal);
			break;
		}

		listaVentaCabecera = new ArrayList<>(listaVentaCabeceraProvicional.size());
		listaVentaCabecera.addAll(listaVentaCabeceraProvicional);
		double total = 0.00;
		double subTotal = 0.00;
		double totalIva = 0.00;
		double totalDescuento = 0.0;
		for (VentaCabecera v : listaVentaCabecera) {
			total += v.getTotal();
			subTotal += v.getSubtotal();
			totalIva += v.getIva();
			totalDescuento += v.getDescuentoValor();
		}
		this.total = Sistema.redondear(total);
		this.subTotal = Sistema.redondear(subTotal);
		this.totalIva = Sistema.redondear(totalIva);
		this.totalDescuento = Sistema.redondear(totalDescuento);
		footer = String.format(ParametroZul.getInstancia().getFooterMessage(), listaVentaCabecera.size());
	}

	@Command
	public void generarExcel() {
		if (listaVentaCabecera.size() == 0) {
			Messagebox.show("No hay ventas para el reporte");
			return;
		}
		Sucursal sucursal = sucursalDAO.getSucursal(1);
		if (sucursal != null) {
			Map<String, Object> mapreporte = new HashMap<String, Object>();
			mapreporte.put("empresa", sucursal.getInformacionNegocio().getNombre());
			mapreporte.put("direccion", sucursal.getInformacionNegocio().getDireccion());
			mapreporte.put("telefono", sucursal.getInformacionNegocio().getTelefono());
			mapreporte.put("usuario", Sistema.getUsuario().getNombre());
			Sistema.reporteEXCEL(Sistema.reporteVentaJasper, "Venta", mapreporte,
					Sistema.getReporteVentaDataSource(listaVentaCabecera));
		}
	}

	@Command
	public void generarPdf() {
		if (listaVentaCabecera.size() == 0) {
			Messagebox.show("No hay ventas para el reporte");
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("lista", listaVentaCabecera);
		Executions.createComponents("/reportes/impresionReporteVenta.zul", null, parametros);
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("empresa",
				listaVentaCabecera.get(0).getAreaSucursal().getSucursal().getInformacionNegocio().getNombre());
		mapreporte.put("direccion",
				listaVentaCabecera.get(0).getAreaSucursal().getSucursal().getInformacionNegocio().getDireccion());
		mapreporte.put("telefono",
				listaVentaCabecera.get(0).getAreaSucursal().getSucursal().getInformacionNegocio().getTelefono());
		mapreporte.put("usuario", Sistema.getUsuario().getNombre());
		iframe.setContent(Sistema.reportePDF(Sistema.reporteVentaJasper, mapreporte,
				Sistema.getReporteVentaDataSource(listaVentaCabecera)));
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public VentaCabecera getVentaCabecera() {
		return ventaCabecera;
	}

	public void setVentaCabecera(VentaCabecera ventaCabecera) {
		this.ventaCabecera = ventaCabecera;
	}

	public List<VentaCabecera> getListaVentaCabecera() {
		return listaVentaCabecera;
	}

	public void setListaVentaCabecera(List<VentaCabecera> listaVentaCabecera) {
		this.listaVentaCabecera = listaVentaCabecera;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public double getTotalIva() {
		return totalIva;
	}

	public void setTotalIva(double totalIva) {
		this.totalIva = totalIva;
	}

	public double getTotalDescuento() {
		return totalDescuento;
	}

	public void setTotalDescuento(double totalDescuento) {
		this.totalDescuento = totalDescuento;
	}

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public String getFooter() {
		return footer;
	}

	public List<VentaCabecera> getListaVentaCabeceraProvicional() {
		return listaVentaCabeceraProvicional;
	}

	public void setListaVentaCabeceraProvicional(List<VentaCabecera> listaVentaCabeceraProvicional) {
		this.listaVentaCabeceraProvicional = listaVentaCabeceraProvicional;
	}

}
