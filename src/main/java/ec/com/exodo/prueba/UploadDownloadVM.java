package ec.com.exodo.prueba;

import java.io.FileNotFoundException;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;

public class UploadDownloadVM {
 private Media media;

 @NotifyChange("media")
 @Command
 public void uploadFile(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) {
  media = event.getMedia();
  if(Util.uploadFile(media))
   Messagebox.show(Labels.getLabel("app.successfull"));
  else
   Messagebox.show(Labels.getLabel("app.error"));
 }
 
 public Media getMedia(){
  return media;
 }

 @Command
 public void downloadFile(){
  if(media != null)
	try {
		Filedownload.save("/uploads/a.png",null);
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
 }
}