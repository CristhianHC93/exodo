package ec.com.exodo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.exodo.dao.ie.ClienteDAO;
import ec.com.exodo.entity.Cliente;
import ec.com.exodo.entity.Estado;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassCliente {
	@Wire("#winCliente")
	private Window winCliente;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;

	private List<Cliente> listaCliente;
	private ClienteDAO clienteDAO = InstanciaDAO.getInstanciaClienteDAO();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(view, this, false);
		ajustarPantalla(clase);
		cargarDatos();
	}

	private void ajustarPantalla(String clase) {
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (clase.equals("Venta")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "80%";
		}
	}

	private void cargarDatos() {
		if (listaCliente == null) {
			listaCliente = new ArrayList<>();
		}
		listaCliente.clear();
		listaCliente = clienteDAO.getListaClienteByEstado(Sistema.ESTADO_INGRESADO);
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/view/formClienteCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaCliente")
	public void deleteCliente(@BindingParam("cliente") final Cliente cliente) {
		if (cliente.getId() == 1)
			Messagebox.show("Imposible eliminar al cliente" + cliente.getNombre() + ".");
		else {
			Messagebox.show(getIdioma().getMensajeEliminar(cliente.getNombre()), "Confirm",
					Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, event -> {
						if (((Integer) event.getData()).intValue() == Messagebox.OK) {
							cliente.setEstado(new Estado(Sistema.ESTADO_ELIMINADO));
							procesoEliminar(clienteDAO.saveorUpdate(cliente));
						}
					});
		}
	}

	private void procesoEliminar(final Cliente cliente) {
		if (cliente != null) {
			Clients.showNotification(getIdioma().getRegistroEliminadoSatifactoriamente(),
					Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
			BindUtils.postNotifyChange(null, null, this, "listaCliente");
			cargarDatos();
		} else {
			Clients.showNotification(getIdioma().getImposibleEliminarRegistro(), Clients.NOTIFICATION_TYPE_ERROR, null,
					"top_center", 2000);
		}
	}

	@Command
	public void aceptar(@BindingParam("cliente") Cliente cliente) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objetoCliente", cliente);
		BindUtils.postGlobalCommand(null, null, "cargarCliente", parametros);
		salir();
	}

	@Command
	@NotifyChange("cliente")
	public void editar(@BindingParam("cliente") Cliente cliente) {
		if (cliente.getId() == 1) {
			Messagebox.show("Imposible editar al cliente" + cliente.getNombre() + ".");
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", cliente);
		Executions.createComponents("/view/formClienteCE.zul", null, parametros);
	}

	@Command
	@NotifyChange("listaCliente")
	public void filtrar() {
		listaCliente.clear();
		listaCliente
				.addAll((datoBusqueda.trim().equals("")) ? clienteDAO.getListaClienteByEstado(Sistema.ESTADO_INGRESADO)
						: clienteDAO.getFiltroCliente(datoBusqueda));
	}

	@GlobalCommand
	@NotifyChange("listaCliente")
	public void refrescarlista() {
		listaCliente = clienteDAO.getListaClienteByEstado(Sistema.ESTADO_INGRESADO);
	}

	@Command
	public void salir() {
		winCliente.detach();
	}

	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public List<Cliente> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(List<Cliente> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}
}
