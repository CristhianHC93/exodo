package ec.com.exodo;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.exodo.entity.Usuario;
import ec.com.exodo.factory.InstanciaDAO;
import ec.com.exodo.idioma.Idioma;
import ec.com.exodo.sistema.Sistema;

public class ClassPerfil {
	private Usuario usuario;
	private String password;
	private String passwordRepetir;
	private String claveRepetida;
	boolean validarRepetido = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		usuario = (Usuario) Sessions.getCurrent().getAttribute("Usuario");
		desencriptar(usuario);
	}

	@Command
	@NotifyChange({ "claveRepetida", "password", "passwordRepetir", "usuario" })
	public void guardarEditar() {
		if (Sistema.validarRepetido(InstanciaDAO.getInstanciaUsuarioDAO().getListaUsuario(), usuario)) {
			return;
		}
		validarPassword();
		if (!validarRepetido) {
			Messagebox.show("Claves no coinciden");
			return;
		}
		usuario.setPassword(Sistema.Encriptar(password));
		InstanciaDAO.getInstanciaUsuarioDAO().saveorUpdate(usuario);
		Clients.showNotification("Usuario Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
				"middle_center", 1500);
	}

	@Command
	@NotifyChange({ "password", "passwordRepetir" })
	public void desencriptar(Usuario usuario) {
		try {
			password = Sistema.desencriptar(usuario.getPassword());
			passwordRepetir = password;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@NotifyChange("claveRepetida")
	@Command
	public void validarPassword() {
		if (passwordRepetir != null && password != null && !passwordRepetir.trim().equals("")
				&& !password.trim().equals("")) {
			if (!password.trim().equals(passwordRepetir.trim())) {
				validarRepetido = false;
				claveRepetida = "Claves no coinciden";
			} else {
				validarRepetido = true;
				claveRepetida = "";
			}
		}
	}

	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.getIdioma();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getPasswordRepetir() {
		return passwordRepetir;
	}

	public void setPasswordRepetir(String passwordRepetir) {
		this.passwordRepetir = passwordRepetir;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClaveRepetida() {
		return claveRepetida;
	}

	public void setClaveRepetida(String claveRepetida) {
		this.claveRepetida = claveRepetida;
	}

}
