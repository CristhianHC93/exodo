package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "compra_detalle" database table.
 * 
 */
@Entity
@Table(name="\"compra_detalle\"")
@NamedQuery(name="CompraDetalle.findAll", query="SELECT c FROM CompraDetalle c")
public class CompraDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"cantidad\"")
	private double cantidad;

	@Column(name="\"id_compra_cabecera\"")
	private int idCompraCabecera;

	@Column(name="\"id_producto\"")
	private int idProducto;

	@Column(name="\"precio_compra\"")
	private double precioCompra;

	@Column(name="\"subtotal\"")
	private double subtotal;

	//bi-directional many-to-one association to CompraCabecera
	@ManyToOne
	@JoinColumns({
		})
	private CompraCabecera compraCabecera;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumns({
		})
	private Producto producto;

	public CompraDetalle() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public int getIdCompraCabecera() {
		return this.idCompraCabecera;
	}

	public void setIdCompraCabecera(int idCompraCabecera) {
		this.idCompraCabecera = idCompraCabecera;
	}

	public int getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public double getPrecioCompra() {
		return this.precioCompra;
	}

	public void setPrecioCompra(double precioCompra) {
		this.precioCompra = precioCompra;
	}

	public double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public CompraCabecera getCompraCabecera() {
		return this.compraCabecera;
	}

	public void setCompraCabecera(CompraCabecera compraCabecera) {
		this.compraCabecera = compraCabecera;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}