package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "producto_proveedor" database table.
 * 
 */
@Entity
@Table(name="\"producto_proveedor\"")
@NamedQuery(name="ProductoProveedor.findAll", query="SELECT p FROM ProductoProveedor p")
public class ProductoProveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"id_producto\"")
	private int idProducto;

	@Column(name="\"id_proveedor\"")
	private int idProveedor;

	@Column(name="\"precio_costo\"")
	private double precioCosto;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumns({
		})
	private Producto producto;

	//bi-directional many-to-one association to Proveedor
	@ManyToOne
	@JoinColumns({
		})
	private Proveedor proveedor;

	public ProductoProveedor() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public int getIdProveedor() {
		return this.idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public double getPrecioCosto() {
		return this.precioCosto;
	}

	public void setPrecioCosto(double precioCosto) {
		this.precioCosto = precioCosto;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

}