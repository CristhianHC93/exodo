package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the "compra_cabecera" database table.
 * 
 */
@Entity
@Table(name = "\"compra_cabecera\"")
@NamedQuery(name = "CompraCabecera.findAll", query = "SELECT c FROM CompraCabecera c")
public class CompraCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private int id;

	@Column(name = "\"codigo\"")
	private String codigo;

	@Column(name = "\"fecha_recepcion\"")
	private Timestamp fechaRecepcion;

	@Column(name = "\"iva\"")
	private double iva;

	@Column(name = "\"subtotal\"")
	private double subtotal;

	@Column(name = "\"total\"")
	private double total;

	// bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name = "\"id_estado\"")
	private Estado estado;

	// bi-directional many-to-one association to Proveedor
	@ManyToOne
	@JoinColumn(name = "\"id_proveedor\"")
	private Proveedor proveedor;

	// bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name = "\"id_usuario\"")
	private Usuario usuario;

	// bi-directional many-to-one association to CompraDetalle
	@OneToMany(mappedBy = "compraCabecera", fetch = FetchType.EAGER)
	private List<CompraDetalle> compraDetalles;

	// bi-directional many-to-one association to DevolucionCompra
	@OneToMany(mappedBy = "compraCabecera", fetch = FetchType.EAGER)
	private List<DevolucionCompra> devolucionCompras;

	public CompraCabecera() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Timestamp getFechaRecepcion() {
		return this.fechaRecepcion;
	}

	public void setFechaRecepcion(Timestamp fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	public double getIva() {
		return this.iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getTotal() {
		return this.total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<CompraDetalle> getCompraDetalles() {
		return this.compraDetalles;
	}

	public void setCompraDetalles(List<CompraDetalle> compraDetalles) {
		this.compraDetalles = compraDetalles;
	}

	public CompraDetalle addCompraDetalle(CompraDetalle compraDetalle) {
		getCompraDetalles().add(compraDetalle);
		compraDetalle.setCompraCabecera(this);

		return compraDetalle;
	}

	public CompraDetalle removeCompraDetalle(CompraDetalle compraDetalle) {
		getCompraDetalles().remove(compraDetalle);
		compraDetalle.setCompraCabecera(null);

		return compraDetalle;
	}

	public List<DevolucionCompra> getDevolucionCompras() {
		return this.devolucionCompras;
	}

	public void setDevolucionCompras(List<DevolucionCompra> devolucionCompras) {
		this.devolucionCompras = devolucionCompras;
	}

	public DevolucionCompra addDevolucionCompra(DevolucionCompra devolucionCompra) {
		getDevolucionCompras().add(devolucionCompra);
		devolucionCompra.setCompraCabecera(this);

		return devolucionCompra;
	}

	public DevolucionCompra removeDevolucionCompra(DevolucionCompra devolucionCompra) {
		getDevolucionCompras().remove(devolucionCompra);
		devolucionCompra.setCompraCabecera(null);

		return devolucionCompra;
	}

}