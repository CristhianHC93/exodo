package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the "cuadre_inventario" database table.
 * 
 */
@Entity
@Table(name="\"cuadre_inventario\"")
@NamedQuery(name="CuadreInventario.findAll", query="SELECT c FROM CuadreInventario c")
public class CuadreInventario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"id_cuadre_inventario_cabecera\"")
	private int idCuadreInventarioCabecera;

	@Column(name="\"id_producto\"")
	private int idProducto;

	@Column(name="\"observacion\"")
	private String observacion;

	@Column(name="\"stock\"")
	private double stock;

	@Column(name="\"stock_diferencia\"")
	private double stockDiferencia;

	@Column(name="\"stock_inicial\"")
	private double stockInicial;

	@Column(name="\"stock_real\"")
	private double stockReal;

	@Column(name="\"total_compra\"")
	private double totalCompra;

	@Column(name="\"total_compra_anulada\"")
	private double totalCompraAnulada;

	@Column(name="\"total_traspaso_entrada\"")
	private double totalTraspasoEntrada;

	@Column(name="\"total_traspaso_salida\"")
	private double totalTraspasoSalida;

	@Column(name="\"total_venta\"")
	private double totalVenta;

	@Column(name="\"total_venta_anulada\"")
	private double totalVentaAnulada;

	@Column(name="\"valor_compra\"")
	private double valorCompra;

	@Column(name="\"valor_venta\"")
	private double valorVenta;

	//bi-directional many-to-one association to CuadreInventarioCabecera
	@ManyToOne
	@JoinColumns({
		})
	private CuadreInventarioCabecera cuadreInventarioCabecera;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumns({
		})
	private Producto producto;

	//bi-directional many-to-one association to Kardex
	@OneToMany(mappedBy="cuadreInventario", fetch=FetchType.EAGER)
	private List<Kardex> kardexs;

	public CuadreInventario() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdCuadreInventarioCabecera() {
		return this.idCuadreInventarioCabecera;
	}

	public void setIdCuadreInventarioCabecera(int idCuadreInventarioCabecera) {
		this.idCuadreInventarioCabecera = idCuadreInventarioCabecera;
	}

	public int getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public double getStock() {
		return this.stock;
	}

	public void setStock(double stock) {
		this.stock = stock;
	}

	public double getStockDiferencia() {
		return this.stockDiferencia;
	}

	public void setStockDiferencia(double stockDiferencia) {
		this.stockDiferencia = stockDiferencia;
	}

	public double getStockInicial() {
		return this.stockInicial;
	}

	public void setStockInicial(double stockInicial) {
		this.stockInicial = stockInicial;
	}

	public double getStockReal() {
		return this.stockReal;
	}

	public void setStockReal(double stockReal) {
		this.stockReal = stockReal;
	}

	public double getTotalCompra() {
		return this.totalCompra;
	}

	public void setTotalCompra(double totalCompra) {
		this.totalCompra = totalCompra;
	}

	public double getTotalCompraAnulada() {
		return this.totalCompraAnulada;
	}

	public void setTotalCompraAnulada(double totalCompraAnulada) {
		this.totalCompraAnulada = totalCompraAnulada;
	}

	public double getTotalTraspasoEntrada() {
		return this.totalTraspasoEntrada;
	}

	public void setTotalTraspasoEntrada(double totalTraspasoEntrada) {
		this.totalTraspasoEntrada = totalTraspasoEntrada;
	}

	public double getTotalTraspasoSalida() {
		return this.totalTraspasoSalida;
	}

	public void setTotalTraspasoSalida(double totalTraspasoSalida) {
		this.totalTraspasoSalida = totalTraspasoSalida;
	}

	public double getTotalVenta() {
		return this.totalVenta;
	}

	public void setTotalVenta(double totalVenta) {
		this.totalVenta = totalVenta;
	}

	public double getTotalVentaAnulada() {
		return this.totalVentaAnulada;
	}

	public void setTotalVentaAnulada(double totalVentaAnulada) {
		this.totalVentaAnulada = totalVentaAnulada;
	}

	public double getValorCompra() {
		return this.valorCompra;
	}

	public void setValorCompra(double valorCompra) {
		this.valorCompra = valorCompra;
	}

	public double getValorVenta() {
		return this.valorVenta;
	}

	public void setValorVenta(double valorVenta) {
		this.valorVenta = valorVenta;
	}

	public CuadreInventarioCabecera getCuadreInventarioCabecera() {
		return this.cuadreInventarioCabecera;
	}

	public void setCuadreInventarioCabecera(CuadreInventarioCabecera cuadreInventarioCabecera) {
		this.cuadreInventarioCabecera = cuadreInventarioCabecera;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public List<Kardex> getKardexs() {
		return this.kardexs;
	}

	public void setKardexs(List<Kardex> kardexs) {
		this.kardexs = kardexs;
	}

	public Kardex addKardex(Kardex kardex) {
		getKardexs().add(kardex);
		kardex.setCuadreInventario(this);

		return kardex;
	}

	public Kardex removeKardex(Kardex kardex) {
		getKardexs().remove(kardex);
		kardex.setCuadreInventario(null);

		return kardex;
	}

}