package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the "nota_compra_cabecera" database table.
 * 
 */
@Entity
@Table(name = "\"nota_compra_cabecera\"")
@NamedQuery(name = "NotaCompraCabecera.findAll", query = "SELECT n FROM NotaCompraCabecera n")
public class NotaCompraCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private int id;

	@Column(name = "\"codigo\"")
	private String codigo;

	@Column(name = "\"fecha_emision\"")
	private Timestamp fechaEmision;

	@Column(name = "\"hora_emision\"")
	private Time horaEmision;

	@Column(name = "\"iva\"")
	private double iva;

	@Column(name = "\"subtotal\"")
	private double subtotal;

	@Column(name = "\"total\"")
	private double total;

	// bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name = "\"id_estado\"")
	private Estado estado;

	// bi-directional many-to-one association to Proveedor
	@ManyToOne
	@JoinColumn(name = "\"id_proveedor\"")
	private Proveedor proveedor;

	// bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name = "\"id_usuario\"")
	private Usuario usuario;

	// bi-directional many-to-one association to NotaCompraDetalle
	@OneToMany(mappedBy = "notaCompraCabecera", fetch = FetchType.EAGER)
	private List<NotaCompraDetalle> notaCompraDetalles;

	public NotaCompraCabecera() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Timestamp getFechaEmision() {
		return this.fechaEmision;
	}

	public void setFechaEmision(Timestamp fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Time getHoraEmision() {
		return this.horaEmision;
	}

	public void setHoraEmision(Time horaEmision) {
		this.horaEmision = horaEmision;
	}

	public double getIva() {
		return this.iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getTotal() {
		return this.total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<NotaCompraDetalle> getNotaCompraDetalles() {
		return this.notaCompraDetalles;
	}

	public void setNotaCompraDetalles(List<NotaCompraDetalle> notaCompraDetalles) {
		this.notaCompraDetalles = notaCompraDetalles;
	}

	public NotaCompraDetalle addNotaCompraDetalle(NotaCompraDetalle notaCompraDetalle) {
		getNotaCompraDetalles().add(notaCompraDetalle);
		notaCompraDetalle.setNotaCompraCabecera(this);

		return notaCompraDetalle;
	}

	public NotaCompraDetalle removeNotaCompraDetalle(NotaCompraDetalle notaCompraDetalle) {
		getNotaCompraDetalles().remove(notaCompraDetalle);
		notaCompraDetalle.setNotaCompraCabecera(null);

		return notaCompraDetalle;
	}

}