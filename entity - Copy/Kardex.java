package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the "kardex" database table.
 * 
 */
@Entity
@Table(name="\"kardex\"")
@NamedQuery(name="Kardex.findAll", query="SELECT k FROM Kardex k")
public class Kardex implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"cantidad\"")
	private double cantidad;

	@Column(name="\"cantidad_actual\"")
	private double cantidadActual;

	@Column(name="\"cantidad_resultante\"")
	private double cantidadResultante;

	@Column(name="\"fecha\"")
	private Timestamp fecha;

	@Column(name="\"id_concepto_movimiento\"")
	private int idConceptoMovimiento;

	@Column(name="\"id_cuadre_inventario\"")
	private int idCuadreInventario;

	@Column(name="\"observacion\"")
	private String observacion;

	//bi-directional many-to-one association to ConceptoMovimiento
	@ManyToOne
	@JoinColumns({
		})
	private ConceptoMovimiento conceptoMovimiento;

	//bi-directional many-to-one association to CuadreInventario
	@ManyToOne
	@JoinColumns({
		})
	private CuadreInventario cuadreInventario;

	public Kardex() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public double getCantidadActual() {
		return this.cantidadActual;
	}

	public void setCantidadActual(double cantidadActual) {
		this.cantidadActual = cantidadActual;
	}

	public double getCantidadResultante() {
		return this.cantidadResultante;
	}

	public void setCantidadResultante(double cantidadResultante) {
		this.cantidadResultante = cantidadResultante;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public int getIdConceptoMovimiento() {
		return this.idConceptoMovimiento;
	}

	public void setIdConceptoMovimiento(int idConceptoMovimiento) {
		this.idConceptoMovimiento = idConceptoMovimiento;
	}

	public int getIdCuadreInventario() {
		return this.idCuadreInventario;
	}

	public void setIdCuadreInventario(int idCuadreInventario) {
		this.idCuadreInventario = idCuadreInventario;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public ConceptoMovimiento getConceptoMovimiento() {
		return this.conceptoMovimiento;
	}

	public void setConceptoMovimiento(ConceptoMovimiento conceptoMovimiento) {
		this.conceptoMovimiento = conceptoMovimiento;
	}

	public CuadreInventario getCuadreInventario() {
		return this.cuadreInventario;
	}

	public void setCuadreInventario(CuadreInventario cuadreInventario) {
		this.cuadreInventario = cuadreInventario;
	}

}