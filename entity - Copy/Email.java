package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the "email" database table.
 * 
 */
@Entity
@Table(name="\"email\"")
@NamedQuery(name="Email.findAll", query="SELECT e FROM Email e")
public class Email implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"clave\"")
	private String clave;

	@Column(name="\"correo\"")
	private String correo;

	@Column(name="\"host\"")
	private String host;

	@Column(name="\"puerto\"")
	private String puerto;

	//bi-directional many-to-one association to InformacionNegocio
	@OneToMany(mappedBy="email", fetch=FetchType.EAGER)
	private List<InformacionNegocio> informacionNegocios;

	public Email() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClave() {
		return this.clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPuerto() {
		return this.puerto;
	}

	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}

	public List<InformacionNegocio> getInformacionNegocios() {
		return this.informacionNegocios;
	}

	public void setInformacionNegocios(List<InformacionNegocio> informacionNegocios) {
		this.informacionNegocios = informacionNegocios;
	}

	public InformacionNegocio addInformacionNegocio(InformacionNegocio informacionNegocio) {
		getInformacionNegocios().add(informacionNegocio);
		informacionNegocio.setEmail(this);

		return informacionNegocio;
	}

	public InformacionNegocio removeInformacionNegocio(InformacionNegocio informacionNegocio) {
		getInformacionNegocios().remove(informacionNegocio);
		informacionNegocio.setEmail(null);

		return informacionNegocio;
	}

}