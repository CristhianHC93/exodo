package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "devolucion_compra" database table.
 * 
 */
@Entity
@Table(name="\"devolucion_compra\"")
@NamedQuery(name="DevolucionCompra.findAll", query="SELECT d FROM DevolucionCompra d")
public class DevolucionCompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"cantidad\"")
	private double cantidad;

	@Column(name="\"id_compra_cabecera\"")
	private int idCompraCabecera;

	@Column(name="\"id_producto\"")
	private int idProducto;

	@Column(name="\"id_usuario\"")
	private int idUsuario;

	@Column(name="\"iva\"")
	private double iva;

	@Column(name="\"observacion\"")
	private String observacion;

	@Column(name="\"precio_compra\"")
	private double precioCompra;

	@Column(name="\"subtotal\"")
	private double subtotal;

	@Column(name="\"total\"")
	private double total;

	//bi-directional many-to-one association to CompraCabecera
	@ManyToOne
	@JoinColumns({
		})
	private CompraCabecera compraCabecera;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumns({
		})
	private Producto producto;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumns({
		})
	private Usuario usuario;

	public DevolucionCompra() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public int getIdCompraCabecera() {
		return this.idCompraCabecera;
	}

	public void setIdCompraCabecera(int idCompraCabecera) {
		this.idCompraCabecera = idCompraCabecera;
	}

	public int getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public int getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public double getIva() {
		return this.iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public double getPrecioCompra() {
		return this.precioCompra;
	}

	public void setPrecioCompra(double precioCompra) {
		this.precioCompra = precioCompra;
	}

	public double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getTotal() {
		return this.total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public CompraCabecera getCompraCabecera() {
		return this.compraCabecera;
	}

	public void setCompraCabecera(CompraCabecera compraCabecera) {
		this.compraCabecera = compraCabecera;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}