package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "caja_detalle" database table.
 * 
 */
@Entity
@Table(name="\"caja_detalle\"")
@NamedQuery(name="CajaDetalle.findAll", query="SELECT c FROM CajaDetalle c")
public class CajaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"id_caja\"")
	private int idCaja;

	@Column(name="\"id_venta_cabecera\"")
	private int idVentaCabecera;

	//bi-directional many-to-one association to Caja
	@ManyToOne
	@JoinColumns({
		})
	private Caja caja;

	//bi-directional many-to-one association to VentaCabecera
	@ManyToOne
	@JoinColumns({
		})
	private VentaCabecera ventaCabecera;

	public CajaDetalle() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdCaja() {
		return this.idCaja;
	}

	public void setIdCaja(int idCaja) {
		this.idCaja = idCaja;
	}

	public int getIdVentaCabecera() {
		return this.idVentaCabecera;
	}

	public void setIdVentaCabecera(int idVentaCabecera) {
		this.idVentaCabecera = idVentaCabecera;
	}

	public Caja getCaja() {
		return this.caja;
	}

	public void setCaja(Caja caja) {
		this.caja = caja;
	}

	public VentaCabecera getVentaCabecera() {
		return this.ventaCabecera;
	}

	public void setVentaCabecera(VentaCabecera ventaCabecera) {
		this.ventaCabecera = ventaCabecera;
	}

}