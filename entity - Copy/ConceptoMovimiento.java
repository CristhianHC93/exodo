package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the "concepto_movimiento" database table.
 * 
 */
@Entity
@Table(name="\"concepto_movimiento\"")
@NamedQuery(name="ConceptoMovimiento.findAll", query="SELECT c FROM ConceptoMovimiento c")
public class ConceptoMovimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"codigo\"")
	private int codigo;

	@Column(name="\"concepto\"")
	private String concepto;

	@Column(name="\"descripcion\"")
	private String descripcion;

	@Column(name="\"estado\"")
	private boolean estado;

	//bi-directional many-to-one association to Kardex
	@OneToMany(mappedBy="conceptoMovimiento", fetch=FetchType.EAGER)
	private List<Kardex> kardexs;

	public ConceptoMovimiento() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getConcepto() {
		return this.concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean getEstado() {
		return this.estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public List<Kardex> getKardexs() {
		return this.kardexs;
	}

	public void setKardexs(List<Kardex> kardexs) {
		this.kardexs = kardexs;
	}

	public Kardex addKardex(Kardex kardex) {
		getKardexs().add(kardex);
		kardex.setConceptoMovimiento(this);

		return kardex;
	}

	public Kardex removeKardex(Kardex kardex) {
		getKardexs().remove(kardex);
		kardex.setConceptoMovimiento(null);

		return kardex;
	}

}