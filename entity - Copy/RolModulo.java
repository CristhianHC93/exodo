package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "rol_modulo" database table.
 * 
 */
@Entity
@Table(name="\"rol_modulo\"")
@NamedQuery(name="RolModulo.findAll", query="SELECT r FROM RolModulo r")
public class RolModulo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"estado\"")
	private boolean estado;

	@Column(name="\"id_modulo\"")
	private int idModulo;

	@Column(name="\"id_rol\"")
	private int idRol;

	//bi-directional many-to-one association to Modulo
	@ManyToOne
	@JoinColumns({
		})
	private Modulo modulo;

	//bi-directional many-to-one association to Rol
	@ManyToOne
	@JoinColumns({
		})
	private Rol rol;

	public RolModulo() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean getEstado() {
		return this.estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public int getIdModulo() {
		return this.idModulo;
	}

	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}

	public int getIdRol() {
		return this.idRol;
	}

	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}

	public Modulo getModulo() {
		return this.modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public Rol getRol() {
		return this.rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

}