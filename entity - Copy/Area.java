package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the "area" database table.
 * 
 */
@Entity
@Table(name="\"area\"")
@NamedQuery(name="Area.findAll", query="SELECT a FROM Area a")
public class Area implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"descripcion\"")
	private String descripcion;

	//bi-directional many-to-one association to AreaSucursal
	@OneToMany(mappedBy="area", fetch=FetchType.EAGER)
	private List<AreaSucursal> areaSucursals;

	//bi-directional many-to-one association to Categoria
	@OneToMany(mappedBy="area", fetch=FetchType.EAGER)
	private List<Categoria> categorias;

	public Area() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<AreaSucursal> getAreaSucursals() {
		return this.areaSucursals;
	}

	public void setAreaSucursals(List<AreaSucursal> areaSucursals) {
		this.areaSucursals = areaSucursals;
	}

	public AreaSucursal addAreaSucursal(AreaSucursal areaSucursal) {
		getAreaSucursals().add(areaSucursal);
		areaSucursal.setArea(this);

		return areaSucursal;
	}

	public AreaSucursal removeAreaSucursal(AreaSucursal areaSucursal) {
		getAreaSucursals().remove(areaSucursal);
		areaSucursal.setArea(null);

		return areaSucursal;
	}

	public List<Categoria> getCategorias() {
		return this.categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public Categoria addCategoria(Categoria categoria) {
		getCategorias().add(categoria);
		categoria.setArea(this);

		return categoria;
	}

	public Categoria removeCategoria(Categoria categoria) {
		getCategorias().remove(categoria);
		categoria.setArea(null);

		return categoria;
	}

}