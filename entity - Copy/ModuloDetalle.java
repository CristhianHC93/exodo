package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "modulo_detalle" database table.
 * 
 */
@Entity
@Table(name="\"modulo_detalle\"")
@NamedQuery(name="ModuloDetalle.findAll", query="SELECT m FROM ModuloDetalle m")
public class ModuloDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"modulo_id\"")
	private int moduloId;

	@Column(name="\"nombre\"")
	private String nombre;

	@Column(name="\"ruta\"")
	private String ruta;

	//bi-directional many-to-one association to Modulo
	@ManyToOne
	@JoinColumns({
		})
	private Modulo modulo;

	public ModuloDetalle() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getModuloId() {
		return this.moduloId;
	}

	public void setModuloId(int moduloId) {
		this.moduloId = moduloId;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRuta() {
		return this.ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public Modulo getModulo() {
		return this.modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

}