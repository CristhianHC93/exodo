package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the "producto" database table.
 * 
 */
@Entity
@Table(name = "\"producto\"")
@NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p")
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private int id;

	@Column(name = "\"cantidad\"")
	private double cantidad;

	@Column(name = "\"cantidad_alerta\"")
	private double cantidadAlerta;

	@Column(name = "\"cantidad_critica\"")
	private double cantidadCritica;

	@Column(name = "\"codigo\"")
	private String codigo;

	@Column(name = "\"color\"")
	private String color;

	@Column(name = "\"descripcion\"")
	private String descripcion;

	@Column(name = "\"descripcion_corta\"")
	private String descripcionCorta;

	@Column(name = "\"imagen\"")
	private String imagen;

	@Column(name = "\"precio_compra\"")
	private double precioCompra;

	@Column(name = "\"precio_venta\"")
	private double precioVenta;

	// bi-directional many-to-one association to CompraDetalle
	@OneToMany(mappedBy = "producto", fetch = FetchType.EAGER)
	private List<CompraDetalle> compraDetalles;

	// bi-directional many-to-one association to CuadreInventario
	@OneToMany(mappedBy = "producto", fetch = FetchType.EAGER)
	private List<CuadreInventario> cuadreInventarios;

	// bi-directional many-to-one association to DevolucionCompra
	@OneToMany(mappedBy = "producto", fetch = FetchType.EAGER)
	private List<DevolucionCompra> devolucionCompras;

	// bi-directional many-to-one association to DevolucionVenta
	@OneToMany(mappedBy = "producto", fetch = FetchType.EAGER)
	private List<DevolucionVenta> devolucionVentas;

	// bi-directional many-to-one association to NotaCompraDetalle
	@OneToMany(mappedBy = "producto", fetch = FetchType.EAGER)
	private List<NotaCompraDetalle> notaCompraDetalles;

	// bi-directional many-to-one association to AreaSucursal
	@ManyToOne
	@JoinColumn(name = "\"id_area_sucursal\"")
	private AreaSucursal areaSucursal;

	// bi-directional many-to-one association to Categoria
	@ManyToOne
	@JoinColumn(name = "\"id_categoria\"")
	private Categoria categoria;

	// bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name = "\"id_estado\"")
	private Estado estado;

	// bi-directional many-to-one association to Impuesto
	@ManyToOne
	@JoinColumn(name = "\"id_impuesto\"")
	private Impuesto impuesto;

	// bi-directional many-to-one association to Proveedor
	@ManyToOne
	@JoinColumn(name = "\"id_proveedor\"")
	private Proveedor proveedor;

	// bi-directional many-to-one association to ProductoProveedor
	@OneToMany(mappedBy = "producto", fetch = FetchType.EAGER)
	private List<ProductoProveedor> productoProveedors;

	// bi-directional many-to-one association to VentaDetalle
	@OneToMany(mappedBy = "producto", fetch = FetchType.EAGER)
	private List<VentaDetalle> ventaDetalles;

	public Producto() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public double getCantidadAlerta() {
		return this.cantidadAlerta;
	}

	public void setCantidadAlerta(double cantidadAlerta) {
		this.cantidadAlerta = cantidadAlerta;
	}

	public double getCantidadCritica() {
		return this.cantidadCritica;
	}

	public void setCantidadCritica(double cantidadCritica) {
		this.cantidadCritica = cantidadCritica;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionCorta() {
		return this.descripcionCorta;
	}

	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}

	public String getImagen() {
		return this.imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public double getPrecioCompra() {
		return this.precioCompra;
	}

	public void setPrecioCompra(double precioCompra) {
		this.precioCompra = precioCompra;
	}

	public double getPrecioVenta() {
		return this.precioVenta;
	}

	public void setPrecioVenta(double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public List<CompraDetalle> getCompraDetalles() {
		return this.compraDetalles;
	}

	public void setCompraDetalles(List<CompraDetalle> compraDetalles) {
		this.compraDetalles = compraDetalles;
	}

	public CompraDetalle addCompraDetalle(CompraDetalle compraDetalle) {
		getCompraDetalles().add(compraDetalle);
		compraDetalle.setProducto(this);

		return compraDetalle;
	}

	public CompraDetalle removeCompraDetalle(CompraDetalle compraDetalle) {
		getCompraDetalles().remove(compraDetalle);
		compraDetalle.setProducto(null);

		return compraDetalle;
	}

	public List<CuadreInventario> getCuadreInventarios() {
		return this.cuadreInventarios;
	}

	public void setCuadreInventarios(List<CuadreInventario> cuadreInventarios) {
		this.cuadreInventarios = cuadreInventarios;
	}

	public CuadreInventario addCuadreInventario(CuadreInventario cuadreInventario) {
		getCuadreInventarios().add(cuadreInventario);
		cuadreInventario.setProducto(this);

		return cuadreInventario;
	}

	public CuadreInventario removeCuadreInventario(CuadreInventario cuadreInventario) {
		getCuadreInventarios().remove(cuadreInventario);
		cuadreInventario.setProducto(null);

		return cuadreInventario;
	}

	public List<DevolucionCompra> getDevolucionCompras() {
		return this.devolucionCompras;
	}

	public void setDevolucionCompras(List<DevolucionCompra> devolucionCompras) {
		this.devolucionCompras = devolucionCompras;
	}

	public DevolucionCompra addDevolucionCompra(DevolucionCompra devolucionCompra) {
		getDevolucionCompras().add(devolucionCompra);
		devolucionCompra.setProducto(this);

		return devolucionCompra;
	}

	public DevolucionCompra removeDevolucionCompra(DevolucionCompra devolucionCompra) {
		getDevolucionCompras().remove(devolucionCompra);
		devolucionCompra.setProducto(null);

		return devolucionCompra;
	}

	public List<DevolucionVenta> getDevolucionVentas() {
		return this.devolucionVentas;
	}

	public void setDevolucionVentas(List<DevolucionVenta> devolucionVentas) {
		this.devolucionVentas = devolucionVentas;
	}

	public DevolucionVenta addDevolucionVenta(DevolucionVenta devolucionVenta) {
		getDevolucionVentas().add(devolucionVenta);
		devolucionVenta.setProducto(this);

		return devolucionVenta;
	}

	public DevolucionVenta removeDevolucionVenta(DevolucionVenta devolucionVenta) {
		getDevolucionVentas().remove(devolucionVenta);
		devolucionVenta.setProducto(null);

		return devolucionVenta;
	}

	public List<NotaCompraDetalle> getNotaCompraDetalles() {
		return this.notaCompraDetalles;
	}

	public void setNotaCompraDetalles(List<NotaCompraDetalle> notaCompraDetalles) {
		this.notaCompraDetalles = notaCompraDetalles;
	}

	public NotaCompraDetalle addNotaCompraDetalle(NotaCompraDetalle notaCompraDetalle) {
		getNotaCompraDetalles().add(notaCompraDetalle);
		notaCompraDetalle.setProducto(this);

		return notaCompraDetalle;
	}

	public NotaCompraDetalle removeNotaCompraDetalle(NotaCompraDetalle notaCompraDetalle) {
		getNotaCompraDetalles().remove(notaCompraDetalle);
		notaCompraDetalle.setProducto(null);

		return notaCompraDetalle;
	}

	public AreaSucursal getAreaSucursal() {
		return this.areaSucursal;
	}

	public void setAreaSucursal(AreaSucursal areaSucursal) {
		this.areaSucursal = areaSucursal;
	}

	public Categoria getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Impuesto getImpuesto() {
		return this.impuesto;
	}

	public void setImpuesto(Impuesto impuesto) {
		this.impuesto = impuesto;
	}

	public Proveedor getProveedor() {
		return this.proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public List<ProductoProveedor> getProductoProveedors() {
		return this.productoProveedors;
	}

	public void setProductoProveedors(List<ProductoProveedor> productoProveedors) {
		this.productoProveedors = productoProveedors;
	}

	public ProductoProveedor addProductoProveedor(ProductoProveedor productoProveedor) {
		getProductoProveedors().add(productoProveedor);
		productoProveedor.setProducto(this);

		return productoProveedor;
	}

	public ProductoProveedor removeProductoProveedor(ProductoProveedor productoProveedor) {
		getProductoProveedors().remove(productoProveedor);
		productoProveedor.setProducto(null);

		return productoProveedor;
	}

	public List<VentaDetalle> getVentaDetalles() {
		return this.ventaDetalles;
	}

	public void setVentaDetalles(List<VentaDetalle> ventaDetalles) {
		this.ventaDetalles = ventaDetalles;
	}

	public VentaDetalle addVentaDetalle(VentaDetalle ventaDetalle) {
		getVentaDetalles().add(ventaDetalle);
		ventaDetalle.setProducto(this);

		return ventaDetalle;
	}

	public VentaDetalle removeVentaDetalle(VentaDetalle ventaDetalle) {
		getVentaDetalles().remove(ventaDetalle);
		ventaDetalle.setProducto(null);

		return ventaDetalle;
	}

}