package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the "informacion_negocio" database table.
 * 
 */
@Entity
@Table(name = "\"informacion_negocio\"")
@NamedQuery(name = "InformacionNegocio.findAll", query = "SELECT i FROM InformacionNegocio i")
public class InformacionNegocio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private int id;

	@Column(name = "\"direccion\"")
	private String direccion;

	@Column(name = "\"nombre\"")
	private String nombre;

	@Column(name = "\"ruc\"")
	private String ruc;

	@Column(name = "\"telefono\"")
	private String telefono;

	// bi-directional many-to-one association to Email
	@ManyToOne
	@JoinColumn(name = "\"id_email\"")
	private Email email;

	// bi-directional many-to-one association to Sucursal
	@OneToMany(mappedBy = "informacionNegocio", fetch = FetchType.EAGER)
	private List<Sucursal> sucursals;

	public InformacionNegocio() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRuc() {
		return this.ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Email getEmail() {
		return this.email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public List<Sucursal> getSucursals() {
		return this.sucursals;
	}

	public void setSucursals(List<Sucursal> sucursals) {
		this.sucursals = sucursals;
	}

	public Sucursal addSucursal(Sucursal sucursal) {
		getSucursals().add(sucursal);
		sucursal.setInformacionNegocio(this);

		return sucursal;
	}

	public Sucursal removeSucursal(Sucursal sucursal) {
		getSucursals().remove(sucursal);
		sucursal.setInformacionNegocio(null);

		return sucursal;
	}

}