package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the "nota_compra_detalle" database table.
 * 
 */
@Entity
@Table(name = "\"nota_compra_detalle\"")
@NamedQuery(name = "NotaCompraDetalle.findAll", query = "SELECT n FROM NotaCompraDetalle n")
public class NotaCompraDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private int id;

	@Column(name = "\"cantidad\"")
	private double cantidad;

	@Column(name = "\"cantidad_entregada\"")
	private double cantidadEntregada;

	@Column(name = "\"precio_compra\"")
	private double precioCompra;

	@Column(name = "\"precio_subtotal\"")
	private double precioSubtotal;

	// bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name = "\"id_estado\"")
	private Estado estado;

	// bi-directional many-to-one association to NotaCompraCabecera
	@ManyToOne
	@JoinColumn(name = "\"id_nota_compra_cabecera\"")
	private NotaCompraCabecera notaCompraCabecera;

	// bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumn(name = "\"id_producto\"")
	private Producto producto;

	public NotaCompraDetalle() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public double getCantidadEntregada() {
		return this.cantidadEntregada;
	}

	public void setCantidadEntregada(double cantidadEntregada) {
		this.cantidadEntregada = cantidadEntregada;
	}

	public double getPrecioCompra() {
		return this.precioCompra;
	}

	public void setPrecioCompra(double precioCompra) {
		this.precioCompra = precioCompra;
	}

	public double getPrecioSubtotal() {
		return this.precioSubtotal;
	}

	public void setPrecioSubtotal(double precioSubtotal) {
		this.precioSubtotal = precioSubtotal;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public NotaCompraCabecera getNotaCompraCabecera() {
		return this.notaCompraCabecera;
	}

	public void setNotaCompraCabecera(NotaCompraCabecera notaCompraCabecera) {
		this.notaCompraCabecera = notaCompraCabecera;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

}