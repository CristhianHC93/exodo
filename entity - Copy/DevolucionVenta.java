package ec.com.exodo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "devolucion_venta" database table.
 * 
 */
@Entity
@Table(name="\"devolucion_venta\"")
@NamedQuery(name="DevolucionVenta.findAll", query="SELECT d FROM DevolucionVenta d")
public class DevolucionVenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private int id;

	@Column(name="\"cantidad\"")
	private double cantidad;

	@Column(name="\"id_producto\"")
	private int idProducto;

	@Column(name="\"id_usuario\"")
	private int idUsuario;

	@Column(name="\"id_venta_cabecera\"")
	private int idVentaCabecera;

	@Column(name="\"observacion\"")
	private String observacion;

	@Column(name="\"precio_venta\"")
	private double precioVenta;

	@Column(name="\"total\"")
	private double total;

	//bi-directional many-to-one association to Producto
	@ManyToOne
	@JoinColumns({
		})
	private Producto producto;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumns({
		})
	private Usuario usuario;

	//bi-directional many-to-one association to VentaCabecera
	@ManyToOne
	@JoinColumns({
		})
	private VentaCabecera ventaCabecera;

	public DevolucionVenta() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public int getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public int getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdVentaCabecera() {
		return this.idVentaCabecera;
	}

	public void setIdVentaCabecera(int idVentaCabecera) {
		this.idVentaCabecera = idVentaCabecera;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public double getPrecioVenta() {
		return this.precioVenta;
	}

	public void setPrecioVenta(double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public double getTotal() {
		return this.total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public VentaCabecera getVentaCabecera() {
		return this.ventaCabecera;
	}

	public void setVentaCabecera(VentaCabecera ventaCabecera) {
		this.ventaCabecera = ventaCabecera;
	}

}